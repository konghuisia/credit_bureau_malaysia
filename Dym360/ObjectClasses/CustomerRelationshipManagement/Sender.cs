﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.CustomerRelationshipManagement
{
    public class Sender
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
    }
}
