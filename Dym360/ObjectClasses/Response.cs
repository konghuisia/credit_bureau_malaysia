﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Response
    {
        public string Error { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }
        public string Data { get; set; }
    }
}
