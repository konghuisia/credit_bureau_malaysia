﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class ShowInSelect
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string ShowInSelectId { get; set; }
        public string ModuleId { get; set; }
        public string AttachmentTypeId { get; set; }
        public string ShowInSelectName { get; set; }
        public bool Required { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
