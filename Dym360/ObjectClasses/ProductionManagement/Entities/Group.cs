﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProductionManagement.Entities
{
    public class Group
    {
        public string GroupId { get; set; }
        public string Name { get; set; }
    }
}
