﻿using ObjectClasses.InventoryManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProductionManagement.Entities
{
    public class ProductDetail
    {
        public List<Material> Materials { get; set; } = new List<Material>();
    }
}
