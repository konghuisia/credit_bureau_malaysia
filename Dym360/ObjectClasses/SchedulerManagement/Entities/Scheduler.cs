﻿using ObjectClasses.InventoryManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SchedulerManagement.Entities
{
    public class Scheduler
    {
        public string Id { get; set; }
        public string RefNo { get; set; } = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(new Random().Next(1, 100)).Next(1, 100));
        public string Name { get; set; }
        public string Status { get; set; } = "New";
        public string Progress { get; set; } = "0";
        public string ScheduledDate { get; set; } = DateTime.Now.ToString("dd-MM-yyyy");
        public string StartDate { get; set; }
        public string Duration { get; set; }
        public string EndDate { get; set; }
        public string Quantity { get; set; }
        public int State { get; set; }
        public Product Product { get; set; }
        public string Remark { get; set; }
    }
}
