﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class History
    {
        public string RequestHistoryId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Action { get; set; }
        public string ActionDate { get; set; }
        public string Description { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
