﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class ProjectDetail
    {
        public string ProjectId { get; set; }
        public string Estimation { get; set; }
        public string Delivery { get; set; }
        public string ProposalNo { get; set; }
        public string PlantType { get; set; }
        public string PaymentTerm { get; set; }
    }
}
