﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class Project
    {
        public string ProjectId { get; set; }
        public Proposal Proposal { get; set; }
        public string PlantType { get; set; }
        public string PaymentTermId { get; set; }
        public string DeliveryDate { get; set; }
        public string PriceEstimation { get; set; }
        public string Description { get; set; }
        public int Round { get; set; }
        public DateTime CreatedDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
