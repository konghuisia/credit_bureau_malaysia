﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class Company
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string FaxNo { get; set; }
        public string Address { get; set; }
        public string CountryId { get; set; }
        public string Country { get; set; }
        public List<ContactPerson> ContactPersons { get; set; } = new List<ContactPerson>();
    }
}
