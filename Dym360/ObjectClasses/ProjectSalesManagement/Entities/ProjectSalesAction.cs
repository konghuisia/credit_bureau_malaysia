﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class ProjectSalesAction
    {
        public string UserId { get; set; }
        public string RequestId { get; set; }
        public string ReferenceNo { get; set; }
        public string ProjectId { get; set; }
        public string RoleId { get; set; }
        public string ActionId { get; set; }
        public string Comment { get; set; }

    }
}
