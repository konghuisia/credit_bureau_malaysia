﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class ProjectSales : Request
    {
        public Project ProjectInfo { get; set; } = new Project();
        public Customer CustomerInfo { get; set; } = new Customer();
    }
}
