﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectSalesManagement.Entities
{
    public class Customer : Company
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
    }
}
