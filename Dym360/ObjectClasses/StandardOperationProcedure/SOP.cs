﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.StandardOperationProcedure
{
    public class SOP : Request
    {
        public string Name { get; set; }
        public string OwnedByUser { get; set; }
        public string OwnedByDepartment { get; set; }
        public string CreatedDate { get; set; }
        public List<string> DepartmentApplicability { get; set; }
        public string EffectiveDate { get; set; }
        public int Validity { get; set; }
        public Document SOPDocument { get; set; }
        public string UpdatedDate { get; set; }
        public List<History> Histories { get; set; } = new List<History>();
    }
}
