﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class Account
    {
        public int Id { get; set; }
        public AccountTypeEnum AccountType { get; set; }
        public double Balance { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int CustomerId { get; set; }
    }
}