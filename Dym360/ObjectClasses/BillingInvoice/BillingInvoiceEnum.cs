﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public enum AccountTypeEnum
    {
        Prepaid = 1,
        Postpaid = 2
    }
}