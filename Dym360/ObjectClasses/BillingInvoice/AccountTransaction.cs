﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class AccountTransaction
    {
        public int AccountTransactionId { get; set; }
        public int AccountId { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public DateTime TransctionDate { get; set; }
    }
}