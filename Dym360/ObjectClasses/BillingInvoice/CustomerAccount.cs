﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class CustomerAccount
    {
        public int CustomerAccountId { get; set; }
        public int CustomerId { get; set; }
        public int AccountId { get; set; }
    }
}