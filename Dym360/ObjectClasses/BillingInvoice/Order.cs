﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class Order
    {
        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
        public int Id { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public DateTime OrderDate { get; set; }
        public double Total
        {
            get
            {
                var val = 0.00;
                foreach (var oi in OrderItems)
                {
                    val += oi.Sum;
                }
                return val;
            }
        }
    }
}