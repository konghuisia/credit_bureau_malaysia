﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class Invoice
    {
        int invoiceSequence { get; }
        string invoicePrefix { get; } = "INV";
        public string InvoiceNo { get; set; }
        public string InvoiceDate { get; set; }
        public string BillingPeriod { get; set; }
        public double TotalAmount { get; set; }
        public List<Order> Orders { get; set; }
        public Customer Recipient { get; set; }
        public string PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
        public double PaymentAmount { get; set; }
        public string PaymentMethod { get; set; }
        public bool IsPaid { get; set; }
    }
}