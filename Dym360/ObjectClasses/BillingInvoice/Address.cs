﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class Address
    {
        public string Id
        {
            get { return Guid.NewGuid().ToString(); }
        }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ContactNo { get; set; }
        public string PIC { get; set; }
        public string Email { get; set; }

    }
}