﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public Address BillingAddress { get; set; }
        public Address CurrentAddress { get; set; }
        public bool IsNew { get; set; } = false;
        public bool IsModified { get; set; } = false;
        public bool DeletedFlag { get; set; } = false;
    }
}