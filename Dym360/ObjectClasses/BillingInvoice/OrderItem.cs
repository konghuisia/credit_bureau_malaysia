﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.BillingInvoice
{
    public class OrderItem
    {
        public string OrderItemId
        {
            get { return Guid.NewGuid().ToString(); }
        }
        public Product Item { get; set; }
        public int Unit { get; set; }
        public double Sum
        {
            get { return Item.Price * Convert.ToDouble(Unit); }
        }
    }
}