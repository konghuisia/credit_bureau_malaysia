﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.BillingInvoice
{
    public class BillingInfo
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string AccountType { get; set; }
        public string BillingAddress { get; set; }
        public string PIC { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string LastUpdated { get; set; }
    }
}
