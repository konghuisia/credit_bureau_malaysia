﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Process
    {
        public string ModuleId { get; set; }
        public string RoleId { get; set; }
        public string ActionId { get; set; }
    }
}
