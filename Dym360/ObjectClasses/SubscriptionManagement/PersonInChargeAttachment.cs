﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class PersonInChargeAttachment
    {
        public string RequestId { get; set; }
        public string AttachmentId { get; set; }
        public string AttachmentTypeId { get; set; }
        public string PersonInChargeId { get; set; }
        public string PersonInChargeAttachmentId { get; set; }

        public string Name { get; set; }
        public string Path { get; set; }
    }
}
