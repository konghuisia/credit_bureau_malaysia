﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class PersonInCharge
    {
        public int id { get; set; }
        public string PersonInChargeId { get; set; }
        public string RequestId { get; set; }
        public string Name { get; set; }
        public string IcNumber { get; set; }
        public string Designation { get; set; }
        public string TelephoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Type { get; set; }
        public bool IsPrimary { get; set; }
        public bool Consented { get; set; }
        public string Password { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
