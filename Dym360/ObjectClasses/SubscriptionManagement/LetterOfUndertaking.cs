﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class LetterOfUndertaking
    {
        public int Id { get; set; }
        public string LetterOfUndertakingId { get; set; }
        public string RequestId { get; set; }
        public bool Consented { get; set; }
        public bool IsSubmit { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
