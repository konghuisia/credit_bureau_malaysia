﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class CustomerDetails
    {
        public int id { get; set; }
        public string CustomerId { get; set; }
        public string RequestId { get; set; }
        public string BusinessRegNo { get; set; }
        public DateTime? BusinessRegDate { get; set; }
        public string BusinessName { get; set; }
        public string LegalConstitution { get; set; }
        public string CountryOfRegistration { get; set; }
        public string EconomySector { get; set; }
        public string EconomySubSector { get; set; }
        public string BusinessAddress { get; set; }
        public string TelephoneNo { get; set; }
        public string FaxNo { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string PostalCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
