﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class SubscriptionRequest
    {
        public int id { get; set; }
        public string SubscriptionRequestId { get; set; }
        public string RequestId { get; set; }
        public string BusinessName { get; set; }
        public string BusinessRegNo { get; set; }
        public string Title { get; set; }
        public string ConstitutionType { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string PackageId { get; set; }
        public string Password { get; set; }
        public string PurposeOfUse { get; set; }
        public int FrequencyOfUse { get; set; }
        public DateTime? CustomerSubmittedAt { get; set; }
        public string LegalRecommendAction { get; set; }
        public string Reason { get; set; }
        public int EFormStage { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
        public OnBoardingChecklist OnBoardingChecklist { get; set; }
    }
}
