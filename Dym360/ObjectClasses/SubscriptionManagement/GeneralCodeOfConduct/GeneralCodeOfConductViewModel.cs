﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.GeneralCodeOfConduct
{
    public class GeneralCodeOfConductViewModel
    {
        public GeneralCodeOfConductViewModel()
        {
            CustomerDetails = new CustomerDetails();
            GeneralCodeOfConduct = new GeneralCodeOfConduct();
        }

        public bool IsCBM { get; set; }
        public Request Request { get; set; }
        public bool HasWriteAccess { get; set; }
        public CustomerDetails CustomerDetails { get; set; }
        public GeneralCodeOfConduct GeneralCodeOfConduct { get; set; }
    }
}
