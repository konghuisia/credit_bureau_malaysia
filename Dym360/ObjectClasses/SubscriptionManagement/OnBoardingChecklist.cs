﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement
{
    public class OnBoardingChecklist
    {
        public int Id { get; set; }
        public string OnBoardingChecklistId { get; set; }    
        public string RequestId   { get; set; }    
        public bool Corporate   { get; set; } 
        public bool FI { get; set; } 
        public bool MMSDA   { get; set; } 
        public bool Fintech { get; set; } 
        public bool Moneylender { get; set; } 
        public bool P2PLender { get; set; }
        public bool CreditLeasing { get; set; }
        public bool SME { get; set; }
        public bool SubscriptionAgreement { get; set; }
        public bool PackagePricing { get; set; }
        public bool DirectorICCopy { get; set; }
        public bool SecurityAdminForm { get; set; }
        public bool SecurityAdminICCopy { get; set; }
        public bool KYCDeclaration { get; set; }
        public bool CopyOfCBMConsentForm { get; set; }
        public bool PaymentSlip { get; set; }
        public bool CopyOfLicense { get; set; } 
        public DateTime? LicenseExpiryDate { get; set; }
        public bool PromotionalLetter { get; set; }
        public bool CMSServices { get; set; } 
        public string CreatedBy   { get; set; }    
        public DateTime CreatedAt   { get; set; } 
        public string UpdatedBy { get; set; }    
        public DateTime? UpdatedAt   { get; set; }
        public bool Deleted { get; set; } 
    }
}
