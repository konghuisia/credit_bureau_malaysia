﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_DeclarationViewModel
    {
        public KYC_DeclarationViewModel()
        {
            KYC_DealOn = new KYC_DealOn();
            KYC_CompanyType = new KYC_CompanyType();
            KYC_CompanyPolicy = new KYC_CompanyPolicy();
            KYC_ProductService = new KYC_ProductServices();
            KYC_ProtectionOfData = new KYC_ProtectionOfData();
            KYC_ConsentProcurement = new KYC_ConsentProcurement();
            KYC_TargetedClientMarket = new KYC_TargetedClientMarket();
            KYC_PurposeOfSubscription = new KYC_PurposeOfSubscription();
            KYC_SubscriberDeclaration = new KYC_SubscriberDeclaration();
        }

        public bool HasWriteAccess { get; set; }
        public Request Request { get; set; }
        public KYC_DealOn KYC_DealOn { get; set; }
        public KYC_CompanyType KYC_CompanyType { get; set; }
        public KYC_CompanyPolicy KYC_CompanyPolicy { get; set; }
        public KYC_ProductServices KYC_ProductService { get; set; }
        public KYC_ProtectionOfData KYC_ProtectionOfData { get; set; }
        public KYC_ConsentProcurement KYC_ConsentProcurement { get; set; }
        public KYC_TargetedClientMarket KYC_TargetedClientMarket { get; set; }
        public KYC_PurposeOfSubscription KYC_PurposeOfSubscription { get; set; }
        public KYC_SubscriberDeclaration KYC_SubscriberDeclaration { get; set; }
        public bool IsCBM { get; set; }
    }
}
