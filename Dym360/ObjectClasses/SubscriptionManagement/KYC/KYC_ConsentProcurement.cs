﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_ConsentProcurement
    {
        public int Id { get; set; }
        public string ConsentProcurementId { get; set; }
        public string RequestId { get; set; }
        public bool Instinct { get; set; }
        public bool OweMoney { get; set; }
        public bool BadReputation { get; set; }
        public bool CreditReview { get; set; }
        public bool Others { get; set; }
        public string OthersText { get; set; }
        public string PoliciesAndProcedures { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
