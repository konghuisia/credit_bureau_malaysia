﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_ProductServices
    {
        public int Id { get; set; }
        public string ProductServicesId { get; set; }
        public string RequestId { get; set; }
        public bool Advisory { get; set; }
        public string AdvisoryText { get; set; }
        public bool Services { get; set; }
        public string ServicesText { get; set; }
        public bool Provision { get; set; }
        public string ProvisionText { get; set; }
        public bool Others { get; set; }
        public string OthersText { get; set; }

        //Services
        public bool Legal { get; set; }
        public bool Accounting { get; set; }
        public bool Auditing { get; set; }
        public bool Taxation { get; set; }
        public bool Engineering { get; set; }
        public bool SoftwareImplementation { get; set; }
        public bool DataProcessing { get; set; }
        public bool RealEstate { get; set; }
        public bool Advertising { get; set; }
        public bool Transportation { get; set; }
        public bool Agriculture { get; set; }
        public bool Communication { get; set; }
        public bool Education { get; set; }
        public bool InsuranceRelated { get; set; }
        public bool BankingAndFinance { get; set; }
        public bool HealthRelated { get; set; }
        public bool OtherServices { get; set; }
        public string OtherServicesText { get; set; }
        //END

        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
