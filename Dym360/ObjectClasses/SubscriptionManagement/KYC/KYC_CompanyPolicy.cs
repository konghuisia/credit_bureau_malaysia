﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_CompanyPolicy
    {
        public int Id { get; set; }
        public string CompanyPolicyId { get; set; }
        public string RequestId { get; set; }
        public bool DataUserYes { get; set; }
        public bool DataUserNo { get; set; }
        public bool AnswerYes { get; set; }
        public bool AnswerNo { get; set; }
        public bool ExplainToDataSubject { get; set; }
        public bool ObtainIcCopy { get; set; }
        public bool ObtainDataSubjectConsent { get; set; }
        public bool BeforeCreditInfoOthers { get; set; }
        public string BeforeCreditInfoOthersText { get; set; }
        public bool ProvideReportToThirdParty { get; set; }
        public bool ProvideReportToDataSubject { get; set; }
        public bool AfterCreditInfoOthers { get; set; }
        public string AfterCreditInfoOthersText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
