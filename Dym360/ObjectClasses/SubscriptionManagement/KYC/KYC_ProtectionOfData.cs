﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_ProtectionOfData
    {
        public int Id { get; set; }
        public string ProtectionOfDataId { get; set; }
        public string RequestId { get; set; }
        public bool StoreCreditReportInElectronicDevice { get; set; }
        public bool PrintCreditReportForFilling { get; set; }
        public bool ProvideDataSubjectHisReport { get; set; }
        public bool AfterOthers { get; set; }
        public string AfterOthersText { get; set; }
        public bool UsingMachineShredder { get; set; }
        public bool ObtainDataSubjectConsent { get; set; }
        public bool AccessToOfficeComputerIsEncrypted { get; set; }
        public bool AccessToCBMSystemIsLimited { get; set; }
        public bool SharingOrDisplayingPassword { get; set; }
        public bool UsingFirewall { get; set; }
        public string FirewallText { get; set; }
        public bool UsingAntivirus { get; set; }
        public string AntivirusText { get; set; }
        public bool EnsureOthers { get; set; }
        public string EnsureOthersText { get; set; }
        public bool ParentCompany { get; set; }
        public bool SubsidiaryCompany { get; set; }
        public bool AllEmployee { get; set; }
        public bool ThirdParty { get; set; }
        public bool Director_CEO_Management { get; set; }
        public bool People_CompanyOthers { get; set; }
        public string People_CompanyOthersText { get; set; }
        public bool RetainDataYes { get; set; }
        public string RetainDataYesText { get; set; }
        public bool RetainDataNo { get; set; }
        public bool ElectronicCopy { get; set; }
        public bool PhysicalFilling { get; set; }
        public bool CloudSystem { get; set; }
        public bool InternetStorageProvider { get; set; }
        public bool PersonalDatabase { get; set; }
        public bool StoreOthers { get; set; }
        public string StoreOthersText { get; set; }
        public bool CreditInformationOnlyForInternalUsage { get; set; }
        public bool OnlyForSecurityAdministrator { get; set; }
        public bool ReportWillNotBePrinted { get; set; }
        public bool ReportWillNotBeRetained { get; set; }
        public bool ControlOthers { get; set; }
        public string ControlOthersText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
