﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_SubscriberDeclaration
    {
        public int Id { get; set; }
        public string SubscriberDeclarationId { get; set; }
        public string RequestId { get; set; }
        public string Name { get; set; }
        public string IC { get; set; }
        public bool Director { get; set; }
        public bool AuthorisedOfficer { get; set; }
        public string AuthorisedOfficerText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
