﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_CompanyType
    {
        public int Id { get; set; }
        public string CompanyTypeId { get; set; }
        public string RequestId { get; set; }
        public bool Wholesaler { get; set; }
        public bool Distributor { get; set; }
        public bool Retailer { get; set; }
        public bool Manufacturer { get; set; }
        public bool Others { get; set; }
        public string OthersText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
