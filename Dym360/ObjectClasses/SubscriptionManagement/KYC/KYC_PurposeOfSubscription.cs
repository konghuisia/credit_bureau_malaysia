﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_PurposeOfSubscription
    {
        public int Id { get; set; }
        public string PurposeOfSubscriptionId { get; set; }
        public string RequestId { get; set; }
        public string Purpose { get; set; }
        public bool LoanApplication_FinancingFacility { get; set; }
        public bool PeriodicReviewOfLoan { get; set; }
        public bool OpeningAnAccount { get; set; }
        public bool Investment { get; set; }
        public bool Insurance { get; set; }
        public bool FinancialGurantee { get; set; }
        public bool Employment { get; set; }
        public bool HirePurchase { get; set; }
        public bool CrimeFraudDetectionAndPrevention { get; set; }
        public bool InvestigationByAuditFirm_ComplianceUnits_LawFirms { get; set; }
        public bool RentalContract { get; set; }
        public bool DebtRecovery { get; set; }
        public bool Others { get; set; }
        public string OthersText { get; set; }
        public bool No { get; set; }
        public bool TransferCreditReportToThirdParty { get; set; }
        public bool YesOthers { get; set; }
        public string YesOthersText { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
