﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.KYC
{
    public class KYC_DealOn
    {
        public int Id { get; set; }
        public string DealOnId { get; set; }
        public string RequestId { get; set; }
        public bool CreditTerm { get; set; }
        public bool CashTerm { get; set; }
        public bool Both { get; set; }
        public double CreditTermPerc { get; set; }
        public double CashTermPerc { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
