﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.ConsentForm
{
    public class ConsentFormViewModel
    {
        public ConsentFormViewModel()
        {
            CustomerDetails = new CustomerDetails();
            PersonInCharges = new List<PersonInCharge>();
            ConsentFormPeoples = new List<ConsentFormPeople>();
        }

        public bool IsCBM { get; set; }
        public Request Request { get; set; }
        public bool HasWriteAccess { get; set; }
        public CustomerDetails CustomerDetails { get; set; }
        public List<PersonInCharge> PersonInCharges { get; set; } 
        public List<ConsentFormPeople> ConsentFormPeoples { get; set; }
    }
}
