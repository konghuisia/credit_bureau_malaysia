﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.ConsentForm
{
    public class ConsentFormPeople
    {
        public int id { get; set; }
        public string ConsentFormPeopleId { get; set; }
        public string RequestId { get; set; }
        public string Name { get; set; }
        public string NRIC_Passport { get; set; }
        public string Designation { get; set; }
        public string Type { get; set; }
        public bool IsSubmit { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
