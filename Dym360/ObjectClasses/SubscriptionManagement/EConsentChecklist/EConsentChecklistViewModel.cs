﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.EConsentChecklist
{
    public class EConsentChecklistViewModel
    {
        public EConsentChecklistViewModel()
        {
            EConsentChecklist = new EConsentChecklist();
        }

        public bool IsCBM { get; set; }
        public bool HasWriteAccess { get; set; }
        public Request Request { get; set; }
        public EConsentChecklist EConsentChecklist { get; set; }
    }
}
