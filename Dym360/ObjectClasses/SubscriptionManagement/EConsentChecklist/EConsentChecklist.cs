﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SubscriptionManagement.EConsentChecklist
{
    public class EConsentChecklist
    {
        public int Id { get; set; }
        public string EConsentChecklistId { get; set; }
        public string RequestId { get; set; }
        public string Consent { get; set; }
        public string IdentityVerification { get; set; }
        public string ElectronicSignature { get; set; }
        public string DocumentMustBeOriginal { get; set; }
        public string RetentionOfDocument { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
