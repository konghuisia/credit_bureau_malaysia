﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProductSpecificationRequest
{
    public class SpecificationRequest 
    {
        public string ProductSpecificationId { get; set; }
        public Request Request { get; set; }
        public string Customer { get; set; }
        public string CustomerId { get; set; }
        public string Product { get; set; }
        public string Reference { get; set; }
        public string Proposal { get; set; }
        public string Code { get; set; } = "N/A";
        public string SpecDate { get; set; }
        public string Recommendation { get; set; }
    }
}
