﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ApprovalManagement.Entities
{
    public class Matrix
    {
        public string Id { get; set; }
        public string ModuleId { get; set; }
        public List<Approval> Approvals { get; set; }
    }
}
