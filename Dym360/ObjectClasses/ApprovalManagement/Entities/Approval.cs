﻿using ObjectClasses.ApprovalManagement.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ObjectClasses.MyEnum;

namespace ObjectClasses.ApprovalManagement.Entities
{
    public class Approval
    {
        public string Id { get; set; }
        public Module Module { get; set; }
        public Condition Condition { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
