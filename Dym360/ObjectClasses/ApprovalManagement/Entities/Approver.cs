﻿using ObjectClasses.ApprovalManagement.IServices;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ApprovalManagement.Entities
{
    public class Approver
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string Type { get; set; }
    }
}
