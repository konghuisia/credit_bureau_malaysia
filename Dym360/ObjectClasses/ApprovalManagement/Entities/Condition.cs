﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ObjectClasses.MyEnum;

namespace ObjectClasses.ApprovalManagement.Entities
{
    public class Condition
    {
        public string Text { get; set; }
        public string Description { get; set; }
        public List<Operation> Operations { get; set; }
        public List<Approver> Approvers { get; set; }
        
    }
}
