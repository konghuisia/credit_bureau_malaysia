﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ObjectClasses.MyEnum;

namespace ObjectClasses.ApprovalManagement.Entities
{
    public class Operation
    {
        public OperationType OperationType { get; set; }
        public double Value { get; set; }
    }
}
