﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ApprovalManagement.IServices
{
    public interface IRequestService
    {
        void Submit(Request request);
        void Cancel(Request request);
        void Delete(Request request);
    }
}
