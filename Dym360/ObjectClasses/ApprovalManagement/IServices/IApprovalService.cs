﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ApprovalManagement.IServices
{
    public interface IApprovalService
    {
        void Approve(Request request);
        void Decline(Request request);
        void RequestMore(Request request);
    }
}
