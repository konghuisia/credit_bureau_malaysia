﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class ReferenceSequence
    {
        public string ReferenceSequenceId { get; set; }
        public string ReferenceId { get; set; }
        public int SequenceNo { get; set; }
    }
}
