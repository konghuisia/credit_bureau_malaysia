﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Access
    {
        public int id { get; set; }
        public string AccessId { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
    }
}
