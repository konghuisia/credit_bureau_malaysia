﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class ProcessFlow
    {
        public string Id { get; set; }
        public string ActionId { get; set; }
        public ProcessFlowRole CurrentPIC { get; set; }
        public RequestState CurrentState { get; set; }
        public MyEnum.Module Module { get; set; }
        public RequestState NextState { get; set; } 
        public ProcessFlowRole NextPIC { get; set; }
        public DateTime CreatedDate { get; set; }
        public float Progress { get; set; }
        public string CreatedBy { get; set; }
    }
}
