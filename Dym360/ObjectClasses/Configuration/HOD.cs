﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class HOD
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentKeyword { get; set; }
    }
}
