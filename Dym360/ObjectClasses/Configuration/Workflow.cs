﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Workflow
    {
        public string WorkflowId { get; set; }
        public string WorkflowProcessId { get; set; }
        public string ModuleId { get; set; }
        public string RoleId { get; set; }
        public string ActionId { get; set; }
        public string NextRoleId { get; set; }
        public string NextWorkflowProcessId { get; set; }
        public string NextWorkflowProcess { get; set; }
        public int SequenceNo { get; set; }
        public int Percentage { get; set; }
        public List<WorkflowApproval> workflowApprovals { get; set; }

    }
}
