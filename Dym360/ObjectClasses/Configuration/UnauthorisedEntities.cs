﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class UnauthorisedEntities
    {
        public int id { get; set; }
        public string UnauthorisedEntitiesId { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public DateTime DateAddedToAlertList { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
