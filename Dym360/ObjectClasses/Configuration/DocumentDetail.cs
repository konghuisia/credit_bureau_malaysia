﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class DocumentDetail
    {
        public string DocumentDetailId { get; set; }
        public string DocumentTypeId { get; set; }
        public string DocumentType { get; set; }
        public string Code { get; set; }
        public int Version { get; set; }
    }
}
