﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Document
    {
        public Document()
        {
            DocumentDetail = new DocumentDetail();
        }
        public string Id { get; set; }
        public string DocumentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Extension { get; set; }
        public string Path { get; set; }
        public string Size { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedDate { get; set; }
        public DocumentDetail DocumentDetail { get; set; }
    }
}
