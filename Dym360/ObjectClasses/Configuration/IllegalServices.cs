﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class IllegalServices
    {
        public int id { get; set; }
        public string IllegalServicesId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime DateOfWarningLetterSent { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Deleted { get; set; }
    }
}
