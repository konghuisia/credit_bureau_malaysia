﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Attachment
    {
        public string AttachmentId { get; set; }
        public string AttachmentTypeId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AbsolutePath { get; set; }
        public string Path { get; set; }
        public string Size { get; set; }
        public string Extension { get; set; }
        public string FileType { get; set; }
        public string MIMEType { get; set; }
        public string ModifiedDate { get; set; }
        public string CreatedDate { get; set; }
        public string ClassificationId { get; set; } = "";
        public bool IsOwner { get; set; } = false;
        public string UploadedBy { get; set; }
        public System.IO.Stream File { get; set; }
    }
}
