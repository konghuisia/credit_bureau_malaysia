﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Reference
    {
        public string ReferenceId { get; set; }
        public string ModuleId { get; set; }
        public string Prefix { get; set; }
        public int Length { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
