﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class WorkflowProcess
    {
        public string WorkflowProcessId { get; set; }
        public string RoleId { get; set; }
        public string Role { get; set; }
    }
}
