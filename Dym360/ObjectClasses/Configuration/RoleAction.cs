﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class RoleAction
    {
        public string RoleId { get; set; }
        public string ActionID { get; set; }
        public string ActionName { get; set; }
        public string ActionKeyword { get; set; }
        public string ActionDescription { get; set; }
        public string RoleName { get; set; }
    }
}
