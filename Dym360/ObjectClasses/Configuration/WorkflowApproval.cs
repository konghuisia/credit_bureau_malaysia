﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class WorkflowApproval : User
    {
        public string WorkflowApprovalId { get; set; }
    }
}
