﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class RequestState
    {
        public string Id { get; set; }
        public string RequestStateId { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string State { get; set; } = "";
        public string PreviousState { get; set; } = "";
        public DateTime UpdatedDate { get; set; }
    }
}
