﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Location
    {
        public string LocationId { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
    }
}
