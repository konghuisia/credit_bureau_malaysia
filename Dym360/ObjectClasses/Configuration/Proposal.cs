﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Proposal
    {
        public string ProposalNo { get; set; }
        public int ProposalNoSeq { get; set; }
        public string CreatedBy { get; set; }
    }
}
