﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class AttachmentType
    {
        public string AttachmentTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
