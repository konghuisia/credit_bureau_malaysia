﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PositionId { get; set; }
        public string Grad { get; set; }
        public string Title { get; set; }
        public string Abbreviation { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
