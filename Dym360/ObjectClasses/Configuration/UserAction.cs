﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class UserAction
    {
        public int Id { get; set; }
        public string RequestId { get; set; }
        public string Status { get; set; }
        public string NextStatus { get; set; }
        public string CurrentRoleId { get; set; }
        public string ExpectedRoleId { get; set; }
        public string ActionId { get; set; }
        public string UserId { get; set; }
        public string ButtonName { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string ModuleId { get; set; }
    }
}
