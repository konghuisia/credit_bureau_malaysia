﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.Configuration
{
    public class User
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Position { get; set; }
        public string PositionId { get; set; }
        public string Grad { get; set; }
        public string Department { get; set; }
        public string DepartmentId { get; set; }
        public string RoleId { get; set; }
        public string Role { get; set; }
        public string Reporting { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public string AccountTypeId { get; set; }
        public string Salt { get; set; }
        public string Source { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string DeletedFlag { get; set; }
        public bool Active { get; set; }
        public string Oldname { get; set; }
    }
}
