﻿using ObjectClasses.InventoryManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SupplierManagement.Entities
{
    public class Supplier
    {
        public string Id { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public string Contact1 { get; set; }
        public string Contact2 { get; set; }
        public List<String> SupplyIds { get; set; }
        public List<Supply> SupplyItems { get; set; } = new List<Supply>();
        public List<ContactPerson> ContactPersons { get; set; }
        public string PaymentTerm { get; set; }
        public string Email { get; set; }
        public string FaxNo { get; set; }
        public string Address { get; set; }
        public List<SupplierHistory> SupplierHistories { get; set; } = new List<SupplierHistory>();
    }
}
