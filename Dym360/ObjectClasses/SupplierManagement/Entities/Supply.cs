﻿using ObjectClasses.InventoryManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SupplierManagement.Entities
{
    public class Supply
    {
        public Material Material { get; set; }
        public int Quantity { get; set; }
        public string UOM { get; set; }
        public double Price { get; set; }
    }
}
