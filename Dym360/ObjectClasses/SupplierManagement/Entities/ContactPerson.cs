﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SupplierManagement.Entities
{
    public class ContactPerson
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
    }
}
