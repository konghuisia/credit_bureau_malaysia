﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.SupplierManagement.Entities
{
    public class SupplierHistory
    {
        public string Action { get; set; }
        public string ActionDate { get; set; }
        public string UpdatedBy { get; set; }
        public string Description { get; set; }
    }
}
