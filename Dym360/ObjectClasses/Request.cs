﻿using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Request : Process
    {
        public string Id { get; set; }
        public string RequestId { get; set; }
        public string RequestType { get; set; }
        public string RequestTypeId { get; set; }
        public string ReferenceNo { get; set; }
        public string Subject { get; set; } = "";
        public string Description { get; set; } = "";
        public string Status { get; set; } = "New";
        public string UserId { get; set; }
        public string Comment { get; set; }
        public string Remark { get; set; }
        public Requester Requester { get; set; } = new Requester();
        public DateTime CreatedDate { get; set; }
        public string RequestedBy { get; set; }
        public RequestState RequestState { get; set; } = new RequestState();
        public List<History> Histories { get; set; }
        public List<Attachment> Attachments { get; set; }
        public List<CommentRemark> CommentRemarks { get; set; }
        public List<Remark> Remarks { get; set; }
        public List<Approval> PEXs { get; set; }
        public List<Approval> Reviewers { get; set; }
        public List<Approval> Approvers { get; set; }
        public List<Approval> FinalApprovers { get; set; }

        //FOR CBM
        public List<Approval> FinanceTeam { get; set; }
        public List<Approval> CSTeam { get; set; }
        public List<Approval> AdminTeam { get; set; }
        public string AccountId { get; set; }
        public double Credit { get; set; }
        public double OldBalance { get; set; }
        public List<Approval> HODs { get; set; }
        public Approval CEO { get; set; }
        public DateTime? Period { get; set; }
        public string Link { get; set; }
        public Approval ARM { get; set; }

        //Subscription
        public string Name { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string ConstitutionType { get; set; }
        public string BusinessName { get; set; }
        public string BusinessRegNo { get; set; }
        public string Designation { get; set; }
        public string ContactNo { get; set; }
        public bool BusinessExists { get; set; }
        public Approval ARMHead { get; set; }
        public string PackageId { get; set; }
        public string CustomerLink { get; set; }
        public string PurposeOfUse { get; set; }
        public int FrequencyOfUse { get; set; }
        public string PersonInChargeId { get; set; }
        public Approval Customer { get; set; }
        public DateTime? CustomerSubmittedAt { get; set; }
        public List<PersonInChargeAttachment> PersonInChargeAttachments { get; set; }
        public string Website { get; set; }
        public List<Approval> LegalTeam { get; set; }
        public List<Approval> ManagementTeam { get; set; }
        public string LegalRecommendAction { get; set; }
        public string Reason { get; set; }
        public OnBoardingChecklist OnBoardingChecklist { get; set; }
        public UserAction UserAction { get; set; }
    }
}
