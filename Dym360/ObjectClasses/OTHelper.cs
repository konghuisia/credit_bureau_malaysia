﻿using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public static class OTHelper
    {
        private static int proposalNo = 1;
        public static List<ProjectSales> GetProjectSales()
        {
            var psList = new List<ProjectSales>();

            //psList.Add(new ProjectSales()
            //{
            //    Id = "1",
            //    //CustomerInfo = new Customer()
            //    //{
            //    //    Id = "1",
            //    //    Name = "ABC",
            //    //    ContactNo = "03-112233445",
            //    //    Email = "abc@email.com",
            //    //    FaxNo = "03-111122334",
            //    //    Address = new Address()
            //    //    {
            //    //        Country = "Malaysia",
            //    //        State = "Kuala Lumpur",
            //    //        City = "Bukit Bintang",
            //    //        PostalCode = "55100",
            //    //        Street = "Jalan Imbi"
            //    //    },
            //    //    AddressString = "abc 5 Jalan Imbi Bukit Bintang, 55100 Kuala Lumpur, Malaysia",
            //    //    ContactPersons = new List<ContactPerson>()
            //    //},
            //    //ProjectInfo = new Project()
            //    //{
            //    //    Id = "1",
            //    //    Name = "Project 1",
            //    //    Description = "Project 1 Description",
            //    //    //Estimation = "10000000.00",
            //    //    //Delivery = "",
            //    //    //ProposalNo = "1"
            //    //},
            //    //RequestStatus = new RequestStatus() {
            //    //    Status = StringEnum.GetStringValue(MyEnum.Workflow.REVIEW),
            //    //    CurrentStatus = MyEnum.Workflow.REVIEW,
            //    //    PreviousStatus = MyEnum.Workflow.NEW,
            //    //},
            //    //Requester = new Requester()
            //    //{
            //    //    Id = "8A86ED23-E718-4C88-A616-8179D89DFECB",
            //    //    Name = "Thermal-Matic 1",
            //    //    Role = "PSO",
            //    //    Position = "45"
            //    //},
            //    //RequestDate = "30 Jul 2018 10:40:50",
            //    //CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "Pending")
            //});
            //psList[0].CustomerInfo.ContactPersons.Add(new ContactPerson()
            //{
            //    Id = "1",
            //    Name = "CP 1",
            //    Email = "cp1@email.com",
            //    ContactNo = "013-8887779901"
            //});


            return psList;
        }

        public static string GetProposalNo()
        {
            return (proposalNo++).ToString();
        }

        public static Dictionary<string,string> GetDocumentType()
        {
            var d = new Dictionary<string, string>();
            d.Add("1", "Requiremnt");
            d.Add("2", "Proposal");
            d.Add("3", "Form");
            d.Add("4", "Additional");

            return d;
        }
    }
}
