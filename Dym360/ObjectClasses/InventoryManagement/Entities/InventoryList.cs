﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class InventoryList
    {
        public List<Product> Products { get; set; }
        public List<Material> Materials { get; set; }
    }
}
