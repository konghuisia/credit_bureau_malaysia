﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class Product : Inventory
    {
        public string Id { get; set; }
        public string UOM { get; set; }
        public List<Material> Materials { get; set; } = new List<Material>();
        public string Remark { get; set; } = "";
        public int Output { get; set; }
        public int Duration { get; set; }
        public double TotalCost { get; set; }
    }
}
