﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class Cost
    {
        public string SupplierId { get; set; }
        public double Price { get; set; } = 0.00;
        public float Quantity { get; set; } = 0;
    }
}
