﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class InventoryProduct
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
