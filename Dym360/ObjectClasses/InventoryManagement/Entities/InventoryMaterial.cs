﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class InventoryMaterial
    {
        public Material Material { get; set; }
        public int Quantity { get; set; }
        public double PurchasePricePerUOM { get; set; }
    }
}
