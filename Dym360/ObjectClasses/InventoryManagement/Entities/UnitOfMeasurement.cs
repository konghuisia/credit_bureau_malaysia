﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class UnitOfMeasurement
    {
        public string Purchase { get; set; }
        public double PurchaseUOMCost { get; set; } = 0.00;
        public string Production { get; set; }
        public double ProductionUOMCost { get; set; } = 0.00;
        public double AvgPurPrice { get; set; }
        public double QtyPrice { get; set; }
    }
}
