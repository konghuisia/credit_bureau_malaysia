﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.InventoryManagement.Entities
{
    public class Material : Inventory
    {
        public string Id { get; set; }
        public UnitOfMeasurement UOM { get; set; }
        public int Required { get; set; }
        public string Remark { get; set; }
    }
}
