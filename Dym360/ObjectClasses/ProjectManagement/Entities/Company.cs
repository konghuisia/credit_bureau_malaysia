﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class Company
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string FaxNo { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
    }
}
