﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class ProjectPlanTask
    {
        public string Id { get; set; }
        public string TaskId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string UserId { get; set; }
        public int Depth { get; set; }
        public string ParentId { get; set; }
        public string State { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ProjectId { get; set; }
        public string Remark { get; set; }
        public List<Remark> Remarks { get; set; }
        public string Sequence { get; set; }
    }
}
