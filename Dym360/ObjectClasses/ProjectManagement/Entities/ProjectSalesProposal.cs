﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class ProjectSalesProposal
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public Company CompanyInfo { get; set; }
        public ProjectInfo ProjectInfo { get; set; }
        public Contract ContractInfo { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<ProjectCommentInfo> Comments { get; set; }
        public List<ProjectController> ProjectControllers { get; set; } = new List<ProjectController>();
        public List<ProjectEngineer> projectEngineers { get; set; } = new List<ProjectEngineer>();
        public List<Attachment> Attachments { get; set; } = new List<Attachment>();
    }
}