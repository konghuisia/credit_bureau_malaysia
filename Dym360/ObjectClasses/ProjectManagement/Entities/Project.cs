﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class Project
    {
        public string ProjectId { get; set; }
        public string ReferenceNo { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string Status { get; set; }
        public string SalesProposalId { get; set; }
        public string Remark { get; set; }
        public string ActualEndDate { get; set; }
        public List<ProjectPlanTask> Tasks { get; set; }
    }
}
