﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class ProjectCommentInfo
    {
        public string Id { get; set; }
        public string ProjectId { get; set; }
        public string CreatedBy { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
    }
}