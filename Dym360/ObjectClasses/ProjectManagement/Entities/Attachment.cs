﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class Attachment
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public string Ext { get; set; }
        public string Location { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string UploadedDate { get; set; }
    }
}