﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class ProjectInfo
    {
        [Required]
        [Display(Name = "Reference No.")]
        public string RefNo { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Project No.")]
        public string No { get; set; }
        
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Project Estimation")]
        public string Estimation { get; set; }

        [Display(Name = "SalesType")]
        public int SalesType { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        public string Actual { get; set; }
    }
}