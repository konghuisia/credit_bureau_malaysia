﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ProjectManagement.Entities
{
    public class ProjectStateInfo
    {
        public string PositionId { get; set; }
        public string RoleId { get; set; }
        public string State { get; set; }
    }
}
