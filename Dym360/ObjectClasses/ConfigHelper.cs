﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public static class ConfigHelper
    {
        public static int SeqNo = 0;
        private static List<KeyValuePair<string, string>> ActionIds = new List<KeyValuePair<string, string>>();
        private static List<KeyValuePair<string, string>> ActionRoleIds = new List<KeyValuePair<string, string>>();

        public static List<RoleAction> RoleActions = GetRoleActions();
        public static List<ProcessFlowRole> ProcessFlowRoles = GetProcessFlowRole();
        public static List<RoleTypeBy> RoleTypes = GetRoleTypes();
        public static List<RequestState> RequestStates = GetRequestState();
        public static List<ProcessFlow> ProcessFlows = GetProcessFlow();
        public static List<User> Users = GetUsers();
        public static List<UserAccess> UserAccesses = GetUserAccesses();
        public static List<Document> Documents = GetDocuments();
        public static List<DocumentType> DocumentTypes = GetDocumentTypes();

        private static List<ProcessFlowRole> GetProcessFlowRole()
        {
            var aRList = new List<ProcessFlowRole>
            {
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("Requester"),
                //    Name = "Requester",
                //    Description = "Anyone can be requester",
                //    Actions = RoleActions.Where(x => x.Name == "Submit" || x.Name == "Save" || x.Name == "Cancel").ToList()
                //},
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("Reviewer"),
                //    Name = "Reviewer",
                //    Description = "Manager and above",
                //    Actions = RoleActions.Where(x => x.Name == "Approve" || x.Name == "Send Back" || x.Name == "Reject").ToList(),
                //    Role = "PSO"
                //},
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("Approver"),
                //    Name = "Approver",
                //    Description = "Manager and above",
                //    Actions = RoleActions.Where(x => x.Name == "Approve" || x.Name == "Send Back" || x.Name == "Reject").ToList(),
                //    Role = "GMO"
                //},
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("PEx"),
                //    Name = "PEx Team",
                //    Description = "PEx Team",
                //    Actions = RoleActions.Where(x => x.Name == "Submit" || x.Name == "Save" || x.Name == "Send Back").ToList(),
                //    Role = "GMO"
                //},
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("HOD"),
                //    Name = "HOD",
                //    Description = "Head of Department",
                //    Actions = RoleActions.Where(x => x.Name == "Approve " || x.Name == "send Back" || x.Name == "Reject").ToList(),
                //    Role = "HOD"
                //},
                //new ProcessFlowRole()
                //{
                //    Id = GenerateRequestRoleGuid("CEO"),
                //    Name = "CEO",
                //    Description = "Cheif Executive Officer",
                //    Actions = RoleActions.Where(x => x.Name == "Approve" || x.Name == "Send Back" || x.Name == "Reject").ToList(),
                //    Role = "CEO"
                //}
            };
            return aRList;
        }
        private static List<RoleTypeBy> GetRoleTypes()
        {
            var rt = new List<RoleTypeBy>
            {
                new RoleTypeBy()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Position",
                    Description = "Role type based on position"
                },

                new RoleTypeBy()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Department",
                    Description = "Role type based on department"
                },

                new RoleTypeBy()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "User",
                    Description = "Role type based on user"
                }
            };

            return rt;
        }
        private static string GenerateRequestRoleGuid(string actionRole)
        {
            var guid = Guid.NewGuid().ToString();
            ActionRoleIds.Add(new KeyValuePair<string, string>(guid, actionRole));
            return guid;
        }
        private static List<RoleAction> GetRoleActions()
        {
            var aList = new List<Configuration.RoleAction>();

            ActionIds = new List<KeyValuePair<string, string>>();

            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Save"),
            //    Name = "Save",
            //    Description = "Save draft, reamin same screen "
            //});
            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Submit"),
            //    Name = "Submit",
            //    Description = "Submit, proceed to next process flow"
            //});
            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Cancel"),
            //    Name = "Cancel",
            //    Description = "Cancel, cancel submission and stop process flow"
            //});
            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Approve"),
            //    Name = "Approve",
            //    Description = "Approve, stamp approve and proceed to next process flow"
            //});
            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Send Back"),
            //    Name = "Send Back",
            //    Description = "Send Back, stamp send back and proceed to previous process flow"
            //});
            //aList.Add(new Configuration.RoleAction()
            //{
            //    Id = GenerateRequestActionGuid("Reject"),
            //    Name = "Reject",
            //    Description = "Reject, close process flow with rejected status"
            //});

            return aList;
        }
        private static string GenerateRequestActionGuid(string action)
        {
            var guid = Guid.NewGuid().ToString();
            ActionIds.Add(new KeyValuePair<string, string>(guid, action));
            return guid;
        }
        private static List<RequestState> GetRequestState()
        {
            var asList = new List<RequestState>
            {
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "New",
                //    Description = "New",
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Draft",
                //    Description = "Saved as draft",
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Pending",
                //    Description = "Pending",
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Send Back - Pending",
                //    Description = "Pending requester update",
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Completed",
                //    Description = "Completed but have not close required close action"
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Closed",
                //    Description = "Completely closed no more action required"
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Rejected",
                //    Description = "Closed rejected no more action required"
                //},
                //new RequestState()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    Name = "Pending PEx",
                //    Description = "Closed rejected no more action required"
                //}
            };
            return asList;
        }
        private static List<ProcessFlow> GetProcessFlow()
        {
            var afList = new List<ProcessFlow>
            {
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Requester"),
                //    ActionId = ActionIds.Find(x => x.Value == "Save").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Draft"),
                //    Module = MyEnum.Module.PROJECT_SALES
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Requester"),
                //    ActionId = ActionIds.Find(x => x.Value == "Submit").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Progress = 5,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "Reviewer")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Requester"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Send Back - Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Submit").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Progress = 5,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "Reviewer")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Reviewer"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Approve").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Progress = 10,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "Approver")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Reviewer"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Send Back").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Progress = 0,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "Requester")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Reviewer"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Reject").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Rejected"),
                //    Progress = 5,
                //    Module = MyEnum.Module.PROJECT_SALES
                //},

                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Approver"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Approve").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Closed"),
                //    Progress = 15,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Approver"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Send Back").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Progress = 0,
                //    Module = MyEnum.Module.PROJECT_SALES,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "Requester")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Approver"),
                //    CurrentState = RequestStates.Find(x => x.Name == "Pending"),
                //    ActionId = ActionIds.Find(x => x.Value == "Reject").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Rejected"),
                //    Progress = 10,
                //    Module = MyEnum.Module.PROJECT_SALES
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Requester"),
                //    ActionId = ActionIds.Find(x => x.Value == "Save").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Draft"),
                //    Module = MyEnum.Module.PROCESS_VAULT
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "Requester"),
                //    ActionId = ActionIds.Find(x => x.Value == "Submit").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Module = MyEnum.Module.PROCESS_VAULT,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "PEx Team")
                //},
                //new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "PEx"),
                //    ActionId = ActionIds.Find(x => x.Value == "Submit").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Module = MyEnum.Module.PROCESS_VAULT,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "HOD")
                //},
                // new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "HOD"),
                //    ActionId = ActionIds.Find(x => x.Value == "Approve").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Module = MyEnum.Module.PROCESS_VAULT,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "CEO")
                //},
                //  new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "CEO"),
                //    ActionId = ActionIds.Find(x => x.Value == "Approve").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Pending"),
                //    Module = MyEnum.Module.PROCESS_VAULT,
                //    NextPIC = ProcessFlowRoles.Find(x => x.Name == "PEx")
                //},
                //    new ProcessFlow()
                //{
                //    Id = Guid.NewGuid().ToString(),
                //    CurrentPIC = ProcessFlowRoles.Find(x => x.Name == "PEx"),
                //    ActionId = ActionIds.Find(x => x.Value == "Submit").Key,
                //    NextState = RequestStates.Find(x => x.Name == "Closed"),
                //    Module = MyEnum.Module.PROCESS_VAULT
                //},
            };

            return afList;
        }
        private static List<User> GetUsers()
        {
            var u = new List<User>
            {
                new User()
                {
                    Id = "28AD54F9-A1D5-4905-8793-BE157A17FBCF",
                    Email = "nu1@email.com",
                    Name = "Normal User",
                    Grad = "45",
                    Position = "Senior Executive",
                    Role = "PSO"
                },
                new User()
                {
                    Id = "4108AE17-B073-4B4A-B801-FDB945E72A20",
                    Email = "md1@email.com",
                    Name = "Managing Director",
                    Grad = "99",
                    Position = "General Manager (Operation)",
                    Role = "GMO"
                },
                new User()
                {
                    Id = "85A40FBC-3FF5-42B0-AE6B-6227DAEB1271",
                    Email = "m1@email.com",
                    Name = "Manager 1",
                    Grad = "50",
                    Position = "Manager",
                    Role = "PSO"
                }
            };
            return u;
        }
        private static List<UserAccess> GetUserAccesses()
        {
            var u = new List<UserAccess>
            {
                new UserAccess()
                {
                    Id = Guid.NewGuid().ToString(),
                    Module = MyEnum.Module.PROJECT_SALES,
                    UserId = "28AD54F9-A1D5-4905-8793-BE157A17FBCF",
                    UserName = "Normal User",
                    Create = true,
                    Read = true,
                    Update = true,
                    Delete = true,
                    CreatedDate = DateTime.Now.AddDays(-30),
                    CreatedBy = "Admin",
                }
            };
            return u;
        }
        private static List<Document> GetDocuments()
        {
            var d = new List<Document>
            {
                new Document()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Standard Library 1.pdf",
                    Description = "Standard Library sample 1",
                    Size = "80400",
                    Extension = ".pdf",
                    Path = "http://localhost:50933/Documents/"
                },
                new Document()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Standard Library 2.pdf",
                    Description = "Standard Library sample 2",
                    Size = "89300",
                    Extension = ".pdf",
                    Path = "http://localhost:50933/Documents/"
                }
            };

            return d;
        }
        private static List<DocumentType> GetDocumentTypes()
        {
            var dt = new List<DocumentType>
            {
                new DocumentType()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Standard Library",
                    Description = "Document of standard library",
                    CreatedDate = DateTime.Now.AddDays(-5).ToString("dd MMM yyyy HH:mm:ss")

                },
                new DocumentType()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Requirement",
                    Description = "Document of requirement",
                    CreatedDate = DateTime.Now.AddDays(-10).ToString("dd MMM yyyy HH:mm:ss")
                },
                new DocumentType()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Proposal",
                    Description = "Document of proposal",
                    CreatedDate = DateTime.Now.AddDays(-11).ToString("dd MMM yyyy HH:mm:ss")
                },
                new DocumentType()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Contract",
                    Description = "Document of contract",
                    CreatedDate = DateTime.Now.AddDays(-11).ToString("dd MMM yyyy HH:mm:ss")
                }
            };
            return dt;
        }

        public static ProcessFlow GetActionFlow(string actionId)
        {
            return ProcessFlows.Find(x => x.ActionId == actionId);
        }
    }
}
