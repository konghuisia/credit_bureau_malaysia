﻿using ObjectClasses.Configuration;
using ObjectClasses.StandardOperationProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class MTHelper
    {
        public static List<SOP> SOPs = GetSOPs();

        private static List<SOP> GetSOPs()
        {
            var s = new List<SOP>();

            s.Add(new SOP()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Decommissioning Application Standard Operation Procedure",
                OwnedByDepartment = "IT",
                OwnedByUser = "HOD-IT",
                Validity = 48,
                CreatedDate = DateTime.Now.AddDays(-10).ToString("dd MMM yyyy HH:mm:ss"),
                EffectiveDate = DateTime.Now.AddDays(7).ToString("dd MMM yyyy HH:mm:ss"),
                DepartmentApplicability = new List<string>()
                {
                    "IT", "HR", "FIN"
                },
                SOPDocument = new Document()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Non-disclosure.pdf",
                    Path = "Documents/",
                    Extension = ".pdf",
                    Size = "506000",
                },
                //CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "Closed")
            });
            s.Add(new SOP()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Standard Operation Procedure",
                OwnedByDepartment = "IT",
                OwnedByUser = "HOD-IT",
                Validity = 48,
                CreatedDate = DateTime.Now.AddDays(-10).ToString("dd MMM yyyy HH:mm:ss"),
                EffectiveDate = DateTime.Now.AddDays(7).ToString("dd MMM yyyy HH:mm:ss"),
                DepartmentApplicability = new List<string>()
                {
                    "IT", "HR", "FIN"
                },
                SOPDocument = new Document()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "SOP_for_SOP_Creation_MS_v0_140518.xlsx",
                    Path = "Documents/SOP/SOP_for_SOP_Creation_MS_v0_140518.htm",
                    Extension = ".xlsx",
                    Size = "272000",
                },
                //CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "Closed")
            });
            return s;
        }
    }
}
