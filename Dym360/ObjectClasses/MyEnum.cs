﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class MyEnum
    {
        public enum Stage
        {
            [StringValue("New")]
            NEW = 0,
            [StringValue("CNC PUNCHING")]
            CNC_PUNCHIN = 1,
            [StringValue("SHEARING & BENDING")]
            SHEARING_BENDING = 2,
            [StringValue("FIN PUNCHING")]
            FIN_PUNCHING = 3,
            [StringValue("COIL ASSEMBLY")]
            COIL_ASSEMBLY = 4,
            [StringValue("TUBE STRAIGHTENING")]
            TUBE_STRAIGHTENING = 5,
            [StringValue("TUBE EXPENSION")]
            TUBE_EXPENSION = 6,
            [StringValue("BRAZING HEADER & U-BEND")]
            BRAZING_HEADER_UBEND = 7,
            [StringValue("LEAK TESTING")]
            LEAK_TESTING = 8,
            [StringValue("FINAL TOUCH UP / PACKING")]
            FINAL_TOUCHUP_PACKING = 9,
            [StringValue("Closed")]
            CLOSED = 10
        }

        public enum OperationType
        {
            [StringValue("Equal To")]
            EQUAL = 1,
            [StringValue("Greater Than")]
            GREATER = 2,
            [StringValue("Greater Or Equal To")]
            GREATEROREQUAL = 3,
            [StringValue("Lesser Than")]
            LESSER = 4,
            [StringValue("Lesser Or Equal To")]
            LESSEROREQUAL = 5
        }

        public enum Workflow
        {
            [StringValue("New")]
            NEW = 0,
            [StringValue("Draft")]
            DRAFT = 1,
            [StringValue("Review")]
            REVIEW,
            [StringValue("Approval")]
            APPROVAL,
            [StringValue("Reject")]
            REJECT,
            [StringValue("Send Back")]
            SEND_BACK,
            [StringValue("PEx Team")]
            PEX,
            [StringValue("Closed")]
            CLOSED
        }

        public enum Module
        {
            [StringValue("Project Sales")]
            PROJECT_SALES = 1,
            [StringValue("Contract Management")]
            CONTRACT,
            [StringValue("Project Management")]
            PROJECT,
            [StringValue("Document Portal")]
            PROCESS_VAULT,
            [StringValue("Approval")]
            APPROVAL


        }
    }
}
