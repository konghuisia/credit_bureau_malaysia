﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Remark
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public string ActionDate { get; set; }
    }
}
