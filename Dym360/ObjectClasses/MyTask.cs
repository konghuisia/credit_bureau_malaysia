﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class MyTask
    {
        public string ModuleId { get; set; }
        public string Module { get; set; }
        public string RequestId { get; set; }
        public string ReferenceNo { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string RequestType { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedDate { get; set; }
        public string Type { get; set; }
        public string Department { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string BusinessName { get; set; }
        public string BusinessRegNo { get; set; }

        //For Subscription Module
        public string LegalRecommendAction { get; set; }
        public string LegalRecommendActionId { get; set; }
    }
}
