﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Approvals
    {
        public string RoleId { get; set; }
        public List<Approval> ApprovalList { get; set; }
    }
}
