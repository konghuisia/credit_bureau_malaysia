﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.CreditManagement
{
    public class AccountRequest
    {
        public int id { get; set; }
        public string AccountRequestId { get; set; }
        public string AccountId { get; set; }
        public string RequestId { get; set; }
        public double OldBalance { get; set; }
        public double Credit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Deleted { get; set; }

        public string ReferenceNo { get; set; }
        public string Status { get; set; }
    }
}
