﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.CreditManagement
{
    public class Account
    {
        public int id { get; set; }
        public string AccountId { get; set; }
        public string Name { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public int? CreditLimit { get; set; }
        public int? Balance { get; set; }
        public int? Overdue { get; set; }
        public string PaymentFrequency { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Deleted { get; set; }
        public double Credit { get; set; }
        public double OldBalance { get; set; }
    }
}
