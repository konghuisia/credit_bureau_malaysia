﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class CommentRemark
    {
        public string UserName { get; set; }
        public string Comment { get; set; }
        public string ActionDate { get; set; }
    }
}
