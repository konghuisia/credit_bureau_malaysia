﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.DocumentPortal
{
    public class ECNDocument
    {
        public string ECNDocumentId { get; set; }
        public string DocumentName { get; set; }
        public string CompletionDate { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public int Size { get; set; }
        public DateTime PublishedDate { get; set; }
        public List<ECNTaskScope> ECNTaskScopes { get; set; }
        public string Remark { get; set; }
        public List<Attachment> Documents { get; set; }
    }
}
