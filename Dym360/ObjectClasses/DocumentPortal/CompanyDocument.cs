﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.DocumentPortal
{
    public class CompanyDocument
    {
        public string CompanyDocumentId { get; set; }
        public string SOPName { get; set; }
        public string SOPVersion { get; set; }
        public string UserOwnerId { get; set; }
        public string UserOwner { get; set; }
        public string DepartmentOwnerId { get; set; }
        public string DepartmentOwner { get; set; }
        public string SubmittedDate { get; set; }
        public string DepartmentApplicability { get; set; }
        public string LocationApplicability { get; set; }
        public string EffectiveDate { get; set; }
        public int Validity { get; set; }
        public string Classification { get; set; }
        public string Code { get; set; }
        public Document Document { get; set; }
    }
}
