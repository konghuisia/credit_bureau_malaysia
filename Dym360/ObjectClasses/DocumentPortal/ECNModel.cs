﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.DocumentPortal
{
    public class ECNModel
    {
        public string RequestId { get; set; }
        public string ReferenceNo { get; set; }
        public string Status { get; set; }
        public ECNDocument ECNDocument { get; set; }
    }
}
