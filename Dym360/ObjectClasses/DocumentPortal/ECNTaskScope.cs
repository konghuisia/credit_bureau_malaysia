﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.DocumentPortal
{
    public class ECNTaskScope
    {
        public string ECNTaskScopeId { get; set; }
        public string UserId { get; set; }
    }
}
