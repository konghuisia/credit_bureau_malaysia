﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Requester
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string PositionId { get; set; }
        public string Department { get; set; }
        public string DepartmentId { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string Reporting { get; set; }
        public string ReportingId { get; set; }
    }
}
