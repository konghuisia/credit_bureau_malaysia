﻿using ObjectClasses.Configuration;
using ObjectClasses.ProjectSalesManagement.Entities;
using ObjectClasses.StandardOperationProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public static class SessionHelper
    {
        public static List<ProjectSales> ProjectSalesSession { get; set; }
        public static List<ProcessFlow> ProcessFlows { get; set; }
        public static List<SOP> SOPs { get; set; }
    }
}
