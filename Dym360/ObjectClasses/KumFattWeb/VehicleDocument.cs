﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class VehicleDocument
    {
        public int Id { get; set; }
        public string VehicleDocumentId { get; set; }
        public string VehicleId { get; set; }
        public string VehicleDocumentType { get; set; }
        public string VehicleDocumentTypeId { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string ExpiryDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
