﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeWage
    {
        public string WageTypeId { get; set; }
        public string WageType { get; set; }
        public decimal Amount { get; set; }
        public decimal HourlyRate { get; set; }
        public bool IsEpfSocso { get; set; }
        public string MonthlyWageTypeId { get; set; }
        public string MonthlyWageType { get; set; }
        public bool IsSocso { get; set; }
    }
}