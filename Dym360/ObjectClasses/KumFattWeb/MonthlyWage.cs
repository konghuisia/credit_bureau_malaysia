﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class MonthlyWage
    {
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Date { get; set; }
        public string DayInMonth { get; set; }
        public int WorkHours { get; set; }
        public int Overtime { get; set; }
        public double Basic { get; set; }
        public double Allowance { get; set; }
        public string Team { get; set; }
        public string Position { get; set; }
        public double AdvanceAmount { get; set; }
        public double RemainingAmount { get; set; }
        public double MonthlyRepaymentAmount { get; set; }
        public string AdvanceDate { get; set; }
        public string WorkPermitExpiry { get; set; }
    }
}
