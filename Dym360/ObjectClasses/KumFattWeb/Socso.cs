﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class Socso
    {
        public string SocsoId { get; set; }
        public string Name { get; set; }
        public decimal Greater { get; set; }
        public decimal Lesser { get; set; }
        public decimal Rate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
