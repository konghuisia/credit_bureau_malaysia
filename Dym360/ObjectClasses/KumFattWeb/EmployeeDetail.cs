﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeDetail
    {
        public string Nationality { get; set; }
        public string IdentityNo { get; set; }
        public string DOB { get; set; }
        public string JoinDate { get; set; }
        public string Address { get; set; }
        public string HomeAddress { get; set; }
        public string ResignationDate { get; set; }
        public string RunawayDate { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public string Remark { get; set; }

        public string WorkPermitExpiry { get; set; }
        public string WorkPermit { get; set; }
    }
}