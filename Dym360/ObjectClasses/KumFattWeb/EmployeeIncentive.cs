﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeIncentive
    {
        public int Id { get; set; }
        public string IncentiveTypeId { get; set; }
        public decimal Rate { get; set; }
        public bool DeletedFlag { get; set; } = true;
    }
}