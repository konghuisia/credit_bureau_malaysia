﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class ProjectEngineer
    {
        public string ProjectEngineerId { get; set; }
        public string ProjectId { get; set; }
        public string EmployeeId { get; set; }
        public bool DeletedFlag { get; set; } = false;
    }
}
