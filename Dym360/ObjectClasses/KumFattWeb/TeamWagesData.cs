﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class TeamWagesData
    {
        public List<TeamWageAttendance> Attendances { get; set; }
    }
}
