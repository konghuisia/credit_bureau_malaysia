﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class ProjectSite : Logger
    {
        public ProjectSite()
        {
            Quotations = new List<Quotation>();
            PurchaseOrders = new List<PurchaseOrder>();
            LetterOfClaims = new List<LetterOfClaim>();
            TaxInvoices = new List<TaxInvoice>();
            Teams = new List<Team>();
            Contractors = new List<Contractor>();
            Documents = new List<Document>();
            Engineers = new List<ProjectEngineer>();
        }

        public string ProjectId { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string PersonInCharge { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<Quotation> Quotations { get; set; }
        public List<PurchaseOrder> PurchaseOrders { get; set; }
        public List<LetterOfClaim> LetterOfClaims { get; set; }
        public List<TaxInvoice> TaxInvoices { get; set; }
        public List<Team> Teams { get; set; }
        public List<Contractor> Contractors { get; set; }
        public List<Document> Documents { get; set; }
        public List<ProjectEngineer> Engineers { get; set; }

    }
}
