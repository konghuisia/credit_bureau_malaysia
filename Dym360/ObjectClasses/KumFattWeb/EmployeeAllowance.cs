﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeAllowance
    {
        public string AllowanceTypeId { get; set; } = "03D229FF-6362-4418-9CAB-18CEFB4AA5C9";
        public decimal Amount { get; set; }
        public decimal HourlyRate { get; set; }
    }
}