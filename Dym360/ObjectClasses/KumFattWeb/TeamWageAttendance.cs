﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class TeamWageAttendance
    {

        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string WageAmount { get; set; }
        public string Date { get; set; }
        public string DayInMonth { get; set; }
        public string WorkHours { get; set; }
    }
}
