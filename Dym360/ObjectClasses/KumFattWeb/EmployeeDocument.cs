﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeDocument
    {
        public int Id { get; set; }
        public string EmployeeDocumentId { get; set; }
        public string EmployeeId { get; set; }
        public string DocumentType { get; set; }
        public string DocumentTypeId { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string ExpiryDate { get; set; }
        public string CreatedBy { get; set; }
    }
}