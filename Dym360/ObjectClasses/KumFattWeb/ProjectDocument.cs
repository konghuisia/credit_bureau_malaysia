﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class ProjectDocument
    {
        public string ProjectSiteId { get; set; }
        public string DateOfIssue { get; set; }
        public string No { get; set; }
        public string Amount { get; set; }
        public string CreatedBy { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public int FileSize { get; set; }
        public bool DeletedFlag { get; set; }
    }
}
