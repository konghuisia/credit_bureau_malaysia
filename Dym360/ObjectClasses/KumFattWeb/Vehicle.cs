﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class Vehicle
    {
        public string VehicleId { get; set; }
        public string VehicleType { get; set; }
        public string VehicleTypeId { get; set; }
        public string RegistrationNo { get; set; }
        public List<VehicleDocument> VehicleDocuments { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool DeletedFlag { get; set; }
    }
}
