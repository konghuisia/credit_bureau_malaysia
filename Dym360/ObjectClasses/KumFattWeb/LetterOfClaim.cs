﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class LetterOfClaim : ProjectDocument
    {
        public string LetterOfClaimId { get; set; }
        public string ClaimType { get; set; }
    }
}
