﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class TaxInvoice : ProjectDocument
    {
        public string TaxInvoiceId { get; set; }
        public string ClaimType { get; set; }
        public string Status { get; set; }
    }
}
