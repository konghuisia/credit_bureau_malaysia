﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class EmployeeQR
    {
        public string EmployeeId { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string CreatedBy { get; set; }
    }
}
