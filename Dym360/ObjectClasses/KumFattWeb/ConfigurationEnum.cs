﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public enum MaritalStatus
    {
        Married = 1,
        Single,
    }
    public enum MonthlyRepaymentType
    {
        Monthly = 1,
        Bimonthly
    }
}
