﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class Account
    {
        public Account()
        {
            Accesses = new List<Access>();
        }

        public string AccountId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

        public User User { get; set; }

        public List<Access> Accesses { get; set; }
    }
}
