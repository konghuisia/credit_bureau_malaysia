﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class TeamWagesReport
    {
        public List<TeamWagesData> Records { get; set; }
    }
}
