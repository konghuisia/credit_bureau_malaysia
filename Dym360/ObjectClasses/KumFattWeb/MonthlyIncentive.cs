﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class MonthlyIncentive
    {
        public string IncentiveType { get; set; }
        public string EmployeeId { get; set; }
        public double Meter { get; set; }
        public double Rate { get; set; }
    }
}
