﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class User : Logger
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Source { get; set; } = "KumFatt";
        public string AccountTypeId { get; set; }
        public string AccountType { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string Salt { get; set; }

        public List<UserAccess> Accesses { get; set; }
    }
}
