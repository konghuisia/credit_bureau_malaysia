﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class Document
    {
        public string ProjectDocumentId { get; set; }
        public string CreatedBy { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public int FileSize { get; set; } = 0;
        public bool DeletedFlag { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
