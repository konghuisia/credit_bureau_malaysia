﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class AdvancePayment
    {
        public string EmployeeLoanId { get; set; }
        public string AdvancePaymentId { get; set; }
        public string AdvanceDate { get; set; }
        public decimal AdvanceAmount { get; set; }
        public string RepaymentEffDate { get; set; }
        public decimal MonthlyRepaymentAmount { get; set; }
        public MonthlyRepaymentType MonthlyRepaymentType { get; set; }
    }
}
