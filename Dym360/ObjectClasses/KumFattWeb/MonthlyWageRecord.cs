﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class MonthlyWageRecord
    {
        public List<MonthlyWage> MonthlyWages { get; set; }
        public List<MonthlyIncentive> MonthlyIncentives { get; set; }
    }
}
