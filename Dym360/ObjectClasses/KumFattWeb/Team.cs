﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class Team : Logger
    {
        public Team()
        {
            Members = new List<TeamMember>();
            Vehicles = new List<Vehicle>();
        }
        public string TeamId { get; set; }
        public string Name { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TeamLeadId { get; set; }
        public string TeamLeader { get; set; }
        public int MemberCount { get; set; }
        public List<TeamMember> Members { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        public bool DeletedFlag { get; set; } = false;
    }
}
