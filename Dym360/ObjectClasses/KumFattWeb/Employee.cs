﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectClasses.KumFattWeb
{
    public class Employee : Logger
    {
        public Employee()
        {
            Detail = new EmployeeDetail();
            Allowance = new EmployeeAllowance();
            Documents = new List<EmployeeDocument>();
            Incentives = new List<EmployeeIncentive>();
            AdvancePayment = new AdvancePayment();
        }
        public int Id { get; set; }
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public string OfficeContactNo { get; set; }
        public string EmployeeTypeId { get; set; }
        public string EmployeeType { get; set; }
        public string JobTitleId { get; set; }
        public string JobTitleName { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public EmployeeDetail Detail { get; set; }
        public EmployeeAllowance Allowance { get; set; }
        public ICollection<EmployeeDocument> Documents { get; set; }
        public EmployeeWage Wage { get; set; }
        public List<EmployeeIncentive> Incentives { get; set; }
        public ProfileImage ProfileImage { get; set; }
        public EmployeeQR QR { get; set; }
        public string EMPName { get; set; }
        public string EMPContactNo { get; set; }
        public string RelationshipId { get; set; }
        public string RelationshipName { get; set; }
        public AdvancePayment AdvancePayment { get; set; }
        public string AgencyName { get; set; }
        public string AgencyId { get; set; }
        public string OtherAgency { get; set; }
        public Team Team { get; set; }
    }
}