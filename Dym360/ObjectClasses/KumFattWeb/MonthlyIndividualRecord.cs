﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.KumFattWeb
{
    public class MonthlyIndividualRecord
    {
        public string Date { get; set; }
        public string DayInMonth { get; set; }
        public string WorkHours { get; set; }
        public string ProjectName { get; set; }
    }
}
