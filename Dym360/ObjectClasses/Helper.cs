﻿using ObjectClasses.ApprovalManagement.Entities;
using ObjectClasses.InventoryManagement.Entities;
using ObjectClasses.ProductionManagement.Entities;
using ObjectClasses.ProjectManagement.Entities;
using ObjectClasses.SchedulerManagement.Entities;
using ObjectClasses.SupplierManagement.Entities;
using System;
using System.Collections.Generic;
using static ObjectClasses.MyEnum;

namespace ObjectClasses
{
    public class Helper
    {
        public static List<Supplier> GetSuppliers()
        {
            var S = new List<Supplier>();
            var c = new List<ContactPerson>();
            var i = new List<Supply>();

            c.Add(new ContactPerson()
            {
                Name = "George",
                Email = "george@email.com",
                ContactNo = "0169991234"
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "1" })[0],
                Quantity = 10,
                Price = 200.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "2" })[0],
                Quantity = 20,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "3" })[0],
                Quantity = 30,
                Price = 300.00
            });
            S.Add(new Supplier
            {
                Id = "1",
                Name = "Steel Pipe Supplier",
                Contact1 = "0169991109",
                Email = "steelpipe@email.com",
                FaxNo = "0312345678",
                PaymentTerm = "Payment In Advance",
                ContactPersons = c,
                Address = "Steel Pipe Supplier Address",
                SupplyItems = i
            });

            c = new List<ContactPerson>();
            c.Add(new ContactPerson()
            {
                Name = "Paviani",
                Email = "paviani@email.com",
                ContactNo = "0138829384"
            });
            i = new List<Supply>();
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "4" })[0],
                Quantity = 10,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "5" })[0],
                Quantity = 60,
                Price = 300.00
            });
            S.Add(new Supplier
            {
                Id = "2",
                Name = "Fitting Supplier",
                Contact1 = "0189922001",
                Email = "pvc@email.com",
                FaxNo = "034481921",
                PaymentTerm = "End Of Month",
                ContactPersons = c,
                Address = "Fitting Supplier Address",
                SupplyItems = i
            });
            c = new List<ContactPerson>();
            c.Add(new ContactPerson()
            {
                Name = "James",
                Email = "james@email.com",
                ContactNo = "0198274938"
            });
            i = new List<Supply>();
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "1" })[0],
                Quantity = 20,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "3" })[0],
                Quantity = 60,
                Price = 600.00
            });
            S.Add(new Supplier
            {
                Id = "3",
                Name = "Nut & Bolt Supplier",
                Contact1 = "012778109",
                Email = "nut_bolt@email.com",
                FaxNo = "031112893",
                PaymentTerm = "Monthly Credit Payment",
                ContactPersons = c,
                Address = "Nut & Bolt Supplier Address",
                SupplyItems = i
            });
            c = new List<ContactPerson>();
            c.Add(new ContactPerson()
            {
                Name = "James",
                Email = "james@email.com",
                ContactNo = "0198274938"
            });
            i = new List<Supply>();
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "6" })[0],
                Quantity = 20,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "7" })[0],
                Quantity = 60,
                Price = 600.00
            });
            S.Add(new Supplier
            {
                Id = "4",
                Name = "Fan Supplier",
                Contact1 = "03882789",
                Email = "fan@email.com",
                FaxNo = "031112893",
                PaymentTerm = "End Of Month",
                ContactPersons = c,
                Address = "Fan Sdb Bhd Address",
                SupplyItems = i
            });
            c = new List<ContactPerson>();
            c.Add(new ContactPerson()
            {
                Name = "James",
                Email = "james@email.com",
                ContactNo = "0198274938"
            });
            i = new List<Supply>();
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "8" })[0],
                Quantity = 30,
                Price = 90.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "9" })[0],
                Quantity = 60,
                Price = 200.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "10" })[0],
                Quantity = 20,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "11" })[0],
                Quantity = 60,
                Price = 1200.00
            });
            S.Add(new Supplier
            {
                Id = "5",
                Name = "Mr Paul Mac",
                Contact1 = "0168889907",
                Email = "paul_mac@email.com",
                FaxNo = "031112893",
                PaymentTerm = "Cash On Delivery",
                ContactPersons = c,
                Address = "Paul Mac Address"
            });
            c = new List<ContactPerson>();
            c.Add(new ContactPerson()
            {
                Name = "James",
                Email = "james@email.com",
                ContactNo = "0198274938"
            });
            i = new List<Supply>();
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "12" })[0],
                Quantity = 15,
                Price = 150.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "13" })[0],
                Quantity = 50,
                Price = 25.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "14" })[0],
                Quantity = 20,
                Price = 100.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "15" })[0],
                Quantity = 6,
                Price = 600.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "16" })[0],
                Quantity = 15,
                Price = 150.00
            });
            i.Add(new Supply()
            {
                Material = GetMaterials(new List<string>() { "10" })[0],
                Quantity = 60,
                Price = 600.00
            });
            S.Add(new Supplier
            {
                Id = "6",
                Name = "Mrs Courtney Murray",
                Contact1 = "0124427788",
                Email = "courtney_murray@email.com",
                FaxNo = "031112893",
                PaymentTerm = "Payment In Advance",
                ContactPersons = c,
                Address = "Courtney Murray Address"
            });
            return S;

        }
        public static List<History> GetInventoryHistories()
        {
            var I = new List<History>();
            I.Add(new History()
            {
                Action = "Update",
                ActionDate = "2018-07-18 08:40:14",
                UserName = "Thermal-Matic 1",
                Description = "Update [BF BRASS FITTING ACCESS VALVE BF-AV02] Quantity to 100"
            });
            I.Add(new History()
            {
                Action = "Update",
                ActionDate = "2018-07-18 12:45:54",
                UserName = "Thermal-Matic 1",
                Description = "Update [CF COPPER FITTING U-BEND CF-UB04] Quantity to 150"
            });
            I.Add(new History()
            {
                Action = "Update",
                ActionDate = "2018-07-18 16:30:14",
                UserName = "Thermal-Matic 1",
                Description = "Update [FA FASTENERS NUTS FA-NU-M4-A4SS] Quantity to 200"
            });

            return I;
        }
        public static List<History> GetProductHistories()
        {
            var P = new List<History>();
            P.Add(new History()
            {
                Action = "Update",
                ActionDate = "2018-07-18 08:40:14",
                UserName = "Thermal-Matic 1",
                AdditionalInfo = "AC-Q10-AL/EG",
                Description = "Description updated from 'THERMAL-MATIC Q10 AIR-COOLED CONDENSER. (AL FIN/STUCCO AL CASING/FAN SET 2)' to 'THERMAL-MATIC Q10 AIR-COOLED CONDENSER. (AL FIN/STUCCO AL CASING/FAN SET)'"
            });
            P.Add(new History()
            {
                Action = "Add",
                ActionDate = "2018-07-18 12:45:54",
                UserName = "Thermal-Matic 2",
                AdditionalInfo = "UC-RUC990",
                Description = "Added new product UNIT COOLER"
            });
            P.Add(new History()
            {
                Action = "Delete",
                ActionDate = "2018-07-18 16:30:14",
                UserName = "Thermal-Matic 3",
                AdditionalInfo = "UC-RUC440",
                Description = "Delete product UNIT COOLER"
            });

            return P;
        }
        
        public static List<Product> GetProducts(List<String> ids = null)
        {
            var products = new List<Product>();
            products.Add(new Product()
            {
                Id = "1",
                GroupId = "HE",
                Group = "HEAT EXCHANGER",
                Type = "CHILLED WATER COIL",
                ItemId = "HE-CW-AL4",
                Description = "CHILLED WATER COIL DIM: 53.75''FH X 85.04''FL X 6ROW X 12FPI X 1/2''DIA  MAT'L USED: 0.12MM AL FIN / 0.41MM CU TUBE / 1.5MM GI FRAME",
                UOM = "UNIT",
                Remark = "",
                Duration = 8,
                Output = 4,
                Materials = GetMaterials(new List<String>() { "14","22", "2","23","24","26","25" })
            });
            products.Add(new Product()
            {
                Id = "2",
                GroupId = "HE",
                Group = "HEAT EXCHANGER",
                Type = "STEAM COIL",
                ItemId = "HE-ST-BL5",
                Description = "STEAM COIL DIM: ''FH X ''FL X ROW X FPI X 5/8''DIA  MAT'L USED: MM BLUE FIN / MM CU TUBE / 1.5MM GI FRAME",
                UOM = "UNIT",
                Remark = "",
                Duration = 6,
                Output = 3,
                Materials = GetMaterials(new List<string>() { "1", "2" })
            });
            products.Add(new Product()
            {
                Id = "3",
                GroupId = "HE",
                Group = "Heat Exchanger",
                Type = "BARE COIL",
                ItemId = "HE-BC-AL5",
                Description = "BARE COIL COIL DIM: ''FH X ''FL X ROW X FPI X 5/8''DIA  MAT'L USED: MM AL FIN / MM CU TUBE",
                UOM = "UNIT",
                Remark = "",
                Duration = 8,
                Output = 4,
                Materials = GetMaterials(new List<string>() { "12", "20" })

            });
            products.Add(new Product()
            {
                Id = "4",
                GroupId = "HE",
                Group = "HEAT EXCHANGER",
                Type = "CONDENSER COIL",
                ItemId = "HE-CC-BL4",
                Description = "CONDENSER COIL DIM: ''FH X ''FL X ROW X FPI X 1/2''DIA  MAT'L USED: MM BLUE FIN / MM CU TUBE / 1.5MM GI FRAME",
                UOM = "UNIT",
                Remark = "",
                Duration = 8,
                Output = 4,
                Materials = GetMaterials(new List<string>() { "11", "21" })
            });
            products.Add(new Product()
            {
                Id = "5",
                GroupId = "PM",
                Group = "PACKING MATERIAL",
                Type = "WOODEN CASE",
                ItemId = "PM-WOODCASE",
                Description = "WOODEN CASE (LOOSE / ENCLOSED)",
                UOM = "UNIT",
                Remark = "",
                Duration = 8,
                Output = 4,
                Materials = GetMaterials(new List<string>() { "3", "6", "8" })
            });
            products.Add(new Product()
            {
                Id = "6",
                GroupId = "AC",
                Group = "AIR-COOLDED CONDENSER",
                Type = "Q10",
                ItemId = "AC-Q10-AL/EG",
                Description = "THERMAL-MATIC Q10 AIR-COOLED CONDENSER. (AL FIN/STUCCO AL CASING/FAN SET)",
                UOM = "UNIT",
                Remark = "AC-Q10BVAEMACP",
                Duration = 4,
                Output = 2,
                Materials = GetMaterials(new List<string>() { "10", "2","6","9" })
            });
            products.Add(new Product()
            {
                Id = "7",
                GroupId = "AC",
                Group = "AIR-COOLDED CONDENSER",
                Type = "T105",
                ItemId = "AC-T105CC-CU/SS",
                Description = "THERMAL-MATIC T105 AIR-COOLED CONDENSER COIL C/W CASING ONLY. (CU FIN/S/STEEL CASING)",
                UOM = "UNIT",
                Remark = "",
                Duration = 4,
                Output = 2,
                Materials = GetMaterials(new List<string>() { "2", "3","4","5" })
            });
            products.Add(new Product()
            {
                Id = "8",
                GroupId = "AC",
                Group = "AIR-COOLDED CONDENSER",
                Type = "U303",
                ItemId = "AC-U303SET-CU/EG",
                Description = "THERMAL-MATIC U303 AIR-COOLED CONDENSER (COMPLETE SET). (CU FIN/STUCCO AL CASING/FAN SET)",
                UOM = "UNIT",
                Remark = "",
                Duration = 4,
                Output = 2,
                Materials = GetMaterials(new List<string>() { "10", "20" })
            });
            products.Add(new Product()
            {
                Id = "9",
                GroupId = "AC",
                Group = "AIR-COOLDED CONDENSER",
                Type = "U303",
                ItemId = "AC-U303SET-CU/SS",
                Description = "THERMAL-MATIC U303 AIR-COOLED CONDENSER (COMPLETE SET). (CU FIN/S/STEEL CASING/FAN SET)",
                UOM = "UNIT",
                Remark = "",
                Duration = 4,
                Output = 2,
                Materials = GetMaterials(new List<string>() { "5", "10","15","20","25" })
            });
            products.Add(new Product()
            {
                Id = "10",
                GroupId = "UC",
                Group = "UNIT COOLER",
                Type = "EP",
                ItemId = "UC-EP040",
                Description = "THERMAL-MATIC UNIT COOLER EP-040",
                UOM = "UNIT",
                Remark = "UC-TMEP040",
                Duration = 2,
                Output = 1,
                Materials = GetMaterials(new List<string>() { "2", "4","6","8" })
            });
            products.Add(new Product()
            {
                Id = "11",
                GroupId = "UC",
                Group = "UNIT COOLER",
                Type = "RUC",
                ItemId = "UC-RUC990",
                Description = "THERMAL-MATIC UNIT COOLER RUC-990",
                UOM = "UNIT",
                Remark = "UC-TMRUC990",
                Duration = 2,
                Output = 1,
                Materials = GetMaterials(new List<string>() { "7", "14","21" })
            });

            var result = new List<Product>();
            
            if (ids != null)
            {
                foreach (var id in ids)
                {
                    result.Add(products.Find(x => x.Id == id));
                }
            }
            else
            {
                result = products;
            }
            return result;
        }
        public static List<Material> GetMaterials(List<String> ids = null)
        {
            var M = new List<Material>();
            M.Add(new Material()
            {
                Id = "1",
                GroupId = "BF",
                Group = "BRASS FITTING",
                Type = "ACCESS VALVE",
                ItemId = "BF-AV02",
                Description = "1/4'' ACCESS VALVE C / W TUBING",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "2",
                GroupId = "CF",
                Group = "COPPER FITTING",
                Type = "U-BEND",
                ItemId = "CF-UB04",
                Description = "1/2''U - BEND",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "3",
                GroupId = "CF",
                Group = "COPPER FITTING",
                Type = "COPPER ELBOW",
                ItemId = "CF-CE90-05",
                Description = "5/8'' COPPER ELBOW 90°",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "4",
                GroupId = "CF",
                Group = "COPPER FITTING",
                Type = "COPPER ELBOW",
                ItemId = "CF-CE90-06",
                Description = "3/4'' COPPER ELBOW 90°",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "5",
                GroupId = "CH",
                Group = "CONDENSER HEADER",
                Type = "CONDENSER HEADER",
                ItemId = "CH-01HP550-010",
                Description = "COPPER TUBE WITH BENDING PART NO: 01HP550-010 (3/4'')",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "6",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "NUTS",
                ItemId = "FA-NU-M4-A4SS",
                Description = "S/S A4 HEX NUTS DIN 934 M4",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "7",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "NUTS",
                ItemId = "FA-NU-M4-BLINDSS",
                Description = "S/S BLIND NUTS (CSK TYPE) M4 (SEM)-SM425",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "8",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "BOLTS",
                ItemId = "FA-BO-0516X4-UNC",
                Description = "HT HEX BOLTS UNC 5/16 X 4",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "9",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "BOLTS",
                ItemId = "FA-BO-06X3-UNC",
                Description = "HT HEX BOLTS UNC 3/4 X 3",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "10",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "SCREWS",
                ItemId = "FA-SCS-M6X60",
                Description = "SOCKET CAP SCREWS DIN 912 M6 X 60",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "11",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "SCREWS",
                ItemId = "FA-SCS-M6X65",
                Description = "SOCKET CAP SCREWS DIN 912 M6 X 65",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "12",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "WASHERS",
                ItemId = "FA-WSS-M4",
                Description = "SPRING WASHERS (Z/P) M4",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "13",
                GroupId = "FA",
                Group = "FASTENERS",
                Type = "WASHERS",
                ItemId = "FA-WSS-M4-A4SS316",
                Description = "S/S A4/316 SPRING WASHERS M4",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "14",
                GroupId = "FIN",
                Group = "FIN",
                Type = "ALUMINIUM FIN",
                ItemId = "FIN-AL-0.12X510",
                Description = "0.12MM X 510MM ALUMINIUM FIN (AA1100-0)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "KG",
                    Production = "KG"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "15",
                GroupId = "FS",
                Group = "FAN SET",
                Type = "FAN SET 14''",
                ItemId = "FS-YD14-1P",
                Description = "14'' FAN SET YDWF74L47P4 - 422N - 350S 220V - 240V, 50HZ(1PH)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SET",
                    Production = "SET"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "15",
                GroupId = "FS",
                Group = "FAN SET",
                Type = "FAN SET 16''",
                ItemId = "FS-YD16-3P",
                Description = "16'' FAN SET YSWF74L60P4 - 470N - 400LF 410V - 440V, 50HZ(3PH)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SET",
                    Production = "SET"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "16",
                GroupId = "FS",
                Group = "FAN SET",
                Type = "FAN SET 18''",
                ItemId = "FS-YD18-3P",
                Description = "18'' FAN SET YSWF102L45P4 - 522N - 450LF 410V - 440V, 50HZ(3PH)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SET",
                    Production = "SET"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "17",
                GroupId = "FS",
                Group = "FAN SET",
                Type = "FAN SET 20''",
                ItemId = "FS-YD20-3P",
                Description = "20'' FAN SET YSWF102L50P4 - 570N - 500LF 410V - 440V, 50HZ(3PH)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SET",
                    Production = "SET"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "18",
                GroupId = "HT",
                Group = "HEATER",
                Type = "COIL HEATER",
                ItemId = "HT-CH-C83TT",
                Description = "COIL HEATER 83''L(C83TT)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "LGTH",
                    Production = "LGHT"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "19",
                GroupId = "HT",
                Group = "HEATER",
                Type = "DRAINPAN HEATER",
                ItemId = "HT-DH-D101TT",
                Description = "DRAIN PAN HEATER 101''L(D101TT / 861 - 382516B - 12D)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "LGTH",
                    Production = "LGHT"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "20",
                GroupId = "ML",
                Group = "MISCELLANEOUS",
                Type = "ANGLE BAR",
                ItemId = "ML-AB5X50X50",
                Description = "5(2) X 50 X 50 X 6M M.S.ANGLE BAR",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "21",
                GroupId = "ML",
                Group = "MISCELLANEOUS",
                Type = "ROUND PLATE",
                ItemId = "ML-RP0316X79",
                Description = "3/16'' X 79MM ROUND PLATE",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "PC",
                    Production = "PC"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "22",
                GroupId = "PT",
                Group = "PIPE/TUBE",
                Type = "COPPER TUBE - LWC",
                ItemId = "PT-CTLWC-04X0.41",
                Description = "12.7MM (1/2'') X 0.41MM COPPER TUBE LWC",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "KG",
                    Production = "KG"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "23",
                GroupId = "SH",
                Group = "SHEET",
                Type = "GI SHEET",
                ItemId = "SH-GI-1.5MMX4X10",
                Description = "GI SHEET 1.5MM X 4'(1219) X 10'(3048)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SHEET",
                    Production = "SQ. IN"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "24",
                GroupId = "SH",
                Group = "SHEET",
                Type = "GI SHEET",
                ItemId = "SH-GI-1.5MMX4X10",
                Description = "GI SHEET 1.5MM X 4'(1219) X 10'(3048)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "SHEET",
                    Production = "SQ. IN"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "25",
                GroupId = "WT",
                Group = "WELDING ROD/ FLUX",
                Type = "WELDING ROD",
                ItemId = "WT-WR-0P",
                Description = "BRAZING ROD - HARRIS 0 (0%) 0.05'' X 1 / 8'' X 20''(11.34KG / BOX / 714PCS)",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "KG",
                    Production = "PCS"
                },
                Remark = ""
            });
            M.Add(new Material()
            {
                Id = "26",
                GroupId = "PT",
                Group = "PIPE/TUBE",
                Type = "GI PIPE",
                ItemId = "PT-GI-400",
                Description = "G.I PIPE (CLASS B) 6M X 4''",
                UOM = new UnitOfMeasurement()
                {
                    
                    Purchase = "LGTH",
                    Production = "IN"
                },
                Required = 0,
                Remark = ""
            });

            var result = new List<Material>();

            if (ids != null)
            {
                foreach (var id in ids)
                {
                    result.Add(M.Find(x => x.Id == id));
                }
            }
            else
            {
                result = M;
            }

            return result;
        }

        public static List<Scheduler> GetPendingSchedulers()
        {
            var S = new List<Scheduler>();
            S.Add(new Scheduler()
            {
                Id = "1",
                RefNo = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(1).Next(1, 25)),
                Name = "Heat Exchanger for Export",
                Status = StringEnum.GetStringValue((Stage)2),
                Progress = (20).ToString() + "%",
                State = 2,
                ScheduledDate = "22 Jul 2018",
                StartDate = "22 Jul 2018",
                EndDate = "24 Jul 2018",
                Duration = "2",
                Quantity = "2",
                Product = GetProducts(new List<string>() { "1" })[0]
            });
            S.Add(new Scheduler()
            {
                Id = "2",
                RefNo = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(2).Next(26, 50)),
                Name = "Heat Exchanger 2",
                Status = StringEnum.GetStringValue((Stage)1),
                Progress = (10).ToString() + "%",
                State = 1,
                ScheduledDate = "23 Jul 2018",
                StartDate = "23 Jul 2018",
                EndDate = "26 Jul 2018",
                Duration = "3",
                Quantity = "3",
                Product = GetProducts(new List<string>() { "2" })[0]
            });
            S.Add(new Scheduler()
            {
                Id = "3",
                RefNo = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(2).Next(26, 50)),
                Name = "Heat Exchanger 3",
                Status = StringEnum.GetStringValue((Stage)0),
                Progress = (0).ToString() + "%",
                State = 0,
                ScheduledDate = DateTime.Now.ToString("dd MMM yyyy"),
                StartDate = DateTime.Now.AddDays(4).ToString("dd MMM yyyy"),
                EndDate = DateTime.Now.AddDays(9).ToString("dd MMM yyyy"),
                Duration = "5",
                Quantity = "3",
                Product = GetProducts(new List<string>() { "3" })[0]
            });
            return S;
        }
        public static List<Scheduler> GetCloseSchedulers()
        {
            var S = new List<Scheduler>();
            S.Add(new Scheduler()
            {
                Id = "1",
                RefNo = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(2).Next(1, 25)),
                Name = "Air-Cooled Condenser for Export",
                Status = "Closed",
                Progress = "100%",
                State = 0,
                ScheduledDate = "27 Jun 2018",
                StartDate = "28 Jun 2018",
                Duration = "4",
                Quantity = "2",
                Product = GetProducts(new List<string>() { "9" })[0]
            });
            S.Add(new Scheduler()
            {
                Id = "2",
                RefNo = "PRO-" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "-" + String.Format("{0:D4}", new Random(2).Next(1, 25)),
                Name = "Unit Cooler for Export",
                Status = "Closed",
                Progress = "100%",
                State = 0,
                ScheduledDate = "01 Jul 2018",
                StartDate = "02 Jul 2018",
                Duration = "1",
                Quantity = "1",
                Product = GetProducts(new List<string>() { "11" })[0]
            });
            return S;
        }

        public static string ScheduledDate()
        {
            return DateTime.Now.ToString("dd MMM yyyy");
        }

        public static List<Remark> GetRemarks()
        {
            var remarks = new List<Remark>();
            //remarks.Add(new Remark()
            //{
            //    Id = "1",
            //    Name = "Thermal-Matic 1",
            //    Description = "Some remark for next PIC",
            //    Date = new DateTime().ToString("dd-MM-yyyy hh:mm"),
            //    Action = "Create"

            //});
            return remarks;
        }

        public static List<ProjectSalesProposal> GetProjects()
        {
            var P = new List<ProjectSalesProposal>();

            P.Add(GetProject("1"));
            return P;
        }

        public static ProjectSalesProposal GetProject(string id)
        {
            var P = new List<ProjectSalesProposal>();

            P.Add(new ProjectSalesProposal()
            {
                Id = "1",
                CreatedBy = "User 1",
                CompanyInfo = new Company()
                {
                    Name = "Hup Seng",
                    ContactNo = "03-8884491",
                    Email = "hupseng@email.com",
                    FaxNo = "03-9982910",
                    Address = "Hup Seng company address"
                },
                ProjectInfo = new ProjectInfo()
                {
                    RefNo = "PRO-30072018124523-0001",
                    No = "12345",
                    Estimation = "1000.00",
                    Description = "Project Description",
                    Name = "Palm Refinery Machine",
                    Status = "Project Controller Nomination"
                },
                ContractInfo = new Contract()
                {
                    Name = "Thermal Matic.doc",
                    Location = @"C:\Users\owen_\Documents\master\Dym360\Contracts",
                    No = "100001",
                    Description = "Hup Seng project contract"
                },
                Attachments = GetAttachment(new List<string>() { "1" })
            });



            return P.Find(x => x.Id == id);
        }
        public static List<Attachment> GetAttachment(List<string> ids)
        {
            var A = new List<Attachment>();

            var tmp = new List<Attachment>();

            tmp.Add(new Attachment()
            {
                Id = "1",
                Name = "Thermal Matic.doc",
                Ext = ".doc",
                Location = @"C:\Users\owen_\Documents\master\Dym360\Contracts",
                Description = "Sample attachment",
                Type = "Project Plan",
                UploadedDate = "30 Jul 2018 10:42:16",
                Size = "19000"

            });

            foreach (var i in ids)
            {
                A.Add(tmp.Find(x => x.Id == i));
            }

            return A;
        }

        public static List<ApprovalManagement.Entities.Approval> GetProposalApprovals()
        {
            var A = new List<ApprovalManagement.Entities.Approval>();
            var oList = new List<Operation>();
            var aList = new List<Approver>();

            oList.Add(new Operation()
            {
                OperationType = OperationType.LESSEROREQUAL,
                Value = 5000000.00
            });
            aList.Add(new Approver()
            {
                Id = "1",
                Role = "PSO",
                Type = "Primary"
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "1",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "Up to 5 Million",
                    Description = "Project sales approval matrix under 5 million",
                    Operations = oList,
                    Approvers = aList
                },
                CreatedDate = "30 Jul 2018 10:30:15",
                CreatedBy = "Admin 1"
            });

            oList = new List<Operation>();
            oList.Add(new Operation()
            {
                OperationType = OperationType.GREATEROREQUAL,
                Value = 5000000.00
            });
            oList.Add(new Operation()
            {
                OperationType = OperationType.LESSEROREQUAL,
                Value = 10000000.00,
            });
            aList = new List<Approver>();
            aList.Add(new Approver()
            {
                Id = "2",
                Role = "PSO",
                Type = "Primary"
            });
            aList.Add(new Approver()
            {
                Id = "3",
                Role = "GM",
                Type = "Secondary"
            });
            aList.Add(new Approver()
            {
                Id = "4",
                Role = "MD",
                Type = "Delegate"
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "2",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "5 to 10 Million",
                    Description = "Project sales approval matrix between 5 million to 10 million",
                    Operations = oList,
                    Approvers = aList
                },
                CreatedDate = "30 Jul 2018 10:30:15",
                CreatedBy = "Admin 1"
            });

            oList = new List<Operation>();
            oList.Add(new Operation()
            {
                OperationType = OperationType.GREATER,
                Value = 10000000.00,
            });
            aList = new List<Approver>();
            aList.Add(new Approver()
            {
                Id = "5",
                Role = "PSO",
                Type = "Primary"
            });
            aList.Add(new Approver()
            {
                Id = "6",
                Role = "MD",
                Type = "Secondary"
            });
            aList.Add(new Approver()
            {
                Id = "7",
                Role = "GM",
                Type = "Delegate"
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "3",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "More than 10 Million",
                    Description = "Project sales approval matrix more than 10 million",
                    Operations = oList,
                    Approvers = aList
                },
                CreatedDate = "30 Jul 2018 10:30:15",
                CreatedBy = "Admin 1"
            });

            return A;
        }
        public static List<ApprovalManagement.Entities.Approval> GetSalesContractApprovals()
        {
            var A = new List<ApprovalManagement.Entities.Approval>();

            var oList = new List<Operation>();

            oList.Add(new Operation()
            {
                OperationType = OperationType.LESSEROREQUAL,
                Value = 20000000.00
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "4",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "Up to 20 Million",
                    Description = "Project sales approval matrix under 20 million",
                    Operations = oList,
                }
            });

            oList = new List<Operation>();
            oList.Add(new Operation()
            {
                OperationType = OperationType.GREATEROREQUAL,
                Value = 20000000.00,
            });
            oList.Add(new Operation()
            {
                OperationType = OperationType.LESSEROREQUAL,
                Value = 50000000.00,
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "5",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "20 to 50 Million",
                    Description = "Project sales approval matrix between 20 million to 50 million",
                    Operations = oList
                }
            });

            oList = new List<Operation>();
            oList.Add(new Operation()
            {
                OperationType = OperationType.GREATER,
                Value = 50000000.00,
            });
            A.Add(new ApprovalManagement.Entities.Approval()
            {
                Id = "6",
                Module = Module.PROJECT_SALES,
                Condition = new Condition()
                {
                    Text = "More than 50 Million",
                    Description = "Project sales approval matrix more than 50 million",
                    Operations = oList
                }
            });

            return A;
        }


    }
}
