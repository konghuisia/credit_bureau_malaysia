﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class Approval
    {
        public string RoleId { get; set; }
        public string Role { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public DateTime ActionDate { get; set; }
        public string Action { get; set; }
    }
}
