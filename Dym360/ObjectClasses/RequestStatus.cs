﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses
{
    public class RequestStatus
    {
        public string Id { get; set; }
        public string RequestId { get; set; }
        public string Status { get; set; } = StringEnum.GetStringValue(MyEnum.Workflow.NEW);
        public MyEnum.Workflow CurrentStatus { get; set; } = MyEnum.Workflow.NEW;
        public MyEnum.Workflow PreviousStatus { get; set; } = MyEnum.Workflow.NEW;
    }
}
