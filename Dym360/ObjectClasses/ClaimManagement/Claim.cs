﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ClaimManagement
{
    public class Claim
    {
        public int id { get; set; }
        public string RequestId { get; set; }
        public DateTime? Period { get; set; }
    }
}
