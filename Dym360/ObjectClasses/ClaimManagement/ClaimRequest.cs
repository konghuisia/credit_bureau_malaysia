﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ClaimManagement
{
    public class ClaimRequest
    {
        public int id { get; set; }
        public string ClaimRequestId { get; set; }
        public string RequestId { get; set; }
        public string ReferenceNo { get; set; }
        public string Status { get; set; }
        public string StaffName { get; set; }
        public string StaffNumber { get; set; }
        public string Department { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public DateTime Period { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }
    }
}
