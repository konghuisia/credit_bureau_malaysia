﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectClasses.ClaimManagement
{
    public class ClaimItems
    {
        public int id { get; set; }
        public string RequestId { get; set; }
        public string ClaimId { get; set; }
        public string ClaimTypeId { get; set; }
        public decimal Mileage { get; set; }
        public decimal ClaimAmount { get; set; }
        public string AttachmentId { get; set; }
        public DateTime Date { get; set; }
        public string Remark { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public bool Deleted { get; set; }

        public string DateText { get; set; }
    }
}
