﻿function init() {
    initInputRestriction();
}

function Close() {
    //var confirm_result = confirm("Are you sure you want to quit?");
    //if (confirm_result == true) {
    //    window.close();
    //}
    $(window).on('beforeunload', function () { alert('Bye now') });
}

function initInputRestriction() {
    $('.datepicker').keypress(function () {
        return false;
    });

    $('#inptFrequencyOfUse').keypress(function (e) {
        if (this.value.length == 0 && e.which == 48) {
            return false;
        }
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() === ".") {
            $(e.target).val('');
        }
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    $("body").on("keydown", ".contact", function (e) {
        restrictIntegerInput(e);
    });

    $("body").on("keydown", ".name", function (e) {
        restrictAlphabetInput(e);
    });

    $("body").on("blur", ".name", function (e) {
        trimSpacing(e);
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#Status').val() === "Pending Form Submission") {
        if (parseInt($('#EFormStage').val()) > 0) {
            if ($('#inptPurposeOfUse').val() === '0') {
                isValid = false;
                if ($('#inptPurposeOfUse').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select purpose of use to proceed</p>').insertAfter($('#inptPurposeOfUse'));
                }
            }
            else {
                if ($('#inptPurposeOfUse').next().length > 0) {
                    $('#inptPurposeOfUse').next().remove();
                }
            }

            if ($('#inptFrequencyOfUse').val() === '') {
                isValid = false;
                if ($('#inptFrequencyOfUse').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter frequency of use to proceed</p>').insertAfter($('#inptFrequencyOfUse'));
                }
            }
            else {
                if ($('#inptFrequencyOfUse').next().length > 0) {
                    $('#inptFrequencyOfUse').next().remove();
                }

                if ($('#inptFrequencyOfUse').val() === '0' || $('#inptFrequencyOfUse').val() === '00' || $('#inptFrequencyOfUse').val() === '000') {
                    isValid = false;
                    if ($('#inptFrequencyOfUse').next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please enter a valid frequency of use to proceed</p>').insertAfter($('#inptFrequencyOfUse'));
                    }
                }
            }

            if ($('#inptBusinessRegDate').val() === '') {
                isValid = false;
                if ($('#inptBusinessRegDate').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter business registration date to proceed</p>').insertAfter($('#inptBusinessRegDate'));
                }
            }
            else {
                if ($('#inptBusinessRegDate').next().length > 0) {
                    $('#inptBusinessRegDate').next().remove();
                }
            }

            if ($('#inptLegalConstitution').val() === '0') {
                isValid = false;
                if ($('#inptLegalConstitution').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select legal constitution to proceed</p>').insertAfter($('#inptLegalConstitution'));
                }
            }
            else {
                if ($('#inptLegalConstitution').next().length > 0) {
                    $('#inptLegalConstitution').next().remove();
                }
            }

            if ($('#inptCountryOfRegistration').val() === '0') {
                isValid = false;
                if ($('#inptCountryOfRegistration').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select country of registration to proceed</p>').insertAfter($('#inptCountryOfRegistration'));
                }
            }
            else {
                if ($('#inptCountryOfRegistration').next().length > 0) {
                    $('#inptCountryOfRegistration').next().remove();
                }
            }

            if ($('#inptEconomySector').val() === '0') {
                isValid = false;
                if ($('#inptEconomySector').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select economy sector to proceed</p>').insertAfter($('#inptEconomySector'));
                }
            }
            else {
                if ($('#inptEconomySector').next().length > 0) {
                    $('#inptEconomySector').next().remove();
                }
            }

            if ($('#inptBusinessAddress').val() === "") {
                isValid = false;
                if ($('#inptBusinessAddress').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter business address to proceed</p>').insertAfter($('#inptBusinessAddress'));
                }
            }
            else {
                if ($('#inptBusinessAddress').next().length > 0) {
                    $('#inptBusinessAddress').next().remove();
                }
            }

            if ($('#inptPostalCode').val() === "") {
                isValid = false;
                if ($('#inptPostalCode').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter postal code to proceed</p>').insertAfter($('#inptPostalCode'));
                }
            }
            else {
                if ($('#inptPostalCode').next().length > 0) {
                    $('#inptPostalCode').next().remove();
                }

                if ($('#inptPostalCode').val().length < 5) {
                    isValid = false;
                    if ($('#inptPostalCode').next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please enter a valid postal code to proceed</p>').insertAfter($('#inptPostalCode'));
                    }
                }
            }

            if ($('#inptTelephoneNo').val() === "") {
                isValid = false;
                if ($('#inptTelephoneNo').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter telephone number to proceed</p>').insertAfter($('#inptTelephoneNo'));
                }
            }
            else {
                if ($('#inptTelephoneNo').next().length > 0) {
                    $('#inptTelephoneNo').next().remove();
                }
            }

            if ($('#inptEmailAddress').val() === "") {
                isValid = false;
                if ($('#inptEmailAddress').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter email address to proceed</p>').insertAfter($('#inptEmailAddress'));
                }
            }
            else {
                if ($('#inptEmailAddress').next().length > 0) {
                    $('#inptEmailAddress').next().remove();
                }
            }

            if ($('#inptSA tbody tr').length === 0) {
                isValid = false;
                if ($('#inptSA').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter security admin information to proceed</p>').insertAfter($('#inptSA'));
                }
            }
            else {
                if ($('#inptSA').next().length > 0) {
                    $('#inptSA').next().remove();
                }
            }

            if ($('#inptDIC tbody tr').length === 0) {
                isValid = false;
                if ($('#inptDIC').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter director information to proceed</p>').insertAfter($('#inptDIC'));
                }
            }
            else {
                if ($('#inptDIC').next().length > 0) {
                    $('#inptDIC').next().remove();
                }
            }
        }

        if (parseInt($('#EFormStage').val()) > 1) {
            //KYC DECLARATION
            if ($('#KYC_SubscriberDeclarationName').val() === "") {
                isValid = false;
                if ($('#KYC_SubscriberDeclarationName').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#KYC_SubscriberDeclarationName'));
                }
            }
            else {
                if ($('#KYC_SubscriberDeclarationName').next().length > 0) {
                    $('#KYC_SubscriberDeclarationName').next().remove();
                }
            }

            if ($('#KYC_SubscriberDeclarationIC').val() === "") {
                isValid = false;
                if ($('#KYC_SubscriberDeclarationIC').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter I/C no to proceed</p>').insertAfter($('#KYC_SubscriberDeclarationIC'));
                }
            }
            else {
                if ($('#KYC_SubscriberDeclarationIC').next().length > 0) {
                    $('#KYC_SubscriberDeclarationIC').next().remove();
                }

                if ($('#KYC_SubscriberDeclarationIC').val().length < 12) {
                    if ($('#KYC_SubscriberDeclarationIC').next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please enter a valid I/C no to proceed</p>').insertAfter($('#KYC_SubscriberDeclarationIC'));
                    }
                }
            }

            if ($('#DirectorCB').is(':not(:checked)') && $('#AuthorisedOfficerTextCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#AuthorisedOfficerTextCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select designation to proceed</p>').insertAfter($('#AuthorisedOfficerTextCB').next());
                }
            }
            else {
                if ($('#AuthorisedOfficerTextCB').next().next().length > 0) {
                    $('#AuthorisedOfficerTextCB').next().next().remove();
                }
            }

            if ($('#AuthorisedOfficerTextCB').is(':checked') && $('#AuthorisedOfficerText').val() === "") {
                isValid = false;
                if ($('#AuthorisedOfficerText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter authorised officer designation to proceed</p>').insertAfter($('#AuthorisedOfficerText'));
                }
            }
            else {
                if ($('#AuthorisedOfficerText').next().length > 0) {
                    $('#AuthorisedOfficerText').next().remove();
                }
            }

            if ($('#WholesalerCB').is(':not(:checked)') && $('#DistributorCB').is(':not(:checked)') && $('#RetailerCB').is(':not(:checked)') && $('#ManufacturerCB').is(':not(:checked)') && $('#ComOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#ComOthersCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select company type to proceed</p>').insertAfter($('#ComOthersCB').next());
                }
            }
            else {
                if ($('#ComOthersCB').next().next().length > 0) {
                    $('#ComOthersCB').next().next().remove();
                }
            }

            if ($('#ComOthersCB').is(':checked') && $('#ComOthersText').val() === "") {
                isValid = false;
                if ($('#ComOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter company type to proceed</p>').insertAfter($('#ComOthersText'));
                }
            }
            else {
                if ($('#ComOthersText').next().length > 0) {
                    $('#ComOthersText').next().remove();
                }
            }

            if ($('#AdvisoryCB').is(':not(:checked)') && $('#ServicesCB').is(':not(:checked)') && $('#ProvisionCB').is(':not(:checked)') && $('#ProOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#ProductServicesTypeSub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select product & services type to proceed</p>').insertAfter($('#ProductServicesTypeSub'));
                }
            }
            else {
                if ($('#ProductServicesTypeSub').next().length > 0) {
                    $('#ProductServicesTypeSub').next().remove();
                }
            }

            if ($('#ServicesCB').is(':checked') && $('#LegalCB').is(':not(:checked)') && $('#AccountingCB').is(':not(:checked)') && $('#AuditingCB').is(':not(:checked)') && $('#TaxationCB').is(':not(:checked)') && $('#EngineeringCB').is(':not(:checked)') && $('#SoftwareImplementationCB').is(':not(:checked)') && $('#DataProcessingCB').is(':not(:checked)') && $('#RealEstateCB').is(':not(:checked)') && $('#AdvertisingCB').is(':not(:checked)') && $('#TransportationCB').is(':not(:checked)') && $('#AgricultureCB').is(':not(:checked)') && $('#CommunicationCB').is(':not(:checked)') && $('#EducationCB').is(':not(:checked)') && $('#InsuranceRelatedCB').is(':not(:checked)') && $('#BankingAndFinanceCB').is(':not(:checked)') && $('#HealthRelatedCB').is(':not(:checked)') && $('#OtherServicesCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#OtherServicesCB').parent().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select services type to proceed</p>').insertAfter($('#OtherServicesCB').parent());
                }
            }
            else {
                if ($('#OtherServicesCB').parent().next().length > 0) {
                    $('#OtherServicesCB').parent().next().remove();
                }
            }

            if ($('#AdvisoryCB').is(':checked') && $('#AdvisoryText').val() === "") {
                isValid = false;
                if ($('#AdvisoryText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter product & services type to proceed</p>').insertAfter($('#AdvisoryText'));
                }
            }
            else {
                if ($('#AdvisoryText').next().length > 0) {
                    $('#AdvisoryText').next().remove();
                }
            }

            if ($('#OtherServicesCB').is(':checked') && $('#ServicesText').val() === "") {
                isValid = false;
                if ($('#ServicesText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter product & services type to proceed</p>').insertAfter($('#ServicesText'));
                }
            }
            else {
                if ($('#ServicesText').next().length > 0) {
                    $('#ServicesText').next().remove();
                }
            }

            if ($('#ProvisionCB').is(':checked') && $('#ProvisionText').val() === "") {
                isValid = false;
                if ($('#ProvisionText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter product & services type to proceed</p>').insertAfter($('#ProvisionText'));
                }
            }
            else {
                if ($('#ProvisionText').next().length > 0) {
                    $('#ProvisionText').next().remove();
                }
            }

            if ($('#ProOthersCB').is(':checked') && $('#proOthersText').val() === "") {
                isValid = false;
                if ($('#proOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter product & services type to proceed</p>').insertAfter($('#proOthersText'));
                }
            }
            else {
                if ($('#proOthersText').next().length > 0) {
                    $('#proOthersText').next().remove();
                }
            }

            if ($('#KYC_TargetedClientMarketWholesalerCB').is(':not(:checked)') && $('#KYC_TargetedClientMarketDistributorCB').is(':not(:checked)') && $('#KYC_TargetedClientMarketRetailerCB').is(':not(:checked)') && $('#KYC_TargetedClientMarketManufacturerCB').is(':not(:checked)') && $('#KYC_TargetedClientMarketOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#KYC_TargetedClientMarketOthersCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select targeted clients & market type to proceed</p>').insertAfter($('#KYC_TargetedClientMarketOthersCB').next());
                }
            }
            else {
                if ($('#KYC_TargetedClientMarketOthersCB').next().next().length > 0) {
                    $('#KYC_TargetedClientMarketOthersCB').next().next().remove();
                }
            }

            if ($('#KYC_TargetedClientMarketOthersCB').is(':checked') && $('#KYC_TargetedClientMarketOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_TargetedClientMarketOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter targeted clients & market to proceed</p>').insertAfter($('#KYC_TargetedClientMarketOthersText'));
                }
            }
            else {
                if ($('#KYC_TargetedClientMarketOthersText').next().length > 0) {
                    $('#KYC_TargetedClientMarketOthersText').next().remove();
                }
            }

            if ($('#CreditTermCB').is(':not(:checked)') && $('#CashTermCB').is(':not(:checked)') && $('#BothCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#BothCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select deal on to proceed</p>').insertAfter($('#BothCB').next());
                }
            }
            else {
                if ($('#BothCB').next().next().length > 0) {
                    $('#BothCB').next().next().remove();
                }
            }

            if ($('#BothCB').is(':checked')) {
                if ($('#CreditTermPerc').val() === "") {
                    isValid = false;
                    if ($('#CreditTermPerc').next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please enter credit term & to proceed</p>').insertAfter($('#CreditTermPerc'));
                    }
                }
                else {
                    $('#CreditTermPerc').next().remove();
                }
                if ($('#CashTermPerc').val() === "") {
                    isValid = false;
                    if ($('#CashTermPerc').next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please enter cash term & to proceed</p>').insertAfter($('#CashTermPerc'));
                    }
                }
                else {
                    $('#CashTermPerc').next().remove();
                }
            }
            else {
                if ($('#CreditTermPerc').next().length > 0) {
                    $('#CreditTermPerc').next().remove();
                }
                if ($('#CashTermPerc').next().length > 0) {
                    $('#CashTermPerc').next().remove();
                }
            }

            if ($('#Purpose').val() === "") {
                isValid = false;
                if ($('#Purpose').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter purpose of subscribing to proceed</p>').insertAfter($('#Purpose'));
                }
            }
            else {
                if ($('#Purpose').next().length > 0) {
                    $('#Purpose').next().remove();
                }
            }

            if ($('#LoanApplication_FinancingFacilityCB').is(':not(:checked)') && $('#PeriodicReviewOfLoanCB').is(':not(:checked)') && $('#OpeningAnAccountCB').is(':not(:checked)') && $('#InvestmentCB').is(':not(:checked)') && $('#InsuranceCB').is(':not(:checked)') && $('#FinancialGuranteeCB').is(':not(:checked)') && $('#EmploymentCB').is(':not(:checked)') && $('#HirePurchaseCB').is(':not(:checked)') && $('#CrimeFraudDetectionAndPreventionCB').is(':not(:checked)') && $('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').is(':not(:checked)') && $('#RentalContractCB').is(':not(:checked)') && $('#DebtRecoveryCB').is(':not(:checked)') && $('#KYC_PurposeOfSubscriptionOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').parent().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select purpose of extracting credit report to proceed</p>').insertAfter($('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').parent());
                }
            }
            else {
                if ($('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').parent().next().length > 0) {
                    $('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').parent().next().remove();
                }
            }

            if ($('#KYC_PurposeOfSubscriptionOthersCB').is(':checked') && $('#KYC_PurposeOfSubscriptionOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_PurposeOfSubscriptionOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter purpose of extracting credit report to proceed</p>').insertAfter($('#KYC_PurposeOfSubscriptionOthersText'));
                }
            }
            else {
                if ($('#KYC_PurposeOfSubscriptionOthersText').next().length > 0) {
                    $('#KYC_PurposeOfSubscriptionOthersText').next().remove();
                }
            }

            if ($('#KYC_PurposeOfSubscriptionNoCB').is(':not(:checked)') && $('#TransferCreditReportToThirdPartyCB').is(':not(:checked)') && $('#KYC_PurposeOfSubscriptionYesOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#KYC_PurposeOfSubscriptionNoCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select to proceed</p>').insertAfter($('#KYC_PurposeOfSubscriptionNoCB').next());
                }
            }
            else {
                if ($('#KYC_PurposeOfSubscriptionNoCB').next().next().length > 0) {
                    $('#KYC_PurposeOfSubscriptionNoCB').next().next().remove();
                }
            }

            if ($('#KYC_PurposeOfSubscriptionYesOthersCB').is(':checked') && $('#KYC_PurposeOfSubscriptionYesOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_PurposeOfSubscriptionYesOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter to proceed</p>').insertAfter($('#KYC_PurposeOfSubscriptionYesOthersText'));
                }
            }
            else {
                if ($('#KYC_PurposeOfSubscriptionYesOthersText').next().length > 0) {
                    $('#KYC_PurposeOfSubscriptionYesOthersText').next().remove();
                }
            }

            if ($('#DataUserYesCB').is(':not(:checked)') && $('#DataUserNoCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#DataUserNoCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select are you a data user required to be registered under the Personal Data Protection Act (PDPA) 2010 to proceed</p>').insertAfter($('#DataUserNoCB').next());
                }
            }
            else {
                if ($('#DataUserNoCB').next().next().length > 0) {
                    $('#DataUserNoCB').next().next().remove();
                }
            }

            if ($('#AnswerYesCB').is(':not(:checked)') && $('#AnswerNoCB').is(':not(:checked)') && $('#DataUserYesCB').is(':checked')) {
                isValid = false;
                if ($('#AnswerNoCB').next().next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select if the answer to the above is yes, do you provide Notice under Section 7 PDPA 2010 to the data subject to proceed</p>').insertAfter($('#AnswerNoCB').next());
                }
            }
            else {
                if ($('#AnswerNoCB').next().next().length > 0) {
                    $('#AnswerNoCB').next().next().remove();
                }
            }

            if ($('#ExplainToDataSubjectCB').is(':not(:checked)') && $('#ObtainIcCopyCB').is(':not(:checked)') && $('#ObtainDataSubjectConsentCB').is(':not(:checked)') && $('#BeforeCreditInfoOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#beforedatasubjectsub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select what do you do before you retrieve credit information of a data subject to proceed</p>').insertAfter($('#beforedatasubjectsub'));
                }
            }
            else {
                if ($('#beforedatasubjectsub').next().length > 0) {
                    $('#beforedatasubjectsub').next().remove();
                }
            }

            if ($('#BeforeCreditInfoOthersCB').is(':checked') && $('#KYC_CompanyPolicyBeforeCreditInfoOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_CompanyPolicyBeforeCreditInfoOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter what do you do before you retrieve credit information of a data subject to proceed</p>').insertAfter($('#KYC_CompanyPolicyBeforeCreditInfoOthersText'));
                }
            }
            else {
                if ($('#KYC_CompanyPolicyBeforeCreditInfoOthersText').next().length > 0) {
                    $('#KYC_CompanyPolicyBeforeCreditInfoOthersText').next().remove();
                }
            }

            if ($('#ProvideReportToThirdPartyCB').is(':not(:checked)') && $('#ProvideReportToDataSubjectCB').is(':not(:checked)') && $('#AfterCreditInfoOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#afterdatasubjectsub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select what do you do after you retrieve credit information of a data subject to proceed</p>').insertAfter($('#afterdatasubjectsub'));
                }
            }
            else {
                if ($('#afterdatasubjectsub').next().length > 0) {
                    $('#afterdatasubjectsub').next().remove();
                }
            }

            if ($('#AfterCreditInfoOthersCB').is(':checked') && $('#KYC_CompanyPolicyAfterCreditInfoOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_CompanyPolicyAfterCreditInfoOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter what do you do after you retrieve credit information of a data subject to proceed</p>').insertAfter($('#KYC_CompanyPolicyAfterCreditInfoOthersText'));
                }
            }
            else {
                if ($('#KYC_CompanyPolicyAfterCreditInfoOthersText').next().length > 0) {
                    $('#KYC_CompanyPolicyAfterCreditInfoOthersText').next().remove();
                }
            }

            if ($('#InstinctCB').is(':not(:checked)') && $('#OweMoneyCB').is(':not(:checked)') && $('#BadReputationCB').is(':not(:checked)') && $('#CreditReviewCB').is(':not(:checked)') && $('#KYC_ConsentProcurementOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#determinesub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select how do you determine whether there is a need to retrieve credit information of a data subject to proceed</p>').insertAfter($('#determinesub'));
                }
            }
            else {
                if ($('#determinesub').next().length > 0) {
                    $('#determinesub').next().remove();
                }
            }

            if ($('#KYC_ConsentProcurementOthersCB').is(':checked') && $('#KYC_CompanyPolicyAfterCreditInfoOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_ConsentProcurementOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter how do you determine whether there is a need to retrieve credit information of a data subject to proceed</p>').insertAfter($('#KYC_ConsentProcurementOthersText'));
                }
            }
            else {
                if ($('#KYC_ConsentProcurementOthersText').next().length > 0) {
                    $('#KYC_ConsentProcurementOthersText').next().remove();
                }
            }

            if ($('#PoliciesAndProcedures').val() === "") {
                isValid = false;
                if ($('#PoliciesAndProcedures').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter the policies and procedures you have implemented for procurement of consent from data subject to proceed</p>').insertAfter($('#PoliciesAndProcedures'));
                }
            }
            else {
                if ($('#PoliciesAndProcedures').next().length > 0) {
                    $('#PoliciesAndProcedures').next().remove();
                }
            }

            if ($('#StoreCreditReportInElectronicDeviceCB').is(':not(:checked)') && $('#PrintCreditReportForFillingCB').is(':not(:checked)') && $('#ProvideDataSubjectHisReportCB').is(':not(:checked)') && $('#KYC_ProtectionOfDataAfterOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#aftercreditreportsub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select what do you do after retrieving credit information from CBM to proceed</p>').insertAfter($('#aftercreditreportsub'));
                }
            }
            else {
                if ($('#aftercreditreportsub').next().length > 0) {
                    $('#aftercreditreportsub').next().remove();
                }
            }

            if ($('#KYC_ProtectionOfDataAfterOthersCB').is(':checked') && $('#KYC_ProtectionOfDataAfterOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataAfterOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter what do you do after retrieving credit information from CBM to proceed</p>').insertAfter($('#KYC_ProtectionOfDataAfterOthersText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataAfterOthersText').next().length > 0) {
                    $('#KYC_ProtectionOfDataAfterOthersText').next().remove();
                }
            }

            if ($('#UsingMachineShredderCB').is(':not(:checked)') && $('#AccessToOfficeComputerIsEncryptedCB').is(':not(:checked)') && $('#AccessToCBMSystemIsLimitedCB').is(':not(:checked)') && $('#SharingOrDisplayingPasswordCB').is(':not(:checked)') && $('#UsingFirewallCB').is(':not(:checked)') && $('#UsingAntivirusCB').is(':not(:checked)') && $('#EnsureOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#ensuresub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select how do you ensure confidentiality of data and credit information obtained from CBM to proceed</p>').insertAfter($('#ensuresub'));
                }
            }
            else {
                if ($('#ensuresub').next().length > 0) {
                    $('#ensuresub').next().remove();
                }
            }

            if ($('#UsingFirewallCB').is(':checked') && $('#KYC_ProtectionOfDataFirewallText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataFirewallText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter firewall name to proceed</p>').insertAfter($('#KYC_ProtectionOfDataFirewallText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataFirewallText').next().length > 0) {
                    $('#KYC_ProtectionOfDataFirewallText').next().remove();
                }
            }

            if ($('#UsingAntivirusCB').is(':checked') && $('#KYC_ProtectionOfDataAntivirusText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataAntivirusText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter antivirus name to proceed</p>').insertAfter($('#KYC_ProtectionOfDataAntivirusText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataAntivirusText').next().length > 0) {
                    $('#KYC_ProtectionOfDataAntivirusText').next().remove();
                }
            }

            if ($('#EnsureOthersCB').is(':checked') && $('#KYC_ProtectionOfDataEnsureOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataEnsureOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter how do you ensure confidentiality of data and credit information obtained from CBM to proceed</p>').insertAfter($('#KYC_ProtectionOfDataEnsureOthersText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataEnsureOthersText').next().length > 0) {
                    $('#KYC_ProtectionOfDataEnsureOthersText').next().remove();
                }
            }

            if ($('#ParentCompanyCB').is(':not(:checked)') && $('#SubsidiaryCompanyCB').is(':not(:checked)') && $('#AllEmployeeCB').is(':not(:checked)') && $('#ThirdPartyCB').is(':not(:checked)') && $('#Director_CEO_ManagementCB').is(':not(:checked)') && $('#People_CompanyOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#accesssub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select who are the people / company that have access to report of a data subject to proceed</p>').insertAfter($('#accesssub'));
                }
            }
            else {
                if ($('#accesssub').next().length > 0) {
                    $('#accesssub').next().remove();
                }
            }

            if ($('#People_CompanyOthersCB').is(':checked') && $('#KYC_ProtectionOfDataPeople_CompanyOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataPeople_CompanyOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter who are the people / company that have access to report of a data subject to proceed</p>').insertAfter($('#KYC_ProtectionOfDataPeople_CompanyOthersText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataPeople_CompanyOthersText').next().length > 0) {
                    $('#KYC_ProtectionOfDataPeople_CompanyOthersText').next().remove();
                }
            }

            if ($('#RetainDataYesCB').is(':not(:checked)') && $('#RetainDataNoCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#retainsub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select would you retain data and credit information obtained from CBM to proceed</p>').insertAfter($('#retainsub'));
                }
            }
            else {
                if ($('#retainsub').next().length > 0) {
                    $('#retainsub').next().remove();
                }
            }

            if ($('#RetainDataYesCB').is(':checked') && $('#KYC_ProtectionOfDataRetainDataYesText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataRetainDataYesText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter how long would you retain data to proceed</p>').insertAfter($('#KYC_ProtectionOfDataRetainDataYesText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataRetainDataYesText').next().length > 0) {
                    $('#KYC_ProtectionOfDataRetainDataYesText').next().remove();
                }
            }

            if ($('#ElectronicCopyCB').is(':not(:checked)') && $('#PhysicalFillingCB').is(':not(:checked)') && $('#CloudSystemCB').is(':not(:checked)') && $('#InternetStorageProviderCB').is(':not(:checked)') && $('#PersonalDatabaseCB').is(':not(:checked)') && $('#KYC_ProtectionOfDataStoreOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#storesub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select how do you store report of a data subject obtained from CBM to proceed</p>').insertAfter($('#storesub'));
                }
            }
            else {
                if ($('#storesub').next().length > 0) {
                    $('#storesub').next().remove();
                }
            }

            if ($('#KYC_ProtectionOfDataStoreOthersCB').is(':checked') && $('#KYC_ProtectionOfDataRetainDataYesText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataStoreOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter how do you store report of a data subject obtained from CBM to proceed</p>').insertAfter($('#KYC_ProtectionOfDataStoreOthersText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataStoreOthersText').next().length > 0) {
                    $('#KYC_ProtectionOfDataStoreOthersText').next().remove();
                }
            }

            if ($('#CreditInformationOnlyForInternalUsageCB').is(':not(:checked)') && $('#OnlyForSecurityAdministratorCB').is(':not(:checked)') && $('#ReportWillNotBePrintedCB').is(':not(:checked)') && $('#ReportWillNotBeRetainedCB').is(':not(:checked)') && $('#KYC_ProtectionOfDataControlOthersCB').is(':not(:checked)')) {
                isValid = false;
                if ($('#controlsub').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select how do you control/monitor access to data, credit information and report obtained from CBM to proceed</p>').insertAfter($('#controlsub'));
                }
            }
            else {
                if ($('#controlsub').next().length > 0) {
                    $('#controlsub').next().remove();
                }
            }

            if ($('#KYC_ProtectionOfDataControlOthersCB').is(':checked') && $('#KYC_ProtectionOfDataControlOthersText').val() === "") {
                isValid = false;
                if ($('#KYC_ProtectionOfDataControlOthersText').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter how do you control/monitor access to data, credit information and report obtained from CBM to proceed</p>').insertAfter($('#KYC_ProtectionOfDataControlOthersText'));
                }
            }
            else {
                if ($('#KYC_ProtectionOfDataControlOthersText').next().length > 0) {
                    $('#KYC_ProtectionOfDataControlOthersText').next().remove();
                }
            }

            //END KYC Declaration
        }

        if (parseInt($('#EFormStage').val()) > 2) {
            //Consent Form
            if ($('#CFUpperName').val() === "") {
                isValid = false;
                if ($('#CFUpperName').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#CFUpperName'));
                }
            }
            else {
                if ($('#CFUpperName').next().length > 0) {
                    $('#CFUpperName').next().remove();
                }
            }

            if ($('#CFUpperNRIC_Passport').val() === "") {
                isValid = false;
                if ($('#CFUpperNRIC_Passport').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter nric/passport no to proceed</p>').insertAfter($('#CFUpperNRIC_Passport'));
                }
            }
            else {
                if ($('#CFUpperNRIC_Passport').next().length > 0) {
                    $('#CFUpperNRIC_Passport').next().remove();
                }
            }

            if ($('#CFUpperDesignation').val() === "") {
                isValid = false;
                if ($('#CFUpperDesignation').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#CFUpperDesignation'));
                }
            }
            else {
                if ($('#CFUpperDesignation').next().length > 0) {
                    $('#CFUpperDesignation').next().remove();
                }
            }

            if ($('#ConsentFormPersonLowerTable tbody tr').length === 0) {
                isValid = false;
                if ($('#ConsentFormPersonLowerTable').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter to proceed</p>').insertAfter($('#ConsentFormPersonLowerTable'));
                }
            }
            else {
                if ($('#ConsentFormPersonLowerTable').next().length > 0) {
                    $('#ConsentFormPersonLowerTable').next().remove();
                }
            }
            //END Consent Form
        }

        if (parseInt($('#EFormStage').val()) > 3) {
            //E-Consent Checklist
            if ($('#ECCConsent').val() === '') {
                isValid = false;
                if ($('#ECCConsent').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter response to proceed</p>').insertAfter($('#ECCConsent'));
                }
            }
            else {
                if ($('#ECCConsent').next().length > 0) {
                    $('#ECCConsent').next().remove();
                }
            }

            if ($('#ECCIdentityVerification').val() === '') {
                isValid = false;
                if ($('#ECCIdentityVerification').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter response to proceed</p>').insertAfter($('#ECCIdentityVerification'));
                }
            }
            else {
                if ($('#ECCIdentityVerification').next().length > 0) {
                    $('#ECCIdentityVerification').next().remove();
                }
            }

            if ($('#ECCElectronicSignature').val() === '') {
                isValid = false;
                if ($('#ECCElectronicSignature').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter response to proceed</p>').insertAfter($('#ECCElectronicSignature'));
                }
            }
            else {
                if ($('#ECCElectronicSignature').next().length > 0) {
                    $('#ECCElectronicSignature').next().remove();
                }
            }

            if ($('#ECCDocumentMustBeOriginal').val() === '') {
                isValid = false;
                if ($('#ECCDocumentMustBeOriginal').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter response to proceed</p>').insertAfter($('#ECCDocumentMustBeOriginal'));
                }
            }
            else {
                if ($('#ECCDocumentMustBeOriginal').next().length > 0) {
                    $('#ECCDocumentMustBeOriginal').next().remove();
                }
            }

            if ($('#ECCRetentionOfDocument').val() === '') {
                isValid = false;
                if ($('#ECCRetentionOfDocument').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter response to proceed</p>').insertAfter($('#ECCRetentionOfDocument'));
                }
            }
            else {
                if ($('#ECCRetentionOfDocument').next().length > 0) {
                    $('#ECCRetentionOfDocument').next().remove();
                }
            }
            //End E-Consent Checklist
        }

        if (parseInt($('#EFormStage').val()) > 4) {
            //General Code of conduct
            if ($('#GCOCName').val() === '') {
                isValid = false;
                if ($('#GCOCName').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#GCOCName'));
                }
            }
            else {
                if ($('#GCOCName').next().length > 0) {
                    $('#GCOCName').next().remove();
                }
            }

            if ($('#GCOCNRIC').val() === '') {
                isValid = false;
                if ($('#GCOCNRIC').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter nric to proceed</p>').insertAfter($('#GCOCNRIC'));
                }
            }
            else {
                if ($('#GCOCNRIC').next().length > 0) {
                    $('#GCOCNRIC').next().remove();
                }
            }

            if ($('#GCOCDesignation').val() === '') {
                isValid = false;
                if ($('#GCOCDesignation').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#GCOCDesignation'));
                }
            }
            else {
                if ($('#GCOCDesignation').next().length > 0) {
                    $('#GCOCDesignation').next().remove();
                }
            }
            //End General Code of conduct
        }

        if (parseInt($('#EFormStage').val()) > 5) {
            var SAHaventConcent = 0;

            if ($('#LOUConsented').is(':not(:checked)')) {
                isValid = false;
                if ($('#ErrorMsgForLOUConsented').next().length < 1) {
                    $('<p class="invalid-alert" style="color:red;">Please select to proceed</p>').insertAfter($('#ErrorMsgForLOUConsented'));
                }
            }
            else {
                if ($('#ErrorMsgForLOUConsented').next().length > 0) {
                    $('#ErrorMsgForLOUConsented').next().remove();
                }
            }

            $('#inptSA tbody tr').each(function () {
                var Consent = $(this).find("td:last").html();

                if (Consent.search('data-consented="True"') < 1) {
                    isValid = false;
                    SAHaventConcent++;
                    if ($(this).find("td:first a").next().length < 1) {
                        $('<p class="invalid-alert" style="color:red;">Please give consent to proceed</p>').insertAfter($(this).find("td:first a"));
                    }
                }
                else {
                    if ($(this).find("td:first a").next().length > 0) {
                        $(this).find("td:first a").next().remove();
                    }
                }
            });
            
            if (SAHaventConcent > 0) {
                $('#btnErrorBox').click();
            }
        }
    }

    if ($('#Status').val() === "Pending Payment Slip") {
        if ($('#inptPaymentSlipAttachment tbody tr').length === 0) {
            isValid = false;
            if ($('#inptPaymentSlipAttachment').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please attach payment slips to proceed</p>').insertAfter($('#inptPaymentSlipAttachment'));
            }
        }
        else {
            if ($('#inptPaymentSlipAttachment').next().length > 0) {
                $('#inptPaymentSlipAttachment').next().remove();
            }
        }
    }
    
    return isValid;
}

function Submit() {
    if (inputValidation()) {
        
        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var Request = {
            "RequestId": $('#RequestId').val(),
            "Subject": $('#inptSubject').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
        };

        var SubRequest = {
            "RequestId": $('#RequestId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val()
        };

        var CustomerDetails = {
            "BusinessRegDate": $('#inptBusinessRegDate').val(),
            "LegalConstitution": $('#inptLegalConstitution').val(),
            "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
            "EconomySector": $('#inptEconomySector').val(),
            "EconomySubSector": $('#inptEconomySubSector').val(),
            "BusinessAddress": $('#inptBusinessAddress').val(),
            "PostalCode": $('#inptPostalCode').val(),
            "TelephoneNo": $('#inptTelephoneNo').val(),
            "FaxNo": $('#inptFaxNo').val(),
            "EmailAddress": $('#inptEmailAddress').val(),
            "Website": $('#inptWebsite').val()
        };

        var KYC_SubscriberDeclaration = {
            'Name': $('#KYC_SubscriberDeclarationName').val(),
            'IC': $('#KYC_SubscriberDeclarationIC').val(),
            'Director': $('#DirectorCB').is(':checked'),
            'AuthorisedOfficer': $('#AuthorisedOfficerTextCB').is(':checked'),
            'AuthorisedOfficerText': $('#AuthorisedOfficerText').val()
        };

        var KYC_CompanyType = {
            'Wholesaler': $('#WholesalerCB').is(':checked'),
            'Distributor': $('#DistributorCB').is(':checked'),
            'Retailer': $('#RetailerCB').is(':checked'),
            'Manufacturer': $('#ManufacturerCB').is(':checked'),
            'Others': $('#ComOthersCB').is(':checked'),
            'OthersText': $('#ComOthersText').val()
        };

        var KYC_ProductService = {
            'Advisory': $('#AdvisoryCB').is(':checked'),
            'AdvisoryText': $('#AdvisoryText').val(),
            'Services': $('#ServicesCB').is(':checked'),
            'Legal': $('#LegalCB').is(':checked'),
            'Accounting': $('#AccountingCB').is(':checked'),
            'Auditing': $('#AuditingCB').is(':checked'),
            'Taxation': $('#TaxationCB').is(':checked'),
            'Engineering': $('#EngineeringCB').is(':checked'),
            'SoftwareImplementation': $('#SoftwareImplementationCB').is(':checked'),
            'DataProcessing': $('#DataProcessingCB').is(':checked'),
            'RealEstate': $('#RealEstateCB').is(':checked'),
            'Advertising': $('#AdvertisingCB').is(':checked'),
            'Transportation': $('#TransportationCB').is(':checked'),
            'Agriculture': $('#AgricultureCB').is(':checked'),
            'Communication': $('#CommunicationCB').is(':checked'),
            'Education': $('#EducationCB').is(':checked'),
            'InsuranceRelated': $('#InsuranceRelatedCB').is(':checked'),
            'BankingAndFinance': $('#BankingAndFinanceCB').is(':checked'),
            'HealthRelated': $('#HealthRelatedCB').is(':checked'),
            'OtherServices': $('#OtherServicesCB').is(':checked'),
            'ServicesText': $('#ServicesText').val(),
            'Provision': $('#ProvisionCB').is(':checked'),
            'ProvisionText': $('#ProvisionText').val(),
            'Others': $('#ProOthersCB').is(':checked'),
            'OthersText': $('#proOthersText').val(),
        };

        var KYC_TargetedClientMarket = {
            'Wholesaler': $('#KYC_TargetedClientMarketWholesalerCB').is(':checked'),
            'Distributor': $('#KYC_TargetedClientMarketDistributorCB').is(':checked'),
            'Retailer': $('#KYC_TargetedClientMarketRetailerCB').is(':checked'),
            'Manufacturer': $('#KYC_TargetedClientMarketManufacturerCB').is(':checked'),
            'Others': $('#KYC_TargetedClientMarketOthersCB').is(':checked'),
            'OthersText': $('#KYC_TargetedClientMarketOthersText').val(),
        };

        var KYC_DealOn = {
            'CreditTerm': $('#CreditTermCB').is(':checked'),
            'CashTerm': $('#CashTermCB').is(':checked'),
            'Both': $('#BothCB').is(':checked'),
            'CreditTermPerc': $('#CreditTermPerc').val(),
            'CashTermPerc': $('#CashTermPerc').val(),
        };

        var KYC_PurposeOfSubscription = {
            'Purpose': $('#Purpose').val(),
            'LoanApplication_FinancingFacility': $('#LoanApplication_FinancingFacilityCB').is(':checked'),
            'PeriodicReviewOfLoan': $('#PeriodicReviewOfLoanCB').is(':checked'),
            'OpeningAnAccount': $('#OpeningAnAccountCB').is(':checked'),
            'Investment': $('#InvestmentCB').is(':checked'),
            'Insurance': $('#InsuranceCB').is(':checked'),
            'FinancialGurantee': $('#FinancialGuranteeCB').is(':checked'),
            'Employment': $('#EmploymentCB').is(':checked'),
            'HirePurchase': $('#HirePurchaseCB').is(':checked'),
            'CrimeFraudDetectionAndPrevention': $('#CrimeFraudDetectionAndPreventionCB').is(':checked'),
            'InvestigationByAuditFirm_ComplianceUnits_LawFirms': $('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').is(':checked'),
            'RentalContract': $('#RentalContractCB').is(':checked'),
            'DebtRecovery': $('#DebtRecoveryCB').is(':checked'),
            'Others': $('#KYC_PurposeOfSubscriptionOthersCB').is(':checked'),
            'OthersText': $('#KYC_PurposeOfSubscriptionOthersText').val(),
            'No': $('#KYC_PurposeOfSubscriptionNoCB').is(':checked'),
            'TransferCreditReportToThirdParty': $('#TransferCreditReportToThirdPartyCB').is(':checked'),
            'YesOthers': $('#KYC_PurposeOfSubscriptionYesOthersCB').is(':checked'),
            'YesOthersText': $('#KYC_PurposeOfSubscriptionYesOthersText').val(),
        };

        var KYC_CompanyPolicy = {
            'DataUserYes': $('#DataUserYesCB').is(':checked'),
            'DataUserNo': $('#DataUserNoCB').is(':checked'),
            'AnswerYes': $('#AnswerYesCB').is(':checked'),
            'AnswerNo': $('#AnswerNoCB').is(':checked'),
            'ExplainToDataSubject': $('#ExplainToDataSubjectCB').is(':checked'),
            'ObtainIcCopy': $('#ObtainIcCopyCB').is(':checked'),
            'ObtainDataSubjectConsent': $('#ObtainDataSubjectConsentCB').is(':checked'),
            'BeforeCreditInfoOthers': $('#BeforeCreditInfoOthersCB').is(':checked'),
            'BeforeCreditInfoOthersText': $('#KYC_CompanyPolicyBeforeCreditInfoOthersText').val(),
            'ProvideReportToThirdParty': $('#ProvideReportToThirdPartyCB').is(':checked'),
            'ProvideReportToDataSubject': $('#ProvideReportToDataSubjectCB').is(':checked'),
            'AfterCreditInfoOthers': $('#AfterCreditInfoOthersCB').is(':checked'),
            'AfterCreditInfoOthersText': $('#KYC_CompanyPolicyAfterCreditInfoOthersText').val(),
        };

        var KYC_ConsentProcurement = {
            'Instinct': $('#InstinctCB').is(':checked'),
            'OweMoney': $('#OweMoneyCB').is(':checked'),
            'BadReputation': $('#BadReputationCB').is(':checked'),
            'CreditReview': $('#CreditReviewCB').is(':checked'),
            'Others': $('#KYC_ConsentProcurementOthersCB').is(':checked'),
            'OthersText': $('#KYC_ConsentProcurementOthersText').val(),
            'PoliciesAndProcedures': $('#PoliciesAndProcedures').val(),
        };

        var KYC_ProtectionOfData = {
            'StoreCreditReportInElectronicDevice': $('#StoreCreditReportInElectronicDeviceCB').is(':checked'),
            'PrintCreditReportForFilling': $('#PrintCreditReportForFillingCB').is(':checked'),
            'ProvideDataSubjectHisReport': $('#ProvideDataSubjectHisReportCB').is(':checked'),
            'AfterOthers': $('#KYC_ProtectionOfDataAfterOthersCB').is(':checked'),
            'AfterOthersText': $('#KYC_ProtectionOfDataAfterOthersText').val(),
            'UsingMachineShredder': $('#UsingMachineShredderCB').is(':checked'),
            'AccessToOfficeComputerIsEncrypted': $('#AccessToOfficeComputerIsEncryptedCB').is(':checked'),
            'AccessToCBMSystemIsLimited': $('#AccessToCBMSystemIsLimitedCB').is(':checked'),
            'SharingOrDisplayingPassword': $('#SharingOrDisplayingPasswordCB').is(':checked'),
            'UsingFirewall': $('#UsingFirewallCB').is(':checked'),
            'FirewallText': $('#KYC_ProtectionOfDataFirewallText').val(),
            'UsingAntivirus': $('#UsingAntivirusCB').is(':checked'),
            'AntivirusText': $('#KYC_ProtectionOfDataAntivirusText').val(),
            'EnsureOthers': $('#EnsureOthersCB').is(':checked'),
            'EnsureOthersText': $('#KYC_ProtectionOfDataEnsureOthersText').val(),
            'ParentCompany': $('#ParentCompanyCB').is(':checked'),
            'SubsidiaryCompany': $('#SubsidiaryCompanyCB').is(':checked'),
            'AllEmployee': $('#AllEmployeeCB').is(':checked'),
            'ThirdParty': $('#ThirdPartyCB').is(':checked'),
            'Director_CEO_Management': $('#Director_CEO_ManagementCB').is(':checked'),
            'People_CompanyOthers': $('#People_CompanyOthersCB').is(':checked'),
            'People_CompanyOthersText': $('#KYC_ProtectionOfDataPeople_CompanyOthersText').val(),
            'RetainDataYes': $('#RetainDataYesCB').is(':checked'),
            'RetainDataYesText': $('#KYC_ProtectionOfDataRetainDataYesText').val(),
            'RetainDataNo': $('#RetainDataNoCB').is(':checked'),
            'ElectronicCopy': $('#ElectronicCopyCB').is(':checked'),
            'PhysicalFilling': $('#PhysicalFillingCB').is(':checked'),
            'CloudSystem': $('#CloudSystemCB').is(':checked'),
            'InternetStorageProvider': $('#InternetStorageProviderCB').is(':checked'),
            'PersonalDatabase': $('#PersonalDatabaseCB').is(':checked'),
            'StoreOthers': $('#KYC_ProtectionOfDataStoreOthersCB').is(':checked'),
            'StoreOthersText': $('#KYC_ProtectionOfDataStoreOthersText').val(),
            'CreditInformationOnlyForInternalUsage': $('#CreditInformationOnlyForInternalUsageCB').is(':checked'),
            'OnlyForSecurityAdministrator': $('#OnlyForSecurityAdministratorCB').is(':checked'),
            'ReportWillNotBePrinted': $('#ReportWillNotBePrintedCB').is(':checked'),
            'ReportWillNotBeRetained': $('#ReportWillNotBeRetainedCB').is(':checked'),
            'ControlOthers': $('#KYC_ProtectionOfDataControlOthersCB').is(':checked'),
            'ControlOthersText': $('#KYC_ProtectionOfDataControlOthersText').val()
        };

        var KYC_Declaration = {
            "KYC_SubscriberDeclaration": KYC_SubscriberDeclaration,
            "KYC_CompanyType": KYC_CompanyType,
            "KYC_ProductService": KYC_ProductService,
            "KYC_TargetedClientMarket": KYC_TargetedClientMarket,
            "KYC_DealOn": KYC_DealOn,
            "KYC_PurposeOfSubscription": KYC_PurposeOfSubscription,
            "KYC_CompanyPolicy": KYC_CompanyPolicy,
            "KYC_ConsentProcurement": KYC_ConsentProcurement,
            "KYC_ProtectionOfData": KYC_ProtectionOfData
        };

        var ConsentFormPeoplesUpper = {
            'Name': $('#CFUpperName').val(),
            'NRIC_Passport': $('#CFUpperNRIC_Passport').val(),
            'Designation': $('#CFUpperDesignation').val(),
            'Type': 'Upper',
            'IsSubmit': true,
            "RequestId": $('#RequestId').val()
        };

        var EConsentChecklist = {
            'RequestId': $('#RequestId').val(),
            'Consent': $('#ECCConsent').val(),
            'IdentityVerification': $('#ECCIdentityVerification').val(),
            'ElectronicSignature': $('#ECCElectronicSignature').val(),
            'DocumentMustBeOriginal': $('#ECCDocumentMustBeOriginal').val(),
            'RetentionOfDocument': $('#ECCRetentionOfDocument').val(),
        };

        var GeneralCodeOfConduct = {
            'Name': $('#GCOCName').val(),
            'NRIC': $('#GCOCNRIC').val(),
            'Designation': $('#GCOCDesignation').val(),
            'IsSubmit': true,
            "RequestId": $('#RequestId').val()
        };

        var LetterOfUndertaking = {
            'Consented': $('#LOUConsented').is(':checked'),
            "RequestId": $('#RequestId').val(),
            'IsSubmit': true,
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/CustomerSubmitRequest",
            data: { "req": Request, "subReq": SubRequest, "cusDet": CustomerDetails, 'KYC_Declaration': KYC_Declaration, 'ConsentFormPeoplesUpper': ConsentFormPeoplesUpper, 'EConsentChecklist': EConsentChecklist, 'GeneralCodeOfConduct': GeneralCodeOfConduct, 'LetterOfUndertaking': LetterOfUndertaking },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function SaveDraft() {

    if ($('#CanSubmit').val() === "true") {
        $('#CanSubmit').val('false');
    }
    else {
        return;
    }

    var Request = {
        "RequestId": $('#RequestId').val()
    };

    var SubRequest = {
        "RequestId": $('#RequestId').val(),
        "PurposeOfUse": $('#inptPurposeOfUse').val(),
        "FrequencyOfUse": $('#inptFrequencyOfUse').val()
    };

    var CustomerDetails = {
        "BusinessRegDate": $('#inptBusinessRegDate').val(),
        "LegalConstitution": $('#inptLegalConstitution').val(),
        "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
        "EconomySector": $('#inptEconomySector').val(),
        "EconomySubSector": $('#inptEconomySubSector').val(),
        "BusinessAddress": $('#inptBusinessAddress').val(),
        "PostalCode": $('#inptPostalCode').val(),
        "TelephoneNo": $('#inptTelephoneNo').val(),
        "FaxNo": $('#inptFaxNo').val(),
        "EmailAddress": $('#inptEmailAddress').val(),
        "Website": $('#inptWebsite').val()
    };

    var KYC_SubscriberDeclaration = {
        'Name': $('#KYC_SubscriberDeclarationName').val(),
        'IC': $('#KYC_SubscriberDeclarationIC').val(),
        'Director': $('#DirectorCB').is(':checked'),
        'AuthorisedOfficer': $('#AuthorisedOfficerTextCB').is(':checked'),
        'AuthorisedOfficerText': $('#AuthorisedOfficerText').val()
    };

    var KYC_CompanyType = {
        'Wholesaler': $('#WholesalerCB').is(':checked'),
        'Distributor': $('#DistributorCB').is(':checked'),
        'Retailer': $('#RetailerCB').is(':checked'),
        'Manufacturer': $('#ManufacturerCB').is(':checked'),
        'Others': $('#ComOthersCB').is(':checked'),
        'OthersText': $('#ComOthersText').val()
    };

    var KYC_ProductService = {
        'Advisory': $('#AdvisoryCB').is(':checked'),
        'AdvisoryText': $('#AdvisoryText').val(),
        'Services': $('#ServicesCB').is(':checked'),
        'Legal': $('#LegalCB').is(':checked'),
        'Accounting': $('#AccountingCB').is(':checked'),
        'Auditing': $('#AuditingCB').is(':checked'),
        'Taxation': $('#TaxationCB').is(':checked'),
        'Engineering': $('#EngineeringCB').is(':checked'),
        'SoftwareImplementation': $('#SoftwareImplementationCB').is(':checked'),
        'DataProcessing': $('#DataProcessingCB').is(':checked'),
        'RealEstate': $('#RealEstateCB').is(':checked'),
        'Advertising': $('#AdvertisingCB').is(':checked'),
        'Transportation': $('#TransportationCB').is(':checked'),
        'Agriculture': $('#AgricultureCB').is(':checked'),
        'Communication': $('#CommunicationCB').is(':checked'),
        'Education': $('#EducationCB').is(':checked'),
        'InsuranceRelated': $('#InsuranceRelatedCB').is(':checked'),
        'BankingAndFinance': $('#BankingAndFinanceCB').is(':checked'),
        'HealthRelated': $('#HealthRelatedCB').is(':checked'),
        'OtherServices': $('#OtherServicesCB').is(':checked'),
        'ServicesText': $('#ServicesText').val(),
        'Provision': $('#ProvisionCB').is(':checked'),
        'ProvisionText': $('#ProvisionText').val(),
        'Others': $('#ProOthersCB').is(':checked'),
        'OthersText': $('#proOthersText').val(),
    };

    var KYC_TargetedClientMarket = {
        'Wholesaler': $('#KYC_TargetedClientMarketWholesalerCB').is(':checked'),
        'Distributor': $('#KYC_TargetedClientMarketDistributorCB').is(':checked'),
        'Retailer': $('#KYC_TargetedClientMarketRetailerCB').is(':checked'),
        'Manufacturer': $('#KYC_TargetedClientMarketManufacturerCB').is(':checked'),
        'Others': $('#KYC_TargetedClientMarketOthersCB').is(':checked'),
        'OthersText': $('#KYC_TargetedClientMarketOthersText').val(),
    };

    var KYC_DealOn = {
        'CreditTerm': $('#CreditTermCB').is(':checked'),
        'CashTerm': $('#CashTermCB').is(':checked'),
        'Both': $('#BothCB').is(':checked'),
        'CreditTermPerc': $('#CreditTermPerc').val(),
        'CashTermPerc': $('#CashTermPerc').val(),
    };

    var KYC_PurposeOfSubscription = {
        'Purpose': $('#Purpose').val(),
        'LoanApplication_FinancingFacility': $('#LoanApplication_FinancingFacilityCB').is(':checked'),
        'PeriodicReviewOfLoan': $('#PeriodicReviewOfLoanCB').is(':checked'),
        'OpeningAnAccount': $('#OpeningAnAccountCB').is(':checked'),
        'Investment': $('#InvestmentCB').is(':checked'),
        'Insurance': $('#InsuranceCB').is(':checked'),
        'FinancialGurantee': $('#FinancialGuranteeCB').is(':checked'),
        'Employment': $('#EmploymentCB').is(':checked'),
        'HirePurchase': $('#HirePurchaseCB').is(':checked'),
        'CrimeFraudDetectionAndPrevention': $('#CrimeFraudDetectionAndPreventionCB').is(':checked'),
        'InvestigationByAuditFirm_ComplianceUnits_LawFirms': $('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').is(':checked'),
        'RentalContract': $('#RentalContractCB').is(':checked'),
        'DebtRecovery': $('#DebtRecoveryCB').is(':checked'),
        'Others': $('#KYC_PurposeOfSubscriptionOthersCB').is(':checked'),
        'OthersText': $('#KYC_PurposeOfSubscriptionOthersText').val(),
        'No': $('#KYC_PurposeOfSubscriptionNoCB').is(':checked'),
        'TransferCreditReportToThirdParty': $('#TransferCreditReportToThirdPartyCB').is(':checked'),
        'YesOthers': $('#KYC_PurposeOfSubscriptionYesOthersCB').is(':checked'),
        'YesOthersText': $('#KYC_PurposeOfSubscriptionYesOthersText').val(),
    };

    var KYC_CompanyPolicy = {
        'DataUserYes': $('#DataUserYesCB').is(':checked'),
        'DataUserNo': $('#DataUserNoCB').is(':checked'),
        'AnswerYes': $('#AnswerYesCB').is(':checked'),
        'AnswerNo': $('#AnswerNoCB').is(':checked'),
        'ExplainToDataSubject': $('#ExplainToDataSubjectCB').is(':checked'),
        'ObtainIcCopy': $('#ObtainIcCopyCB').is(':checked'),
        'ObtainDataSubjectConsent': $('#ObtainDataSubjectConsentCB').is(':checked'),
        'BeforeCreditInfoOthers': $('#BeforeCreditInfoOthersCB').is(':checked'),
        'BeforeCreditInfoOthersText': $('#KYC_CompanyPolicyBeforeCreditInfoOthersText').val(),
        'ProvideReportToThirdParty': $('#ProvideReportToThirdPartyCB').is(':checked'),
        'ProvideReportToDataSubject': $('#ProvideReportToDataSubjectCB').is(':checked'),
        'AfterCreditInfoOthers': $('#AfterCreditInfoOthersCB').is(':checked'),
        'AfterCreditInfoOthersText': $('#KYC_CompanyPolicyAfterCreditInfoOthersText').val(),
    };

    var KYC_ConsentProcurement = {
        'Instinct': $('#InstinctCB').is(':checked'),
        'OweMoney': $('#OweMoneyCB').is(':checked'),
        'BadReputation': $('#BadReputationCB').is(':checked'),
        'CreditReview': $('#CreditReviewCB').is(':checked'),
        'Others': $('#KYC_ConsentProcurementOthersCB').is(':checked'),
        'OthersText': $('#KYC_ConsentProcurementOthersText').val(),
        'PoliciesAndProcedures': $('#PoliciesAndProcedures').val(),
    };

    var KYC_ProtectionOfData = {
        'StoreCreditReportInElectronicDevice': $('#StoreCreditReportInElectronicDeviceCB').is(':checked'),
        'PrintCreditReportForFilling': $('#PrintCreditReportForFillingCB').is(':checked'),
        'ProvideDataSubjectHisReport': $('#ProvideDataSubjectHisReportCB').is(':checked'),
        'AfterOthers': $('#KYC_ProtectionOfDataAfterOthersCB').is(':checked'),
        'AfterOthersText': $('#KYC_ProtectionOfDataAfterOthersText').val(),
        'UsingMachineShredder': $('#UsingMachineShredderCB').is(':checked'),
        'AccessToOfficeComputerIsEncrypted': $('#AccessToOfficeComputerIsEncryptedCB').is(':checked'),
        'AccessToCBMSystemIsLimited': $('#AccessToCBMSystemIsLimitedCB').is(':checked'),
        'SharingOrDisplayingPassword': $('#SharingOrDisplayingPasswordCB').is(':checked'),
        'UsingFirewall': $('#UsingFirewallCB').is(':checked'),
        'FirewallText': $('#KYC_ProtectionOfDataFirewallText').val(),
        'UsingAntivirus': $('#UsingAntivirusCB').is(':checked'),
        'AntivirusText': $('#KYC_ProtectionOfDataAntivirusText').val(),
        'EnsureOthers': $('#EnsureOthersCB').is(':checked'),
        'EnsureOthersText': $('#KYC_ProtectionOfDataEnsureOthersText').val(),
        'ParentCompany': $('#ParentCompanyCB').is(':checked'),
        'SubsidiaryCompany': $('#SubsidiaryCompanyCB').is(':checked'),
        'AllEmployee': $('#AllEmployeeCB').is(':checked'),
        'ThirdParty': $('#ThirdPartyCB').is(':checked'),
        'Director_CEO_Management': $('#Director_CEO_ManagementCB').is(':checked'),
        'People_CompanyOthers': $('#People_CompanyOthersCB').is(':checked'),
        'People_CompanyOthersText': $('#KYC_ProtectionOfDataPeople_CompanyOthersText').val(),
        'RetainDataYes': $('#RetainDataYesCB').is(':checked'),
        'RetainDataYesText': $('#KYC_ProtectionOfDataRetainDataYesText').val(),
        'RetainDataNo': $('#RetainDataNoCB').is(':checked'),
        'ElectronicCopy': $('#ElectronicCopyCB').is(':checked'),
        'PhysicalFilling': $('#PhysicalFillingCB').is(':checked'),
        'CloudSystem': $('#CloudSystemCB').is(':checked'),
        'InternetStorageProvider': $('#InternetStorageProviderCB').is(':checked'),
        'PersonalDatabase': $('#PersonalDatabaseCB').is(':checked'),
        'StoreOthers': $('#KYC_ProtectionOfDataStoreOthersCB').is(':checked'),
        'StoreOthersText': $('#KYC_ProtectionOfDataStoreOthersText').val(),
        'CreditInformationOnlyForInternalUsage': $('#CreditInformationOnlyForInternalUsageCB').is(':checked'),
        'OnlyForSecurityAdministrator': $('#OnlyForSecurityAdministratorCB').is(':checked'),
        'ReportWillNotBePrinted': $('#ReportWillNotBePrintedCB').is(':checked'),
        'ReportWillNotBeRetained': $('#ReportWillNotBeRetainedCB').is(':checked'),
        'ControlOthers': $('#KYC_ProtectionOfDataControlOthersCB').is(':checked'),
        'ControlOthersText': $('#KYC_ProtectionOfDataControlOthersText').val()
    };

    var KYC_Declaration = {
        "KYC_SubscriberDeclaration": KYC_SubscriberDeclaration,
        "KYC_CompanyType": KYC_CompanyType,
        "KYC_ProductService": KYC_ProductService,
        "KYC_TargetedClientMarket": KYC_TargetedClientMarket,
        "KYC_DealOn": KYC_DealOn,
        "KYC_PurposeOfSubscription": KYC_PurposeOfSubscription,
        "KYC_CompanyPolicy": KYC_CompanyPolicy,
        "KYC_ConsentProcurement": KYC_ConsentProcurement,
        "KYC_ProtectionOfData": KYC_ProtectionOfData
    };

    var ConsentFormPeoplesUpper = {
        'Name': $('#CFUpperName').val(),
        'NRIC_Passport': $('#CFUpperNRIC_Passport').val(),
        'Designation': $('#CFUpperDesignation').val(),
        'Type': 'Upper',
        "RequestId": $('#RequestId').val()
    };

    var EConsentChecklist = {
        'RequestId': $('#RequestId').val(),
        'Consent': $('#ECCConsent').val(),
        'IdentityVerification': $('#ECCIdentityVerification').val(),
        'ElectronicSignature': $('#ECCElectronicSignature').val(),
        'DocumentMustBeOriginal': $('#ECCDocumentMustBeOriginal').val(),
        'RetentionOfDocument': $('#ECCRetentionOfDocument').val(),
    };

    var GeneralCodeOfConduct = {
        'Name': $('#GCOCName').val(),
        'NRIC': $('#GCOCNRIC').val(),
        'Designation': $('#GCOCDesignation').val(),
        "RequestId": $('#RequestId').val()
    };

    var LetterOfUndertaking = {
        'Consented': $('#LOUConsented').is(':checked'),
        "RequestId": $('#RequestId').val()
    };

    $.ajax({
        url: window.location.origin + "/SubscriptionManagement/Home/CustomerSaveDraft",
        data: { "req": Request, "subReq": SubRequest, "cusDet": CustomerDetails, 'KYC_Declaration': KYC_Declaration, 'ConsentFormPeoplesUpper': ConsentFormPeoplesUpper, 'EConsentChecklist': EConsentChecklist, 'GeneralCodeOfConduct': GeneralCodeOfConduct, 'LetterOfUndertaking': LetterOfUndertaking },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                $('#CanSubmit').val('true');
                showToast(d.Error, "error");
            }
        }
    });
}

function Continue() {
    if (inputValidation()) {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var Request = {
            "RequestId": $('#RequestId').val(),
            "Subject": $('#inptSubject').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
        };

        var SubRequest = {
            "RequestId": $('#RequestId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val()
        };

        var CustomerDetails = {
            "BusinessRegDate": $('#inptBusinessRegDate').val(),
            "LegalConstitution": $('#inptLegalConstitution').val(),
            "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
            "EconomySector": $('#inptEconomySector').val(),
            "EconomySubSector": $('#inptEconomySubSector').val(),
            "BusinessAddress": $('#inptBusinessAddress').val(),
            "PostalCode": $('#inptPostalCode').val(),
            "TelephoneNo": $('#inptTelephoneNo').val(),
            "FaxNo": $('#inptFaxNo').val(),
            "EmailAddress": $('#inptEmailAddress').val(),
            "Website": $('#inptWebsite').val()
        };

        var KYC_SubscriberDeclaration = {
            'Name': $('#KYC_SubscriberDeclarationName').val(),
            'IC': $('#KYC_SubscriberDeclarationIC').val(),
            'Director': $('#DirectorCB').is(':checked'),
            'AuthorisedOfficer': $('#AuthorisedOfficerTextCB').is(':checked'),
            'AuthorisedOfficerText': $('#AuthorisedOfficerText').val()
        };

        var KYC_CompanyType = {
            'Wholesaler': $('#WholesalerCB').is(':checked'),
            'Distributor': $('#DistributorCB').is(':checked'),
            'Retailer': $('#RetailerCB').is(':checked'),
            'Manufacturer': $('#ManufacturerCB').is(':checked'),
            'Others': $('#ComOthersCB').is(':checked'),
            'OthersText': $('#ComOthersText').val()
        };

        var KYC_ProductService = {
            'Advisory': $('#AdvisoryCB').is(':checked'),
            'AdvisoryText': $('#AdvisoryText').val(),
            'Services': $('#ServicesCB').is(':checked'),
            'Legal': $('#LegalCB').is(':checked'),
            'Accounting': $('#AccountingCB').is(':checked'),
            'Auditing': $('#AuditingCB').is(':checked'),
            'Taxation': $('#TaxationCB').is(':checked'),
            'Engineering': $('#EngineeringCB').is(':checked'),
            'SoftwareImplementation': $('#SoftwareImplementationCB').is(':checked'),
            'DataProcessing': $('#DataProcessingCB').is(':checked'),
            'RealEstate': $('#RealEstateCB').is(':checked'),
            'Advertising': $('#AdvertisingCB').is(':checked'),
            'Transportation': $('#TransportationCB').is(':checked'),
            'Agriculture': $('#AgricultureCB').is(':checked'),
            'Communication': $('#CommunicationCB').is(':checked'),
            'Education': $('#EducationCB').is(':checked'),
            'InsuranceRelated': $('#InsuranceRelatedCB').is(':checked'),
            'BankingAndFinance': $('#BankingAndFinanceCB').is(':checked'),
            'HealthRelated': $('#HealthRelatedCB').is(':checked'),
            'OtherServices': $('#OtherServicesCB').is(':checked'),
            'ServicesText': $('#ServicesText').val(),
            'Provision': $('#ProvisionCB').is(':checked'),
            'ProvisionText': $('#ProvisionText').val(),
            'Others': $('#ProOthersCB').is(':checked'),
            'OthersText': $('#proOthersText').val(),
        };

        var KYC_TargetedClientMarket = {
            'Wholesaler': $('#KYC_TargetedClientMarketWholesalerCB').is(':checked'),
            'Distributor': $('#KYC_TargetedClientMarketDistributorCB').is(':checked'),
            'Retailer': $('#KYC_TargetedClientMarketRetailerCB').is(':checked'),
            'Manufacturer': $('#KYC_TargetedClientMarketManufacturerCB').is(':checked'),
            'Others': $('#KYC_TargetedClientMarketOthersCB').is(':checked'),
            'OthersText': $('#KYC_TargetedClientMarketOthersText').val(),
        };

        var KYC_DealOn = {
            'CreditTerm': $('#CreditTermCB').is(':checked'),
            'CashTerm': $('#CashTermCB').is(':checked'),
            'Both': $('#BothCB').is(':checked'),
            'CreditTermPerc': $('#CreditTermPerc').val(),
            'CashTermPerc': $('#CashTermPerc').val(),
        };

        var KYC_PurposeOfSubscription = {
            'Purpose': $('#Purpose').val(),
            'LoanApplication_FinancingFacility': $('#LoanApplication_FinancingFacilityCB').is(':checked'),
            'PeriodicReviewOfLoan': $('#PeriodicReviewOfLoanCB').is(':checked'),
            'OpeningAnAccount': $('#OpeningAnAccountCB').is(':checked'),
            'Investment': $('#InvestmentCB').is(':checked'),
            'Insurance': $('#InsuranceCB').is(':checked'),
            'FinancialGurantee': $('#FinancialGuranteeCB').is(':checked'),
            'Employment': $('#EmploymentCB').is(':checked'),
            'HirePurchase': $('#HirePurchaseCB').is(':checked'),
            'CrimeFraudDetectionAndPrevention': $('#CrimeFraudDetectionAndPreventionCB').is(':checked'),
            'InvestigationByAuditFirm_ComplianceUnits_LawFirms': $('#InvestigationByAuditFirm_ComplianceUnits_LawFirmsCB').is(':checked'),
            'RentalContract': $('#RentalContractCB').is(':checked'),
            'DebtRecovery': $('#DebtRecoveryCB').is(':checked'),
            'Others': $('#KYC_PurposeOfSubscriptionOthersCB').is(':checked'),
            'OthersText': $('#KYC_PurposeOfSubscriptionOthersText').val(),
            'No': $('#KYC_PurposeOfSubscriptionNoCB').is(':checked'),
            'TransferCreditReportToThirdParty': $('#TransferCreditReportToThirdPartyCB').is(':checked'),
            'YesOthers': $('#KYC_PurposeOfSubscriptionYesOthersCB').is(':checked'),
            'YesOthersText': $('#KYC_PurposeOfSubscriptionYesOthersText').val(),
        };

        var KYC_CompanyPolicy = {
            'DataUserYes': $('#DataUserYesCB').is(':checked'),
            'DataUserNo': $('#DataUserNoCB').is(':checked'),
            'AnswerYes': $('#AnswerYesCB').is(':checked'),
            'AnswerNo': $('#AnswerNoCB').is(':checked'),
            'ExplainToDataSubject': $('#ExplainToDataSubjectCB').is(':checked'),
            'ObtainIcCopy': $('#ObtainIcCopyCB').is(':checked'),
            'ObtainDataSubjectConsent': $('#ObtainDataSubjectConsentCB').is(':checked'),
            'BeforeCreditInfoOthers': $('#BeforeCreditInfoOthersCB').is(':checked'),
            'BeforeCreditInfoOthersText': $('#KYC_CompanyPolicyBeforeCreditInfoOthersText').val(),
            'ProvideReportToThirdParty': $('#ProvideReportToThirdPartyCB').is(':checked'),
            'ProvideReportToDataSubject': $('#ProvideReportToDataSubjectCB').is(':checked'),
            'AfterCreditInfoOthers': $('#AfterCreditInfoOthersCB').is(':checked'),
            'AfterCreditInfoOthersText': $('#KYC_CompanyPolicyAfterCreditInfoOthersText').val(),
        };

        var KYC_ConsentProcurement = {
            'Instinct': $('#InstinctCB').is(':checked'),
            'OweMoney': $('#OweMoneyCB').is(':checked'),
            'BadReputation': $('#BadReputationCB').is(':checked'),
            'CreditReview': $('#CreditReviewCB').is(':checked'),
            'Others': $('#KYC_ConsentProcurementOthersCB').is(':checked'),
            'OthersText': $('#KYC_ConsentProcurementOthersText').val(),
            'PoliciesAndProcedures': $('#PoliciesAndProcedures').val(),
        };

        var KYC_ProtectionOfData = {
            'StoreCreditReportInElectronicDevice': $('#StoreCreditReportInElectronicDeviceCB').is(':checked'),
            'PrintCreditReportForFilling': $('#PrintCreditReportForFillingCB').is(':checked'),
            'ProvideDataSubjectHisReport': $('#ProvideDataSubjectHisReportCB').is(':checked'),
            'AfterOthers': $('#KYC_ProtectionOfDataAfterOthersCB').is(':checked'),
            'AfterOthersText': $('#KYC_ProtectionOfDataAfterOthersText').val(),
            'UsingMachineShredder': $('#UsingMachineShredderCB').is(':checked'),
            'AccessToOfficeComputerIsEncrypted': $('#AccessToOfficeComputerIsEncryptedCB').is(':checked'),
            'AccessToCBMSystemIsLimited': $('#AccessToCBMSystemIsLimitedCB').is(':checked'),
            'SharingOrDisplayingPassword': $('#SharingOrDisplayingPasswordCB').is(':checked'),
            'UsingFirewall': $('#UsingFirewallCB').is(':checked'),
            'FirewallText': $('#KYC_ProtectionOfDataFirewallText').val(),
            'UsingAntivirus': $('#UsingAntivirusCB').is(':checked'),
            'AntivirusText': $('#KYC_ProtectionOfDataAntivirusText').val(),
            'EnsureOthers': $('#EnsureOthersCB').is(':checked'),
            'EnsureOthersText': $('#KYC_ProtectionOfDataEnsureOthersText').val(),
            'ParentCompany': $('#ParentCompanyCB').is(':checked'),
            'SubsidiaryCompany': $('#SubsidiaryCompanyCB').is(':checked'),
            'AllEmployee': $('#AllEmployeeCB').is(':checked'),
            'ThirdParty': $('#ThirdPartyCB').is(':checked'),
            'Director_CEO_Management': $('#Director_CEO_ManagementCB').is(':checked'),
            'People_CompanyOthers': $('#People_CompanyOthersCB').is(':checked'),
            'People_CompanyOthersText': $('#KYC_ProtectionOfDataPeople_CompanyOthersText').val(),
            'RetainDataYes': $('#RetainDataYesCB').is(':checked'),
            'RetainDataYesText': $('#KYC_ProtectionOfDataRetainDataYesText').val(),
            'RetainDataNo': $('#RetainDataNoCB').is(':checked'),
            'ElectronicCopy': $('#ElectronicCopyCB').is(':checked'),
            'PhysicalFilling': $('#PhysicalFillingCB').is(':checked'),
            'CloudSystem': $('#CloudSystemCB').is(':checked'),
            'InternetStorageProvider': $('#InternetStorageProviderCB').is(':checked'),
            'PersonalDatabase': $('#PersonalDatabaseCB').is(':checked'),
            'StoreOthers': $('#KYC_ProtectionOfDataStoreOthersCB').is(':checked'),
            'StoreOthersText': $('#KYC_ProtectionOfDataStoreOthersText').val(),
            'CreditInformationOnlyForInternalUsage': $('#CreditInformationOnlyForInternalUsageCB').is(':checked'),
            'OnlyForSecurityAdministrator': $('#OnlyForSecurityAdministratorCB').is(':checked'),
            'ReportWillNotBePrinted': $('#ReportWillNotBePrintedCB').is(':checked'),
            'ReportWillNotBeRetained': $('#ReportWillNotBeRetainedCB').is(':checked'),
            'ControlOthers': $('#KYC_ProtectionOfDataControlOthersCB').is(':checked'),
            'ControlOthersText': $('#KYC_ProtectionOfDataControlOthersText').val()
        };

        var KYC_Declaration = {
            "KYC_SubscriberDeclaration": KYC_SubscriberDeclaration,
            "KYC_CompanyType": KYC_CompanyType,
            "KYC_ProductService": KYC_ProductService,
            "KYC_TargetedClientMarket": KYC_TargetedClientMarket,
            "KYC_DealOn": KYC_DealOn,
            "KYC_PurposeOfSubscription": KYC_PurposeOfSubscription,
            "KYC_CompanyPolicy": KYC_CompanyPolicy,
            "KYC_ConsentProcurement": KYC_ConsentProcurement,
            "KYC_ProtectionOfData": KYC_ProtectionOfData
        };

        var ConsentFormPeoplesUpper = {
            'Name': $('#CFUpperName').val(),
            'NRIC_Passport': $('#CFUpperNRIC_Passport').val(),
            'Designation': $('#CFUpperDesignation').val(),
            'Type': 'Upper',
            "RequestId": $('#RequestId').val()
        };

        var EConsentChecklist = {
            'RequestId': $('#RequestId').val(),
            'Consent': $('#ECCConsent').val(),
            'IdentityVerification': $('#ECCIdentityVerification').val(),
            'ElectronicSignature': $('#ECCElectronicSignature').val(),
            'DocumentMustBeOriginal': $('#ECCDocumentMustBeOriginal').val(),
            'RetentionOfDocument': $('#ECCRetentionOfDocument').val(),
        };

        var GeneralCodeOfConduct = {
            'Name': $('#GCOCName').val(),
            'NRIC': $('#GCOCNRIC').val(),
            'Designation': $('#GCOCDesignation').val(),
            "RequestId": $('#RequestId').val()
        };

        var LetterOfUndertaking = {
            'Consented': $('#LOUConsented').is(':checked'),
            "RequestId": $('#RequestId').val()
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/CustomerContinue",
            data: { "req": Request, "subReq": SubRequest, "cusDet": CustomerDetails, 'KYC_Declaration': KYC_Declaration, 'ConsentFormPeoplesUpper': ConsentFormPeoplesUpper, 'EConsentChecklist': EConsentChecklist, 'GeneralCodeOfConduct': GeneralCodeOfConduct, 'LetterOfUndertaking': LetterOfUndertaking },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function uploadDIC() {
    var isValid = true;

    if ($('#dicName').val() === '') {
        isValid = false;
        if ($('#dicName').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#dicName'));
        }
    }
    else {
        if ($('#dicName').next().length > 0) {
            $('#dicName').next().remove();
        }
    }

    if ($('#dicTelephoneNumber').val() === '') {
        isValid = false;
        if ($('#dicTelephoneNumber').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter telephone number to proceed</p>').insertAfter($('#dicTelephoneNumber'));
        }
    }
    else {
        if ($('#dicTelephoneNumber').next().length > 0) {
            $('#dicTelephoneNumber').next().remove();
        }
    }

    if ($('#dicEmailAddress').val() === '') {
        isValid = false;
        if ($('#dicEmailAddress').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter email address to proceed</p>').insertAfter($('#dicEmailAddress'));
        }
    }
    else {
        if ($('#dicEmailAddress').next().length > 0) {
            $('#dicEmailAddress').next().remove();
        }
    }

    if ($('#dicIcNumber').val() === '') {
        isValid = false;
        if ($('#dicIcNumber').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter ic number to proceed</p>').insertAfter($('#dicIcNumber'));
        }
    }
    else {
        if ($('#dicIcNumber').next().length > 0) {
            $('#dicIcNumber').next().remove();
        }
    }

    if ($('#dicDesignation').val() === '') {
        isValid = false;
        if ($('#dicDesignation').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#dicDesignation'));
        }
    }
    else {
        if ($('#dicDesignation').next().length > 0) {
            $('#dicDesignation').next().remove();
        }
    }

    if ($('#fileSelectDicIc').val() === '') {
        isValid = false;
        if ($('#fileSelectDicIc').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please attach ic to proceed</p>').insertAfter($('#fileSelectDicIc'));
        }
    }
    else {
        if ($('#fileSelectDicIc').next().length > 0) {
            $('#fileSelectDicIc').next().remove();
        }
    }

    if (isValid) {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#RequestId').val());
        formdata.append("Name", $('#dicName').val());
        formdata.append("TelephoneNumber", $('#dicTelephoneNumber').val());
        formdata.append("EmailAddress", $('#dicEmailAddress').val());
        formdata.append("IcNumber", $('#dicIcNumber').val());
        formdata.append("Designation", $('#dicDesignation').val());
        formdata.append("IsPrimary", $("input:radio[name=dicIsPrimary]:checked").val());
        formdata.append("Type", "PIC");
        var ic = document.getElementById('fileSelectDicIc');
        var license = document.getElementById('fileSelectDicLicense');
        var payslip = document.getElementById('fileSelectDicPaySlip');
        
        for (var i = 0; i < ic.files.length; i++) {
            //Appending each file to FormData object
            formdata.append(ic.files[i].name + "1", ic.files[i]);
            formdata.append("AttachmentTypeId1", "EDDEDB1A-9192-427A-90BA-DE7546C086AA");//IC
        }

        for (var i = 0; i < license.files.length; i++) {
            //Appending each file to FormData object
            formdata.append(license.files[i].name + "2", license.files[i]);
            formdata.append("AttachmentTypeId2", "4D13748E-8036-4C8F-9911-8E7C2EA71D6C");//License
        }

        for (var i = 0; i < payslip.files.length; i++) {
            //Appending each file to FormData object
            formdata.append(payslip.files[i].name + "3", payslip.files[i]);
            formdata.append("AttachmentTypeId3", "55771298-516E-401B-A2EF-2FE3EA0A7124");//payslip
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/SubscriptionManagement/Home/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);
                var row = "";

                if ($('#inptDIC').next().length > 0) {
                    $('#inptDIC').next().remove();
                }

                var c = "No";
                if ($("input:radio[name=dicIsPrimary]:checked").val() === "true") {
                    c = "Yes";
                    $('#dic1').attr('disabled', 'disabled');
                    $('#DicPrimary').val('true');
                }

                var ic = '';
                if ($('#SelectDicIc').html() !== "Select Ic") {

                    $(d.PersonInChargeAttachments).each(function () {
                        if (this.AttachmentTypeId === "EDDEDB1A-9192-427A-90BA-DE7546C086AA" && this.PersonInChargeId == d.PersonInChargeId)//IC
                        {
                            ic = "<a href='/" + this.Path + this.Name + "' target='_blank'>" + this.Name + "</a>";
                        }
                    });
                }

                var license = '';
                if ($('#SelectDicLicense').html() !== "Select License") {
                    $(d.PersonInChargeAttachments).each(function () {
                        if (this.AttachmentTypeId === "4D13748E-8036-4C8F-9911-8E7C2EA71D6C" && this.PersonInChargeId == d.PersonInChargeId)//License
                        {
                            license = "<a href='/" + this.Path + this.Name + "' target='_blank'>" + this.Name + "</a>";
                        }
                    });
                }

                var payslip = '';
                if ($('#SelectDicPaySlip').html() !== "Select Payslip") {
                    $(d.PersonInChargeAttachments).each(function () {
                        if (this.AttachmentTypeId === "55771298-516E-401B-A2EF-2FE3EA0A7124" && this.PersonInChargeId == d.PersonInChargeId)//Payslip
                        {
                            payslip = "<a href='/" + this.Path + this.Name + "' target='_blank'>" + this.Name + "</a>";
                        }
                    });
                }

                row += '<tr>' +
                    '<td style="vertical-align:middle;">' + $('#dicName').val() + '</td>' +
                    '<td style="vertical-align:middle;">' + $('#dicTelephoneNumber').val() + '</td>' +
                    '<td style="vertical-align:middle;">' + $('#dicEmailAddress').val() + '</td> ' +
                    '<td style="vertical-align:middle;">' + $('#dicIcNumber').val() + '</td> ' +
                    '<td style="vertical-align:middle;">' + $('#dicDesignation').val() + '</td> ' +
                    '<td class="text-center" style="vertical-align:middle; word-break:break-all">' + ic + '</td>' +
                    '<td class="text-center" style="vertical-align:middle; word-break:break-all">' + license + '</td> ' +
                    '<td class="text-center" style="vertical-align:middle; word-break:break-all">' + payslip + '</td> ' +
                    '<td class="text-center" style="vertical-align:middle;">' + c + '</td> ' +
                    '<td data-value="' + d.PersonInChargeId + '" style="text-align:center; vertical-align:middle"><a class="delete" data-value="' + d.PersonInChargeId + '" data-type="PIC" data-primary="' + c + '" title="Delete" onclick="removePic(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                '</tr>';

                $('#inptDIC').append(row);
                $('#dicName').val("");
                $('#dicTelephoneNumber').val("");
                $('#dicEmailAddress').val("");
                $('#dicIcNumber').val("");
                $('#dicDesignation').val("");
                $('#fileSelectDicIc').val("");
                $('#fileSelectDicLicense').val("");
                $('#fileSelectDicPaySlip').val("");
                $('#SelectDicIc').html("Select Ic");
                $('#SelectDicLicense').html("Select License");
                $('#SelectDicPaySlip').html("Select Payslip");
                $($('#dic2')).prop('checked', true);
            }
        };
    }
}

function uploadSA() {
    var isValid = true;

    if ($('#saName').val() === '') {
        isValid = false;
        if ($('#saName').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#saName'));
        }
    }
    else {
        if ($('#saName').next().length > 0) {
            $('#saName').next().remove();
        }
    }

    if ($('#saTelephoneNumber').val() === '') {
        isValid = false;
        if ($('#saTelephoneNumber').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter telephone number to proceed</p>').insertAfter($('#saTelephoneNumber'));
        }
    }
    else {
        if ($('#saTelephoneNumber').next().length > 0) {
            $('#saTelephoneNumber').next().remove();
        }
    }

    if ($('#saEmailAddress').val() === '') {
        isValid = false;
        if ($('#saEmailAddress').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter email address to proceed</p>').insertAfter($('#saEmailAddress'));
        }
    }
    else {
        if ($('#saEmailAddress').next().length > 0) {
            $('#saEmailAddress').next().remove();
        }
    }

    if ($('#saIcNumber').val() === '') {
        isValid = false;
        if ($('#saIcNumber').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter ic number to proceed</p>').insertAfter($('#saIcNumber'));
        }
    }
    else {
        if ($('#saIcNumber').next().length > 0) {
            $('#saIcNumber').next().remove();
        }
    }

    if ($('#saDesignation').val() === '') {
        isValid = false;
        if ($('#saDesignation').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#saDesignation'));
        }
    }
    else {
        if ($('#saDesignation').next().length > 0) {
            $('#saDesignation').next().remove();
        }
    }
    
    if ($('#fileSelectSaIc').val() === '') {
        isValid = false;
        if ($('#fileSelectSaIc').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please attach ic to proceed</p>').insertAfter($('#fileSelectSaIc'));
        }
    }
    else {
        if ($('#fileSelectSaIc').next().length > 0) {
            $('#fileSelectSaIc').next().remove();
        }
    }

    if (isValid) {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#RequestId').val());
        formdata.append("Name", $('#saName').val());
        formdata.append("TelephoneNumber", $('#saTelephoneNumber').val());
        formdata.append("EmailAddress", $('#saEmailAddress').val());
        formdata.append("IcNumber", $('#saIcNumber').val());
        formdata.append("Designation", $('#saDesignation').val());
        formdata.append("IsPrimary", $("input:radio[name=saIsPrimary]:checked").val());
        formdata.append("Type", "SA");
        var ic = document.getElementById('fileSelectSaIc');

        for (var i = 0; i < ic.files.length; i++) {
            //Appending each file to FormData object
            formdata.append(ic.files[i].name + "1", ic.files[i]);
            formdata.append("AttachmentTypeId1", "EDDEDB1A-9192-427A-90BA-DE7546C086AA");//IC
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/SubscriptionManagement/Home/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);
                var row = "";

                if ($('#inptSA').next().length > 0) {
                    $('#inptSA').next().remove();
                }

                var c = "No";
                if ($("input:radio[name=saIsPrimary]:checked").val() === "true") {
                    c = "Yes";
                    $('#sa1').attr('disabled', 'disabled');
                    $('#SaPrimary').val('true');
                }

                var ic = '';
                if ($('#SelectSaIc').html() !== "Select Ic") {

                    $(d.PersonInChargeAttachments).each(function () {
                        if (this.AttachmentTypeId === "EDDEDB1A-9192-427A-90BA-DE7546C086AA" && this.PersonInChargeId == d.PersonInChargeId)//IC
                        {
                            ic = "<a href='/" + this.Path + this.Name + "' target='_blank'>" + this.Name + "</a>";
                        }
                    });
                }

                row += '<tr>' +
                    '<td style="vertical-align:middle;">' + $('#saName').val() + '<a target="_blank" href="/SubscriptionManagement/Home/SecurityAdminForm?PersonInChargeId=' + d.PersonInChargeId + '&RequestId=' + $('#RequestId').val() +'&CameFrom=Customer"><img src="/Images/share-icon.png" style="width:7%; margin-left:2%"></a></td>' +
                    '<td style="vertical-align:middle;">' + $('#saTelephoneNumber').val() + '</td>' +
                    '<td style="vertical-align:middle;">' + $('#saEmailAddress').val() + '</td> ' +
                    '<td style="vertical-align:middle;">' + $('#saIcNumber').val() + '</td> ' +
                    '<td style="vertical-align:middle;">' + $('#saDesignation').val() + '</td> ' +
                    '<td class="text-center" style="vertical-align:middle; word-break:break-all">' + ic + '</td>' +
                    '<td class="text-center" style="vertical-align:middle;">' + c + '</td> ' +
                    '<td data-consented="false" data-value="' + d.PersonInChargeId + '" style="text-align:center; vertical-align:middle"><a data-consented="false" class="delete" data-value="' + d.PersonInChargeId + '" data-type="SA" data-primary="' + c + '" title="Delete" onclick="removePic(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                    '</tr>';

                $('#inptSA').append(row);
                $('#saName').val("");
                $('#saTelephoneNumber').val("");
                $('#saEmailAddress').val("");
                $('#saIcNumber').val("");
                $('#saDesignation').val("");
                $('#fileSelectSaIc').val("");
                $('#SelectSaIc').html("Select Ic");
                $($('#sa2')).prop('checked', true);

                if ($('#inptSA tbody tr').length === 2) {
                    $('#saFoot').hide();
                }
            }
        };
    }
}

function removePic(e) {
    if (confirm("Please click OK to confirm delete.")) {

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/DeletePic",
            data: { "personInChargeId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    var message = "";
                    if ($(e).data('type') === "PIC") {
                        if ($(e).data('primary') === "Yes") {
                            $('#dic1').removeAttr('disabled');
                        }
                        message = "Director information succesfully deleted.";
                    }
                    else {
                        if ($(e).data('primary') === "Yes") {
                            $('#sa1').removeAttr('disabled');
                        }
                        $('#saFoot').show();
                        message = "Security admin information succesfully deleted.";
                    }
                    showToast(message, "success");
                    $(e).parent().parent().remove();
                }
                else {
                    var emessage = "";
                    if ($(e).data('type') === "PIC") {
                        emessage = "Unable to delete director information, please try again later.";
                    }
                    else {
                        emessage = "Unable to delete security admin information, please try again later.";
                    }
                    showToast(emessage, "error");
                }
            }
        });
    }
}

function DicIc(e) {
    if (e.files.length > 0) {
        $('#SelectDicIc').html(e.files[0].name);
        if ($('#fileSelectDicIc').next().length > 0) {
            $('#fileSelectDicIc').next().remove();
        }
    }
    else {
        $('#SelectDicIc').html("Select Ic");
    }
}

function DicLicense(e) {
    if (e.files.length > 0) {
        $('#SelectDicLicense').html(e.files[0].name);
    }
    else {
        $('#SelectDicLicense').html("Select License");
    }
}

function DicPaySlip(e) {
    if (e.files.length > 0) {
        $('#SelectDicPaySlip').html(e.files[0].name);
    }
    else {
        $('#SelectDicPaySlip').html("Select Payslip");
    }
}

function SaIc(e) {
    if (e.files.length > 0) {
        $('#SelectSaIc').html(e.files[0].name);
        if ($('#fileSelectSaIc').next().length > 0) {
            $('#fileSelectSaIc').next().remove();
        }
    }
    else {
        $('#SelectSaIc').html("Select Ic");
    }
}

function checkCreditTermCB(e) {
    if (e.checked && $('#CashTermCB').is(':checked')) {
        $('#CashTermCB').prop('checked', false);
        $('#CreditTermCB').prop('checked', false);
        $('#BothCB').prop('checked', true);
    }
    else if (e.checked && $('#BothCB').is(':checked')) {
        $('#CashTermCB').prop('checked', false);
        $('#CreditTermCB').prop('checked', true);
        $('#BothCB').prop('checked', false);
    }
}

function checkCashTermCB(e) {
    if (e.checked && $('#CreditTermCB').is(':checked')) {
        $('#CashTermCB').prop('checked', false);
        $('#CreditTermCB').prop('checked', false);
        $('#BothCB').prop('checked', true);
    }
    else if (e.checked && $('#BothCB').is(':checked')) {
        $('#CashTermCB').prop('checked', true);
        $('#CreditTermCB').prop('checked', false);
        $('#BothCB').prop('checked', false);
    }
}

function checkBothCB(e) {
    if (e.checked) {
        $('#CashTermCB').prop('checked', false);
        $('#CreditTermCB').prop('checked', false);
    }
}

function dataUser(e) {
    if (e.checked) {
        $('#' + e.dataset.opositeid).prop('checked', false);
    }
}

function answer(e) {
    if (e.checked) {
        $('#' + e.dataset.opositeid).prop('checked', false);
    }
}

function uploadConsentFormLowerPerson() {
    var isValid = true;

    if ($('#CFLowerName').val() === "") {
        isValid = false;
        if ($('#CFLowerName').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#CFLowerName'));
        }
    }
    else {
        if ($('#CFLowerName').next().length > 0) {
            $('#CFLowerName').next().remove();
        }
    }

    if ($('#CFLowerNRIC_Passport').val() === "") {
        isValid = false;
        if ($('#CFLowerNRIC_Passport').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter nric/passport no to proceed</p>').insertAfter($('#CFLowerNRIC_Passport'));
        }
    }
    else {
        if ($('#CFLowerNRIC_Passport').next().length > 0) {
            $('#CFLowerNRIC_Passport').next().remove();
        }
    }

    if ($('#CFLowerDesignation').val() === "") {
        isValid = false;
        if ($('#CFLowerDesignation').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#CFLowerDesignation'));
        }
    }
    else {
        if ($('#CFLowerDesignation').next().length > 0) {
            $('#CFLowerDesignation').next().remove();
        }
    }

    if (isValid) {
        var ConsentFormPeople = {
            'Name': $('#CFLowerName').val(),
            'NRIC_Passport': $('#CFLowerNRIC_Passport').val(),
            'Designation': $('#CFLowerDesignation').val(),
            'Type': 'Lower',
            "RequestId": $('#RequestId').val()
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/CreateConsentFormLowerPeople",
            data: { 'ConsentFormPeople': ConsentFormPeople },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d !== null) {
                    

                    var row = '<tr>' +
                        '<td style="vertical-align:middle;">' + $('#CFLowerName').val() + '</td>' +
                        '<td style="vertical-align:middle;">' + $('#CFLowerNRIC_Passport').val() + '</td>' +
                        '<td style="vertical-align:middle;">' + $('#CFLowerDesignation').val() + '</td> ' +
                        '<td data-value="' + d + '" style="text-align:center; vertical-align:middle"><a class="delete" data-value="' + d + '" data-type="Lower" title="Delete" onclick="removeConsentFormLowerPerson(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                        '</tr>';

                    $('#ConsentFormPersonLowerTable').append(row);
                    $('#CFLowerName').val("");
                    $('#CFLowerNRIC_Passport').val("");
                    $('#CFLowerDesignation').val("");
                }
            }
        });
    }
}

function removeConsentFormLowerPerson(e) {
    if (confirm("Please click OK to confirm delete.")) {

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/DeleteConsentFormLowerPeople",
            data: { "ConsentFormPeopleId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d) {
                    showToast("Successfully removed.", "success");
                    $(e).parent().parent().remove();
                }
                else {
                    showToast("Unable to remove. Please try again later.", "error");
                }
            }
        });
    }
}

function uploadDoc(type) {
    var formdata = new FormData(); //FormData object
    formdata.append("RequestId", $('#RequestId').val());
    var fileInput;
    if (type === "PaymentSlipAttachment") {
        formdata.append("AttachmentType", "E550248B-0BF1-468E-8890-EF97206B8D72");
        formdata.append("LastModifiedDate", $('#LastModifiedDate').val());
        fileInput = document.getElementById('fileSelectPaymentSlipAttachment');
    }

    //Iterating through each files selected in fileInput
    for (var i = 0; i < fileInput.files.length; i++) {
        //Appending each file to FormData object
        formdata.append(fileInput.files[i].name, fileInput.files[i]);
    }

    //Creating an XMLHttpRequest and sending
    var xhr = new XMLHttpRequest();
    xhr.open('POST', window.location.origin + '/SubscriptionManagement/Home/UploadAttachment');
    xhr.send(formdata);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var d = JSON.parse(xhr.response);

            var row = "";
            if (type === "PaymentSlipAttachment") {
                $('#inptPaymentSlipAttachment tbody')[0].innerHTML = '';
                for (var i = 0; i < d.length; i++) {
                    row += '<tr>' +
                        '<td>' + d[i].CreatedDate + '</td>' +
                        '<td> <a href="' + d[i].Path + d[i].Name + '" target="_blank">' + d[i].Name + '</a></td>' +
                        '<td>' + d[i].Size + '</td> ' +
                        '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                        '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';
                }

                $('#inptPaymentSlipAttachment').append(row);
                $('#fileSelectPaymentSlipAttachment').val('');
            }
        }
    };
}

function removeAttachment(e) {
    if (confirm("Please click OK to confirm delete.")) {
        var attachmentId = $(e).parent().data('value');

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/DeleteAttachment",
            data: { "attachmentId": attachmentId },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                showToast(d.Message, "success");
                $(e).parent().parent().remove();
            }
        });
    }
}

function SubmitPaymentSlip() {
    if (inputValidation()) {
        
        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var Request = {
            "RequestId": $('#RequestId').val(),
            "Subject": $('#inptSubject').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
        };

        var SubRequest = {
            "RequestId": $('#RequestId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val()
        };

        var CustomerDetails = {
            "BusinessRegDate": $('#inptBusinessRegDate').val(),
            "LegalConstitution": $('#inptLegalConstitution').val(),
            "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
            "EconomySector": $('#inptEconomySector').val(),
            "EconomySubSector": $('#inptEconomySubSector').val(),
            "BusinessAddress": $('#inptBusinessAddress').val(),
            "PostalCode": $('#inptPostalCode').val(),
            "TelephoneNo": $('#inptTelephoneNo').val(),
            "FaxNo": $('#inptFaxNo').val(),
            "EmailAddress": $('#inptEmailAddress').val(),
            "Website": $('#inptWebsite').val()
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/CustomerSubmitPaymentSlipRequest",
            data: { "req": Request, "subReq": SubRequest, "cusDet": CustomerDetails },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}