﻿
function init() {
    initInputRestriction();
}

function inptTitleOnChange(e) {
    if (e.value === "0") {
        $('#inptName').hide();
    } else {
        $('#inptName').attr('style','display: inline-block; width:75%');
    }
}

function bindAddApproverClick() {
    var a = $('#inptApprover option:selected');
    if (a.val() !== '0') {
        $('#tblApprover').append('<tr><td colspan="2"><p data-value="' + a.val() + '">' + a.text() + '</p></td>' +
            '<td style="text-align: center">' +
            '<a class="delete" onclick="removeApprover(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
        a.addClass('hidden');
        $('#inptApprover').val(0);
        $('#ApproverFoot').hide();
    }
}

function removeApprover(e) {
    $.ajax({
        url: window.location.origin + "/SubscriptionManagement/Home/RemoveApprover",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $('#inptApprover [value=' + $(e).data('value') + ']').removeClass('hidden');
            $(e).parent().parent().remove();
            $('#ApproverFoot').show();
        }
    });
}

function initInputRestriction() {
    $('.datepicker').keypress(function () {
        return false;
    });

    var today = new Date();
    var tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() === ".") {
            $(e.target).val('');
        }
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });

    $('.name').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('.name').blur(function () {
        trimSpacing(this);
    });

    $('.email').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('.email').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('.contact').keydown(function (e) {
        restrictIntegerInput(e);
    });

    //$('#inptBusinessRegNo').blur(function () {
    //    if ($('#inptBusinessRegNo').val() !== "" && $('#inptBusinessRegNo').val() !== null) {
    //        //debugger
    //        //$.ajax({
    //        //    url: window.location.origin + "/SubscriptionManagement/Home/CheckBusinessRegNo",
    //        //    type: 'GET',
    //        //    cache: false,
    //        //}).done(function (result) {
    //        //    $('#CustomerDetails').html(result);
    //        //});
    //        var url = window.location.origin + "/SubscriptionManagement/Home/CheckBusinessRegNo";
    //        $("#CustomerDetails").load(url + "?businessRegNo=" + $('#inptBusinessRegNo').val());

    //        //if ($('#CustomerDetails').children().length === 0) {
    //        //    if ($('#inptBusinessRegNo').next().length > 0) {
    //        //        $('#inptBusinessRegNo').next().remove();
    //        //    }
    //        //    $('<p class="invalid-alert" style="color:red;">Business reg no does not exist.</p>').insertAfter($('#inptBusinessRegNo'));
    //        //}
    //    }
    //    //$.ajax({
    //    //    type: "POST",
    //    //    url: window.location.origin + "/Home/CheckBusinessRegNo",
    //    //    data: '{businessRegNo: "' + $('#businessRegNo').val() + '" }',
    //    //    contentType: "application/json; charset=utf-8",
    //    //    dataType: "json",
    //    //    success: function (data) {

    //    //        if (data.ProfileExists) {
    //    //            //Email available.
    //    //            $('#BusinessExist').val("true");

    //    //            $("#inptPerson > tbody").html("");
    //    //            $('#ProfileId').val(data.Profile.ProfileId);
    //    //            $('#name').val(data.Profile.Name);

    //    //            if (data.Profile.BusinessType == 1) {
    //    //                $('#c1').prop("checked", true);
    //    //                $('#choice').val("Company");
    //    //            }
    //    //            else if (data.Profile.BusinessType == 2) {
    //    //                $('#c2').prop("checked", true);
    //    //                $('#choice').val("Company");
    //    //            }
    //    //            else if (data.Profile.BusinessType == 3) {
    //    //                $('#b1').prop("checked", true);
    //    //                $('#choice').val("Business");
    //    //            }
    //    //            else if (data.Profile.BusinessType == 4) {
    //    //                $('#b2').prop("checked", true);
    //    //                $('#choice').val("Business");
    //    //            }
    //    //            $('#choice').change();

    //    //            $('#contactNo').val(data.Profile.ContactNo);
    //    //            $('#email').val(data.Profile.Email);
    //    //            $('#initial').val(data.Profile.Initial);

    //    //            if (data.Profile.DMS) {
    //    //                $('#dms').val("true");
    //    //            }
    //    //            else {
    //    //                $('#dms').val("false");
    //    //            }

    //    //            if (data.Profile.DMSStatus === "Active") {
    //    //                $('#viewSubscription').css({
    //    //                    'display': 'inline-block',
    //    //                    'margin-top': '2px'
    //    //                });
    //    //            }

    //    //            $('#source').val(data.Profile.Source);
    //    //            $('#website').val(data.Profile.Website);

    //    //            $('#AddressId').val(data.Address.AddressId);
    //    //            $('#line1').val(data.Address.Line1);
    //    //            $('#line2').val(data.Address.Line2);
    //    //            $('#city').val(data.Address.City);
    //    //            $('#state').val(data.Address.State);
    //    //            $('#postalCode').val(data.Address.PostalCode);
    //    //            $('#line12').val(data.Address.Line12);
    //    //            $('#line22').val(data.Address.Line22);
    //    //            $('#city2').val(data.Address.City2);
    //    //            $('#state2').val(data.Address.State2);
    //    //            $('#postalCode2').val(data.Address.PostalCode2);
    //    //            //$('#country').val(data.Address.Country);

    //    //            $.each(data.Persons, function (index, value) {
    //    //                var YesNo = "No";

    //    //                if (value.IsPrimary) {
    //    //                    YesNo = "Yes";
    //    //                }

    //    //                var row = '<tr>' +
    //    //                    '<td>' + value.Name + '</td>' +
    //    //                    '<td>' + value.ContactNo + '</td> ' +
    //    //                    '<td>' + value.Email + '</td>' +
    //    //                    '<td class="text-center" style="vertical-align:middle">' + YesNo + '</td>' +
    //    //                    '<td data-value="' + value.PersonId + '" style="text-align:center;vertical-align:middle"> <a onclick="editCreditorPerson(this)" class="edit" data-value="' + value.PersonId + '" data-name="' + value.Name + '" data-contact="' + value.ContactNo + '" data-email="' + value.Email + '" data-isprimary="' + value.IsPrimary + '" title="Edit" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> &nbsp&nbsp <a class="delete" title="Delete" onclick="removeCreditorPerson(this)" data-value="' + value.PersonId + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
    //    //                '</tr>';

    //    //                $('#inptPerson').last().append(row);
    //    //            });

    //    //            //$('#PersonId').val(data.Person.PersonId);
    //    //            //$('#picName').val(data.Person.Name);
    //    //            //$('#picContact').val(data.Person.ContactNo);
    //    //            //$('#picEmail').val(data.Person.Email);
    //    //        }
    //    //        else {
    //    //            //Email not available.
    //    //            //alert("not ok");
    //    //        }
    //    //    }
    //    //});
    //});
}

function inputValidation(aid) {
    var isValid = true;

    if (aid === undefined || aid === null || $('#lblStatus').text() === 'Pending Lead Assignment' || $('#lblStatus').text() === 'KIV') {
        if ($('#tblApprover').length) {
            if ($('#tblApprover tbody tr').length < 1) {
                isValid = false;
                if ($('#tblApprover tfoot tr').next().length < 1) {
                    $('#tblApprover tfoot').append('<tr><td colspan="3"><p class="invalid-alert" style="color:red; font-size:10px">Please select arm to proceed</p></td></tr>');
                }
            }
            else {
                if ($('#tblApprover tfoot tr').next().length > 0) {
                    $('#tblApprover tfoot tr:last-child').remove();
                }
            }
        }
    }

    if ($('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
        if ($('#inptBusinessRegDate').val().trim() === '') {
            isValid = false;
            if ($('#inptBusinessRegDate').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please enter business reg date to proceed</p>').insertAfter($('#inptBusinessRegDate'));
            }
        }
        else {
            if ($('#inptBusinessRegDate').next().length > 0) {
                $('#inptBusinessRegDate').next().remove();
            }
        }
    }

    if ($('#lblStatus').text() === 'Pending Legal') {
        if ($('#inptLegalRecommendAction').val() === '0') {
            isValid = false;
            if ($('#inptLegalRecommendAction').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please select recommend action to proceed</p>').insertAfter($('#inptLegalRecommendAction'));
            }
        }
        else {
            if ($('#inptLegalRecommendAction').next().length > 0) {
                $('#inptLegalRecommendAction').next().remove();
            }
        }

        if ($('#inptReason').val() === '0') {
            isValid = false;
            if ($('#inptReason').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please select reason to proceed</p>').insertAfter($('#inptReason'));
            }
        }
        else {
            if ($('#inptReason').next().length > 0) {
                $('#inptReason').next().remove();
            }
        }

        if ($('#OBCCorporate').is(':not(:checked)') && $('#OBCFI').is(':not(:checked)') && $('#OBCMMSDA').is(':not(:checked)') && $('#OBCFintech').is(':not(:checked)') &&
            $('#OBCMoneylender').is(':not(:checked)') && $('#OBCP2PLender').is(':not(:checked)') && $('#OBCCreditLeasing').is(':not(:checked)') && $('#OBCSME').is(':not(:checked)')) {
            isValid = false;
            if ($('#ErrorMsgForMembershipType').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please select membership type to proceed</p>').insertAfter($('#ErrorMsgForMembershipType'));
            }
        }
        else {
            if ($('#ErrorMsgForMembershipType').next().length > 0) {
                $('#ErrorMsgForMembershipType').next().remove();
            }
        }
    }

    if ($('#inptTitle').val() === "0"/* && $('#inptName').val().trim() !== ''*/) {
        if ($('#ErrorBoxForTitleAndName').next().length > 0) {
            $('#ErrorBoxForTitleAndName').next().remove();
        }
        else {
            isValid = false;
            if ($('#ErrorBoxForTitleAndName').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please select title to proceed</p>').insertAfter($('#ErrorBoxForTitleAndName'));
            }
        }
    }

    if ($('#inptName').val().trim() === '' && $('#inptTitle').val() !== "0") {
        if ($('#ErrorBoxForTitleAndName').next().length > 0) {
            $('#ErrorBoxForTitleAndName').next().remove();
        }

        isValid = false;
        if ($('#ErrorBoxForTitleAndName').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter name to proceed</p>').insertAfter($('#ErrorBoxForTitleAndName'));
        }
    }

    if ($('#inptName').val().trim() !== '' && $('#inptTitle').val() !== "0") {
        if ($('#ErrorBoxForTitleAndName').next().length > 0) {
            $('#ErrorBoxForTitleAndName').next().remove();
        }
    }

    if ($('#inptDesignation').val().trim() === '') {
        isValid = false;
        if ($('#inptDesignation').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter designation to proceed</p>').insertAfter($('#inptDesignation'));
        }
    }
    else {
        if ($('#inptDesignation').next().length > 0) {
            $('#inptDesignation').next().remove();
        }
    }

    if ($('#inptContactNo').val().trim() === '') {
        isValid = false;
        if ($('#inptContactNo').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter contact no to proceed</p>').insertAfter($('#inptContactNo'));
        }
    }
    else {
        if ($('#inptContactNo').next().length > 0) {
            $('#inptContactNo').next().remove();
        }
    }

    if ($('#inptBusinessName').val().trim() === '') {
        isValid = false;
        if ($('#inptBusinessName').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter business name to proceed</p>').insertAfter($('#inptBusinessName'));
        }
    }
    else {
        if ($('#inptBusinessName').next().length > 0) {
            $('#inptBusinessName').next().remove();
        }
    }

    if ($('#inptBusinessRegNo').val().trim() === '') {
        isValid = false;
        if ($('#inptBusinessRegNo').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter business reg no to proceed</p>').insertAfter($('#inptBusinessRegNo'));
        }
    }
    else {
        if ($('#inptBusinessRegNo').next().length > 0) {
            $('#inptBusinessRegNo').next().remove();
        }
    }

    if ($('#inptConstitutionType').val().trim() === '') {
        isValid = false;
        if ($('#inptConstitutionType').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter constitution type to proceed</p>').insertAfter($('#inptConstitutionType'));
        }
    }
    else {
        if ($('#inptConstitutionType').next().length > 0) {
            $('#inptConstitutionType').next().remove();
        }
    }

    if ($('#inptEmail').val().trim() === '') {
        isValid = false;
        if ($('#inptEmail').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter email to proceed</p>').insertAfter($('#inptEmail'));
        }
    }
    else {
        if ($('#inptEmail').next().length > 0) {
            $('#inptEmail').next().remove();
        }
    }

    if ($('#inptPackageId').val() === "0" && $('#lblStatus').text() === 'Pending Package Selection') {
        isValid = false;
        if ($('#inptPackageId').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please select package to proceed</p>').insertAfter($('#inptPackageId'));
        }
    }
    else {
        if ($('#inptPackageId').next().length > 0) {
            $('#inptPackageId').next().remove();
        }
    }

    if (($('#inptInvoiceAttachment tbody tr').length === "0" || $('#inptInvoiceAttachment tbody tr').length === 0) && $('#lblStatus').text() === 'Pending Invoice Attachment') {
        isValid = false;
        if ($('#inptInvoiceAttachment').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please upload invoice attachment to proceed</p>').insertAfter($('#inptInvoiceAttachment'));
        }
    }
    else {
        if ($('#inptInvoiceAttachment').next().length > 0) {
            $('#inptInvoiceAttachment').next().remove();
        }
    }

    if ($('#lblStatus').text() === "Pending Invoice Attachment") {
        if ($('#inptInvoiceAttachment tbody tr').length < 1) {
            isValid = false;
            if ($('#inptInvoiceAttachment tfoot tr').next().length < 1) {
                $('#inptInvoiceAttachment tfoot').append('<tr><td colspan="3"><p class="invalid-alert" style="color:red; font-size:10px">Please attach invoice attachments to proceed</p></td></tr>');
            }
        }
        else {
            if ($('#inptInvoiceAttachment tfoot tr').next().length > 0) {
                $('#inptInvoiceAttachment tfoot tr:last-child').remove();
            }
        }
    }
    
    //if ($('#lblStatus').text() === "Pending Document Verification") {
    //    if ($('#OBCCorporate').is(':not(:checked)') && $('#OBCFI').is(':not(:checked)') && $('#OBCMMSDA').is(':not(:checked)') && $('#OBCFintech').is(':not(:checked)') &&
    //        $('#OBCMoneylender').is(':not(:checked)') && $('#OBCP2PLender').is(':not(:checked)') && $('#OBCCreditLeasing').is(':not(:checked)') && $('#OBCSME').is(':not(:checked)')) {
    //        isValid = false;
    //        if ($('#ErrorMsgForMembershipType').next().length < 1) {
    //            $('<p class="invalid-alert" style="color:red;">Please select membership type to proceed</p>').insertAfter($('#ErrorMsgForMembershipType'));
    //        }
    //    }
    //    else {
    //        if ($('#ErrorMsgForMembershipType').next().length > 0) {
    //            $('#ErrorMsgForMembershipType').next().remove();
    //        }
    //    }
    //}

    return isValid;
}

function uploadDoc(type) {
    var formdata = new FormData(); //FormData object
    formdata.append("RequestId", $('#requestId').val());
    var fileInput;
    if (type === "InvoiceAttachment") {
        formdata.append("AttachmentType", "D212680F-8D45-4919-B7EF-ADFE55B15458");
        formdata.append("LastModifiedDate", $('#LastModifiedDate').val());
        fileInput = document.getElementById('fileSelectInvoiceAttachment');
    }

    //Iterating through each files selected in fileInput
    for (var i = 0; i < fileInput.files.length; i++) {
        //Appending each file to FormData object
        formdata.append(fileInput.files[i].name, fileInput.files[i]);
    }

    //Creating an XMLHttpRequest and sending
    var xhr = new XMLHttpRequest();
    xhr.open('POST', window.location.origin + '/SubscriptionManagement/Home/UploadAttachment');
    xhr.send(formdata);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var d = JSON.parse(xhr.response);

            var row = "";
            if (type === "InvoiceAttachment") {
                $('#inptInvoiceAttachment tbody')[0].innerHTML = '';
                for (var i = 0; i < d.length; i++) {
                    row += '<tr>' +
                        '<td>' + d[i].CreatedDate + '</td>' +
                        '<td> <a href="' + d[i].Path + d[i].Name + '" target="_blank">' + d[i].Name + '</a></td>' +
                        '<td>' + d[i].Size + '</td> ' +
                        '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                        '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';
                }

                $('#inptInvoiceAttachment').append(row);
                $('#fileSelectInvoiceAttachment').val('');
            }
        }
    };
}

function removeAttachment(e) {
    if (confirm("Please click OK to confirm delete.")) {
        var attachmentId = $(e).parent().data('value');

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/DeleteAttachment",
            data: { "attachmentId": attachmentId },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                showToast(d.Message, "success");
                $(e).parent().parent().remove();
            }
        });
    }
}

function Submit(actionId, button) {
    if (inputValidation()) {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var approvers = [];
        if ($('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft" || $('#lblStatus').text() === "Pending Lead Assignment" || $('#lblStatus').text() === "KIV") {
            $('#tblApprover tbody tr').each(function () {
                approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
            });
        }

        var request = {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "Comment": $('#inptComment').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Approvers": approvers
        };

        var SubRequest = {
            "RequestId": $('#requestId').val(),
            "Name": $('#inptName').val(),
            "BusinessName": $('#inptBusinessName').val(),
            "BusinessRegNo": $('#inptBusinessRegNo').val(),
            "Designation": $('#inptDesignation').val(),
            "ContactNo": $('#inptContactNo').val(),
            "Email": $('#inptEmail').val(),
            "PackageId": $('#inptPackageId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val(),
            "LegalRecommendAction": $('#inptLegalRecommendAction').val(),
            "Reason": $('#inptReason').val(),
            "Title": $('#inptTitle').val(),
            "ConstitutionType": $('#inptConstitutionType').val()
        };

        var CustomerDetails = {
            "BusinessName": $('#inptBusinessName').val(),
            "BusinessRegDate": $('#inptBusinessRegDate').val(),
            "BusinessRegNo": $('#inptBusinessRegNo').val(),
            "LegalConstitution": $('#inptLegalConstitution').val(),
            "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
            "EconomySector": $('#inptEconomySector').val(),
            "EconomySubSector": $('#inptEconomySubSector').val(),
            "BusinessAddress": $('#inptBusinessAddress').val(),
            "PostalCode": $('#inptPostalCode').val(),
            "TelephoneNo": $('#inptTelephoneNo').val(),
            "FaxNo": $('#inptFaxNo').val(),
            "EmailAddress": $('#inptEmailAddress').val(),
            "Website": $('#inptWebsite').val()
        };

        var UserAction = {
            "Status": $(button).data('status'),
            "ExpectedRoleId": $(button).data('expectedroleid'),
            "ActionId": $(button).data('actionid'),
            "ButtonName": $(button).data('buttonname')
        };

        var OnBoardingChecklist = {
            "RequestId": $('#requestId').val(),
            "Corporate": $('#OBCCorporate').is(':checked'),
            "FI": $('#OBCFI').is(':checked'),
            "MMSDA": $('#OBCMMSDA').is(':checked'),
            "Fintech": $('#OBCFintech').is(':checked'),
            "Moneylender": $('#OBCMoneylender').is(':checked'),
            "P2PLender": $('#OBCP2PLender').is(':checked'),
            "CreditLeasing": $('#OBCCreditLeasing').is(':checked'),
            "SME": $('#OBCSME').is(':checked'),
            "SubscriptionAgreement": $('input[name="OBCSubscriptionAgreement"]:checked').val(),
            "PackagePricing": $('input[name="OBCPackagePricing"]:checked').val(),
            "DirectorICCopy": $('input[name="OBCDirectorICCopy"]:checked').val(),
            "SecurityAdminForm": $('input[name="OBCSecurityAdminForm"]:checked').val(),
            "SecurityAdminICCopy": $('input[name="OBCSecurityAdminICCopy"]:checked').val(),
            "KYCDeclaration": $('input[name="OBCKYCDeclaration"]:checked').val(),
            "CopyOfCBMConsentForm": $('input[name="OBCCopyOfCBMConsentForm"]:checked').val(),
            "PaymentSlip": $('input[name="OBCPaymentSlip"]:checked').val(),
            "CopyOfLicense": $('input[name="OBCCopyOfLicense"]:checked').val(),
            "LicenseExpiryDate": $('#OBCLicenseExpiryDate').val(),
            "PromotionalLetter": $('input[name="OBCPromotionalLetter"]:checked').val(),
            "CMSServices": $('input[name="OBCCMSServices"]:checked').val(),
        };
        
        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/Submit",
            data: { "req": request, "subReq": SubRequest, "cusDet": CustomerDetails, "ua": UserAction, "OnBoardingChecklist": OnBoardingChecklist },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function SaveDraft(actionId, button) {

    if ($('#CanSubmit').val() === "true") {
        $('#CanSubmit').val('false');
    }
    else {
        return;
    }

    var approvers = [];
    if ($('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft" || $('#lblStatus').text() === "Pending Lead Assignment" || $('#lblStatus').text() === "KIV") {
        $('#tblApprover tbody tr').each(function () {
            approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
        });
    }

    var request = {
        "ActionId": actionId,
        "RequestId": $('#requestId').val(),
        "Subject": $('#inptSubject').val(),
        "Description": $('#inptDescription').val(),
        "ReferenceNo": $('#ReferenceNo').val(),
        "Comment": $('#inptComment').val(),
        "Approvers": approvers
    };

    var SubRequest = {
        "RequestId": $('#requestId').val(),
        "Name": $('#inptName').val(),
        "BusinessName": $('#inptBusinessName').val(),
        "BusinessRegNo": $('#inptBusinessRegNo').val(),
        "Designation": $('#inptDesignation').val(),
        "ContactNo": $('#inptContactNo').val(),
        "Email": $('#inptEmail').val(),
        "PackageId": $('#inptPackageId').val(),
        "PurposeOfUse": $('#inptPurposeOfUse').val(),
        "FrequencyOfUse": $('#inptFrequencyOfUse').val(),
        "LegalRecommendAction": $('#inptLegalRecommendAction').val(),
        "Reason": $('#inptReason').val(),
        "Title": $('#inptTitle').val(),
        "ConstitutionType": $('#inptConstitutionType').val()
    };

    var CustomerDetails = {
        "BusinessName": $('#inptBusinessName').val(),
        "BusinessRegDate": $('#inptBusinessRegDate').val(),
        "BusinessRegNo": $('#inptBusinessRegNo').val(),
        "LegalConstitution": $('#inptLegalConstitution').val(),
        "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
        "EconomySector": $('#inptEconomySector').val(),
        "EconomySubSector": $('#inptEconomySubSector').val(),
        "BusinessAddress": $('#inptBusinessAddress').val(),
        "PostalCode": $('#inptPostalCode').val(),
        "TelephoneNo": $('#inptTelephoneNo').val(),
        "FaxNo": $('#inptFaxNo').val(),
        "EmailAddress": $('#inptEmailAddress').val(),
        "Website": $('#inptWebsite').val()
    };

    var UserAction = {
        "Status": $(button).data('status'),
        "ExpectedRoleId": $(button).data('expectedroleid'),
        "ActionId": $(button).data('actionid'),
        "ButtonName": $(button).data('buttonname')
    };
    
    $.ajax({
        url: window.location.origin + "/SubscriptionManagement/Home/SaveDraft",
        data: { "req": request, "subReq": SubRequest, "cusDet": CustomerDetails, "ua": UserAction },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                setTimeout(function () {
                    window.location.href = window.location.origin + d.RedirectUrl;
                }, 2000);
            }
            else {
                $('#CanSubmit').val('true');
                showToast(d.Error, "error");
            }
        }
    });
}

function NotInterested(actionId, button) {
    if ($('#inptComment').val() !== "") {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        if ($('#inptComment').next().length > 0) {
            $('#inptComment').next().remove();
        }

        var approvers = [];
        if ($('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft" || $('#lblStatus').text() === "Pending Lead Assignment" || $('#lblStatus').text() === "KIV") {
            $('#tblApprover tbody tr').each(function () {
                approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
            });
        }

        var request = {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "PackageId": $('#inptPackageId').val(),
            "Comment": $('#inptComment').val(),
            "Approvers": approvers
        };

        var SubRequest = {
            "RequestId": $('#requestId').val(),
            "Name": $('#inptName').val(),
            "BusinessName": $('#inptBusinessName').val(),
            "BusinessRegNo": $('#inptBusinessRegNo').val(),
            "Designation": $('#inptDesignation').val(),
            "ContactNo": $('#inptContactNo').val(),
            "Email": $('#inptEmail').val(),
            "PackageId": $('#inptPackageId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val(),
            "LegalRecommendAction": $('#inptLegalRecommendAction').val(),
            "Reason": $('#inptReason').val(),
            "Title": $('#inptTitle').val(),
            "ConstitutionType": $('#inptConstitutionType').val()
        };

        var CustomerDetails = {
            "BusinessName": $('#inptBusinessName').val(),
            "BusinessRegDate": $('#inptBusinessRegDate').val(),
            "BusinessRegNo": $('#inptBusinessRegNo').val(),
            "LegalConstitution": $('#inptLegalConstitution').val(),
            "CountryOfRegistration": $('#inptCountryOfRegistration').val(),
            "EconomySector": $('#inptEconomySector').val(),
            "EconomySubSector": $('#inptEconomySubSector').val(),
            "BusinessAddress": $('#inptBusinessAddress').val(),
            "PostalCode": $('#inptPostalCode').val(),
            "TelephoneNo": $('#inptTelephoneNo').val(),
            "FaxNo": $('#inptFaxNo').val(),
            "EmailAddress": $('#inptEmailAddress').val(),
            "Website": $('#inptWebsite').val()
        };

        var UserAction = {
            "Status": $(button).data('status'),
            "ExpectedRoleId": $(button).data('expectedroleid'),
            "ActionId": $(button).data('actionid'),
            "ButtonName": $(button).data('buttonname')
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/Submit",
            data: { "req": request, "subReq": SubRequest, "cusDet": CustomerDetails, "ua": UserAction },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter comment/remark to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}

function Approve(actionId, button) {
    if (inputValidation()) {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var Req = {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Comment": $('#inptComment').val()
        };

        var OnBoardingChecklist = {
            "RequestId": $('#requestId').val(),
            "Corporate": $('#OBCCorporate').is(':checked'),
            "FI": $('#OBCFI').is(':checked'),
            "MMSDA": $('#OBCMMSDA').is(':checked'),
            "Fintech": $('#OBCFintech').is(':checked'),
            "Moneylender": $('#OBCMoneylender').is(':checked'),
            "P2PLender": $('#OBCP2PLender').is(':checked'),
            "CreditLeasing": $('#OBCCreditLeasing').is(':checked'),
            "SME": $('#OBCSME').is(':checked'),
            "SubscriptionAgreement": $('input[name="OBCSubscriptionAgreement"]:checked').val(),
            "PackagePricing": $('input[name="OBCPackagePricing"]:checked').val(),
            "DirectorICCopy": $('input[name="OBCDirectorICCopy"]:checked').val(),
            "SecurityAdminForm": $('input[name="OBCSecurityAdminForm"]:checked').val(),
            "SecurityAdminICCopy": $('input[name="OBCSecurityAdminICCopy"]:checked').val(),
            "KYCDeclaration": $('input[name="OBCKYCDeclaration"]:checked').val(),
            "CopyOfCBMConsentForm": $('input[name="OBCCopyOfCBMConsentForm"]:checked').val(),
            "PaymentSlip": $('input[name="OBCPaymentSlip"]:checked').val(),
            "CopyOfLicense": $('input[name="OBCCopyOfLicense"]:checked').val(),
            "LicenseExpiryDate": $('#OBCLicenseExpiryDate').val(),
            "PromotionalLetter": $('input[name="OBCPromotionalLetter"]:checked').val(),
            "CMSServices": $('input[name="OBCCMSServices"]:checked').val(),
        };

        var UserAction = {
            "Status": $(button).data('status'),
            "ExpectedRoleId": $(button).data('expectedroleid'),
            "ActionId": $(button).data('actionid'),
            "ButtonName": $(button).data('buttonname')
        };
        
        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/Approve",
            data: { "req": Req, "OnBoardingChecklist": OnBoardingChecklist, "ua": UserAction },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function RequestMore(actionId, button) {
    if ($('#inptComment').val() !== "") {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        if ($('#inptComment').next().length > 0) {
            $('#inptComment').next().remove();
        }

        var Req = {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Comment": $('#inptComment').val()
        };

        var SubRequest = {
            "RequestId": $('#requestId').val(),
            "Name": $('#inptName').val(),
            "BusinessName": $('#inptBusinessName').val(),
            "BusinessRegNo": $('#inptBusinessRegNo').val(),
            "Designation": $('#inptDesignation').val(),
            "ContactNo": $('#inptContactNo').val(),
            "Email": $('#inptEmail').val(),
            "PackageId": $('#inptPackageId').val(),
            "PurposeOfUse": $('#inptPurposeOfUse').val(),
            "FrequencyOfUse": $('#inptFrequencyOfUse').val(),
            "LegalRecommendAction": $('#inptLegalRecommendAction').val(),
            "Reason": $('#inptReason').val(),
            "Title": $('#inptTitle').val(),
            "ConstitutionType": $('#inptConstitutionType').val()
        };

        var OnBoardingChecklist = {
            "RequestId": $('#requestId').val(),
            "Corporate": $('#OBCCorporate').is(':checked'),
            "FI": $('#OBCFI').is(':checked'),
            "MMSDA": $('#OBCMMSDA').is(':checked'),
            "Fintech": $('#OBCFintech').is(':checked'),
            "Moneylender": $('#OBCMoneylender').is(':checked'),
            "P2PLender": $('#OBCP2PLender').is(':checked'),
            "CreditLeasing": $('#OBCCreditLeasing').is(':checked'),
            "SME": $('#OBCSME').is(':checked'),
            "SubscriptionAgreement": $('input[name="OBCSubscriptionAgreement"]:checked').val(),
            "PackagePricing": $('input[name="OBCPackagePricing"]:checked').val(),
            "DirectorICCopy": $('input[name="OBCDirectorICCopy"]:checked').val(),
            "SecurityAdminForm": $('input[name="OBCSecurityAdminForm"]:checked').val(),
            "SecurityAdminICCopy": $('input[name="OBCSecurityAdminICCopy"]:checked').val(),
            "KYCDeclaration": $('input[name="OBCKYCDeclaration"]:checked').val(),
            "CopyOfCBMConsentForm": $('input[name="OBCCopyOfCBMConsentForm"]:checked').val(),
            "PaymentSlip": $('input[name="OBCPaymentSlip"]:checked').val(),
            "CopyOfLicense": $('input[name="OBCCopyOfLicense"]:checked').val(),
            "LicenseExpiryDate": $('#OBCLicenseExpiryDate').val(),
            "PromotionalLetter": $('input[name="OBCPromotionalLetter"]:checked').val(),
            "CMSServices": $('input[name="OBCCMSServices"]:checked').val(),
        };

        var UserAction = {
            "Status": $(button).data('status'),
            "ExpectedRoleId": $(button).data('expectedroleid'),
            "ActionId": $(button).data('actionid'),
            "ButtonName": $(button).data('buttonname')
        };

        $.ajax({
            url: window.location.origin + "/SubscriptionManagement/Home/RequestMore",
            data: { "req": Req, "SubRequest": SubRequest, "OnBoardingChecklist": OnBoardingChecklist, "ua": UserAction },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}

function Listing() {
    
    $.ajax({
        //url: "http://42.1.62.141:443/api/Report/api/Report/listing?RegNo=002608T&RegName=PERAK%20INDUSTRIAL%20CORPORATION%20SDN%20BHD&DateBR=11/02/1988&ConstitutionTypeStdCode=24",
        url: "http://42.1.62.141:443/api/Report/api/Report/listing",
        //url: "http://42.1.62.141:443/api/Report/api/Report/test/listing",
        data: JSON.stringify({
            "request": {
                "systemID": "SCBS",
                "service": "SMEDTLRPTS",
                "reportType": "BIR",
                "memberID": "SCBS",
                "userID": "PSCBS0220",
                "reqNo": "",
                "sequenceNo": "001",
                "reqDate": "08/01/2020",
                "purposeStdCode": "CREREV",
                "costCenterStdCode": "",
                "consentFlag": "",
                "subject": {
                    "regNo": "175481T",
                    "regName": "HBM MECHANICAL SERVICES SDN. BHD.",
                    "dateBR": "11/02/1988",
                    "constitutionTypeStdCode": "24",
                    "emailAddr": "xxxxx",
                    "mobileNo": "xxxxxx",
                    "businessCouCodeStdCode": "",
                    "businessStaCodeStdCode": "",
                    "entityCode": "",
                    "tradeEntityCode": ""
                }
            }
            //"RegNo": $('#inptBusinessRegNo').val(), "RegName": $('#inptBusinessName').val(), "DateBR": $('#inptBusinessRegDate').val(), "ConstitutionTypeStdCode": $('#inptConstitutionType').val()
        }),
        cache: false,
        type: "POST",
        dataType: "json",
        contentType: 'application/json',

        success: function (data) {
            var d = JSON.parse(data);
            alert('success' + d);
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
}

function Confirm() {

}