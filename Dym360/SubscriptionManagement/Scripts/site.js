﻿function showToast(msg, type) {
    if (type === "success") {
        $.toast({
            heading: 'Success',
            text: msg,
            position: 'top-roght',
            stack: false
        });
    }
    else if (type === "error") {
        $.toast({
            heading: 'Error',
            text: msg,
            position: 'top-roght',
            icon: 'error'
        });
    }
}



function LoadPageView(elemId, url, data) {
    $(elemId).load(url, data);
}

function Search() {
    $('#tblSubscription tbody tr').each(function () {
        var isHide = false;
        $(this).find('td').each(function () {
            var input = $('#inpt' + $(this).data('input')).val();

            if (input !== '' && input !== undefined && input !== 'all') {
                if ($(this).data('input') === "Status") {
                    if ($(this).text().trim() !== input) {
                        isHide = true;
                    }
                }
                else {
                    if ($(this).text().toLowerCase().indexOf(input.toLowerCase()) < 0) {
                        isHide = true;
                    }
                }
            }
        })

        if (isHide) {
            $(this).css('display', 'none');
        }
        else {
            $(this).css('display', 'table-row');
        }
    });
}
function ClearSearch() {
    $('#tblSubscription tbody tr').prop('style', '');
    $('input').val('');
    $('select').val('all');
}

function sortTable(e) {
    var table, rows, switching, i, x, y, shouldSwitch, type, elemId, col;
    table = document.getElementById($(e).parent().parent().parent().parent().prop('id'));
    type = $(e).data('type');
    elemId = $(e).prop('id');
    col = $(e).parent().index();
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            //rows[i].getElementsByTagName("td")[0].innerHTML = i;

            x = rows[i].getElementsByTagName("td")[col];
            y = rows[i + 1].getElementsByTagName("td")[col];
            //check if the two rows should switch place:
            if (type === 'asc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            else {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
    if (type === 'asc') {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="desc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-bottom"></span>');
    }
    else {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="asc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-top"></span>');
    }
}