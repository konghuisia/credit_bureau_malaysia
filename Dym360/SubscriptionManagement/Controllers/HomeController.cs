﻿using DataAccessHelper.ConfigurationHelper.Store;
using DataAccessHelper.RequestHelper.Store;
using DataAccessHelper.SubscriptionManagement.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.ConsentForm;
using ObjectClasses.SubscriptionManagement.KYC;
using SubscriptionManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using iTS = iTextSharp.text;
using MvcRazorToPdf;
using ObjectClasses.SubscriptionManagement.EConsentChecklist;
using ObjectClasses.SubscriptionManagement.GeneralCodeOfConduct;

namespace SubscriptionManagement.Controllers
{
    public class HomeController : Controller
    {
        private static string RoleId;
        const string ModuleId = "F43ED0E5-F3AE-4FC1-AB86-EA925FF80E9F";
        static Role CurRole;
        static User CurrentUser;

        UserStore userStore;
        RoleStore roleStore;
        TitleStore TitleStore;
        ReasonStore ReasonStore;
        CountryStore CountryStore;
        RequestStore requestStore;
        ApprovalStore approvalStore;
        AttachmentStore AttachmentStore;
        UserAccessStore UserAccessStore;
        UserActionStore UserActionStore;
        PurposeOfUseStore PurposeOfUseStore;
        EconomySectorStore EconomySectorStore;
        PersonInChargeStore PersonInChargeStore;
        CustomerDetailStore CustomerDetailsStore;
        KYC_DeclarationStore KYC_DeclarationStore;
        IllegalServicesStore IllegalServicesStore;
        EconomySubSectorStore EconomySubSectorStore;
        DepartmentAccessStore DepartmentAccessStore;
        ConsentFormPeopleStore ConsentFormPeopleStore;
        EConsentChecklistStore EConsentChecklistStore;
        OnBoardingChecklistStore OnBoardingChecklistStore;
        LetterOfUndertakingStore LetterOfUndertakingStore;
        LegalRecommendActionStore LegalRecommendActionStore;
        UnauthorisedEntitiesStore UnauthorisedEntitiesStore;
        SubscriptionPackagesStore SubscriptionPackagesStore;
        GeneralCodeOfConductStore GeneralCodeOfConductStore;
        LegalConstitutionTypeStore LegalConstitutionTypeStore;
        SubscriptionManagementStore SubscriptionManagementStore;

        public HomeController()
        {
            userStore = DependencyResolver.Current.GetService<UserStore>();
            roleStore = DependencyResolver.Current.GetService<RoleStore>();
            TitleStore = DependencyResolver.Current.GetService<TitleStore>();
            ReasonStore = DependencyResolver.Current.GetService<ReasonStore>();
            CountryStore = DependencyResolver.Current.GetService<CountryStore>();
            requestStore = DependencyResolver.Current.GetService<RequestStore>();
            approvalStore = DependencyResolver.Current.GetService<ApprovalStore>();
            AttachmentStore = DependencyResolver.Current.GetService<AttachmentStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            UserActionStore = DependencyResolver.Current.GetService<UserActionStore>();
            PurposeOfUseStore = DependencyResolver.Current.GetService<PurposeOfUseStore>();
            EconomySectorStore = DependencyResolver.Current.GetService<EconomySectorStore>();
            PersonInChargeStore = DependencyResolver.Current.GetService<PersonInChargeStore>();
            CustomerDetailsStore = DependencyResolver.Current.GetService<CustomerDetailStore>();
            KYC_DeclarationStore = DependencyResolver.Current.GetService<KYC_DeclarationStore>();
            IllegalServicesStore = DependencyResolver.Current.GetService<IllegalServicesStore>();
            EconomySubSectorStore = DependencyResolver.Current.GetService<EconomySubSectorStore>();
            DepartmentAccessStore = DependencyResolver.Current.GetService<DepartmentAccessStore>();
            ConsentFormPeopleStore = DependencyResolver.Current.GetService<ConsentFormPeopleStore>();
            EConsentChecklistStore = DependencyResolver.Current.GetService<EConsentChecklistStore>();
            OnBoardingChecklistStore = DependencyResolver.Current.GetService<OnBoardingChecklistStore>();
            LetterOfUndertakingStore = DependencyResolver.Current.GetService<LetterOfUndertakingStore>();
            LegalRecommendActionStore = DependencyResolver.Current.GetService<LegalRecommendActionStore>();
            UnauthorisedEntitiesStore = DependencyResolver.Current.GetService<UnauthorisedEntitiesStore>();
            SubscriptionPackagesStore = DependencyResolver.Current.GetService<SubscriptionPackagesStore>();
            GeneralCodeOfConductStore = DependencyResolver.Current.GetService<GeneralCodeOfConductStore>();
            LegalConstitutionTypeStore = DependencyResolver.Current.GetService<LegalConstitutionTypeStore>();
            SubscriptionManagementStore = DependencyResolver.Current.GetService<SubscriptionManagementStore>();
            CurrentUser = new User();
        }

        public async Task<ActionResult> Index()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            var vm = new IndexViewModel();
            vm.MyTasks = await requestStore.GetSubscriptionRequestsList(Utils.GetUserId());
            vm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());

            if (Utils.GetUserDepartment() == "0837C28C-3367-4866-B244-7C55A9452A4F")//cEO Department Id
            {
                vm.IsManagementDepartment = true;
            }

            return View(vm);
        }

        public async Task<ActionResult> SubscriptionRequestForm(string requestId = null, string cameFrom = null)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            IList<User> users = new List<User>();
            var role = new Role();
            var config = new SubscriptionRequestViewModel();
            CurrentUser = await userStore.GetUser(Utils.GetUserId());
            ViewBag.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            role = await roleStore.GetRoleSubscriptionManagement("", Utils.GetUserDepartment(), Utils.GetUserPosition());
            
            RoleId = role.RoleId;
            if (string.IsNullOrEmpty(requestId))
            {
                config.Access = "Write";
                CurRole = await roleStore.GetRoleSubscriptionManagement("", Utils.GetUserDepartment(), Utils.GetUserPosition());

                config.Request = new Request()
                {
                    ReferenceNo = "SUB-SM-" + DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                    RequestTypeId = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE",
                    ModuleId = ModuleId,
                    BusinessExists = false,
                    Requester = new Requester()
                    {
                        Name = CurrentUser.Name,
                        UserId = CurrentUser.UserId,
                        Position = CurrentUser.Position,
                        Department = CurrentUser.Department,
                        Reporting = CurrentUser.Reporting,
                        Id = int.Parse(CurrentUser.Id),
                    }
                };

                if (role.Abbreviation == "ARM Requester" || role.Abbreviation == "ARM Head")
                {
                    config.IsArm = true;
                    if (role.Abbreviation == "ARM Head")
                    {
                        config.IsArmHead = true;
                    }
                }
                config.Request.CreatedDate = DateTime.Now;
                config.Request.RequestId = await requestStore.NewPreRequest(config.Request);

                ra = await roleStore.GetRoleActions("", CurRole.RoleId);
                apps = await approvalStore.GetAllApproval(config.Request.RequestId);

                if (config.IsArm)
                {
                    if (!config.IsArmHead)
                    {
                        config.Request.ARM = new Approval()
                        {
                            UserId = CurrentUser.UserId,
                            Name = CurrentUser.Name
                        };
                    }
                }

                if (CurRole.Abbreviation == "ARM Head" && config.Request.Status == "New")
                {
                    foreach (var a in ra)
                    {
                        if (a.ActionKeyword == "SUBMIT")
                        {
                            a.ActionName = "Assign";
                        }
                    }
                }
            }
            else
            {
                config.OnBoardingChecklist = await OnBoardingChecklistStore.GetOnBoardingChecklistByRequestId(requestId);
                config.LegalRecommendActions = await LegalRecommendActionStore.GetLegalRecommendActions();
                config.Reasons = await ReasonStore.GetReasons();

                var SubReq = await SubscriptionManagementStore.GetSubscriptionRequest(requestId);

                config.Request = await requestStore.GetRequest(requestId);

                apps = await approvalStore.GetAllApproval(requestId);
                config.Request.BusinessExists = true;
                
                config.Request.AdminTeam = apps.Where(x => x.Role == "ADMIN").ToList();
                config.Request.ARM = apps.ToList().Find(x => x.Role == "ARM");
                config.Request.LegalTeam = apps.Where(x => x.Role == "LEGAL").ToList();
                config.Request.ManagementTeam = apps.Where(x => x.Role == "CEO").ToList();
                var ArmHead = apps.ToList().Find(x => x.Role == "ARM Head");
                var Kiv = apps.ToList().Find(x => x.Role == "KIV");

                if (ArmHead != null)
                {
                    config.Request.ARMHead = ArmHead;
                }
                if (Kiv != null && Kiv.ActionDate.ToShortDateString() != "1/1/0001" || config.Request.Status == "Pending KIV")
                {
                    config.Request.ARMHead = Kiv;
                }

                if (config.Request.Status == "Pending Sales Pitch")
                {
                    config.Request.ARM = apps.ToList().Find(x => x.Role == "Sales Pitch");
                    role = await roleStore.GetRoleSubscriptionManagement("397B8335-B903-431C-91B4-E4515A4C44BE", null, null);//Sales Pitch Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("397B8335-B903-431C-91B4-E4515A4C44BE", null, null);//Sales Pitch Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Website Demo")
                {
                    config.Request.ARM = apps.ToList().Find(x => x.Role == "Website Demo");
                    role = await roleStore.GetRoleSubscriptionManagement("F5864D56-0056-4211-8C5A-D4969573B4F7", null, null);//Website Demo Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("F5864D56-0056-4211-8C5A-D4969573B4F7", null, null);//Website Demo Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Package Selection")
                {
                    config.Request.ARM = apps.ToList().Find(x => x.Role == "ARM");
                    role = await roleStore.GetRoleSubscriptionManagement("94642BFE-4F7F-4459-BF90-08A8D820DDF2", null, null);//package selection Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("94642BFE-4F7F-4459-BF90-08A8D820DDF2", null, null);//package selection Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Document Verification")
                {
                    config.Request.AdminTeam = apps.Where(x => x.Role == "ADMIN").ToList();
                    role = await roleStore.GetRoleSubscriptionManagement("47ECED6E-3F33-40B9-8F24-76843CFA1FBE", null, null);//Document Confirmation Admin Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("47ECED6E-3F33-40B9-8F24-76843CFA1FBE", null, null);//Document Confirmation Admin Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Legal")
                {
                    config.Request.LegalTeam = apps.Where(x => x.Role == "LEGAL").ToList();
                    role = await roleStore.GetRoleSubscriptionManagement("2F9B0AA3-DBC1-46DC-A353-8CC458FE26F9", null, null);//legal Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("2F9B0AA3-DBC1-46DC-A353-8CC458FE26F9", null, null);//legal Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending CEO")
                {
                    config.Request.ManagementTeam = apps.Where(x => x.Role == "CEO").ToList();
                    role = await roleStore.GetRoleSubscriptionManagement("3FD19EDC-FECF-476C-B827-30C22FE192FE", null, null);//ceo Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("3FD19EDC-FECF-476C-B827-30C22FE192FE", null, null);//Ceo Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Invoice Attachment")
                {
                    config.Request.AdminTeam = apps.Where(x => x.Role == "INVOICE ATTACHMENT").ToList();
                    role = await roleStore.GetRoleSubscriptionManagement("48B794C4-4446-4532-B033-71074566D258", null, null);//Invoice Attachment Admin Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("48B794C4-4446-4532-B033-71074566D258", null, null);//Invoice Attachment Admin Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }
                if (config.Request.Status == "Pending Payment Verification")
                {
                    config.Request.AdminTeam = apps.Where(x => x.Role == "SUB Payment Verification").ToList();
                    role = await roleStore.GetRoleSubscriptionManagement("4AB1D492-110B-4D2B-A949-4650C698B86F", null, null);//Payment Confirmation Admin Role
                    CurRole = await roleStore.GetRoleSubscriptionManagement("4AB1D492-110B-4D2B-A949-4650C698B86F", null, null);//Payment Confirmation Admin Role
                    if (CurRole != null)
                    {
                        RoleId = CurRole.RoleId;
                    }
                }

                config.IsArm = true;
                config.SubscriptionPackages = await SubscriptionPackagesStore.GetSubscriptionPackages();
                config.CustomerDetail = await CustomerDetailsStore.GetCustomerDetailsByRequestId(requestId);

                if (SubReq != null)
                {
                    config.Request.Name = SubReq.Name;
                    config.Request.BusinessRegNo = SubReq.BusinessRegNo;
                    config.Request.ContactNo = SubReq.ContactNo;
                    config.Request.Designation = SubReq.Designation;
                    config.Request.Email = SubReq.Email;
                    config.Request.BusinessName = SubReq.BusinessName;
                    config.Request.PackageId = SubReq.PackageId;
                    config.Request.PurposeOfUse = SubReq.PurposeOfUse;
                    config.Request.FrequencyOfUse = SubReq.FrequencyOfUse;
                    config.Request.CustomerSubmittedAt = SubReq.CustomerSubmittedAt;
                    config.Request.LegalRecommendAction = SubReq.LegalRecommendAction;
                    config.Request.Reason = SubReq.Reason;
                    config.Request.OnBoardingChecklist = SubReq.OnBoardingChecklist;
                    config.Request.Title = SubReq.Title;
                    config.Request.ConstitutionType = SubReq.ConstitutionType;

                    if ((config.CustomerDetail != null) && !string.IsNullOrEmpty(config.CustomerDetail.Website))
                    {
                        config.Request.Website = config.CustomerDetail.Website;
                    }


                    var IllegalServices = await IllegalServicesStore.GetIllegalServices();
                    var UnauthorisedEntities = await UnauthorisedEntitiesStore.GetUnauthorisedEntities();

                    //var IllegalService = IllegalServices.Find(x => x.Name.ToLower().Replace(" ","").Contains(SubReq.BusinessRegNo.ToLower().Replace(" ", "")) 
                    //|| x.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessName.ToLower().Replace(" ", "")));
                    //if (IllegalService != null)
                    //{
                    //    config.IllegalService = true;
                    //    config.IllegalServiceDate = IllegalService.DateOfWarningLetterSent;
                    //}

                    foreach(var iss in IllegalServices)
                    {
                        if ((!string.IsNullOrEmpty(SubReq.BusinessRegNo) && !string.IsNullOrEmpty(iss.Name)) && (iss.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessRegNo.ToLower().Replace(" ", ""))) ||
                            (!string.IsNullOrEmpty(SubReq.Name) && !string.IsNullOrEmpty(iss.Name)) && (iss.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessName.ToLower().Replace(" ", ""))))
                        {
                            config.IllegalService = true;
                            config.IllegalServiceDate = iss.DateOfWarningLetterSent;
                        }
                    }

                    foreach(var ue in UnauthorisedEntities)
                    {
                        if ((!string.IsNullOrEmpty(SubReq.BusinessRegNo) && !string.IsNullOrEmpty(ue.Name)) && (ue.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessRegNo.ToLower().Replace(" ", ""))) ||
                            (!string.IsNullOrEmpty(SubReq.BusinessName) && !string.IsNullOrEmpty(ue.Name)) && (ue.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessName.ToLower().Replace(" ", ""))) ||
                            ((!string.IsNullOrEmpty(config.Request.Website) && !string.IsNullOrEmpty(ue.Website)) && ue.Website.ToLower().Replace(" ", "").Contains(config.Request.Website.ToLower().Replace(" ", ""))))
                        {
                            config.UnauthorisedEntity = true;
                            config.UnauthorisedEntityDate = ue.DateAddedToAlertList;
                        }
                    }
                    
                    //var UnauthorisedEntity = UnauthorisedEntities.Find(x => x.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessRegNo.ToLower().Replace(" ", "")) 
                    //|| x.Name.ToLower().Replace(" ", "").Contains(SubReq.BusinessName.ToLower().Replace(" ", "")) 
                    //|| x.Website.ToLower().Replace(" ", "").Contains(config.Request.Website.ToLower().Replace(" ", "")));
                    //if (UnauthorisedEntity != null)
                    //{
                    //    config.UnauthorisedEntity = true;
                    //    config.UnauthorisedEntityDate = UnauthorisedEntity.DateAddedToAlertList;
                    //}
                }

                if (config.Request.Status == "Draft")
                {
                    config.Request.ARM = apps.ToList().Find(x => x.Role == "Sales Pitch");

                    if (config.Request.Requester.UserId == Utils.GetUserId())
                    {
                        config.Access = "Write";
                        CurRole = await roleStore.GetRoleSubscriptionManagement("", Utils.GetUserDepartment(), Utils.GetUserPosition());
                        ra = await roleStore.GetRoleActions("", CurRole.RoleId);
                        RoleId = CurRole.RoleId;

                        if (CurRole.Abbreviation == "ARM Requester" || role.Abbreviation == "ARM Head")
                        {
                            config.IsArm = true;
                            if (role.Abbreviation == "ARM Head")
                            {
                                config.IsArmHead = true;
                            }
                            else {
                                if (!config.IsArmHead)
                                {
                                    config.Request.ARM = new Approval()
                                    {
                                        UserId = CurrentUser.UserId,
                                        Name = CurrentUser.Name
                                    };
                                }
                            }
                        }
                        else
                        {
                            config.IsArm = false;
                        }
                    }
                }
                else
                {
                    if (config.Request.Status == "Pending Sales Pitch")
                    {
                        config.Request.ARM = apps.ToList().Find(x => x.Role == "Sales Pitch");
                    }
                    else if (config.Request.Status == "Pending Website Demo")
                    {
                        config.Request.ARM = apps.ToList().Find(x => x.Role == "Website Demo");
                    }
                    else if (config.Request.Status == "KIV")
                    {
                        config.Request.ARMHead = apps.ToList().Find(x => x.Role == "KIV");
                    }

                    if (config.Request.Status == "Pending Invoice Attachment" || (apps.ToList().Exists(x => x.Role == "INVOICE ATTACHMENT" && x.ActionDate.ToShortDateString() != "01/01/0001" && x.ActionDate.ToShortDateString() != "1/1/0001")))
                    {
                        config.Request.AdminTeam = apps.Where(x => x.Role == "INVOICE ATTACHMENT").ToList();
                    }

                    if (config.Request.Status == "Pending Payment Verification" || (apps.ToList().Exists(x => x.Role == "SUB Payment Verification" && x.ActionDate.ToShortDateString() != "01/01/0001" && x.ActionDate.ToShortDateString() != "1/1/0001")))
                    {
                        //var Previous = apps.Where(x => x.Role == "INVOICE ATTACHMENT").ToList().Find(x => x.ActionDate.ToShortDateString() != "01/01/0001" && x.ActionDate.ToShortDateString() != "1/1/0001");

                        config.Request.AdminTeam = apps.Where(x => x.Role == "SUB Payment Verification").ToList();
                        //config.Request.AdminTeam.Find(x => x.UserId == Previous.UserId).ActionDate = Previous.ActionDate;
                    }

                    if (!config.Request.Status.ToLower().Contains("close"))
                    {
                        var isBlock = true;
                        var pends = apps.ToList().FindAll(x => x.Action == "PENDING");
                        foreach (var app in pends)
                        {
                            if (app.RoleId == config.Request.RequestState.RoleId && app.UserId == Utils.GetUserId())
                            {
                                isBlock = false;
                            }
                        }
                        if (!isBlock)
                        {
                            config.Access = "Write";
                            CurRole = await roleStore.GetRole(config.Request.RequestState.RoleId);
                            ra = await roleStore.GetRoleActions("", config.Request.RequestState.RoleId);
                            RoleId = CurRole.RoleId;

                            if (CurRole.Abbreviation == "ARM Requester" || role.Abbreviation == "ARM Head")
                            {
                                config.IsArm = true;
                                if (role.Abbreviation == "ARM Head")
                                {
                                    config.IsArmHead = true;
                                }
                            }

                            //Admin role, Payment Verification role.
                            if (CurRole.Abbreviation == "ADMIN" || role.RoleId == "47ECED6E-3F33-40B9-8F24-76843CFA1FBE" || CurRole.Abbreviation == "SUB Payment Verification" || role.RoleId == "4AB1D492-110B-4D2B-A949-4650C698B86F")
                            {
                                foreach (var a in ra)
                                {
                                    if (a.ActionKeyword == "APPROVE")
                                    {
                                        a.ActionName = "Complete";
                                    }
                                    if (a.ActionKeyword == "REQUEST_MORE")
                                    {
                                        a.ActionName = "Incomplete";
                                    }
                                }
                            }

                            if (config.Request.Status == "KIV")
                            {
                                foreach (var a in ra)
                                {
                                    if (a.ActionKeyword == "SUBMIT")
                                    {
                                        a.ActionName = "Assign";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            config.Request.RoleId = RoleId;
            config.cameFrom = cameFrom;
            config.CurrentUser = CurrentUser;
            config.RoleActions = ra.ToList();
            if (config.RoleActions.Count > 0 && config.Request.Status == "Pending Package Selection")
            {
                config.RoleActions = config.RoleActions.OrderBy(x => x.ActionKeyword).ToList();
            }
            config.PurposeOfUses = await PurposeOfUseStore.GetPurposeOfUse();
            config.Titles = await TitleStore.GetTitle();

            users = await userStore.GetAllUser();
            
            config.Approvers = users.ToList().FindAll(x => x.DepartmentId == config.CurrentUser.DepartmentId && x.DeletedFlag == "N");

            config.Users = users;
            return View("SubscriptionRequestForm", config);
        }

        //[HttpGet]
        //public async Task<ActionResult> CheckBusinessRegNo (string businessRegNo)
        //{
        //    if (!string.IsNullOrEmpty(businessRegNo))
        //    {
        //        if (businessRegNo == "ABCD1234")
        //        {
        //            var cs = new CustomerDetails();
        //            cs.BusinessName = "ABCD 1234 Sdn Bhd";
        //            cs.BusinessRegDate = DateTime.Now.AddYears(-1);
        //            cs.BusinessRegNo = businessRegNo;

        //            return PartialView("_CustomerDetails", new CustomerDetailsViewModel() {
        //                customerDetails = cs
        //            });
        //        }
        //        else if (businessRegNo == "12345678")
        //        {
        //            var cs = new CustomerDetails();
        //            cs.BusinessName = "12345678 Sdn Bhd";
        //            cs.BusinessRegDate = DateTime.Now.AddYears(-2);
        //            cs.BusinessRegNo = businessRegNo;

        //            return PartialView("_CustomerDetails", new CustomerDetailsViewModel()
        //            {
        //                customerDetails = cs
        //            });
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }

        //    return null;
        //}

        public async Task<ActionResult> LoadCustomerDetails (string requestId)
        {
            return PartialView("_CustomerDetails", new CustomerDetailsViewModel()
            {
                countries = await CountryStore.GetAllCountry(),
                Request = await requestStore.GetRequest(requestId),
                purposeOfUses = await PurposeOfUseStore.GetPurposeOfUse(),
                economySectors = await EconomySectorStore.GetEconomySector(),
                economySubSectors = await EconomySubSectorStore.GetEconomySubSector(),
                Attachments = await AttachmentStore.GetAttachmentByRequestId(requestId),
                //ShowInSelects = await ShowInSelectStore.GetShowInSelectByModuleId(ModuleId),
                personInCharges = await PersonInChargeStore.GetPersonInChargeByRequestId(requestId),
                customerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(requestId),
                legalConstitutionTypes = await LegalConstitutionTypeStore.GetLegalConstitutionTypeControl(),
                personInChargeAttachments = await PersonInChargeStore.GetPersonInChargeAttachmentByRequestId(requestId),
            });
        }

        public async Task<JsonResult> Submit(Request req, SubscriptionRequest subReq, CustomerDetails cusDet, UserAction ua, OnBoardingChecklist OnBoardingChecklist)
        {
            ua.CurrentRoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            ua.CreatedBy = Utils.GetUserId();
            ua.UserId = Utils.GetUserId();
            ua.ModuleId = ModuleId;
            ua.RequestId = req.RequestId;
            await UserActionStore.CreateUserAction(ua);

            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            if (!string.IsNullOrEmpty(ua.ExpectedRoleId))
            {
                req.RoleId = ua.ExpectedRoleId;
            }
            req.Requester.UserId = Utils.GetUserId();
            req.UserId = req.Requester.UserId;
            req.CreatedDate = DateTime.Now;
            req.CustomerLink = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/CustomerSubscriptionRequestFormValidate?requestId=" + req.RequestId + "";
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
            var success = await SubscriptionManagementStore.SubmitRequest(req, subReq, cusDet);

            OnBoardingChecklist.UpdatedBy = Utils.GetUserId();
            OnBoardingChecklist.OnBoardingChecklistId = await OnBoardingChecklistStore.UpdateOnBoardingChecklist(OnBoardingChecklist);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request submitted";
                resp.RedirectUrl = "/SubscriptionManagement/Home";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> CustomerSubmitRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet, KYC_DeclarationViewModel KYC_Declaration, ConsentFormPeople ConsentFormPeoplesUpper, EConsentChecklist EConsentChecklist, GeneralCodeOfConduct GeneralCodeOfConduct, LetterOfUndertaking LetterOfUndertaking)
        {
            req.ModuleId = ModuleId;
            req.RoleId = "42AD37A3-E86A-43CD-B892-FD2309FDA858";//Form Submission Role
            req.ActionId = "6058E97B-2B6E-4758-8A69-2DB84C668258";
            req.CreatedDate = DateTime.Now;
            req.CustomerLink = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/CustomerSubscriptionRequestFormValidate?requestId=" + req.RequestId + "";
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
            var success = await SubscriptionManagementStore.CustomerSubmitRequest(req, subReq, cusDet);
            var KYCDeclaration = await KYC_DeclarationStore.UpdateKYC_DeclarationByRequestId(req.RequestId, KYC_Declaration);
            ConsentFormPeoplesUpper.ConsentFormPeopleId = await ConsentFormPeopleStore.UpdateConsentFormPeople(ConsentFormPeoplesUpper);
            EConsentChecklist.EConsentChecklistId = await EConsentChecklistStore.UpdateConsentFormPeople(EConsentChecklist);
            GeneralCodeOfConduct.GeneralCodeOfConductId = await GeneralCodeOfConductStore.UpdateGeneralCodeOfConduct(GeneralCodeOfConduct);
            LetterOfUndertaking.LetterOfUndertakingId = await LetterOfUndertakingStore.UpdateLetterOfUndertaking(LetterOfUndertaking);

            var resp = new Response();
            if (success)
            {
                resp.Message = "E-Form submitted";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> CustomerSubmitPaymentSlipRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            req.ModuleId = ModuleId;
            req.RoleId = "7029D0D0-0C35-4B3B-9314-7B864F0C478E";//Payment Slip Role
            req.ActionId = "6058E97B-2B6E-4758-8A69-2DB84C668258";
            req.CreatedDate = DateTime.Now;
            req.CustomerLink = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/CustomerSubscriptionRequestFormValidate?requestId=" + req.RequestId + "";
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
            var success = await SubscriptionManagementStore.CustomerSubmitRequest(req, subReq, cusDet);

            var resp = new Response();
            if (success)
            {
                resp.Message = "E-Form submitted";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> SaveDraft(Request req, SubscriptionRequest subReq, CustomerDetails cusDet, UserAction ua)
        {
            ua.CurrentRoleId = RoleId;
            ua.CreatedBy = Utils.GetUserId();
            ua.UserId = Utils.GetUserId();
            ua.ModuleId = ModuleId;
            ua.RequestId = req.RequestId;
            await UserActionStore.CreateUserAction(ua);

            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            if (!string.IsNullOrEmpty(ua.ExpectedRoleId))
            {
                req.RoleId = ua.ExpectedRoleId;
            }
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Status = "Draft";
            var success = await SubscriptionManagementStore.SaveRequest(req, subReq, cusDet);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request saved as draft";
                resp.RedirectUrl = "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> CustomerSaveDraft(Request req, SubscriptionRequest subReq, CustomerDetails cusDet, KYC_DeclarationViewModel KYC_Declaration, ConsentFormPeople ConsentFormPeoplesUpper, EConsentChecklist EConsentChecklist, GeneralCodeOfConduct GeneralCodeOfConduct, LetterOfUndertaking LetterOfUndertaking)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.Status = "Draft";
            var success = await SubscriptionManagementStore.CustomerSaveRequest(req, subReq, cusDet);
            var KYCDeclaration = await KYC_DeclarationStore.UpdateKYC_DeclarationByRequestId(req.RequestId, KYC_Declaration);
            ConsentFormPeoplesUpper.ConsentFormPeopleId = await ConsentFormPeopleStore.UpdateConsentFormPeople(ConsentFormPeoplesUpper);
            EConsentChecklist.EConsentChecklistId = await EConsentChecklistStore.UpdateConsentFormPeople(EConsentChecklist);
            GeneralCodeOfConduct.GeneralCodeOfConductId = await GeneralCodeOfConductStore.UpdateGeneralCodeOfConduct(GeneralCodeOfConduct);
            LetterOfUndertaking.LetterOfUndertakingId = await LetterOfUndertakingStore.UpdateLetterOfUndertaking(LetterOfUndertaking);

            var resp = new Response();
            if (success)
            {
                resp.Message = "E-Form saved as draft";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> CustomerContinue(Request req, SubscriptionRequest subReq, CustomerDetails cusDet, KYC_DeclarationViewModel KYC_Declaration, ConsentFormPeople ConsentFormPeoplesUpper, EConsentChecklist EConsentChecklist, GeneralCodeOfConduct GeneralCodeOfConduct, LetterOfUndertaking LetterOfUndertaking)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.Status = "Draft";
            var success = await SubscriptionManagementStore.CustomerContinueRequest(req, subReq, cusDet);
            var KYCDeclaration = await KYC_DeclarationStore.UpdateKYC_DeclarationByRequestId(req.RequestId, KYC_Declaration);
            ConsentFormPeoplesUpper.ConsentFormPeopleId = await ConsentFormPeopleStore.UpdateConsentFormPeople(ConsentFormPeoplesUpper);
            EConsentChecklist.EConsentChecklistId = await EConsentChecklistStore.UpdateConsentFormPeople(EConsentChecklist);
            GeneralCodeOfConduct.GeneralCodeOfConductId = await GeneralCodeOfConductStore.UpdateGeneralCodeOfConduct(GeneralCodeOfConduct);
            LetterOfUndertaking.LetterOfUndertakingId = await LetterOfUndertakingStore.UpdateLetterOfUndertaking(LetterOfUndertaking);

            var resp = new Response();
            if (success)
            {
                resp.Message = "E-Form saved.";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<ActionResult> CustomerSubscriptionRequestFormValidate (string requestId)
        {
            return View("CustomerSubscriptionRequestFormValidate", new CustomerSubscriptionRequestFormValidateViewModel() {
                RequestId = requestId
            });
        }

        public async Task<JsonResult> ValidatePassword (CustomerSubscriptionRequestFormValidateViewModel vm)
        {
            var r = new Response();

            if (await SubscriptionManagementStore.ValidatePassword(vm.RequestId, vm.Password))
            {
                Session["CustomerSubscriptionRequestForm" + vm.RequestId] = true;
                r.RedirectUrl = "/SubscriptionManagement/Home/CustomerSubscriptionRequestForm?requestId=" + vm.RequestId;
            }
            else
            {
                r.Error = "Invalid Password";
            }

            return Json(r);
        }

        public async Task<ActionResult> CustomerSubscriptionRequestForm (string requestId)
        {
            if (Session["CustomerSubscriptionRequestForm" + requestId] != null)
            {
                return View("CustomerSubscriptionRequestForm", new CustomerDetailsViewModel()
                {
                    countries = await CountryStore.GetAllCountry(),
                    Request = await requestStore.GetRequest(requestId),
                    purposeOfUses = await PurposeOfUseStore.GetPurposeOfUse(),
                    economySectors = await EconomySectorStore.GetEconomySector(),
                    packages = await SubscriptionPackagesStore.GetSubscriptionPackages(),
                    economySubSectors = await EconomySubSectorStore.GetEconomySubSector(),
                    Attachments = await AttachmentStore.GetAttachmentByRequestId(requestId),
                    //ShowInSelects = await ShowInSelectStore.GetShowInSelectByModuleId(ModuleId),
                    personInCharges = await PersonInChargeStore.GetPersonInChargeByRequestId(requestId),
                    customerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(requestId),
                    subscriptionRequest = await SubscriptionManagementStore.GetSubscriptionRequest(requestId),
                    legalConstitutionTypes = await LegalConstitutionTypeStore.GetLegalConstitutionTypeControl(),
                    personInChargeAttachments = await PersonInChargeStore.GetPersonInChargeAttachmentByRequestId(requestId),
                });
            }
            else
            {
                return View("CustomerSubscriptionRequestFormValidate", new CustomerSubscriptionRequestFormValidateViewModel()
                {
                    RequestId = requestId
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            request.RequestId = Request.Params.GetValues("RequestId")[0];

            var truefalse = false;
            if (Request.Params.GetValues("IsPrimary")[0] == "true")
            {
                truefalse = true;
            }

            var pic = new PersonInCharge()
            {
                IsPrimary = truefalse,
                RequestId = request.RequestId,
                Type = Request.Params.GetValues("Type")[0],
                Name = Request.Params.GetValues("Name")[0],
                IcNumber = Request.Params.GetValues("IcNumber")[0],
                Designation = Request.Params.GetValues("Designation")[0],
                EmailAddress = Request.Params.GetValues("EmailAddress")[0],
                TelephoneNumber = Request.Params.GetValues("TelephoneNumber")[0],
            };
            //var uploadBy = Request.Params.GetValues("UploadBy")[0];
            var classificationId = "";
            request.PersonInChargeId = await PersonInChargeStore.CreatePersonInCharge(pic, System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SecurityAdminFormValidate?personInChargeId=");

            var folder = "Attachments";
            var AttachmentTypeId = "";

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var c = i + 1;
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                var type = Request.Files.Keys[i].Substring(Request.Files.Keys[i].Length - 1);
                if (type == "1")
                {
                    AttachmentTypeId = "EDDEDB1A-9192-427A-90BA-DE7546C086AA";//ic
                }
                else if (type == "2")
                {
                    AttachmentTypeId = "4D13748E-8036-4C8F-9911-8E7C2EA71D6C";//license
                }
                else if (type == "3")
                {
                    AttachmentTypeId = "55771298-516E-401B-A2EF-2FE3EA0A7124";//payslip
                }

                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/SubscriptionManagement/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    Path = path,
                    AbsolutePath = location,
                    ModifiedDate = DateTime.Now.ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = classificationId
                });
            }

            request = await AttachmentStore.NewPersonInChargeAttachment(request);
            request.PersonInChargeAttachments = await PersonInChargeStore.GetPersonInChargeAttachmentByRequestId(request.RequestId);
            return Json(request);
        }

        public async Task<JsonResult> DeletePic (string personInChargeId)
        {
            await AttachmentStore.DeletePersonInCharge(personInChargeId);
            return Json(new Response());
        }

        [HttpPost]
        public async Task<JsonResult> UploadAttachment()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            if (Utils.IfUserAuthenticated())
            {
                request.Requester.UserId = Utils.GetUserId();
            }
            request.RequestId = Request.Params.GetValues("RequestId")[0];
            var AttachmentTypeId = Request.Params.GetValues("AttachmentType")[0];
            var classificationId = "";
            var folder = "Attachments";

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/SubscriptionManagement/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    Path = path,
                    AbsolutePath = location,
                    ModifiedDate = Convert.ToDateTime(Request.Params.GetValues("LastModifiedDate")[i]).ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = classificationId
                });
            }

            request = await AttachmentStore.CreateAsync(request);

            foreach (var a in request.Attachments)
            {
                a.CreatedDate = Convert.ToDateTime(a.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            return Json(request.Attachments.FindAll(x => x.AttachmentTypeId == AttachmentTypeId));

        }

        [HttpPost]
        public async Task<JsonResult> DeleteAttachment(string attachmentId)
        {
            await AttachmentStore.DeleteAttachment(attachmentId);
            return Json(new Response()
            {
                Message = "Attachment removed"
            });
        }

        public async Task<ActionResult> LoadMyTask()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            IList<MyTask> requests = await requestStore.GetSubscriptionRequestsList(Utils.GetUserId());

            return View("MyTask", requests.ToList());
        }

        public async Task<JsonResult> RequestMore(Request req, SubscriptionRequest SubRequest, OnBoardingChecklist OnBoardingChecklist, UserAction ua)
        {
            ua.CurrentRoleId = RoleId;
            ua.CreatedBy = Utils.GetUserId();
            ua.UserId = Utils.GetUserId();
            ua.ModuleId = ModuleId;
            ua.RequestId = req.RequestId;
            await UserActionStore.CreateUserAction(ua);

            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            if (!string.IsNullOrEmpty(ua.ExpectedRoleId))
            {
                req.RoleId = ua.ExpectedRoleId;
            }
            req.UserId = Utils.GetUserId();
            req.CustomerLink = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/CustomerSubscriptionRequestFormValidate?requestId=" + req.RequestId + "";
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
            var success = await SubscriptionManagementStore.RequestMoreRequest(req, SubRequest);
            OnBoardingChecklist.UpdatedBy = Utils.GetUserId();
            OnBoardingChecklist.OnBoardingChecklistId = await OnBoardingChecklistStore.UpdateOnBoardingChecklist(OnBoardingChecklist);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request sent back for clarification";
                resp.RedirectUrl = "/SubscriptionManagement/Home";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Approve(Request req, OnBoardingChecklist OnBoardingChecklist, UserAction ua)
        {
            ua.CurrentRoleId = RoleId;
            ua.CreatedBy = Utils.GetUserId();
            ua.UserId = Utils.GetUserId();
            ua.ModuleId = ModuleId;
            ua.RequestId = req.RequestId;
            await UserActionStore.CreateUserAction(ua);

            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            if (!string.IsNullOrEmpty(ua.ExpectedRoleId))
            {
                req.RoleId = ua.ExpectedRoleId;
            }
            req.UserId = Utils.GetUserId();
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
            var success = await SubscriptionManagementStore.ApproveRequest(req);
            OnBoardingChecklist.UpdatedBy = Utils.GetUserId();
            OnBoardingChecklist.OnBoardingChecklistId = await OnBoardingChecklistStore.UpdateOnBoardingChecklist(OnBoardingChecklist);

            var resp = new Response();
            if (success)
            {
                if (req.RoleId == "47ECED6E-3F33-40B9-8F24-76843CFA1FBE" || req.RoleId == "4AB1D492-110B-4D2B-A949-4650C698B86F" && req.ActionId == "A3E0E540-20F0-48D4-AC55-6CC8CA599D14")//Admin Team Role ID, Payment Verification role, Approve Action Id
                {
                    resp.Message = "Request complete";
                }
                else
                {
                    resp.Message = "Request approved";
                }
                resp.RedirectUrl = "/SubscriptionManagement/Home";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Management(List<Request> Requests, string Action)
        {
            foreach (var req in Requests)
            {
                req.UserAction.CurrentRoleId = "3FD19EDC-FECF-476C-B827-30C22FE192FE";
                req.UserAction.CreatedBy = Utils.GetUserId();
                req.UserAction.UserId = Utils.GetUserId();
                req.UserAction.ModuleId = ModuleId;
                req.UserAction.RequestId = req.RequestId;
                await UserActionStore.CreateUserAction(req.UserAction);

                req.ModuleId = ModuleId;
                req.RoleId = "3FD19EDC-FECF-476C-B827-30C22FE192FE";
                req.UserId = Utils.GetUserId();
                req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/SubscriptionManagement/Home/SubscriptionRequestForm?requestId=" + req.RequestId + "";
                var success = await SubscriptionManagementStore.Management(req, Action);
            }

            var resp = new Response();
            if (Action == "Approved")
            {
                resp.Message = "Request approved";
            }
            else if (Action == "Reject")
            {
                resp.Message = "Request rejected";
            }

            return Json(resp);
        }
        [HttpPost]
        public async Task<JsonResult> RemoveApprover(string userId, string requestId)
        {
            var roleId = "397B8335-B903-431C-91B4-E4515A4C44BE";//sales pitch roleid
            var success = await approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Approver removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<ActionResult> KYC_Declaration (string RequestId, string CameFrom)
        {
            var KYC_Declaration = await KYC_DeclarationStore.GetKYC_DeclarationByRequestId(RequestId);

            if (CameFrom == "Customer")
            {
                KYC_Declaration.HasWriteAccess = true;

                return PartialView(KYC_Declaration);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                KYC_Declaration.IsCBM = true;

                //String url = "http://localhost:52251/SubscriptionManagement/Home/KYC_Declaration?RequestId=DAE7FDAF-48C1-4BDB-9E27-9536CE8211E4";

                //PdfDocument doc = new PdfDocument();

                //PdfPageSettings setting = new PdfPageSettings();

                //setting.Size = new SizeF(1000, 1000);
                //setting.Margins = new Spire.Pdf.Graphics.PdfMargins(20);

                //PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
                //htmlLayoutFormat.IsWaiting = true;

                //Thread thread = new Thread(() =>
                //{ doc.LoadFromHTML(url, false, false, false, setting, htmlLayoutFormat); });
                //thread.SetApartmentState(ApartmentState.STA);
                //thread.Start();
                //thread.Join();
                ////Save pdf file.
                //doc.SaveToFile("output-wiki.pdf");
                //doc.Close();
                ////Launching the Pdf file.
                //System.Diagnostics.Process.Start("output-wiki.pdf");

                //byte[] result = ControllerContext.GeneratePdf(KYC_Declaration, "KYC_Declaration", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("KYC_Declaration");
                //});
                //return File(result, "application/pdf");

                return PartialView(KYC_Declaration);
            }
            return null;
        }

        public async Task<ActionResult> ConsentForm(string RequestId, string CameFrom)
        {
            var ConsentForm = new ConsentFormViewModel()
            {
                Request = await requestStore.GetRequest(RequestId),
                PersonInCharges = await PersonInChargeStore.GetPersonInChargeByRequestId(RequestId),
                CustomerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(RequestId),
                ConsentFormPeoples = await ConsentFormPeopleStore.GetConsentFormPeopleByRequestId(RequestId)
            };

            if (CameFrom == "Customer")
            {
                ConsentForm.HasWriteAccess = true;

                return PartialView(ConsentForm);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                ConsentForm.IsCBM = true;

                //byte[] result = ControllerContext.GeneratePdf(ConsentForm, "ConsentForm", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("ConsentForm");
                //});
                //return File(result, "application/pdf");

                return PartialView(ConsentForm);
            }
            return null;
        }

        public async Task<ActionResult> EConsentChecklist(string RequestId, string CameFrom)
        {
            var EConsentChecklist = new EConsentChecklistViewModel()
            {
                Request = await requestStore.GetRequest(RequestId),
                EConsentChecklist = await EConsentChecklistStore.GetEConsentChecklistByRequestId(RequestId)
            };

            if (CameFrom == "Customer")
            {
                EConsentChecklist.HasWriteAccess = true;

                return PartialView(EConsentChecklist);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                EConsentChecklist.IsCBM = true;

                //byte[] result = ControllerContext.GeneratePdf(EConsentChecklist, "EConsentChecklist", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("E-Consent Checklist");
                //});
                //return File(result, "application/pdf");

                return PartialView(EConsentChecklist);
            }
            return null;
        }

        public async Task<ActionResult> GeneralCodeOfConduct(string RequestId, string CameFrom)
        {
            var GeneralCodeOfConduct = new GeneralCodeOfConductViewModel()
            {
                Request = await requestStore.GetRequest(RequestId),
                CustomerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(RequestId),
                GeneralCodeOfConduct = await GeneralCodeOfConductStore.GetGeneralCodeOfConductByRequestId(RequestId)
            };

            if (CameFrom == "Customer")
            {
                GeneralCodeOfConduct.HasWriteAccess = true;

                return PartialView(GeneralCodeOfConduct);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                GeneralCodeOfConduct.IsCBM = true;

                //byte[] result = ControllerContext.GeneratePdf(EConsentChecklist, "EConsentChecklist", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("E-Consent Checklist");
                //});
                //return File(result, "application/pdf");

                return PartialView(GeneralCodeOfConduct);
            }
            return null;
        }

        public async Task<ActionResult> LetterOfUndertaking(string RequestId, string CameFrom)
        {
            var LetterOfUndertaking = new LetterOfUndertakingViewModel()
            {
                Request = await requestStore.GetRequest(RequestId),
                CustomerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(RequestId),
                LetterOfUndertaking = await LetterOfUndertakingStore.GetLetterOfUndertakingByRequestId(RequestId)
            };

            if (CameFrom == "Customer")
            {
                LetterOfUndertaking.HasWriteAccess = true;

                return PartialView(LetterOfUndertaking);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                LetterOfUndertaking.IsCBM = true;

                //byte[] result = ControllerContext.GeneratePdf(EConsentChecklist, "EConsentChecklist", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("E-Consent Checklist");
                //});
                //return File(result, "application/pdf");

                return PartialView(LetterOfUndertaking);
            }
            return null;
        }

        public async Task<ActionResult> SecurityAdminFormValidate(string personInChargeId, string requestId)
        {
            return View(new SecurityAdminFormValidateViewModel()
            {
                PersonInChargeId = personInChargeId,
                RequestId = requestId
            });
        }

        public async Task<JsonResult> ValidateSecurityAdminFormPassword(SecurityAdminFormValidateViewModel vm)
        {
            var r = new Response();

            if (await PersonInChargeStore.ValidatePassword(vm.PersonInChargeId, vm.Password))
            {
                Session["SecurityAdminForm" + vm.PersonInChargeId] = true;
                r.RedirectUrl = "/SubscriptionManagement/Home/SecurityAdminForm?PersonInChargeId=" + vm.PersonInChargeId + "&RequestId=" + vm.RequestId + "&CameFrom=SA";
            }
            else
            {
                r.Error = "Invalid Password";
            }

            return Json(r);
        }

        public async Task<ActionResult> SecurityAdminForm(string PersonInChargeId, string RequestId, string CameFrom)
        {
            var SecurityAdminForm = new SecurityAdminFormViewModel()
            {
                Request = await requestStore.GetRequest(RequestId),
                CustomerDetails = await CustomerDetailsStore.GetCustomerDetailsByRequestId(RequestId),
                PersonInCharge = await PersonInChargeStore.GetPersonInChargeByPersonInChargeId(PersonInChargeId),
            };

            if (CameFrom == "SA")
            {
                if (Session["SecurityAdminForm" + PersonInChargeId] != null)
                {
                    SecurityAdminForm.HasWriteAccess = true;

                    return PartialView(SecurityAdminForm);
                }
                else
                {
                    return View(new SecurityAdminFormValidateViewModel()
                    {
                        PersonInChargeId = PersonInChargeId,
                        RequestId = RequestId
                    });
                }
            }
            else if (CameFrom == "Customer")
            {
                return PartialView(SecurityAdminForm);
            }
            else
            {
                if (!Utils.IfUserAuthenticated())
                {
                    //return RedirectToAction("Index", "Login", new { area = "" });
                    return null;
                }
                SecurityAdminForm.IsCBM = true;

                //byte[] result = ControllerContext.GeneratePdf(EConsentChecklist, "EConsentChecklist", (writer, document) =>
                //{
                //    document.SetPageSize(iTS.PageSize.A4.Rotate());
                //    document.NewPage();
                //    document.AddTitle("E-Consent Checklist");
                //});
                //return File(result, "application/pdf");

                return PartialView(SecurityAdminForm);
            }
            return null;
        }

        public async Task<JsonResult> CreateConsentFormLowerPeople(ConsentFormPeople ConsentFormPeople)
        {
            return Json(await ConsentFormPeopleStore.UpdateConsentFormPeople(ConsentFormPeople));
        }

        public async Task<JsonResult> DeleteConsentFormLowerPeople(string ConsentFormPeopleId)
        {
            return Json(await ConsentFormPeopleStore.DeleteConsentFormPeople(ConsentFormPeopleId));
        }

        public async Task<JsonResult> CustomerSubmitSecurityAdminForm(PersonInCharge PersonInCharge)
        {
            return Json(await PersonInChargeStore.UpdatePersonInChargeConsent(PersonInCharge));
        }
    }
}