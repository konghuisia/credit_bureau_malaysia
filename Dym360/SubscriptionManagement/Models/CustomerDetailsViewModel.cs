﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class CustomerDetailsViewModel
    {
        public CustomerDetailsViewModel()
        {
            personInCharges = new List<PersonInCharge>();
            Attachments = new List<Attachment>();
            personInChargeAttachments = new List<PersonInChargeAttachment>();
        }

        public CustomerDetails customerDetails { get; set; }
        public List<PersonInCharge> personInCharges { get; set; }
        public List<ShowInSelect> ShowInSelects { get; internal set; }
        public SubscriptionRequest subscriptionRequest { get; internal set; }
        public Request Request { get; internal set; }
        public List<SubscriptionPackages> packages { get; internal set; }
        public IList<Country> countries { get; internal set; }
        public List<EconomySector> economySectors { get; internal set; }
        public List<EconomySubSector> economySubSectors { get; internal set; }
        public List<LegalConstitutionType> legalConstitutionTypes { get; internal set; }
        public List<Attachment> Attachments { get; internal set; }
        public List<PersonInChargeAttachment> personInChargeAttachments { get; internal set; }
        public List<PurposeOfUse> purposeOfUses { get; internal set; }
    }
}