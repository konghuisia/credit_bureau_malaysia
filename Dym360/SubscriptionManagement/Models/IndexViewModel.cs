﻿using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class IndexViewModel
    {
        public List<MyTask> MyTasks { get; set; }
        public List<UserAccess> UserAccesses { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
        public bool IsManagementDepartment { get; set; }
    }
}