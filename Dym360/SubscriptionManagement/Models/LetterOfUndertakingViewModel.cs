﻿using ObjectClasses;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class LetterOfUndertakingViewModel
    {
        public bool IsCBM { get; set; }
        public bool HasWriteAccess { get; set; }
        public Request Request { get; set; }
        public CustomerDetails CustomerDetails { get; set; }
        public LetterOfUndertaking LetterOfUndertaking { get; set; }
    }
}