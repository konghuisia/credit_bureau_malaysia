﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class SubscriptionRequestViewModel
    {
        public SubscriptionRequestViewModel()
        {
            Approvers = new List<User>();
            SubscriptionRequest = new SubscriptionRequest();
        }
        public string Access { get; set; }
        public string cameFrom { get; set; }
        public Request Request { get; set; }
        public User CurrentUser { get; set; }
        public IList<User> Users { get; set; }
        public List<User> Approvers { get; set; }
        public SubscriptionRequest SubscriptionRequest { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public bool IsArm { get; set; }
        public bool IsArmHead { get; set; }
        public List<SubscriptionPackages> SubscriptionPackages { get; set; }
        public List<ShowInSelect> ShowInSelects { get; set; }
        public List<PurposeOfUse> PurposeOfUses { get; set; }
        public bool IllegalService { get; set; }
        public DateTime IllegalServiceDate { get; set; }
        public bool UnauthorisedEntity { get; set; }
        public DateTime UnauthorisedEntityDate { get; set; }
        public List<LegalRecommendAction> LegalRecommendActions { get; set; }
        public List<Reason> Reasons { get; set; }
        public OnBoardingChecklist OnBoardingChecklist { get; set; }
        public string RoleId { get; set; }
        public List<Title> Titles { get; set; }
        public CustomerDetails CustomerDetail { get; internal set; }
    }
}