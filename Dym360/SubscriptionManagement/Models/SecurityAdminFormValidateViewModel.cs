﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class SecurityAdminFormValidateViewModel
    {
        public string Password { get; set; }
        public string RequestId { get; set; }
        public string PersonInChargeId { get; set; }
    }
}