﻿using ObjectClasses;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SubscriptionManagement.Models
{
    public class SecurityAdminFormViewModel
    {
        public bool IsCBM { get; set; }
        public Request Request { get; set; }
        public bool HasWriteAccess { get; internal set; }
        public CustomerDetails CustomerDetails { get; set; }
        public PersonInCharge PersonInCharge { get; set; }
    }
}