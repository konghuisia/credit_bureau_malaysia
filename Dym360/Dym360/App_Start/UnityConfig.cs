using ProcessVault.Interfaces;
using ProcessVault.Repositories;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Dym360
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterSingleton<ICustomerRepository, CustomerRepository>();
            container.RegisterSingleton<IOrderRepository, OrderRepository>();
            container.RegisterSingleton<IAccountRepository, AccountRepository>();
            container.RegisterSingleton<ICustomerOrderRepository, CustomerOrderRepository>();
            container.RegisterSingleton<IItemRepository, ItemRepository>();
            container.RegisterSingleton<IBillingRepository, BillingRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}