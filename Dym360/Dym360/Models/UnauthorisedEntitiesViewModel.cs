﻿using System.Collections.Generic;
using ObjectClasses.Configuration;

namespace Dym360.Models
{
    public class UnauthorisedEntitiesViewModel
    {
        public List<UnauthorisedEntities> UnauthorisedEntities { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
    }
}