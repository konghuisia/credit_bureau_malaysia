﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClasses.Configuration;

namespace Dym360.Models
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            User = new User();
        }
        public User User { get; set; }
        public IList<Position> Positions { get; set; }
        public List<Department> Departments { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
    }
}