﻿using ObjectClasses.Configuration;

namespace Dym360.Models
{
    public class UserManagementIndexViewModel
    {
        public string Module { get; set; }
        public User User { get; set; }
    }
}