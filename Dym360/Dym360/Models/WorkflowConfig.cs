﻿using ObjectClasses.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dym360.Models
{
    public class WorkflowConfig
    {
        public string ModuleId { get; set; }
        public string Module { get; set; }
        public List<Workflow> Workflows { get; set; }
        public List<Role> Roles { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public Reference Reference { get; set; }
        public List<WorkflowProcess> WorkflowProcesses { get; set; }
    }
}