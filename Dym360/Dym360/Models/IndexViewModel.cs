﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dym360.Models
{
    public class IndexViewModel
    {
        public User User { get; set; }
        public List<DepartmentAccess> CurrentDepartmentAccesses { get; set; } 
    }
}