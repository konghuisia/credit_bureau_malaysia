﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dym360.Models
{
    public class DepartmentViewModel
    {
        public DepartmentViewModel()
        {
            Department = new Department();
            DepartmentAccesses = new List<DepartmentAccess>();
        }
        public Department Department { get; set; }
        public List<Access> Accesses { get; set; }
        public List<DepartmentAccess> DepartmentAccesses { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
    }
}