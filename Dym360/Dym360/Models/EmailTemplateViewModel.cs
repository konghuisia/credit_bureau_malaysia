﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClasses.Configuration;

namespace Dym360.Models
{
    public class EmailTemplateViewModel
    {
        public EmailTemplate EmailTemplate { get; set; }
        public List<EmailTemplate> EmailTemplates { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
    }
}