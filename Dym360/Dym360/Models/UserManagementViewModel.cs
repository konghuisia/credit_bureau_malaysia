﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClasses.Configuration;

namespace Dym360.Models
{
    public class UserManagementViewModel
    {
        public List<Department> Departments { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
        public IList<User> Users { get; set; }
    }
}