﻿using ObjectClasses.Configuration;
using System.Collections.Generic;

namespace Dym360.Models
{
    public class IllegalMoneyServicesViewModel
    {
        public List<IllegalServices> IllegalServices { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; set; }
    }
}