﻿function LoadConfigPage(url, module) {
    $('#ConfigPage').load(url, { "module": module });
}
function LoadWorkflowConfig(url, id) {
    $('#ConfigPage').load(url, { "moduleId": id });
}
function UpdateWorkflow(id) {
    var data = null;
    data = {
        "workflow": {
            "WorkflowId": id,
            "RoleId": $('#inptRole option:selected').val(),
            "ActionId": $('#inptAction option:selected').val(),
            "NextRoleId": $('#inptNextRole option:selected').val(),
            "SequenceNo": $('#inptSequence').val(),
            "Percentage": $('#inptPercentage').val(),
        }
    }
    if (data !== null) {
        $.ajax({
            url: window.location.origin + "/Module/SaveModuleConfig",
            data: { "module": module, "obj": data },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}
function SaveConfig(module) {
    var data = null;
    switch (module) {
        case "modules":
            data = { "Name": $('#inptName').val(), "Abbreviation": $('#inptAbbreviation').val(), "Description": $('#inptDescription').val() };
            break;
    }
    if (data !== null) {
        $.ajax({
            url: window.location.origin + "/Module/SaveModuleConfig",
            data: { "module": module, "obj": data },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function SavePrefix(id) {
    $.ajax({
        url: window.location.origin + "/Module/SaveModulePrefix",
        data: {
            "reference": {
                "ModuleId": id,
                "Prefix": $('#inptPrefix').val(),
                "Length": $('#inptLength').val()
            }
        },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}
function showToast(msg, type) {
    if (type === "success") {
        $.toast({
            heading: 'Success',
            text: msg,
            position: 'top-roght',
            stack: false
        });
    }
    else if (type === "error") {
        $.toast({
            heading: 'Error',
            text: msg,
            position: 'top-roght',
            icon: 'error'
        });
    }
}

function Convert() {
    $.ajax({
        url: window.location.origin + "/Configuration/Convert",
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {

        }
    });
}

function SetActionsInput() {
    $('#intpWorkflowProcess').change(function () {
        $.ajax({
            url: window.location.origin + "/Configuration/GetRoleActions",
            data: { "workflowProcessId": $(this).val() },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);

                var options = '';
                for (var i = 0; i < d.length; i++) {
                    options += '<option value="' + d[i].ActionID + '">' + d[i].ActionName + '</option>';
                }
                $('#inptAction')[0].innerHTML = options;
            }
        });
    });
}
