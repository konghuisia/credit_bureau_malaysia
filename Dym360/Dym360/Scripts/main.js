﻿
function initMain() {
    initRestriction();
    bindSaveButtonClick();
}
function initRestriction() {
    if ($('#lastlogindate').val() === '') {
        $('#updatePassword').css('display', 'block');
        $('#updatePassword').toggleClass('in');
        $('<div class="modal-backdrop fade in"></div>').insertAfter('#updatePassword');
    }

    $('#inptPass').blur(function () {
        if (validatePasswordPattern($(this).val())) {
            if ($(this).next().length < 1) {
                $('<p class="invalid-alert">Please enter a valid password to proceed.</p>').insertAfter($(this));
            }
        }
        else {
            if ($(this).next().length > 0) {
                $(this).next().remove();
            }
            $('#inptPass1').blur();
        }
        validatePassword();
    });

    $('#inptPass1').blur(function () {
        if ($(this).val() !== $('#inptPass').val()) {
            if ($(this).next().length < 1) {
                $('<p class="invalid-alert">Password doesn&acute;t match.</p>').insertAfter($(this));
            }
        }
        else {
            if ($(this).next().length > 0) {
                $(this).next().remove();
            }
        }
        validatePassword();
    });
}

function bindSaveButtonClick() {
    $('#btnUpdate').click(function () {
        var p = $('#inptPass').val().trim();
        var p1 = $('#inptPass1').val().trim();

        var url = "ChangePassword";
        var data;
        if ($('#lastlogindate').val() === '') {
            url = "UpdatePassword";
            data = { "pass": p };
        }
        else {
            data = { 'curPass': $('#inptCurPass').val().trim(), 'pass': p };
        }

        if (p === p1 && !validatePasswordPattern(p)) {
            $.ajax({
                url: window.location.origin + "/Home/" + url,
                data: data,
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        $('#updatePassword').css('display', 'none');
                        $('#updatePassword').toggleClass('in');
                        $('#updatePassword').next().remove();
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function validatePasswordPattern(p) {
    var regx = new RegExp('^(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*|[a-zA-Z0-9]*)$');
    return regx.test(p);
}

function validatePassword() {
    if ($('#inptPass').val() === $('#inptPass1').val()) {
        $('#btnUpdate').prop('disabled', false);
    }
    else {
        $('#btnUpdate').prop('disabled', true);
    }
}
function setCookie(name, value) {
    document.cookie = name + "=" + (value || "") + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

function restrictCapitalLetter(keydownEvent) {
    var e = keydownEvent;
    if (e < 65 || e > 90) return;
}
function trimSpacing(e) {
    $(e).val($(e).val().trim());
}

function convertToLower(e) {
    $(e).val($(e).val().toLowerCase());
}
function restrictAlphabetInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('.') > 0) {
        if (e.key === '.') {
            return false;
        }
    }
    if (e.shiftKey && e.keyCode === 190) {
        e.preventDefault();
    }

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190, 32]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: alias @
        (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    if ((e.shiftKey && (e.keyCode === 190)) || (e.keyCode > 64 && e.keyCode < 91) || (e.keyCode > 96 && e.keyCode < 123) || e.keyCode === 8) {
        return;
    }
    else {
        e.preventDefault();
    }
}
function restrictEmailInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('@') > -1) {
        if (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) {
            e.preventDefault();
        }
    }
    // Allow: backspace, delete, tab, escape, enter , dash and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 189, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: alias @
        (e.keyCode === 50 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: underscore _
        (e.keyCode === 189 && (e.shiftKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 90)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function sortTable(e) {
    var table, rows, switching, i, x, y, shouldSwitch, type, elemId, col;
    table = document.getElementById($(e).parent().parent().parent().parent().prop('id'));
    type = $(e).data('type');
    elemId = $(e).prop('id');
    col = $(e).parent().index();
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            //rows[i].getElementsByTagName("td")[0].innerHTML = i;

            x = rows[i].getElementsByTagName("td")[col];
            y = rows[i + 1].getElementsByTagName("td")[col];
            //check if the two rows should switch place:
            if (type === 'asc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            else {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
    if (type === 'asc') {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="desc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-bottom"></span>');
    }
    else {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="asc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-top"></span>');
    }
}


function restrictIntegerInput(keydownEvent) {
    var e = keydownEvent;

    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}