﻿function GetWO() {
    $.ajax({
        url: "https://dym360mobile.azurewebsites.net/Home/GetCompleteWorkOrder",
        cache: false,
        type: "POST",
        success: function (data, textStatus, XMLHttpRequest) {
            var wo = JSON.parse(data.WO);

            $('#WONo').text(wo.WorkOrder.WONo);
            $('#WRDate').text(wo.RequestDate);
            $('#ClinicName').text(wo.Clinic.ClinicName);
            $('#WODate').text(wo.WorkOrder.WODate);
            $('#BENo').text(wo.Asset.BENo);
            $('#BECategory').text(wo.Asset.BECategory);
            var audits = wo.Audits;
            for (var i = 0; i < audits.length; i++) {
                var a = audits[i];
                if (a.Action === 'Responded') {
                    $('#WORespondedDate').text(a.ActionDate);
                    $('#WRRespondedDate').text(a.ActionDate);
                }
                if (a.Action === 'Completed') {
                    $('#WOCompletedDate').text(a.ActionDate);
                }
            }
            $('#ProblemCode').text(wo.WorkOrder.Detail.ProblemCode);
            $('#EngName').text(wo.WorkOrder.Detail.AssignTo);
        }
    });
}