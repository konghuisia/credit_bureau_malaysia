﻿function inputValidation() {
    var isValid = true;

    if ($('#CreateName').val() === '') {
        if ($('#CreateName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#CreateName');
        }
        isValid = false;
    }
    else {
        if ($('#CreateName').next('.invalid-alert').length > 0) {
            $('#CreateName').next('.invalid-alert').remove();
        }
    }

    if ($('#CreateAddress').val() === '') {
        if ($('#CreateAddress').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter('#CreateAddress');
        }
        isValid = false;
    }
    else {
        if ($('#CreateAddress').next('.invalid-alert').length > 0) {
            $('#CreateAddress').next('.invalid-alert').remove();
        }
    }

    if ($('#CreateDateAdded').val() === '') {
        if ($('#CreateDateAdded').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter date to proceed.</label>').insertAfter('#CreateDateAdded');
        }
        isValid = false;
    }
    else {
        if ($('#CreateDateAdded').next('.invalid-alert').length > 0) {
            $('#CreateDateAdded').next('.invalid-alert').remove();
        }
    }

    return isValid;
}

function CreateIllegalServices() {
    if (inputValidation()) {


        var ue = {
            "Name": $('#CreateName').val(),
            "Address": $('#CreateAddress').val(),
            "DateOfWarningLetterSent": $('#CreateDateOfWarning').val()
        };

        $.ajax({
            url: window.location.origin + "/Configuration/CreateIllegalServices",
            data: { "ue": ue },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        $("#right-panel").load("Configuration/LoadConfigurationPage/?module=_IllegalMoneyServices");
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function deleteIllegalServices(e) {
    if (confirm("Please click OK to confirm delete.")) {
        $.ajax({
            url: window.location.origin + "/Configuration/DeleteIllegalServices",
            data: { "IllegalServicesId": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        $("#right-panel").load("Configuration/LoadConfigurationPage/?module=_IllegalMoneyServices");
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}


function editIllegalServices(e) {
    var id = $(e).data('value');
    var name = $(e).data('name');
    var address = $(e).data('address');
    var date = $(e).data('date');

    $(e).parent().parent().children("td:nth-child(2)").html('<input type="text" class="form-control" value="' + name + '" id="UpdateName">');
    $(e).parent().parent().children("td:nth-child(3)").html('<input type="text" class="form-control" value="' + address + '" id="UpdateAddress">');
    $(e).parent().parent().children("td:nth-child(4)").html('<input type="text" id="UpdateDateAdded" class="form-control datepickerRestrictFutureUpdate" style="text-align:center" value="' + date + '" />');
    $(e).parent().parent().children("td:nth-child(5)").html('<a data-value=' + id + ' data-name="' + name + '" data-address="' + address + '" data-date="' + date + '" class="btn btn-xs" style="color:green; padding:0px" title="Update" onclick="updateIllegalServices(this)"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></a> <a data-value=' + id + ' data-name="' + name + '" data-address="' + address + '" data-date="' + date + '" class="btn btn-xs" style="color:red; padding:0px;" title="Close" onclick="closeIllegalServices(this)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>');
}

function closeIllegalServices(e) {
    var id = $(e).data('value');
    var name = $(e).data('name');
    var address = $(e).data('address');
    var date = $(e).data('date');

    $(e).parent().parent().children("td:nth-child(2)").html(name);
    $(e).parent().parent().children("td:nth-child(3)").html(address);
    $(e).parent().parent().children("td:nth-child(4)").html(date);

    
    if ($('.delete').length > 0) {
        $(e).parent().parent().children("td:nth-child(5)").html('<a onclick="editIllegalServices(this)" class="edit" data-value=' + id + ' data-name="' + name + '" data-address="' + address + '" data-date="' + date + '" title="Edit" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a> <a onclick="deleteIllegalServices(this);" class="delete" data-value=' + id + ' title="Delete" data-toggle="tooltip" style="padding-left:5px;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>');
    }
    else {
        $(e).parent().parent().children("td:nth-child(5)").html('<a onclick="editIllegalServices(this)" class="edit" data-value=' + id + ' data-name="' + name + '" data-address="' + address + '" data-date="' + date + '" title="Edit" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>');
    }

}

function updateIllegalServices(e) {
    var name = $(e).parent().parent().children("td:nth-child(2)").children('input[type=text]').val();
    var address = $(e).parent().parent().children("td:nth-child(3)").children('input[type=text]').val();
    var date = $(e).parent().parent().children("td:nth-child(4)").children('input[type=text]').val();

    var isValid = true;

    if (name === '') {
        if ($(e).parent().parent().children("td:nth-child(2)").children('input[type=text]').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter($(e).parent().parent().children("td:nth-child(2)").children('input[type=text]'));
        }
        isValid = false;
    }
    else {
        if ($(e).parent().parent().children("td:nth-child(2)").children('input[type=text]').next('.invalid-alert').length > 0) {
            $(e).parent().parent().children("td:nth-child(2)").children('input[type=text]').next('.invalid-alert').remove();
        }
    }

    if (address === '') {
        if ($(e).parent().parent().children("td:nth-child(3)").children('input[type=text]').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter address to proceed.</label>').insertAfter($(e).parent().parent().children("td:nth-child(3)").children('input[type=text]'));
        }
        isValid = false;
    }
    else {
        if ($(e).parent().parent().children("td:nth-child(3)").children('input[type=text]').next('.invalid-alert').length > 0) {
            $(e).parent().parent().children("td:nth-child(3)").children('input[type=text]').next('.invalid-alert').remove();
        }
    }

    if (date === '') {
        if ($(e).parent().parent().children("td:nth-child(4)").children('input[type=text]').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter date to proceed.</label>').insertAfter($(e).parent().parent().children("td:nth-child(4)").children('input[type=text]'));
        }
        isValid = false;
    }
    else {
        if ($(e).parent().parent().children("td:nth-child(4)").children('input[type=text]').next('.invalid-alert').length > 0) {
            $(e).parent().parent().children("td:nth-child(4)").children('input[type=text]').next('.invalid-alert').remove();
        }
    }

    if (isValid) {
        $(e).data('name', name);
        $(e).data('address', address);
        $(e).data('date', date);

        var ue = {
            "Name": name,
            "Address": address,
            "DateOfWarningLetterSent": date,
            "IllegalServicesId": $(e).data('value')
        };

        $.ajax({
            url: window.location.origin + "/Configuration/UpdateIllegalServices",
            data: { "ue": ue },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        closeIllegalServices(e);
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}