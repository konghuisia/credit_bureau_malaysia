﻿function init() {
    bindSaveEmailTemplateButtonClick();
    //initInputRestriction();
}

//function initInputRestriction() {
//$('.number').keydown(function (e) {
//    restrictDecimalInput(e);
//});

//$('.number').blur(function (e) {
//    if ($(e.target).val().trim() === ".") {
//        $(e.target).val('');
//    }
//    if ($(e.target).val().trim() !== "") {
//        var t = $(e.target).val().replace(/\,/g, '');
//        $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
//    }
//    if ($(e.target).val().trim() === "") {
//        $(e.target).val('0.00');
//    }
//});

//$('#inptName').keydown(function (e) {
//    restrictAlphabetInput(e);
//});
//$('#inptName').blur(function () {
//    trimSpacing(this);
//});

//$('#inptEmail').keydown(function (e) {
//    restrictEmailInput(e);
//});
//$('#inptEmail').blur(function () {
//    if ($(this).val().trim() !== '') {
//        if (validateEmail($(this).val())) {
//            if ($(this).next().length > 0) {
//                $(this).next().remove();
//            }
//        } else {
//            $(this).next().remove();
//            if ($(this).next().length < 1) {
//                $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
//            }
//        }
//    }
//    else {
//        $(this).next().remove();
//    }
//});

//$('#inptUsername').keydown(function (e) {
//    restrictCapitalLetter(e);
//});
//$('#inptUsername').blur(function () {
//    trimSpacing(this);
//    convertToLower(this);
//});
//$('#inptRemark').keydown(function (e) {
//    restrictAlphanumericInput(e);
//});
//}


function bindSaveEmailTemplateButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {


            var emailTemplate = {
                "EmailTemplateId": $('#EmailTemplateId').val(),
                "ModuleId": $('#ModuleId').val(),
                "Name": $('#inptName').val(),
                "Description": $('#inptDescription').val(),
                "Active": $('[name=status]:checked').val(),
                "Subject": encodeURIComponent($('#inptSubject').val()),
                "Body": encodeURIComponent($('#inptBody').val())
            };


            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/Configuration/" + url + "EmailTemplate",
                data: { "emailTemplate": emailTemplate },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        //if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = "http://localhost:52251/Configuration/Index?module=Email"
                            //window.location.href = window.location.origin + d.RedirectUrl;
                            //window.location.href = "http://42.1.62.141:98/Configuration"
                        }, 2000);
                        //}
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#inptName').val() === '') {
        if ($('#inptName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
        }
        isValid = false;
    }
    else {
        if ($('#inptName').next('.invalid-alert').length > 0) {
            $('#inptName').next('.invalid-alert').remove();
        }
    }

    return isValid;
}

function clearInput() {
    $('#inptName').val('');
    $('#inptDescription').val('');
    $('#inptSubject').val('');
    $('#inptBody').val('');
    $($('[name=status]')[1]).prop('checked', true);
}