﻿function init() {
    bindSaveAccountButtonClick();
    initInputRestriction();
    //initResetPasswordButtonClick();
}
function initInputRestriction() {
    $('#inptName').keydown(function (e) {
        restrictAlphanumericInput(e);
    });
    //$('#inptName').blur(function () {
    //    trimSpacing(this);
    //});

    //$('#inptEmail').keydown(function (e) {
    //    restrictEmailInput(e);
    //});
    //$('#inptEmail').blur(function () {
    //    if ($(this).val().trim() !== '') {
    //        if (validateEmail($(this).val())) {
    //            if ($(this).next().length > 0) {
    //                $(this).next().remove();
    //            }
    //        } else {
    //            $(this).next().remove();
    //            if ($(this).next().length < 1) {
    //                $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
    //            }
    //        }
    //    }
    //    else {
    //        $(this).next().remove();
    //    }
    //});

    //$('#inptUsername').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
    //$('#inptUsername').blur(function () {
    //    trimSpacing(this);
    //});
    $('#inptRemark').keydown(function (e) {
        restrictAlphanumericInput(e);
    });
}

//function validateCheck(e) {
//    var ctrl = $(e);
//    var tr = ctrl.parent().parent();

//    var re, ed, ad;

//    if (ctrl.prop('checked')) {
//        switch (ctrl.prop('name')) {
//            case "readonly":
//                ed = tr.find('[name=editor]');
//                if (ed.prop('checked')) ed.prop('checked', false);

//                ad = tr.find('[name=admin]');
//                if (ad.prop('checked')) ad.prop('checked', false);
//                break;
//            case "editor":
//                re = tr.find('[name=readonly]');
//                if (re.prop('checked')) re.prop('checked', false);

//                ad = tr.find('[name=admin]');
//                if (ad.prop('checked')) ad.prop('checked', false);
//                break;
//            case "admin":
//                re = tr.find('[name=readonly]');
//                if (re.prop('checked')) re.prop('checked', false);

//                ed = tr.find('[name=editor]');
//                if (ed.prop('checked')) ed.prop('checked', false);
//                break;
//        }
//    }
//}

function bindSaveAccountButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {
            var accesses = [];
            $('#tblDepartmentAccess tbody tr').each(function () {
                var tds = $(this).find('td');
                var readonly = $(tds[1]).find('[name=readonly]').prop('checked');
                var create = $(tds[2]).find('[name=create]').prop('checked');
                var update = $(tds[3]).find('[name=update]').prop('checked');
                var deleteonly = $(tds[4]).find('[name=delete]').prop('checked');

                var c = false, r = false, u = false, d = false;

                if (readonly) {
                    r = true;
                }
                if (create) {
                    c = true;
                }
                if (update) {
                    u = true;
                }
                if (deleteonly) {
                    d = true;
                }

                accesses.push({ "AccessId": $(tds[0]).data('access'), "IsCreate": c, "IsRead": r, "IsUpdate": u, "IsDelete": d });
            });

            var department = {
                "DepartmentId": $('#DepartmentId').val(),
                "Name": $('#inptName').val(),
                "Description": $('#inptRemark').val(),
                "Abbreviation": $('#inptAbbreviation').val()
            };

            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/UserManagement/" + url + "Department",
                data: { "department": department, "access": accesses },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#inptName').val() === '') {
        if ($('#inptName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
        }
        isValid = false;
    }
    else {
        if ($('#inptName').next('.invalid-alert').length > 0) {
            $('#inptName').next('.invalid-alert').remove();
        }
    }
    return isValid;
}

//function initResetPasswordButtonClick() {
//    $('#btnResetPassword').click(function () {
//        if (confirm("Please click OK to confirm delete.")) {
//            $.ajax({
//                url: window.location.origin + "/Account/Reset",
//                data: { "userId": $('#userId').val() },
//                cache: false,
//                type: "POST",
//                dataType: "html",
//                success: function (data, textStatus, XMLHttpRequest) {
//                    var d = JSON.parse(data);
//                    if (d.Error === null) {
//                        showToast(d.Message, "success");
//                        if (d.RedirectUrl !== null) {
//                            setTimeout(function () {
//                                window.location.href = window.location.origin + d.RedirectUrl;
//                            }, 2000);
//                        }
//                    }
//                    else {
//                        showToast(d.Error, "error");
//                    }
//                }
//            });
//        }
//    });
//}

function clearInput() {
    $('#inptName').val('');
    $('#inptRemark').val('');
    $($('[name=status]')[0]).prop('checked', true);
    $('[type=checkbox]').prop('checked', false);
    $('textarea').val('');
}
