﻿function init() {
    bindSaveAccountButtonClick();
    initInputRestriction();
    initResetPasswordButtonClick();

    $('#inptUsername').blur(function () {
        if ($('#btnSave').data('state') === 'CreateNew') {
            checkUserId();
        }
    });

    //$('#inptInitial').blur(function () {
    //    $('#inptInitial').val($('#inptInitial').val().toUpperCase());
    //    checkUserInitial();
    //});
}

function initInputRestriction() {
    $('.number').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.number').blur(function (e) {
        if ($(e.target).val().trim() === ".") {
            $(e.target).val('');
        }
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
        if ($(e.target).val().trim() === "") {
            $(e.target).val('0.00');
        }
    });

    $('#inptName').keydown(function (e) {
        restrictAlphabetInput(e);
    });
    $('#inptName').blur(function () {
        trimSpacing(this);
    });

    $('#inptEmail').keydown(function (e) {
        restrictEmailInput(e);
    });
    $('#inptEmail').blur(function () {
        if ($(this).val().trim() !== '') {
            if (validateEmail($(this).val())) {
                if ($(this).next().length > 0) {
                    $(this).next().remove();
                }
            } else {
                $(this).next().remove();
                if ($(this).next().length < 1) {
                    $('<p class="invalid-alert">Please enter a valid email to proceed.</p>').insertAfter($(this));
                }
            }
        }
        else {
            $(this).next().remove();
        }
    });

    $('#inptUsername').keydown(function (e) {
        restrictCapitalLetter(e);
    });
    $('#inptUsername').blur(function () {
        trimSpacing(this);
        convertToLower(this);
    });
    //$('#inptRemark').keydown(function (e) {
    //    restrictAlphanumericInput(e);
    //});
}


function bindSaveAccountButtonClick() {
    $('#btnSave').click(function () {
        if (inputValidation()) {


            var user = {
                "UserId": $('#userId').val(),
                "Username": $('#inptUsername').val(),
                "Name": $('#inptName').val(),
                "Email": $('#inptEmail').val(),
                "Remark": $('#inptRemark').val(),
                "Status": $('[name=status]:checked').val(),
                "Position": $('#inptPosition option:selected').val(),
                "Department": $('#inptDepartment option:selected').val(),
                //"Initial": $('#inptInitial').val(),
                //"ProfileId": $('#inptProfile option:selected').val(),
                "Oldname": $('#oldName').val(),
            };
            

            var url = $(this).data('state') === 'Update' ? 'Update' : 'CreateNew';

            $.ajax({
                url: window.location.origin + "/UserManagement/" + url + "User",
                data: { "user": user },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                window.location.href = window.location.origin + d.RedirectUrl;
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function inputValidation() {
    var isValid = true;

    if ($('#inptEmail').val() === '') {
        if ($('#inptEmail').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter email to proceed.</label>').insertAfter('#inptEmail');
        }
        isValid = false;
    }
    else {
        if (validateEmail($('#inptEmail').val())) {
            if ($('#inptEmail').next('.invalid-alert').length > 0) {
                $('#inptEmail').next('.invalid-alert').remove();
            }
        }
        else {
            isValid = false;
        }
    }

    if ($('#inptName').val() === '') {
        if ($('#inptName').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter name to proceed.</label>').insertAfter('#inptName');
        }
        isValid = false;
    }
    else {
        if ($('#inptName').next('.invalid-alert').length > 0) {
            $('#inptName').next('.invalid-alert').remove();
        }
    }

    //if ($('#inptInitial').val() === '') {
    //    if ($('#inptInitial').next('.invalid-alert').length < 1) {
    //        $('<label class="invalid-alert">Please enter initial to proceed.</label>').insertAfter('#inptInitial');
    //    }
    //    isValid = false;
    //}
    //else {
    //    if ($('#inptInitial').next('.invalid-alert').length > 0) {
    //        $('#inptInitial').next('.invalid-alert').remove();
    //    }

    //    checkUserInitial();
    //    if ($('#UserInitialExist').val() === "true") {
    //        isValid = false;
    //    }
    //}

    if ($('#inptUsername').val() === '') {
        if ($('#inptUsername').next('.invalid-alert').length < 1) {
            $('<label class="invalid-alert">Please enter user id to proceed.</label>').insertAfter('#inptUsername');
        }
        isValid = false;
    }
    else {
        if ($('#inptUsername').next('.invalid-alert').length > 0) {
            $('#inptUsername').next('.invalid-alert').remove();
        }
    }

    if ($('#btnSave').data('state') === 'CreateNew' && $('#inptUsername').val() !== '') {
        checkUserId();
        if ($('#UsernameExist').val() === "true") {
            isValid = false;
        }
    }

    return isValid;
}

function initResetPasswordButtonClick() {
    $('#btnResetPassword').click(function () {
        if (confirm("Please click OK to reset password.")) {
            $.ajax({
                url: window.location.origin + "/UserManagement/ResetUserPassword",
                data: { "userId": $('#userId').val() },
                cache: false,
                type: "POST",
                dataType: "html",
                success: function (data, textStatus, XMLHttpRequest) {
                    var d = JSON.parse(data);
                    if (d.Error === null) {
                        showToast(d.Message, "success");
                        if (d.RedirectUrl !== null) {
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                    }
                    else {
                        showToast(d.Error, "error");
                    }
                }
            });
        }
    });
}

function clearInput() {
    $('#inptUsername').val('');
    $('#inptName').val('');
    $('#inptGroup').val('');
    $('#inptPosition').val('');
    $('#inptProfile').val('');
    $('#inptEmail').val('');
    $('#inptRemark').val('');
    $($('[name=status]')[0]).prop('checked', true);
    $('[type=checkbox]').prop('checked', false);
    $('textarea').val('');
}

function checkUserId() {
    $.ajax({
        url: window.location.origin + "/UserManagement/CheckUserName",
        data: { "username": $('#inptUsername').val() },
        cache: false,
        type: "POST",
        dataType: "html",
        success: function (data, textStatus, XMLHttpRequest) {
            if (data === "true") {
                //Email available.
                if ($('#inptUsername').next('.invalid-alert').length < 1) {
                    $('<label class="invalid-alert">User ID taken. Please try again.</label>').insertAfter('#inptUsername');
                }
                else {
                    $('#inptUsername').next('.invalid-alert').remove();
                    $('<label class="invalid-alert">User ID taken. Please try again.</label>').insertAfter('#inptUsername');
                }
                $('#UsernameExist').val('true');
            }
            else {
                if ($('#inptUsername').next('.invalid-alert').length > 0) {
                    $('#inptUsername').next('.invalid-alert').remove();
                }
                $('#UsernameExist').val('false');
            }
        }
    });
}

//function checkUserInitial() {
//    $.ajax({
//        url: window.location.origin + "/Confirguration/CheckUserInitial",
//        data: { "initial": $('#inptInitial').val(), "userid": $('#userId').val() },
//        cache: false,
//        type: "POST",
//        dataType: "html",
//        success: function (data, textStatus, XMLHttpRequest) {
//            if (data === "true") {
//                //Email available.
//                if ($('#inptInitial').next('.invalid-alert').length < 1) {
//                    $('<label class="invalid-alert">Initial taken. Please try again.</label>').insertAfter('#inptInitial');
//                }
//                else {
//                    $('#inptInitial').next('.invalid-alert').remove();
//                    $('<label class="invalid-alert">Initial taken. Please try again.</label>').insertAfter('#inptInitial');
//                }
//                $('#UserInitialExist').val('true');
//            }
//            else {
//                if ($('#inptInitial').next('.invalid-alert').length > 0) {
//                    $('#inptInitial').next('.invalid-alert').remove();
//                }
//                $('#UserInitialExist').val('false');
//            }
//        }
//    });
//}