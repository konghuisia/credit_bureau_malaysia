﻿using DataAccessHelper.Configuration.Store;
using DataAccessHelper.ConfigurationHelper.Store;
using Dym360.Models;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Dym360.Controllers
{
    public class ConfigurationController : Controller
    {
        private RoleStore _roleStore;
        private ModuleStore _moduleStore;
        private PositionStore _positionStore;
        private WorkflowStore _workflowStore;
        private ReferenceStore _referenceStore;
        private AccessTypeStore _accessTypeStore;
        private EmailTemplateStore EmailTemplateStore;
        private IllegalServicesStore IllegalServicesStore;
        private DepartmentAccessStore DepartmentAccessStore;
        private UnauthorisedEntitiesStore UnauthorisedEntitiesStore;

        private static IList<RoleAction> roleActions;
        private static IList<Module> modules;
        private static IList<WorkflowProcess> workflowProcesses;

        public ConfigurationController()
        {
            _roleStore = DependencyResolver.Current.GetService<RoleStore>();
            _moduleStore = DependencyResolver.Current.GetService<ModuleStore>();
            _positionStore = DependencyResolver.Current.GetService<PositionStore>();
            _workflowStore = DependencyResolver.Current.GetService<WorkflowStore>();
            _referenceStore = DependencyResolver.Current.GetService<ReferenceStore>();
            _accessTypeStore = DependencyResolver.Current.GetService<AccessTypeStore>();
            EmailTemplateStore = DependencyResolver.Current.GetService<EmailTemplateStore>();
            IllegalServicesStore = DependencyResolver.Current.GetService<IllegalServicesStore>();
            DepartmentAccessStore = DependencyResolver.Current.GetService<DepartmentAccessStore>();
            UnauthorisedEntitiesStore = DependencyResolver.Current.GetService<UnauthorisedEntitiesStore>();
        }
        // GET: Configuration
        public async Task<ActionResult> Index(string module)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.ConfigurationIndexViewModel();
            vm.Module = module;

            if (!string.IsNullOrEmpty(module))
            {
                module = null;
            }

            return View(vm);

            //modules = await _moduleStore.GetAllModule();
            //roleActions = await _roleStore.GetRoleActions();
            //return View();
        }

        public async Task<ActionResult> LoadConfigurationPage(string module)
        {
            switch (module)
            {
                case "modules":
                    return PartialView("_Module", await _moduleStore.GetAllModule());
                case "positions":
                    return PartialView("_Position", await _positionStore.GetAllPosition());
                case "accesstypes":
                    return PartialView("_AccessType", await _accessTypeStore.GetAllAccessType());
                case "roletypes":
                    return PartialView("_RoleType", await _roleStore.GetAllRoleType());
                case "_Email":
                default:
                    var vm = new Models.EmailTemplateViewModel();
                    vm.EmailTemplates = await EmailTemplateStore.GetEmailTemplates();
                    vm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());
                    return PartialView("_Email", vm);
                case "_UnauthorisedEntities":
                    var uevm = new Models.UnauthorisedEntitiesViewModel();
                    uevm.UnauthorisedEntities = await UnauthorisedEntitiesStore.GetUnauthorisedEntities();
                    uevm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());
                    return PartialView("_UnauthorisedEntities", uevm);
                case "_IllegalMoneyServices":
                    var imsvm = new Models.IllegalMoneyServicesViewModel();
                    imsvm.IllegalServices = await IllegalServicesStore.GetIllegalServices();
                    imsvm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());
                    return PartialView("_IllegalMoneyServices", imsvm);
            }
        }
        public async Task<ActionResult> LoadWorkflowConfig(string moduleId)
        {
            IList<Role> roles = await _roleStore.GetRoles("");
            IList<Workflow> workflows = await _workflowStore.GetAllWorkflow(moduleId);
            workflowProcesses = await _workflowStore.GetWorkflowProcesses(moduleId);

            return PartialView("_Workflow", new WorkflowConfig()
            {
                ModuleId = moduleId,
                Module = modules.First(x => x.ModuleId == moduleId).Name,
                Roles = roles.ToList(),
                Workflows = workflows.ToList(),
                RoleActions = roleActions.ToList(),
                Reference = await _referenceStore.GetReference(moduleId),
                WorkflowProcesses = workflowProcesses.ToList()
            });
        }
        public JsonResult GetRoleActions(string workflowProcessId)
        {
            var roleId = workflowProcesses.ToList().Find(x => x.WorkflowProcessId == workflowProcessId).RoleId;
            return Json(roleActions.Where(x => x.RoleId == roleId));
        }
        public async Task<JsonResult> SaveWorkflowConfig(Workflow workflow)
        {
            await _workflowStore.UpdateWorkflow(workflow);

            return Json(new Response()
            {
                Message = "Workflow updated",
                RedirectUrl = "/Configuration"
            });
        }

        #region EmailTemplate
        public async Task<ActionResult> EditEmailTemplate(string EmailTemplateId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }
            return View(new Models.EmailTemplateViewModel()
            {
                EmailTemplate = await EmailTemplateStore.GetEmailTemplateByEmailTemplateId(EmailTemplateId),
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment())
            });
        }
        public async Task<ActionResult> PreviewEmailTemplate(string EmailTemplateId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }
            return View(new Models.EmailTemplateViewModel()
            {
                EmailTemplate = await EmailTemplateStore.GetEmailTemplateByEmailTemplateId(EmailTemplateId),
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment())
            });
        }
        #endregion

        #region JsonEmailTemplate
        public async Task<JsonResult> UpdateEmailTemplate(EmailTemplate emailTemplate)
        {
            var r = new  Response();

            if (!string.IsNullOrEmpty(emailTemplate.EmailTemplateId = await EmailTemplateStore.UpdateEmailTemplate(emailTemplate, Utils.GetUserId())))
            {
                r.Message = "Email template edited successfully.";
            }
            else
            {
                r.Error = "Unable to save email template, please try again later.";
            }
            return Json(r);
        }
        #endregion

        #region UnauthorisedEntities
        public async Task<JsonResult> CreateUnauthorisedEntities (UnauthorisedEntities ue)
        {
            var r = new Response();

            if (!string.IsNullOrEmpty(await UnauthorisedEntitiesStore.CreateUnauthorisedEntities(ue, Utils.GetUserId())))
            {
                r.Message = "Unauthorised entity created successfully.";
            }
            else
            {
                r.Error = "Unable to create unauthorised entity, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateUnauthorisedEntities(UnauthorisedEntities ue)
        {
            var r = new Response();

            if (!string.IsNullOrEmpty(await UnauthorisedEntitiesStore.UpdateUnauthorisedEntities(ue, Utils.GetUserId())))
            {
                r.Message = "Unauthorised entity edited successfully.";
            }
            else
            {
                r.Error = "Unable to save unauthorised entity, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteUnauthorisedEntities(string UnauthorisedEntitiesId)
        {
            var r = new Response();

            if (await UnauthorisedEntitiesStore.DeleteUnauthorisedEntities(UnauthorisedEntitiesId, Utils.GetUserId()))
            {
                r.Message = "Unauthorised entity deleted successfully.";
            }
            else
            {
                r.Error = "Unable to delete unauthorised entity, please try again later.";
            }

            return Json(r);
        }
        #endregion

        #region IllegalServices
        public async Task<JsonResult> CreateIllegalServices(IllegalServices ue)
        {
            var r = new Response();

            if (!string.IsNullOrEmpty(await IllegalServicesStore.CreateIllegalServices(ue, Utils.GetUserId())))
            {
                r.Message = "Illegal service created successfully.";
            }
            else
            {
                r.Error = "Unable to create illegal service, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateIllegalServices(IllegalServices ue)
        {
            var r = new Response();

            if (!string.IsNullOrEmpty(await IllegalServicesStore.UpdateIllegalServices(ue, Utils.GetUserId())))
            {
                r.Message = "Illegal service edited successfully.";
            }
            else
            {
                r.Error = "Unable to save illegal service, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteIllegalServices(string IllegalServicesId)
        {
            var r = new Response();

            if (await IllegalServicesStore.DeleteIllegalServices(IllegalServicesId, Utils.GetUserId()))
            {
                r.Message = "Illegal service deleted successfully.";
            }
            else
            {
                r.Error = "Unable to delete illegal service, please try again later.";
            }

            return Json(r);
        }
        #endregion
    }
}