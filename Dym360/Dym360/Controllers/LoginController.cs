﻿using DataAccessHelper.KumFattWebHelper.Users;
using DataAccessHelper.KumFattWebHelper.Users.Store;
using Dym360.App_Start;
using IdentityManagement;
using IdentityManagement.DAL;
using IdentityManagement.Entities;
using IdentityManagement.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dym360.Controllers
{
    public class LoginController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Logout()
        {
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Login");
        }

        // GET: Login
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            if (Utils.IfUserAuthenticated())
            {
                return RedirectToLocal("");
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginInfo objLogin, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var userStore = DependencyResolver.Current.GetService<UserStore>();
                ApplicationUser oUser = await SignInManager.UserManager.FindByNameAsync(objLogin.UserName);
                if (oUser != null && HashSalt.VerifyPassword(objLogin.Password, oUser.Password, oUser.Salt))
                {
                    switch (oUser.Status)
                    {
                        case EnumUserStatus.Pending:
                            ModelState.AddModelError("", "Error: User account has not been verified.");
                            break;
                        case EnumUserStatus.Active:
                            if (!string.IsNullOrEmpty(oUser.LastLoginDate))
                            {
                                await userStore.UpdateLoginDate(oUser.UserId);
                            }
                            SignInManager.SignIn(oUser, false, false);
                            //IList<string> roleList = UserRoleController.GetUserRoles(oUser.UserId);
                            //foreach (string role in roleList)
                            //{
                            //    UserManager.AddToRole(oUser.UserId, role);
                            //}

                            //if no return url provided then redirect page based on role
                            //if (string.IsNullOrEmpty(returnUrl))
                            //{
                            //    if (roleList.IndexOf("Administrator") >= 0)
                            //    {
                            //        return RedirectToAction("Index", "Admin");
                            //    }
                            //    else
                            //    {
                            //        return RedirectToAction("Index", "Member");
                            //    }
                            //}
                          
                            return RedirectToLocal(returnUrl);
                        case EnumUserStatus.Inactive:
                            ModelState.AddModelError("", "Error: User account is inactive.");
                            break;
                        case EnumUserStatus.Banned:
                            ModelState.AddModelError("", "Error: User account has been banned.");
                            break;
                        case EnumUserStatus.LockedOut:
                            ModelState.AddModelError("", "Error: User account has been locked out due to multiple login tries.");
                            break;
                    }
                }
                else
                {
                    if (oUser != null)
                    {
                        userStore.UpdateLoginAttempt(oUser.UserId);
                        if (oUser.Status == EnumUserStatus.LockedOut)
                        {
                            ModelState.AddModelError("", "Error: User account has been locked out due to multiple login tries.");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Error: Invalid login details.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Error: Invalid login details.");
                    }
                }
            }
            return View(objLogin);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}