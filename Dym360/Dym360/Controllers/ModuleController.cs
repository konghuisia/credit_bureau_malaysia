﻿using DataAccessHelper.Configuration.Store;
using DataAccessHelper.ConfigurationHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dym360.Controllers
{
    public class ModuleController : Controller
    {
        private ModuleStore _moduleController;
        private ReferenceStore _referenceStore;

        public ModuleController()
        {
            _moduleController = DependencyResolver.Current.GetService<ModuleStore>();
            _referenceStore = DependencyResolver.Current.GetService<ReferenceStore>();
        }

        // GET: Module
        public ActionResult Index()
        {
            return PartialView("_Module");
        }

        public async Task<JsonResult> SaveModuleConfig(string module, Module obj)
        {
            var result = new JsonResponse();

            var tmp = obj as Module;
            if (tmp.Name != null || tmp.Name.Trim() != "")
            {
                try
                {
                    tmp.CreatedBy = Utils.GetUserId();
                    await _moduleController.CreateAsync(tmp);
                    result.Message = "Save module completed";
                    result.RedirectUrl = Request.UrlReferrer.AbsoluteUri;
                }
                catch (Exception ex)
                {
                    result.Message = ex.Message;
                }
            }

            return Json(result);
        }

        public async Task<JsonResult> SaveModulePrefix(Reference reference)
        {
            var result = new JsonResponse();

            reference.CreatedBy = Utils.GetUserId();

            var success = await _referenceStore.NewReference(reference);
            
            if (success)
            {
                result.Message = "Module prefix saved";
                result.RedirectUrl = Request.UrlReferrer.AbsoluteUri;
            }
            else
            {
                result.Error = "Unable to proceed, please try again later";
            }
           
            return Json(result);
        }
    }
}