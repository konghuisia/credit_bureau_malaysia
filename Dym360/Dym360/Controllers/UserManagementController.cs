﻿using DataAccessHelper.Configuration.Store;
using DataAccessHelper.ConfigurationHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Dym360.Controllers
{
    public class UserManagementController : Controller
    {
        UserStore UserStore;
        AccessStore AccessStore;
        PositionStore PositionStore;
        DepartmentStore DepartmentStore;
        DepartmentAccessStore DepartmentAccessStore;

        public UserManagementController()
        {
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            AccessStore = DependencyResolver.Current.GetService<AccessStore>();
            PositionStore = DependencyResolver.Current.GetService<PositionStore>();
            DepartmentStore = DependencyResolver.Current.GetService<DepartmentStore>();
            DepartmentAccessStore = DependencyResolver.Current.GetService<DepartmentAccessStore>();
        }

        // GET: UserManagement
        public async Task<ActionResult> Index(string module)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.UserManagementIndexViewModel();
            vm.Module = module;

            if (!string.IsNullOrEmpty(module))
            {
                module = null;
            }

            return View(vm);
        }
        public async Task<ActionResult> LoadPage(string module)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            var vm = new Models.UserManagementViewModel();
            vm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());
            vm.Users = await UserStore.GetAllUser();
            //vm.Users = await UserStore.GetAllUser();
            //vm.CurrentUser = await UserAccessStore.GetUserAccessByUserPositionId(position);

            switch (module)
            {
                case "_Department":
                    vm.Departments = await DepartmentStore.GetAllDepartments();
                    break;
                case "_UserAccount":
                    //vm.UserPositions = await UserPositionStore.GetAllUserPosition();
                    //vm.UserPositions.RemoveAll(x => x.Deleted == true);
                    //vm.Positions = await PositionsStore.GetPositions();
                    break;
            }


            return PartialView(module, vm);
        }

        #region Department
        public async Task<ActionResult> CreateDepartment(string departmentId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View("EditDepartment", new Models.DepartmentViewModel()
            {
                Accesses = await AccessStore.GetAllAccesses(),
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
            });
        }

        public async Task<ActionResult> EditDepartment(string departmentId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Models.DepartmentViewModel()
            {
                Accesses = await AccessStore.GetAllAccesses(),
                Department = await DepartmentStore.GetDepartmentByDepartmentId(departmentId),
                DepartmentAccesses = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(departmentId),
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
            });
        }

        public async Task<ActionResult> PreviewDepartment(string departmentId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Models.DepartmentViewModel()
            {
                Accesses = await AccessStore.GetAllAccesses(),
                Department = await DepartmentStore.GetDepartmentByDepartmentId(departmentId),
                DepartmentAccesses = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(departmentId),
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
            });
        }
        #endregion

        #region User
        public async Task<ActionResult> CreateUser()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View("EditUser", new Models.UserViewModel() {
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
                Departments = await DepartmentStore.GetAllDepartments(),
                Positions = await PositionStore.GetAllPosition()
            });
        }
        public async Task<ActionResult> EditUser(string userId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Models.UserViewModel()
            {
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
                Departments = await DepartmentStore.GetAllDepartments(),
                Positions = await PositionStore.GetAllPosition(),
                User = await UserStore.GetUser(userId),
            });
        }
        public async Task<ActionResult> PreviewUser(string userId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login");
            }

            return View(new Models.UserViewModel()
            {
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
                Departments = await DepartmentStore.GetAllDepartments(),
                Positions = await PositionStore.GetAllPosition(),
                User = await UserStore.GetUser(userId),
            });
        }
        #endregion

        #region JsonDepartment 
        public async Task<JsonResult> CreateNewDepartment(Department department, List<DepartmentAccess> access)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            department = await DepartmentStore.CreateNewDepartment(department, id);

            if (!string.IsNullOrEmpty(department.DepartmentId))
            {
                bool b = await DepartmentAccessStore.CreateDepartmentAccess(access, id, department.DepartmentId);

                if (b)
                {
                    r.Message = "Department saved successfully.";
                    r.RedirectUrl = "/UserManagement?module=Department";
                }
            }
            else
            {
                r.Error = "Unable to save department, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateDepartment(Department department, List<DepartmentAccess> access)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            var a = await DepartmentStore.UpdateDepartment(department, id);
            bool b = await DepartmentAccessStore.UpdateDepartmentAccess(access, id, department.DepartmentId);

            if (!string.IsNullOrEmpty(a.DepartmentId) && b)
            {
                r.Message = "Department edited successfully.";
            }
            else
            {
                r.Error = "Unable to save department, please try again later.";
            }

            return Json(r);
        }

        public async Task<JsonResult> DeleteDepartment(string departmentId)
        {
            string id = Utils.GetUserId();
            var r = new Response();

            if (await DepartmentStore.DeleteDepartment(departmentId, id))
            {
                r.Message = "Department deleted successfully.";
            }
            else
            {
                r.Error = "Unable to delete department, please try again later.";
            }

            return Json(r);
        }
        #endregion

        #region JsonUserAccount
        public async Task<JsonResult> CreateNewUser(User user)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool exist = await UserStore.CheckUserName(user.Username);

            if (!exist)
            {
                user.Source = "CBM";
                user.UserId = await UserStore.CreateNewUser(user, id);

                if (!string.IsNullOrEmpty(user.UserId))
                {
                    r.Message = "User account saved successfully.";
                    r.RedirectUrl = "/UserManagement?module=UserAccount";
                }
                else
                {
                    r.Error = "Unable to save user account, please try again later.";
                }
            }
            else
            {
                r.Error = "User id taken, please try again.";
            }

            return Json(r);
        }
        public async Task<JsonResult> UpdateUser(User user)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool existed = true;

            if (user.Oldname != user.Username)
            {
                existed = await UserStore.CheckUserName(user.Username);
            }
            else
            {
                existed = false;
            }

            if (!existed)
            {
                user.Source = "CBM";
                user.UserId = await UserStore.UpdateUser(user, id);

                if (!string.IsNullOrEmpty(user.UserId))
                {
                    r.Message = "User account saved successfully.";
                }
                else
                {
                    r.Error = "Unable to save user account, please try again later.";
                }
            }
            else
            {
                r.Error = "User id taken, please try again.";
            }

            return Json(r);
        }
        public async Task<JsonResult> ResetUserPassword(string userId)
        {
            string id = Utils.GetUserId();
            var r = new Response();
            bool b = await UserStore.ResetPassword(userId , "", id);

            if (b)
            {
                r.Message = "Password saved successfully.";
            }
            else
            {
                r.Error = "Unable to reset password, please try again later.";
            }

            return Json(r);
        }
        public async Task<JsonResult> DeleteUserAccount(string userId)
        {
            var r = new Response();

            if (await UserStore.DeleteUser(userId, Utils.GetUserId()))
            {
                r.Message = "User account deleted successfully.";
            }
            else
            {
                r.Error = "Unable to delete user account, please try again later.";
            }

            return Json(r);
        }

        public async Task<JsonResult> CheckUserName(string username)
        {
            return Json(await UserStore.CheckUserName(username));
        }

        //public async Task<JsonResult> CheckUserInitial(string initial, string userid)
        //{
        //    return Json(await UserStore.CheckUserInitial(initial, userid));
        //}
        #endregion
    }
}