﻿
using DataAccessHelper.ConfigurationHelper.Store;
using Dym360.App_Start;
using IdentityManagement.Entities;
using IdentityManagement.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Dym360.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        UserStore userStore;
        UserAccessStore UserAccessStore;
        DepartmentAccessStore DepartmentAccessStore;

        public HomeController()
        {
            userStore = DependencyResolver.Current.GetService<UserStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            DepartmentAccessStore = DependencyResolver.Current.GetService<DepartmentAccessStore>();
        }
        public async Task<ActionResult> Index()
        {
            if (Utils.IfUserAuthenticated())
            {
                var vm = new Models.IndexViewModel();
                vm.User = await userStore.GetUser(Utils.GetUserId());
                vm.CurrentDepartmentAccesses = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());
                return View(vm);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public async Task<JsonResult> UpdatePassword(string pass)
        {
            var username = Utils.GetUsername();
            var resp = new Response();
           if (await userStore.UpdatePassword(new User() { UserId = Utils.GetUserId() , Password = pass, CreatedBy = Utils.GetUserId() }))
            {
                resp.Message = "Password updated.";
                
                SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                ApplicationUser oUser = await SignInManager.UserManager.FindByNameAsync(username);

                SignInManager.SignIn(oUser, false, false);
                    
            }
           else
            {
                resp.Error = "Unable to update password, please try again later.";
            }
            return Json(resp);
        }

        public async Task<JsonResult> ChangePassword(string curPass, string pass)
        {
            var username = Utils.GetUsername();
            var resp = new Response();
            try
            {
                if (await userStore.ChangePassword(new User() { UserId = Utils.GetUserId(), Password = curPass, CreatedBy = Utils.GetUserId() }, pass))
                {
                    resp.Message = "Password changed.";

                    SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    ApplicationUser oUser = await SignInManager.UserManager.FindByNameAsync(username);

                    SignInManager.SignIn(oUser, false, false);
                }
                else
                {
                    resp.Error = "Unable to update password, please try again later.";
                }
                return Json(resp);
            }
            catch(Exception ex)
            {
                resp.Error = ex.Message;
                return Json(resp);
            }
        }

        public ActionResult Setting()
        {
            return View();
        }
    }
}
