﻿using System.Web.Mvc;

namespace Dym360.Areas.ProcessVault
{
    public class ProcessVaultAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ProcessVault";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ProcessVault_default",
                "ProcessVault/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "ProcessVault.Controllers" }
            );
        }
    }
}