﻿function InputValidate(status) {
    var isValid = true;
    if (status === "New" || status === "Draft" || status === "Pending Requester") {
        if ($('#inptSubject').val() === "") {
            if ($('#inptSubject').next().length < 1) {
                $('<p style="color:red;">Please enter subject to proceed</p>').insertAfter('#inptSubject');
            }
            isValid = false;
        }
        else {
            $('#inptSubject').next().remove();
        }

        //if ($('#inptDescription').val() === "") {
        //    if ($('#inptDescription').next().length < 1) {
        //        $('<p style="color:red;">Please enter description to proceed</p>').insertAfter('#inptDescription');
        //    }
        //    isValid = false;
        //}
        //else {
        //    $('#inptDescription').next().remove();
        //}

        //if ($('#tblReviewer tbody tr').length < 1) {
        //    if ($('#tblReviewer tfoot tr').length < 2) {
        //        $('<tr><td colspan="3"><p style="color:red;">Please enter description to proceed</p></td></tr>').insertAfter('#tblReviewer tfoot tr');
        //    }
        //    isValid = false;
        //}
        //else {
        //    $('#tblReviewer tfoot tr:nth-child(2)').remove();
        //}
    }
    if (status === "Pending Requester" || (status === "Pending PEx Team" && $('#previousState').val() === "REQUEST_MORE")) {
        if ($('#inptComment').val() === "") {
            if ($('#inptComment').next().length < 1) {
                $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter('#inptComment');
            }
            isValid = false;
        }
        else {
            $('#inptComment').next().remove();
        }
    }

    return isValid;
}

function SaveDraft(actionId) {
    if (InputValidate()) {

        var reviewers = [];
        $('#tblReviewer tbody tr').each(function () {
            reviewers.push({ "UserId": $(this).find('label').data('value') });
        });

        var data = {
            "req": {
                "ActionId": actionId,
                "RequestId": $('#requestId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "Comment": $('#inptComment').val(),
                "Reviewers": reviewers
            }
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/SaveDraft",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Submit(actionId) {
    if (InputValidate($('#lblStatus').text())) {

        var reviewers = [];
        if ($('#lblStatus').text() === "Pending Requester" || $('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
            $('#tblReviewer tbody tr').each(function () {
                reviewers.push({ "UserId": $(this).find('label').data('value') });
            });
        }

        var data = {
            "req": {
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "Comment": $('#inptComment').val(),
                "Reviewers": reviewers
            }
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/Submit",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Approve(actionId) {
    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    };
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/Approve",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Reject(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/Reject",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
    }
}
function Assign(actionId) {
    var scopes = [];

    if ($('#inptQA').prop('checked')) {
        scopes.push({ "Remark": $('#inptQARemark').val(), "UserId": $('#tblApprover').find('label[name=QA]').data('value') });
    }
    if ($('#inptPMC').prop('checked')) {
        scopes.push({ "Remark": $('#inptPMCRemark').val(), "UserId": $('#tblApprover').find('label[name=PM]').data('value') });
    }
    if ($('#inptLGC').prop('checked')) {
        scopes.push({ "Remark": $('#inptLGCRemark').val(), "UserId": $('#tblApprover').find('label[name=LGC]').data('value') });
    }
    if ($('#inptPROD').prop('checked')) {
        scopes.push({ "Remark": $('#inptPRODRemark').val(), "UserId": $('#tblApprover').find('label[name=PRD]').data('value') });
    }

    var approvers = [];
    if ($('#lblStatus').text() === "Pending Engineer") {
        $('#tblApprover tbody tr').each(function () {
            approvers.push({ "UserId": $(this).find('label').data('value') });
        });
    }

    var finalApprovers = [];
    if ($('#lblStatus').text() === "Pending Engineer") {
        $('#tblFinalApprover tbody tr').each(function () {
            finalApprovers.push({ "UserId": $(this).find('label').data('value') });
        });
    }

    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val(),
            "Approvers": approvers,
            "FinalApprovers": finalApprovers
        },
        "ecnDoc": {
            "DocumentName": $('#inptPublishDocumentName').val(),
            "CompletionDate": $('#inptCompletionDate').val(),
            "Code": $('#inptDocumentCode').val(),
            "ECNTaskScopes": scopes,
            "Remark": $('#inptRemark').val()
        }
    };

    var isValid = true;
    if ($('#tblEngrAtts tbody tr').length <= 0) {
        if ($('#tblEngrAtts').next().length < 1) {
            $('<p style="color:red;">Please upload new document proceed</p>').insertAfter('#tblEngrAtts');
        }
        isValid = false;
    }
    else {
        if ($('#tblEngrAtts').next().length > 0) {
            $('#tblEngrAtts').next().remove();
        }
    }
    if ($('#tblFinalApprover tbody tr').length <= 0) {
        if ($('#tblFinalApprover tfoot tr:nth-child(1)').next().length < 1) {
            $('<tr id="alert"><td colspan="3" style="color:red;">Please select final approver proceed</td></tr>').insertAfter('#tblFinalApprover tfoot tr:last-child');
        }
        isValid = false;
    }
    else {
        if ($('#tblFinalApprover tfoot tr:nth-child(1)').next().length > 0) {
            $('#tblFinalApprover tfoot tr:nth-child(1)').remove();
        }
    }

    if (isValid) {
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/Assign",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Start(actionId) {
    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    };
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/Start",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                $('button:contains("Start")').css('display', 'none');
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Complete(actionId) {
    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    };
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/Complete",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function RequestMore(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        };
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/RequestMore",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}

function uploadDoc(type) {
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectEngr').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#requestId').val());
        formdata.append("UploadBy", type);
        var fileInput;
        if (type === "requester") {
            fileInput = document.getElementById('fileSelectRequester');
        }
        else if (type === "engr") {
            fileInput = document.getElementById('fileSelectEngr');
        }
        else {
            fileInput = document.getElementById('fileSelectPIC');
        }

        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/ProcessVault/ECN/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);

                var r = $('#tblRequesterAtts tbody tr').length + 1;

                var row = "";
                if (type === "requester") {
                    $('#tblRequesterAtts tbody')[0].innerHTML = '';
                    for (var i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                        '</tr>';
                    }

                    $('#tblRequesterAtts').append(row);
                    $('#fileSelectRequester').val('');
                }
                else if (type === "engr") {
                    $('#tblEngrAtts tbody')[0].innerHTML = '';
                    for (i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                        '</tr>';
                    }

                    $('#tblEngrAtts').append(row);
                    $('#fileSelectEngr').val('');
                }
                else {
                    $('#tblPICAttach tbody')[0].innerHTML = '';
                    for (i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td>' + d[i].Size + '</td> ';
                        if (d[i].IsOwner) {
                            row += '<td>' + d[i].Name + '</td>';
                        }
                        else {
                            row += '<td><a href="' + d[i].Path + d[i].Name + '" target="_blank">' + d[i].Name + '</a></td>';
                        }
                        row += '<td>' + d[i].UploadedBy + '</td>';
                        if (d[i].IsOwner) {
                            row += '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>';
                        }
                        row += '</tr>';
                    }

                    $('#tblPICAttach').append(row);
                    $('#fileSelectPIC').val('');
                }
            }
        };
    }
}

function removeAttachment(e) {
    var attachmentId = $(e).parent().data('value');

    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/DeleteAttachment",
        data: { "attachmentId": attachmentId },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            showToast(d.Message, "success");
            $(e).parent().parent().remove();
            if ($('#tblEngrAtts tbody tr').length > 0) {
                $('#pexFileUpload').toggle();
            }
            else {
                $('#pexFileUpload').toggle();
            }
        }
    });
}


function selectReviewer() {
    if ($('#inptReviewer option:selected').val() !== "0") {
        var a = $('#inptReviewer option:selected');
        $('#tblReviewer').append('<tr><td colspan="2"><label data-value="' + a.val() + '">' + a.text() + '</label></td>' +
            '<td style="text-align: center">' +
            '<a class="delete" onclick="removeReviewer(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
    }
}

function removeReviewer(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/RemoveReviewer",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $(e).parent().parent().remove();
        }
    });
}

function selectFinalApprover() {
    if ($('#inptFinalApprover option:selected').val() !== "0") {
        var a = $('#inptFinalApprover option:selected');
        $('#tblFinalApprover').append('<tr><td colspan="2"><label data-value="' + a.val() + '">' + a.text() + '</label></td>' +
            '<td style="text-align: center">' +
            '<a class="delete" onclick="removeFinalApprover(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
    }
}
function removeFinalApprover(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/RemoveFinalApprover",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $(e).parent().parent().remove();
        }
    });
}

function selectApprover(e) {
    if ($(e).prop('checked')) {
        $.ajax({
            url: window.location.origin + "/ProcessVault/ECN/GetHOD",
            data: { "department": $(e).data('value') },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                var hod = JSON.parse(d.Data);
                if ($(e).prop('checked')) {
                    $('#tblApprover').append('<tr><td colspan="2"><label name="' + hod.DepartmentKeyword + '" data-value="' + hod.UserId + '">' + hod.Name + '</label></td>' +
                        '<td style="text-align: center">' +
                        '</td></tr>');
                }
                else {
                    $('#tblApprover tbody tr').each(function () {
                        if ($(this).find('label').data('value') === hod.UserId) {
                            $(this).remove();
                        }
                    });
                }
            }
        });
    }
    else {
        removeApprover(e);
    }
}


function removeApprover(e) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/RemoveApprover",
        data: { "department": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $('#tblApprover label[name="' + $(e).data('value') + '"]').parent().parent().remove();
        }
    });
}


