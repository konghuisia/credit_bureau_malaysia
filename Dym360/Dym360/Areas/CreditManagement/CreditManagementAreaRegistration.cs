﻿using System.Web.Mvc;

namespace Dym360.Areas.CreditManagement
{
    public class CreditManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CreditManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CreditManagement_default",
                "CreditManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "CreditManagement.Controllers" }
            );
        }
    }
}
