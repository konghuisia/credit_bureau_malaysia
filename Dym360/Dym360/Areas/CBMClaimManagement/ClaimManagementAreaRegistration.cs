﻿using System.Web.Mvc;

namespace Dym360.Areas.CBMClaimManagement
{
    public class CBMClaimManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CBMClaimManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CBMClaimManagement_default",
                "CBMClaimManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "CBMClaimManagement.Controllers" }
            );
        }
    }
}