﻿using System.Web.Mvc;

namespace Dym360.Areas.SubscriptionManagement
{
    public class SubscriptionManagementAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SubscriptionManagement";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SubscriptionManagement_default",
                "SubscriptionManagement/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new string[] { "SubscriptionManagement.Controllers" }
            );
        }
    }
}
