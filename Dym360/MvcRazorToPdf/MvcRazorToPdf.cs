﻿using System;
using System.IO;
using System.Text;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;

namespace MvcRazorToPdf
{
    public class MvcRazorToPdf
    {
        public byte[] GeneratePdfOutput(ControllerContext context, object model = null, string viewName = null,
            Action<PdfWriter, Document> configureSettings = null)
        {
            if (viewName == null)
            {
                viewName = context.RouteData.GetRequiredString("action");
            }

            context.Controller.ViewData.Model = model;

            byte[] output;
            using (var document = new Document())
            {
                using (var workStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                    writer.CloseStream = false;

                    if (configureSettings != null)
                    {
                        configureSettings(writer, document);
                    }
                    document.Open();
                    document.NewPage();

                    //for claim management request footer page
                    //if (viewName == "ExportClaimRequestToPdf")
                    //{
                    //    HeaderFooter myevent = new HeaderFooter();
                    //    myevent.model = model;
                    //    writer.PageEvent = myevent;
                    //}

                    using (var reader = new StringReader(RenderRazorView(context, viewName)))
                    {
                        try
                        {
                            //create the default CSS Resolver
                            XMLWorkerHelper helperInstance = XMLWorkerHelper.GetInstance();
                            CssFilesImpl cssFiles = new CssFilesImpl();
                            cssFiles.Add(helperInstance.GetDefaultCSS());
                            StyleAttrCSSResolver cssRevolver = new StyleAttrCSSResolver(cssFiles);

                            //create the default Font provider
                            XMLWorkerFontProvider xmlWorkerFontProvider = new XMLWorkerFontProvider();
                            HtmlPipelineContext htmlContext = new HtmlPipelineContext(new CssAppliersImpl(xmlWorkerFontProvider));

                            //create the default tag provider
                            ITagProcessorFactory tpf = Tags.GetHtmlTagProcessorFactory();
                            htmlContext.SetTagFactory(tpf);

                            //set the image provider, in my case a blank image provider as my docs have no images
                            htmlContext.SetImageProvider(new BlankImageProvider(document));

                            //create the Pipeline
                            IPipeline pipeline = new CssResolverPipeline(cssRevolver, new
                               HtmlPipeline(htmlContext, new PdfWriterPipeline(document, writer)));
                            //parse the XHTML
                            XMLWorker worker = new XMLWorker(pipeline, true);
                            XMLParser p = new XMLParser(worker);
                            p.Parse(reader);
                            
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        try
                        {
                            document.Close();
                        }
                        catch (Exception e)
                        {
                            e.ToString();
                        }
                        output = workStream.ToArray();
                    }
                }
            }
            return output;
        }

        public string RenderRazorView(ControllerContext context, string viewName)
        {
            IView viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewName).View;
            var sb = new StringBuilder();


            using (TextWriter tr = new StringWriter(sb))
            {
                var viewContext = new ViewContext(context, viewEngineResult, context.Controller.ViewData,
                    context.Controller.TempData, tr);
                viewEngineResult.Render(viewContext, tr);
            }
            return sb.ToString();
        }
    }
}