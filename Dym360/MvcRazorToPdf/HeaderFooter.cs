﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.css;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MvcRazorToPdf
{
    public class HeaderFooter : PdfPageEventHelper
    {
        private Font font = FontFactory.GetFont("sans-serif", 8, Font.BOLD, BaseColor.BLACK);
        public object model { get; set; }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            int page = writer.PageNumber;
            
            PdfPTable table = new PdfPTable(1);
            table.WidthPercentage = 100f;

            PdfPCell No = new PdfPCell(new Phrase("Page " + page, font));
            No.HorizontalAlignment = Element.ALIGN_RIGHT;
            No.VerticalAlignment = Element.ALIGN_MIDDLE;
            table.AddCell(No);

            
            document.Add(table);
        }
    }
}
