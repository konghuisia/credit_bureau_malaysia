﻿using FileManager.PDFConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public static class FileUtils
    {
        public static string SaveAttachmentAsDocument(string file)
        {
            var dir = file.Substring(0, file.LastIndexOf('/') + 1);
            var ext = file.Substring(file.LastIndexOf('.')).ToLower();
            var filename = file.Replace(dir, "").Replace(ext, "");
            var newfile = dir.Replace("Attachments", "Documents") + filename;

            System.IO.Directory.CreateDirectory(dir.Replace("Attachments", "Documents"));
            switch (ext)
            {
                case ".doc":
                case ".docx":
                    newfile += ".pdf";
                    WordToPDF.Instance.ConvertTOPDF(dir, filename + ext, newfile);
                    break;
                case ".png":
                case ".jpg":
                case ".jpeg":
                    newfile += ".pdf";
                    ImageToPDF.Instance.SaveImageAsPdf(file, newfile);
                    break;
                case ".xlsx":
                    newfile += ".mht";
                    ExcelToPDF.Instance.ExportWorkbookToWebArchive(file, newfile);
                    break;
                case ".txt":
                    newfile += ".txt";
                    if (!System.IO.File.Exists(newfile))
                    {
                        System.IO.File.Copy(file, newfile);
                    }
                    break;
                case ".pdf":
                    newfile += ".pdf";
                    if (!System.IO.File.Exists(newfile))
                    {
                        System.IO.File.Copy(file, newfile);
                    }
                    break;
                case ".stl":
                    newfile += ".stl";
                    if (!System.IO.File.Exists(newfile))
                    {
                        System.IO.File.Copy(file, newfile);
                    }
                    break;


            }
            return newfile;
        }
    }
}
