﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private static ConcurrentDictionary<int, Order> items =
         new ConcurrentDictionary<int, Order>();

        IItemRepository ItemRepository;
        List<Product> products;
        public OrderRepository()
        {
            ItemRepository = new ItemRepository();
            products = ItemRepository.GetAll().ToList();

            Add(new Order()
            {
                Id = 1,
                OrderItems = new List<OrderItem>
                    {
                        new OrderItem() { Item = products[0], Unit = 2 }
                    },
                OrderDate = DateTime.Now.AddDays(-1)
            });

            Add(new Order()
            {
                Id = 2,
                OrderItems = new List<OrderItem>
                    {
                        new OrderItem() { Item = products[2], Unit = 5 }
                    },
                OrderDate = DateTime.Now.AddDays(-5)
            });
            Add(new Order()
            {
                Id = 3,
                OrderItems = new List<OrderItem>
                    {
                        new OrderItem() { Item = products[1], Unit = 3 }
                    },
                OrderDate = DateTime.Now.AddDays(-3)
            });
            Add(new Order()
            {
                Id = 4,
                OrderItems = new List<OrderItem>
                    {
                        new OrderItem() { Item = products[0], Unit = 2 }
                    },
                OrderDate = DateTime.Now.AddDays(-3)
            });
            Add(new Order()
            {
                Id = 5,
                OrderItems = new List<OrderItem>
                    {
                        new OrderItem() { Item = products[2], Unit = 15 }
                    },
                OrderDate = DateTime.Now.AddDays(-4)
            });
        }

        public void Add(Order item)
        {

            items[item.Id] = item;

        }

        public IEnumerable<Order> GetAll()
        {
            return items.Values;
        }

        public Order Get(int Id)
        {
            return items[Id];
        }
    }
}