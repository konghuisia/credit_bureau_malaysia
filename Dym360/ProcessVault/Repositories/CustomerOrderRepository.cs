﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class CustomerOrderRepository : ICustomerOrderRepository
    {
        private static ConcurrentDictionary<int, CustomerOrder> items =
           new ConcurrentDictionary<int, CustomerOrder>();
        
        public CustomerOrderRepository()
        {
            Add(new CustomerOrder()
            {
                Id = 1,
                CustomerId = 14281029,
                OrderId = 1
            });
            Add(new CustomerOrder()
            {
                Id = 2,
                CustomerId = 14281029,
                OrderId = 2
            });
            Add(new CustomerOrder()
            {
                Id = 3,
                CustomerId = 73829405,
                OrderId = 3
            });
            Add(new CustomerOrder()
            {
                Id = 4,
                CustomerId = 73829405,
                OrderId = 4
            });
        }

        public void Add(CustomerOrder item)
        {
            items[item.Id] = item;
        }

        public CustomerOrder Get(int id)
        {
            return items[id];
        }

        public IEnumerable<CustomerOrder> GetAll()
        {
            return items.Values;
        }
    }
}