﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private static ConcurrentDictionary<int, Customer> items =
           new ConcurrentDictionary<int, Customer>();

        IOrderRepository OrderRepository;
        public CustomerRepository()
        {
            OrderRepository = new OrderRepository();
            var rng = new Random();
            Add(new Customer()
            {
                Id = 14281029,
                Name = "Customer 1",
                Email = "cust1@email.com",
                Description = "Customer 1 Description",
                IsModified = true,
                IsNew = true,
                CurrentAddress = new Address()
                {
                    Line1 = "Current Address Customer 1 Line 1",
                    Line2 = "Current Address Customer 1 Line 2",
                    City = "Current City Customer 10",
                    Country = "Current Country Customer 1",
                    ZipCode = string.Format("{0:00000}", 1),
                    State = "Current State Customer 1",
                },
                BillingAddress = new Address()
                {
                    Line1 = "Billing Address Customer 1 Line 1",
                    Line2 = "Billing Address Customer 1 Line 2",
                    City = "Billing City Customer 1",
                    Country = "Billing Country Customer 1",
                    ZipCode = string.Format("{0:00000}", 1),
                    State = "Billing State 1",
                    ContactNo = "0182849303",
                    PIC = "Customer 1 PIC",
                    Email = "cust1pic@email.com"
                }
            });
            Add(new Customer()
            {
                Id = 29381920,
                Name = "Customer 2",
                Email = "cust10@email.com",
                Description = "Customer 2 Description",
                IsModified = true,
                IsNew = true,
                CurrentAddress = new Address()
                {
                    Line1 = "Current Address Customer 2 Line 1",
                    Line2 = "Current Address Customer 2 Line 2",
                    City = "Current City Customer 2",
                    Country = "Current Country Customer 2",
                    ZipCode = string.Format("{0:00000}", 2),
                    State = "Current State Customer 2"
                },
                BillingAddress = new Address()
                {
                    Line1 = "Billing Address Customer 2 Line 1",
                    Line2 = "Billing Address Customer 2 Line 2",
                    City = "Billing City Customer 2",
                    Country = "Billing Country Customer 2",
                    ZipCode = string.Format("{0:00000}", 2),
                    State = "Billing State 2",
                    ContactNo = "0167489382",
                    PIC = "Customer 2 PIC",
                    Email = "cust2pic@email.com"
                }
            });

            Add(new Customer()
            {
                Id = 73829405,
                Name = "Customer 3",
                Email = "cust3@email.com",
                Description = "Customer 3 Description",
                IsModified = true,
                IsNew = true,
                CurrentAddress = new Address()
                {
                    Line1 = "Current Address Customer 3 Line 1",
                    Line2 = "Current Address Customer 3 Line 2",
                    City = "Current City Customer 3",
                    Country = "Current Country Customer 3",
                    ZipCode = string.Format("{0:00000}", 3),
                    State = "Current State Customer 3"
                },
                BillingAddress = new Address()
                {
                    Line1 = "Billing Address Customer 3 Line 1",
                    Line2 = "Billing Address Customer3 Line 2",
                    City = "Billing City Customer 3",
                    Country = "Billing Country Customer 10",
                    ZipCode = string.Format("{0:00000}", 3),
                    State = "Billing State 3",
                    ContactNo = "0127489583",
                    PIC = "Customer 3 PIC",
                    Email = "cust3pic@email.com"
                }
            });
        }
        
        public void Add(Customer item)
        {
            if (item.IsNew)
            {
                item.IsNew = false;
                item.IsModified = false;
                items[item.Id] = item;
            }
        }

        public IEnumerable<Customer> GetAll()
        {
            return items.Values;
        }
    }
}