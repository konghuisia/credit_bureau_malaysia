﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class ItemRepository : IItemRepository
    {
        private static ConcurrentDictionary<int, Product> items =
           new ConcurrentDictionary<int, Product>();

        public ItemRepository()
        {
            Add(new Product()
            {
                Id = 1,
                Name = "Product 1 Name",
                Description = "Product 1 Description",
                Price = 200.50
            });
            Add(new Product()
            {
                Id = 2,
                Name = "Product 2 Name",
                Description = "Product 2 Description",
                Price = 320.50
            });
            Add(new Product()
            {
                Id = 3,
                Name = "Product 3 Name",
                Description = "Product 3 Description",
                Price = 203.50
            });
            Add(new Product()
            {
                Id = 4,
                Name = "Product 4 Name",
                Description = "Product 4 Description",
                Price = 440.50
            });
        }


        public IEnumerable<Product> GetAll()
        {
            return items.Values;
        }

        void Add(Product item)
        {
            items[item.Id] = item;
        }
    }
}