﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private static ConcurrentDictionary<int, Account> items =
           new ConcurrentDictionary<int, Account>();

        public AccountRepository()
        {
            Add(new Account()
            {
                Id = 1,
                AccountType = AccountTypeEnum.Prepaid,
                Balance = 2000.00,
                CustomerId = 14281029,
                ExpiryDate = DateTime.Now.AddMonths(6)
            });
            Add(new Account()
            {
                Id = 2,
                AccountType = AccountTypeEnum.Postpaid,
                Balance = 0.00,
                CustomerId = 29381920,
                ExpiryDate = DateTime.Now.AddMonths(5)
            });
            Add(new Account()
            {
                Id = 3,
                AccountType = AccountTypeEnum.Postpaid,
                Balance = 0.00,
                CustomerId = 73829405,
                ExpiryDate = DateTime.Now.AddMonths(5)
            });
        }

        public void Add(Account item)
        {
            items[item.Id] = item;
        }

        public Account Get(int id)
        {
            return items[id];
        }

        public IEnumerable<Account> GetAll()
        {
            return items.Values;
        }
    }
}