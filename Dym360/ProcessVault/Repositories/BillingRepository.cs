﻿using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Repositories
{
    public class BillingRepository : IBillingRepository
    {
        private static ConcurrentDictionary<int, BillingInfo> items =
           new ConcurrentDictionary<int, BillingInfo>();

        public void Add(BillingInfo item)
        {
            items[item.Id] = item;
        }

        public IEnumerable<BillingInfo> GetAll()
        {
            return items.Values;
        }
    }
}