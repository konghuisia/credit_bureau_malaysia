﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class InvoiceViewModel
    {
        public InvoiceViewModel()
        {
            Invoices = new List<Invoice>();
        }
        public List<Invoice> Invoices { get; set; }
    }
}