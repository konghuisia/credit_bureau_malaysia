﻿using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class CompanyDocumentSearchModel
    {
        public List<CompanyDocument> CompanyDocuments { get; set; }
        public List<Department> Departments { get; set; }
    }
}