﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class QMSServiceReportSearchModel
    {
       public List<QMSServiceReport> QMSServiceReports { get; set; }
    }
}