﻿using ObjectClasses.StandardOperationProcedure;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class SOPConfig
    {
        public SOP SOP { get; set; }
        public List<User> Users { get; set; }
    }
}