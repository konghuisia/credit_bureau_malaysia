﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class CompanyDocumentConfig
    {
        public string BaseUrl { get; set; }
        public string Access { get; set; } = "Read";
        public bool Print { get; set; }
        public Request Request { get; set; }
        public CompanyDocument CompanyDocument { get; set; }
        public List<DocumentType> DocumentTypes { get; set; }
        public List<RequestType> RequestTypes { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public List<Department> Departments { get; set; }
        public List<Location> Locations { get; set; }
        public List<User> Users { get; set; }
        public List<HOD> HODs { get; set; }
        public List<Classification> Classifications { get; set; }
    }
}