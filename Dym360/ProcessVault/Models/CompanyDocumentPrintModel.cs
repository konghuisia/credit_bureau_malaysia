﻿using ObjectClasses;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class CompanyDocumentPrintModel
    {
        public Request Request { get; set; }
        public CompanyDocument CompanyDocument { get; set; }
    }
}