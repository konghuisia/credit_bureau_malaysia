﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class BillingInfoSearchModel
    {
        public List<Customer> Customers { get; set; }
        public List<BillingInfo> BillingInfos { get; set; }
    }
}