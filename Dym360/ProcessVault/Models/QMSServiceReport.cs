﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models
{
    public class QMSServiceReport
    {
        public string BaseUrl { get; set; }
        public string WONo { get; set; }
        public string ClinicName { get; set; }
        public string BENo { get; set; }
        public string BECategory { get; set; }
        public string WRDate { get; set; }
        public string WODate { get; set; }
        public string WOResponded { get; set; }
        public string WOCompleted { get; set; }
        public string ProblemCode { get; set; }
        public string CorrectiveAction { get; set; }
        public string EngineerName { get; set; }
        public string CustomerSignature { get; set; }
        public string ProblemReported { get; set; }
    }
}