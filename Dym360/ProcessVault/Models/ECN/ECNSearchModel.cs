﻿using ObjectClasses;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models.ECN
{
    public class ECNSearchModel
    {
        public string BaseUrl { get; set; }
        public ECNModel ECNModels { get; set; }
    }
}