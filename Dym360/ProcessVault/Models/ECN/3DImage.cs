﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models.ECN
{
    public class _3DImage
    {
        public string BaseUrl { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}