﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Models.ECN
{
    public class ECNConfig
    {
        public string BaseUrl { get; set; }
        public User CurrentUser { get; set; }
        public string Access { get; set; } = "Read";
        public Request Request { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public List<User> Users { get; set; }
        public List<User> Reviewers { get; set; }
        public List<User> FinalApprovers { get; set; }
        public ECNDocument ECNDocument { get; set; }
        public List<Department> Departments { get; set; }
    }
}