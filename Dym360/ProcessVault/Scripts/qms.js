﻿function formatDateTime(d) {
    var date = new Date(d);

    var hour = date.getHours().padLeft();     // yields hours 
    var minute = date.getMinutes().padLeft(); // yields minutes
    var second = date.getSeconds().padLeft(); // yields seconds

    // After this construct a string with the above results as below
    return date.toShortFormat() + ' ' + hour + ':' + minute + ':' + second;
}

Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
};
Date.prototype.toShortFormat = function () {

    var month_names = ["Jan", "Feb", "Mar",
        "Apr", "May", "Jun",
        "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec"];

    var day = this.getDate();
    var month_index = this.getMonth();
    var year = this.getFullYear();

    return "" + day + " " + month_names[month_index] + " " + year;
};

function LoadData() {
    $('#WONo').text(window.WO.WorkOrder.WONo);
    $('#WRDate').text(formatDateTime(window.WO.RequestDate));
    $('#ClinicName').text(window.WO.Clinic.ClinicName);
    $('#WODate').text(formatDateTime(window.WO.WorkOrder.WODate));
    $('#BENo').text(window.WO.Asset.BENo);
    $('#BECategory').text(window.WO.Asset.BECategory);
    $('#ProblemReported').text(window.WO.Description);
    $('#ActionTaken').text(window.WO.WorkOrder.Action.CorrectiveAction);
    var audits = window.WO.Audits;
    for (var i = 0; i < audits.length; i++) {
        var a = audits[i];
        if (a.Action === 'Responded') {
            $('#WORespondedDate').text(formatDateTime(a.ActionDate));
            $('#WRRespondedDate').text(formatDateTime(a.ActionDate));
        }
        if (a.Action === 'Completed') {
            $('#WOCompletedDate').text(formatDateTime(a.ActionDate));
        }
    }
    $('#ProblemCode').text(window.WO.WorkOrder.Detail.ProblemCode);
    $('#EngName').text(window.WO.WorkOrder.Detail.AssignTo);
    $('#EngName1').text(window.WO.WorkOrder.Detail.AssignTo);
}