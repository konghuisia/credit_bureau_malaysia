﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Interfaces
{
    public interface IBillingRepository 
    {
        void Add(BillingInfo item);
        IEnumerable<BillingInfo> GetAll();
    }
}