﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessVault.Interfaces
{
    public interface IAccountRepository
    {
        void Add(Account item);
        Account Get(int id);
        IEnumerable<Account> GetAll();
    }
}
