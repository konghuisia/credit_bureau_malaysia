﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessVault.Interfaces
{
    public interface IItemRepository
    {
        IEnumerable<Product> GetAll();
    }
}
