﻿using ObjectClasses.BillingInvoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessVault.Interfaces
{
    public interface ICustomerRepository
    {
        void Add(Customer item);
        IEnumerable<Customer> GetAll();
    }
}