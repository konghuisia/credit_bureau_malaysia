﻿using DataAccessHelper.ConfigurationHelper.Store;
using DataAccessHelper.RequestHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using ProcessVault.Models.ECN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using IxMilia.Stl;
using DataAccessHelper.DocumentPortalHelper.Store;
using Newtonsoft.Json;
using ProcessVault.Models;

namespace ProcessVault.Controllers
{
    public class ECNController : Controller
    {
        private RequestStore _requestStore;
        private UserStore _userStore;
        private RoleStore _roleStore;
        private ApprovalStore _approvalStore;
        private AttachmentStore _attachmentStore;
        private DepartmentStore _departmentStore;
        private ECNStore _ecnStore;

        private static List<RoleAction> RoleActions = new List<RoleAction>();
        private static IList<HOD> HODs = new List<HOD>();
        private static string ModuleId = "9FF3181B-E14E-4F14-BC20-341F6477738C";
        private static Role CurRole;
        private static User CurrentUser;

        public ECNController()
        {
            _requestStore = DependencyResolver.Current.GetService<RequestStore>();
            _userStore = DependencyResolver.Current.GetService<UserStore>();
            _roleStore = DependencyResolver.Current.GetService<RoleStore>();
            _approvalStore = DependencyResolver.Current.GetService<ApprovalStore>();
            _attachmentStore = DependencyResolver.Current.GetService<AttachmentStore>();
            _departmentStore = DependencyResolver.Current.GetService<DepartmentStore>();
            _ecnStore = DependencyResolver.Current.GetService<ECNStore>();
            CurrentUser = new User();
        }
        // GET: ECN
        public ActionResult Index(string id)
        {
            return View(new Request()
            {
                RequestId = id
            });
        }
        public async Task<ActionResult> LoadPageView(string requestId, string view = "_NewECN")
        {
            var ecn = new ECNConfig();
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            CurRole = new Role();
            ecn.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            CurrentUser = await _userStore.GetUser(Utils.GetUserId());

            if (string.IsNullOrEmpty(requestId))
            {
                CurRole = await _roleStore.GetRole("");

                ecn.Request = new Request()
                {
                    ReferenceNo = "ECN-" + DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                    RequestTypeId = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE",
                    ModuleId = ModuleId,
                    Requester = new Requester()
                    {
                        Name = CurrentUser.Name,
                        UserId = CurrentUser.UserId,
                        Position = CurrentUser.Position,
                        Department = CurrentUser.Department,
                        Reporting = CurrentUser.Reporting
                    }
                };

                ecn.Request.RequestId = await _requestStore.NewPreRequest(ecn.Request);
                ecn.Access = "Write";
                ra = await _roleStore.GetRoleActions("", CurRole.RoleId);
            }
            else
            {
                ecn.Request = await _requestStore.GetRequest(requestId);
                apps = await _approvalStore.GetAllApproval(requestId);

                ecn.Request.Reviewers = apps.Where(x => x.Role == "REVIEWER").ToList();
                ecn.Request.PEXs = apps.Where(x => x.Role == "ENGR").ToList();
                ecn.Request.Approvers = apps.Where(x => x.Role == "PIC").ToList();
                ecn.Request.FinalApprovers = apps.Where(x => x.Role == "FINAL_APPROVER").ToList();

                if (ecn.Request.Status == "Draft" || ecn.Request.Status == "Pending Requester")
                {
                    if (ecn.Request.Requester.UserId == Utils.GetUserId())
                    {
                        ecn.Access = "Write";
                        CurRole = await _roleStore.GetRole("");
                        ra = await _roleStore.GetRoleActions("", CurRole.RoleId);
                    }
                }
                else
                {
                    if (!ecn.Request.Status.ToLower().Contains("close"))
                    {
                        var isBlock = true;
                        var pends = apps.ToList().FindAll(x => x.Action == "PENDING" || x.Action == "Start");
                        foreach (var app in pends)
                        {
                            if (app.RoleId == ecn.Request.RequestState.RoleId && app.UserId == Utils.GetUserId())
                            {
                                isBlock = false;
                            }
                        }
                        if (!isBlock)
                        {
                            ecn.Access = "Write";
                            CurRole = await _roleStore.GetRole(ecn.Request.RequestState.RoleId);
                            ra = await _roleStore.GetRoleActions("", ecn.Request.RequestState.RoleId);
                        }
                    }
                }
            }

            IList<User> users = new List<User>();
            users = await _userStore.GetAllUser();
            ecn.Reviewers = users.ToList().FindAll(x => x.PositionId == "DF9BF1AC-129B-442B-B7F0-82BF7222DD3F");
            ecn.FinalApprovers = users.ToList().FindAll(x => Convert.ToInt16(x.Grad) >= 60);
            HODs = await _userStore.GetAllHOD();

            var Departments = await _departmentStore.GetAllDepartments();
            ecn.Departments = Departments.ToList();

            ecn.RoleActions = ra.ToList();
            RoleActions = ra.ToList();
            ecn.Users = users.ToList().FindAll(x => x.UserId != Utils.GetUserId());

            ecn.ECNDocument = await _ecnStore.GetECNDocument(ecn.Request.RequestId);
            ecn.CurrentUser = CurrentUser;
            return PartialView(view, ecn);
        }

        public async Task<JsonResult> SaveDraft(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Status = "Draft";
            var success = await _ecnStore.Save(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request saved as draft";
                resp.RedirectUrl = "/ProcessVault/ECN/Index/" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Submit(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.Requester.UserId = Utils.GetUserId();
            req.UserId = req.Requester.UserId;
            var success = await _ecnStore.Submit(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request submitted";
                resp.RedirectUrl = "/ProcessVault/Home";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Assign(Request req, ECNDocument ecnDoc)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.Assign(ecnDoc, req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request approved";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Approve(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.Approve(req, Server.MapPath("~"));

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request approved";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Complete(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.Complete(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request completed";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> RequestMore(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.RequestMore(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request sent back for clarification";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Reject(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId; ;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.Reject(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request rejected";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }
        public async Task<JsonResult> Start(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = CurRole.RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _ecnStore.Start(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Started request process";
                resp.RedirectUrl = "/ProcessVault/ECN/Index/" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            request.Requester.UserId = Utils.GetUserId();
            request.RequestId = Request.Params.GetValues("RequestId")[0];
            var uploadBy = Request.Params.GetValues("UploadBy")[0];

            var folder = "Attachments";
            var AttachmentTypeId = "";
            if (uploadBy == "requester")
            {
                AttachmentTypeId = "D88A3D13-707A-46D0-B4CC-6EAF6832C3E3";
            }
            else if (uploadBy == "engr")
            {
                AttachmentTypeId = "A542CF4A-3986-4548-8AD0-8CA67291DC3B";
            }
            else
            {
                AttachmentTypeId = "64605EDD-8E42-40F6-B5A3-C1A4B46F52D4";
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/ProcessVault/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    AbsolutePath = location,
                    Path = path,
                    ModifiedDate = Convert.ToDateTime(Request.Params.GetValues("LastModifiedDate")[i]).ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = "301AC49F-57EC-424A-B557-D574D1809481"
                });
            }

            request = await _attachmentStore.CreateAsync(request);

            foreach (var a in request.Attachments)
            {
                if (a.UserId == Utils.GetUserId())
                {
                    a.IsOwner = true;
                }
                a.CreatedDate = Convert.ToDateTime(a.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            return Json(request.Attachments.FindAll(x => x.AttachmentTypeId == AttachmentTypeId));

        }

        [HttpPost]
        public async Task<JsonResult> DeleteAttachment(string attachmentId)
        {
            await _attachmentStore.DeleteAttachment(attachmentId);
            return Json(new Response()
            {
                Message = "Attachment removed"
            });
        }
        public ActionResult View3DFile(string filename, string filepath)
        {
            return View(new _3DImage()
            {
                BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "").Replace(Request.Url.Query, ""),
                FileName = filename,
                FilePath = filepath
            });
        }

        public ActionResult Test()
        {
            ViewBag.Message = "Hellow";
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> RemoveReviewer(string userId, string requestId)
        {
            var roleId = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40";
            var success = await _approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Reviewer removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveFinalApprover(string userId, string requestId)
        {
            var roleId = "23CB39F4-19F8-4998-9F1E-2B0E996433D9";
            var success = await _approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Final approver removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }


        [HttpPost]
        public async Task<JsonResult> RemoveApprover(string department, string requestId)
        {
            var roleId = "51B442A7-85A8-4BD8-B63F-D7E5DBA30390";
            if (HODs == null)
            {
                HODs = await _userStore.GetAllHOD();
            }
            var hod = HODs.First(x => x.DepartmentKeyword.Trim() == department);
            var success = await _approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = hod.UserId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Approver removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }


        [HttpPost]
        public async Task<JsonResult> GetHOD(string department)
        {
            if (HODs == null)
            {
                HODs = await _userStore.GetAllHOD();
            }
            var hod = HODs.First(x => x.DepartmentKeyword.Trim() == department);

            var resp = new Response()
            {
                Data = JsonConvert.SerializeObject(hod)
            };

            return Json(resp);
        }

        public ActionResult PrintPreview(string id)
        {
            return View("ServiceReportPrintPreview", new QMSServiceReport());
        }
    }
}