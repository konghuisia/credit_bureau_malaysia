﻿using ObjectClasses.Configuration;
using DataAccessHelper.RequestHelper.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ObjectClasses;
using ProcessVault.Models;
using System.Threading.Tasks;
using IdentityManagement.Utilities;
using DataAccessHelper.ConfigurationHelper.Store;
using ObjectClasses.DocumentPortal;
using DataAccessHelper.DocumentPortalHelper.Store;
using Newtonsoft.Json;
using System.IO;
using TheArtOfDev.HtmlRenderer.PdfSharp;


namespace ProcessVault.Controllers
{
    public class DocumentController : Controller
    {
        private RequestStore _requestStore;
        private UserStore _userStore;
        private RoleStore _roleStore;
        private DepartmentStore _departmentStore;
        private LocationStore _locationStore;
        private DocumentPortalStore _documentPortal;
        private ApprovalStore _approvalStore;
        private AttachmentStore _attachmentStore;

        private static List<RoleAction> RoleActions = new List<RoleAction>();
        private static IList<HOD> HODs = new List<HOD>();
        private static string ModuleId = "D19076F9-A77B-4D8D-BBB2-E3001A76FE01";
        private static string RoleId;

        public DocumentController()
        {
            _requestStore = DependencyResolver.Current.GetService<RequestStore>();
            _userStore = DependencyResolver.Current.GetService<UserStore>();
            _roleStore = DependencyResolver.Current.GetService<RoleStore>();
            _departmentStore = DependencyResolver.Current.GetService<DepartmentStore>();
            _locationStore = DependencyResolver.Current.GetService<LocationStore>();
            _documentPortal = DependencyResolver.Current.GetService<DocumentPortalStore>();
            _approvalStore = DependencyResolver.Current.GetService<ApprovalStore>();
            _attachmentStore = DependencyResolver.Current.GetService<AttachmentStore>();
        }
        // GET: Document
        public ActionResult Index(string id)
        {
            return View(new Request()
            {
                RequestId = id
            });
        }
        public async Task<ActionResult> LoadPageView(string requestId, string view = "_NewDocument")
        {
            var cp = new CompanyDocumentConfig();
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            var role = new Role();
            cp.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            var user = await _userStore.GetUser(Utils.GetUserId());
            role = await _roleStore.GetRole("");
            if (string.IsNullOrEmpty(requestId))
            {
                RoleId = role.RoleId;
                
                cp.Request = new Request()
                {
                    ReferenceNo = "DOC-" + DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                    RequestTypeId = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE",
                    ModuleId = ModuleId,
                    Requester = new Requester()
                    {
                        Name = user.Name,
                        UserId = user.UserId,
                        Position = user.Position,
                        Department = user.Department,
                        Reporting = user.Reporting
                    }
                };

                cp.Request.RequestId = await _requestStore.NewPreRequest(cp.Request);
                cp.Access = "Write";
                ra = await _roleStore.GetRoleActions("", RoleId);
            }
            else
            {
                cp.Request = await _requestStore.GetRequest(requestId);
                cp.CompanyDocument = await _documentPortal.GetCompanyDocument(requestId);
                apps = await _approvalStore.GetAllApproval(requestId);
                cp.Request.Reviewers = apps.ToList().FindAll(x => x.RoleId == "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40");
                cp.Request.Approvers = apps.ToList().FindAll(x => x.RoleId == "EF3FDF2F-5091-41DB-87BC-7E4B7F412D5C");
                cp.Request.PEXs = apps.ToList().FindAll(x => x.RoleId == "52EFA820-7B7A-406D-B3F7-B2F3D613DE14");
                cp.Request.FinalApprovers = apps.ToList().FindAll(x => x.RoleId == "23CB39F4-19F8-4998-9F1E-2B0E996433D9");

                if (cp.Request.Status == "Draft" || cp.Request.Status == "Pending Requester")
                {
                    RoleId = role.RoleId;
                    if (cp.Request.Requester.UserId == Utils.GetUserId())
                    {
                        cp.Access = "Write";
                        ra = await _roleStore.GetRoleActions("", RoleId);
                    }
                }
                else
                {
                    if (!cp.Request.Status.ToLower().Contains("close"))
                    {
                        var isBlock = true;
                        var pends = apps.ToList().FindAll(x => x.Action == "PENDING");
                        foreach (var app in pends)
                        {
                            if (app.RoleId == cp.Request.RequestState.RoleId && app.UserId == Utils.GetUserId())
                            {
                                isBlock = false;
                            }
                        }
                        if (!isBlock)
                        {
                            cp.Access = "Write";
                            ra = await _roleStore.GetRoleActions("", cp.Request.RequestState.RoleId);
                        }
                    }
                    else if (!cp.Request.Status.ToLower().Contains("reject"))
                    {
                        cp.Print = true;
                    }
                    RoleId = cp.Request.RequestState.RoleId;
                }

            }
            
            IList<RequestType> requestTypes = new List<RequestType>();
            requestTypes = await _requestStore.GetAllRequestType();

            IList<Department> departments = new List<Department>();
            departments = await _departmentStore.GetAllDepartments();

            IList<Location> locations = new List<Location>();
            locations = await _locationStore.GetAllLocations();

            IList<User> users = new List<User>();
            users = await _userStore.GetAllUser();

            IList<Classification> classifications = new List<Classification>();
            classifications = await _attachmentStore.GetClassifications();


            HODs = await _userStore.GetAllHOD();

            cp.RequestTypes = requestTypes.ToList();
            cp.RoleActions = ra.ToList();
            RoleActions = ra.ToList();
            cp.Departments = departments.ToList();
            cp.Locations = locations.ToList();
            cp.Users = users.ToList().FindAll(x => x.UserId != Utils.GetUserId());
            cp.Classifications = classifications.ToList();

            return PartialView(view, cp);
        }

        public async Task<JsonResult> SaveDraft(CompanyDocument sop, Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Status = "Draft";
            var success = await _documentPortal.SaveRequest(sop, req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request saved as draft";
                resp.RedirectUrl = "/ProcessVault/Document/Index/" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Submit(CompanyDocument sop, Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId: req.RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            var success = await _documentPortal.SubmitRequest(sop, req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request submitted";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Reject(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _documentPortal.RejectRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request rejected";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Approve(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _documentPortal.ApproveRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request approved";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> RequestMore(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            var success = await _documentPortal.RequestMoreRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request sent back for clarification";
                resp.RedirectUrl = "/ProcessVault/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveReviewer(string userId, string requestId)
        {
            var roleId = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40";
            var success = await _approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Reviewer removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveApprover(string userId, string requestId)
        {
            var roleId = "EF3FDF2F-5091-41DB-87BC-7E4B7F412D5C";
            var success = await _approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Approver removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }


        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            request.Requester.UserId = Utils.GetUserId();
            request.RequestId = Request.Params.GetValues("RequestId")[0];
            var uploadBy = Request.Params.GetValues("UploadBy")[0];
            var classificationId = "";

            var folder = "";
            var AttachmentTypeId = "";
            if (uploadBy == "requester")
            {
                folder = "Attachments";
                AttachmentTypeId = "D88A3D13-707A-46D0-B4CC-6EAF6832C3E3";
            }
            else
            {
                folder = "Documents";
                AttachmentTypeId = "A542CF4A-3986-4548-8AD0-8CA67291DC3B";
                classificationId = Request.Params.GetValues("ClassificationId")[0];
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/ProcessVault/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    Path = path,
                    AbsolutePath = location,
                    ModifiedDate = Convert.ToDateTime(Request.Params.GetValues("LastModifiedDate")[i]).ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = classificationId
                });
            }

            request = await _attachmentStore.CreateAsync(request);

            foreach(var a in request.Attachments)
            {
                a.CreatedDate = Convert.ToDateTime(a.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            return Json(request.Attachments.FindAll(x => x.AttachmentTypeId == AttachmentTypeId));
            
        }

        [HttpPost]
        public async Task<JsonResult> DeleteAttachment(string attachmentId)
        {
            await _attachmentStore.DeleteAttachment(attachmentId);
            return Json(new Response()
            {
                Message = "Attachment removed"
            });
        }

        [HttpPost]
        public JsonResult GetHOD(string departmentId)
        {
            var hod = HODs.First(x => x.DepartmentId == departmentId);

            var resp = new Response()
            {
                Data = JsonConvert.SerializeObject(hod)
            };
            
            return Json(resp);
        }

        public async Task<ActionResult> PrintPreview(string id)
        {
            var cd = new CompanyDocumentConfig();
            cd.Request = await _requestStore.GetRequest(id);
            cd.CompanyDocument = await _documentPortal.GetCompanyDocument(id);
            var depts = await _departmentStore.GetAllDepartments();
            cd.Departments = depts.ToList();
            var locs = await _locationStore.GetAllLocations();
            cd.Locations = locs.ToList();
            var apps = await _approvalStore.GetAllApproval(id);
            cd.Request.Reviewers = apps.ToList().FindAll(x => x.RoleId == "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40");
            cd.Request.Approvers = apps.ToList().FindAll(x => x.RoleId == "EF3FDF2F-5091-41DB-87BC-7E4B7F412D5C");
            cd.Request.PEXs = apps.ToList().FindAll(x => x.RoleId == "52EFA820-7B7A-406D-B3F7-B2F3D613DE14");
            cd.Request.FinalApprovers = apps.ToList().FindAll(x => x.RoleId == "23CB39F4-19F8-4998-9F1E-2B0E996433D9");

            //byte[] pdfOutput = ControllerContext.GeneratePdf(cd,"PrintDocument");
            var pdfOutput = PdfSharpConvert(this.RenderView("PrintDocument", cd));
            string fullPath = Server.MapPath("~/App_Data/FreshlyMade.pdf");

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }
            System.IO.File.WriteAllBytes(fullPath, pdfOutput);
            
            return View("PrintDocument", cd);
        }
        public static Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4);
                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }
    }
}