﻿using Newtonsoft.Json;
using ProcessVault.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProcessVault.Controllers
{
    public class QMSController : Controller
    {
        // GET: QMS
        public ActionResult Index(string fileName)
        {
            var json = "";
            using (StreamReader r = new StreamReader(Server.MapPath("~/") + "Areas/ProcessVault/QMS/" + fileName))
            {
                json = r.ReadToEnd();
            }

            var qmssr = JsonConvert.DeserializeObject<QMSServiceReport>(json);
            qmssr.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            return View(qmssr);
        }
        
        public void UploadFile(string imageData, string jsonData)
        {
            using (StreamWriter file = new StreamWriter(Server.MapPath("~/") + "Areas/ProcessVault/QMS/json.txt"))
            {
                file.WriteLine(jsonData);
                //JsonSerializer serializer = new JsonSerializer();
                ////serialize object directly into file stream
                //serializer.Serialize(file, jsonData);
            }

            string path = "Areas/ProcessVault/QMS/signature.png";
            string fileNameWitPath = Server.MapPath("~/") + path;
            using (FileStream fs = new FileStream(fileNameWitPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))

                {
                    byte[] data = Convert.FromBase64String(imageData);
                    bw.Write(data);
                    bw.Close();
                }
            }
        }
    }
}