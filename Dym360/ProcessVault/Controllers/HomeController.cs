﻿using IdentityManagement.Utilities;
using Newtonsoft.Json;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.StandardOperationProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProcessVault.Models;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using DataAccessHelper.RequestHelper.Store;
using DataAccessHelper.DocumentPortalHelper.Store;
using ObjectClasses.DocumentPortal;
using DataAccessHelper.ConfigurationHelper.Store;
using ProcessVault.Models.ECN;
using ProcessVault.Interfaces;
using ObjectClasses.BillingInvoice;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using FileManager;
using iTS = iTextSharp.text;
using MvcRazorToPdf;

namespace ProcessVault.Controllers
{
    public class HomeController : Controller
    {
        private RequestStore _requestStore;
        private DocumentPortalStore _documentPortal;
        private ECNStore _ecnStore;
        private DepartmentStore _departmentStore;

        private QMSServiceReport qmssr;

        ICustomerRepository CustomerRepository;
        IOrderRepository OrderRepository;
        IAccountRepository AccountRepository;
        IItemRepository ItemRepository;
        IBillingRepository BillingRepository;
        ICustomerOrderRepository CustomerOrderRepository;
        public HomeController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IAccountRepository accountRepository,
            IItemRepository itemRepository, IBillingRepository billingRepository, ICustomerOrderRepository customerOrderRepository)
        {
            CustomerRepository = customerRepository;
            OrderRepository = orderRepository;
            AccountRepository = accountRepository;
            ItemRepository = itemRepository;
            BillingRepository = billingRepository;
            CustomerOrderRepository = customerOrderRepository;

            _requestStore = DependencyResolver.Current.GetService<RequestStore>();
            _documentPortal = DependencyResolver.Current.GetService<DocumentPortalStore>();
            _departmentStore = DependencyResolver.Current.GetService<DepartmentStore>();
            _ecnStore = DependencyResolver.Current.GetService<ECNStore>();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ExcelView()
        {
            return View();
        }
        public async Task<ActionResult> Document(string id)
        {
            var docs = await _documentPortal.GetAllCompanyDocuments();

            return View(docs.ToList().Find(x => x.Document.DocumentId == id));
        }
        public async Task<ActionResult> ECNDocument(string id)
        {
            var ecn = await _ecnStore.GetECNDocument(id);
            var req = await _requestStore.GetRequest(id);
            var ecnm = new ECNModel();
            ecnm.ECNDocument = ecn;
            ecnm.RequestId = id;
            ecnm.ReferenceNo = req.ReferenceNo;
            ecnm.Status = req.Status;

            return View(new ECNSearchModel()
            {
                BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "").Replace(Request.Url.Query, ""),
                ECNModels = ecnm
            });
        }

        public async Task<ActionResult> LoadMyTask()
        {
            IList<MyTask> requests = await _requestStore.GetAllRequest(Utils.GetUserId());

            return View("MyTask", requests.ToList());
        }

        private SOP NewSOP()
        {
            return (new SOP()
            {
                Id = Guid.NewGuid().ToString(),
                //Requester = new Requester()
                //{
                //    Id = Utils.GetUserId(),
                //    Name = Utils.GetDisplayName(),
                //    Position = IdentityManagement.DAL.UserController.GetUser(Utils.GetUserId()).Position + "(" + Utils.GetUserGrad() + ")"
                //},
                //RefNo = "SOP-" + DateTime.Now.ToString("yyyyMMdd") + string.Format("{0:0000}", ConfigHelper.SeqNo++),
                //RequestStatus = new RequestStatus()
                //{
                //    Status = "New"
                //},
                //Module = MyEnum.Module.PROCESS_VAULT,
                //CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "New"),
                //CurrentRole = ConfigHelper.ProcessFlowRoles.Find(x => x.Name == "Requester")
            });
        }

        public JsonResult Save(SOP sop)
        {
            var tmp = SessionHelper.SOPs.Find(x => x.Id == sop.Id);

            tmp.Subject = sop.Subject;
            tmp.Description = sop.Description;
            //tmp.RequestType = sop.RequestType;
            //tmp.RequestStatus.Status = "Draft";
            //tmp.CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "Draft");

            var res = new Response
            {
                Message = "Save draft completed",
                Data = JsonConvert.SerializeObject(tmp)
            };

            return Json(res);
        }

        public JsonResult Submit(SOP sop)
        {
            var tmp = SessionHelper.SOPs.Find(x => x.Id == sop.Id);

            tmp.Subject = sop.Subject;
            tmp.Description = sop.Description;
            //tmp.RequestType = sop.RequestType;
            //tmp.RequestDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");

            //var nextProcess = new ProcessFlow();
            //if (tmp.CurrentState.Name == "New" || tmp.CurrentState.Name == "Draft")
            //{
            //    nextProcess = ConfigHelper.ProcessFlows.Find(x => x.Module == tmp.Module && x.ActionId == sop.ActionId);
            //}
            //else
            //{
            //    nextProcess = ConfigHelper.ProcessFlows.Find(x => x.Module == tmp.Module && x.CurrentPIC == tmp.CurrentRole && x.ActionId == sop.ActionId);
            //}

            //if (nextProcess.NextPIC != null)
            //{
            //    tmp.CurrentRole = nextProcess.NextPIC;
            //    tmp.CurrentState.Name = "Pending";
            //    tmp.RequestStatus.Status = "Pending " + nextProcess.NextPIC.Name;
            //    tmp.RequestStatus.PreviousStatus = tmp.RequestStatus.CurrentStatus;
            //    tmp.RequestStatus.CurrentStatus = MyEnum.Workflow.PEX;
            //}

            //if (sop.CommentRemarks != null && sop.CommentRemarks.Count > 0)
            //{
            //    tmp.CommentRemarks.Add(sop.CommentRemarks[0]);
            //}

            //tmp.Histories.Add(new History()
            //{
            //    UserId = Utils.GetUserId(),
            //    UserName = Utils.GetDisplayName(),
            //    Action = "Submit",
            //    ActionDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"),
            //    Description = "SOP request submmited"
            //});

            var res = new Response
            {
                Message = "SOP request submitted",
                RedirectUrl = "/ProcessVault/Home"
            };

            return Json(res);
        }

        public JsonResult SubmitSOP(SOP sop, string approverRole)
        {
            var tmp = SessionHelper.SOPs.Find(x => x.Id == sop.Id);

            tmp.OwnedByDepartment = sop.OwnedByDepartment;
            tmp.Validity = sop.Validity;
            //tmp.DepartmentApplicability = sop.DepartmentApplicability;
            //tmp.SOPDocument.Version = tmp.SOPDocument.Version;

            //var nextProcess = new ProcessFlow();
            //if (tmp.CurrentState.Name == "New" || tmp.CurrentState.Name == "Draft")
            //{
            //    nextProcess = ConfigHelper.ProcessFlows.Find(x => x.Module == tmp.Module && x.ActionId == sop.ActionId);
            //}
            //else
            //{
            //    nextProcess = ConfigHelper.ProcessFlows.Find(x => x.Module == tmp.Module && x.CurrentPIC == tmp.CurrentRole && x.ActionId == sop.ActionId);
            //}

            //if (nextProcess.NextPIC != null)
            //{
            //    tmp.CurrentRole = nextProcess.NextPIC;
            //    tmp.CurrentState.Name = "Pending";
            //    tmp.RequestStatus.Status = "Pending " + nextProcess.NextPIC.Name;
            //    tmp.RequestStatus.PreviousStatus = tmp.RequestStatus.CurrentStatus;
            //    tmp.RequestStatus.CurrentStatus = MyEnum.Workflow.APPROVAL;
            //}

            //if (sop.CommentRemarks != null && sop.CommentRemarks.Count > 0)
            //{
            //    tmp.CommentRemarks.Add(sop.CommentRemarks[0]);
            //}

            tmp.Histories.Add(new History()
            {
                UserId = Utils.GetUserId(),
                UserName = Utils.GetDisplayName(),
                Action = "Submit",
                ActionDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"),
                Description = "SOP request submmited"
            });

            var res = new Response
            {
                Message = "SOP submitted for approval",
                RedirectUrl = "/ProcessVault/Home"
            };
            return Json(res);
        }
        public JsonResult Approve(string id, string actionId, string comment)
        {
            var tmp = SessionHelper.SOPs.Find(x => x.Id == id);

            //tmp.CurrentState = ConfigHelper.RequestStates.Find(x => x.Name == "Pending");
            //tmp.RequestStatus.PreviousStatus = tmp.RequestStatus.CurrentStatus;

            //var nextProcess = new ProcessFlow();

            //nextProcess = ConfigHelper.ProcessFlows.Find(x => x.Module == tmp.Module && x.CurrentPIC == tmp.CurrentRole && x.ActionId == actionId);

            //if (nextProcess.NextPIC != null)
            //{
            //    tmp.CurrentRole = nextProcess.NextPIC;
            //    tmp.CurrentState.Name = "Pending";
            //    tmp.RequestStatus.Status = "Pending " + nextProcess.NextPIC.Name;
            //    tmp.RequestStatus.CurrentStatus = MyEnum.Workflow.APPROVAL;
            //}

            //if (comment != null && comment != "")
            //{
            //    tmp.CommentRemarks.Add(new CommentRemark()
            //    {
            //        Id = Guid.NewGuid().ToString(),
            //        UserId = Utils.GetUserId(),
            //        UserName = Utils.GetDisplayName(),
            //        Description = comment,
            //        CommentDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss")
            //    });
            //}

            tmp.Histories.Add(new History()
            {
                UserId = Utils.GetUserId(),
                UserName = Utils.GetDisplayName(),
                Action = "Submit",
                ActionDate = DateTime.Now.ToString("dd MMM yyyy hh:mm:ss"),
                Description = "SOP request submmited"
            });

            var res = new Response()
            {
                Message = "SOP approved",
                RedirectUrl = "/ProcessVault/Home"
            };
            return Json(res);
        }
        public async Task<ActionResult> LoadPageView(string type)
        {
            switch (type)
            {
                case "document":
                    IList<CompanyDocument> cds = new List<CompanyDocument>();
                    cds = await _documentPortal.GetAllCompanyDocuments();

                    IList<Department> d = new List<Department>();
                    d = await _departmentStore.GetAllDepartments();

                    return PartialView("_CompanyDocumentSearch", new CompanyDocumentSearchModel() { CompanyDocuments = cds.ToList(), Departments = d.ToList() });
                case "ecn":
                    return PartialView("_ECNSearch", await _ecnStore.GetECNDocuments());
                case "qms":
                    var qmssrsm = new QMSServiceReportSearchModel()
                    {
                        QMSServiceReports = LoadQMSReportList()
                    };
                    return PartialView("_QMSServiceReportSearch", qmssrsm);
                case "training":
                    return PartialView("_TrainingDocumentSearch");
                case "announcement":
                    return PartialView("_AnnouncementSearch");
                case "billing":

                    var custs = CustomerRepository.GetAll()?.ToList() ?? new List<Customer>();
                    var ords = OrderRepository.GetAll()?.ToList() ?? new List<Order>();
                    var cords = CustomerOrderRepository.GetAll()?.ToList() ?? new List<CustomerOrder>();
                    var items = ItemRepository.GetAll()?.ToList() ?? new List<Product>();
                    var accs = AccountRepository.GetAll()?.ToList() ?? new List<Account>();

                    var id = 1;
                    foreach (var cus in custs)
                    {
                        var acc = accs.Find(x => x.CustomerId == cus.Id);

                        if (cus != null && acc != null)
                        {
                            BillingRepository.Add(new BillingInfo
                            {
                                Id = id++,
                                CustomerId = cus.Id,
                                CustomerName = cus.Name,
                                BillingAddress = cus.BillingAddress.Line1 + ", " + cus.BillingAddress.Line2 + ", " + cus.BillingAddress.ZipCode + " " + cus.BillingAddress.City + ", " + cus.BillingAddress.State,
                                AccountType = acc.AccountType.ToString(),
                                LastUpdated = DateTime.Now.AddMonths(2).ToString("dd MMM yyyy HH:mm:ss"),
                                PIC = cus.BillingAddress.PIC,
                                Email = cus.BillingAddress.Email,
                                ContactNo = cus.BillingAddress.ContactNo
                            });
                        }
                    }
                    return PartialView("_BillingSearch", new BillingInfoSearchModel() { BillingInfos = BillingRepository.GetAll()?.ToList() ?? new List<BillingInfo>(), Customers = custs });
                case "adminprocument":
                    return PartialView("_ProcumentSearch");
                case "archive":
                    return PartialView("_ArchiveDocumentSearch");
                default:
                    return PartialView("_CompanyDocumentSearch");
            }
        }
        public List<QMSServiceReport> LoadQMSReportList()
        {
            var qmslist = new List<QMSServiceReport>();
            var json = "";
            if (System.IO.File.Exists(Server.MapPath("~/") + "Areas/ProcessVault/QMS/json1.txt"))
            {
                using (StreamReader r = new StreamReader(Server.MapPath("~/") + "Areas/ProcessVault/QMS/json1.txt"))
                {
                    json = r.ReadToEnd();
                }
            }
            if (json != "")
            {
                qmslist.Add(JsonConvert.DeserializeObject<QMSServiceReport>(json));
            }
            json = "";
            if (System.IO.File.Exists(Server.MapPath("~/") + "Areas/ProcessVault/QMS/json.txt"))
            {
                using (StreamReader r = new StreamReader(Server.MapPath("~/") + "Areas/ProcessVault/QMS/json.txt"))
                {
                    json = r.ReadToEnd();
                }
            }
            if (json != "")
            {
                qmslist.Add(JsonConvert.DeserializeObject<QMSServiceReport>(json));
            }
            return qmslist;
        }

        public FileContentResult GenerateBilling()
        {
            var custs = CustomerRepository.GetAll()?.ToList() ?? new List<Customer>();
            var ords = OrderRepository.GetAll()?.ToList() ?? new List<Order>();
            var cords = CustomerOrderRepository.GetAll()?.ToList() ?? new List<CustomerOrder>();
            var items = ItemRepository.GetAll()?.ToList() ?? new List<Product>();
            var accs = AccountRepository.GetAll()?.ToList() ?? new List<Account>();

            var raws = new List<RawFile>();
            foreach (var cus in custs)
            {
                var acc = accs.Find(x => x.CustomerId == cus.Id);
                var cord = cords.FindAll(x => x.CustomerId == cus.Id);
                foreach (var co in cord)
                {
                    var o = ords.Find(x => x.Id == co.OrderId);
                    foreach (var i in o.OrderItems)
                    {
                        if (cus != null && acc != null)
                        {
                            raws.Add(new RawFile
                            {
                                CustomerId = cus.Id,
                                CustomerName = cus.Name,
                                OrderId = co.OrderId,
                                AccountType = acc.AccountType.ToString(),
                                OrderDate = o.OrderDate.ToString("dd MMM yyyy HH:mm:ss"),
                                ProductId = i.Item.Id,
                                ProductName = i.Item.Name,
                                Price = i.Item.Price,
                                Unit = i.Unit
                            });
                        }
                    }
                }
            }

            var heading = "Billing";
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0} Data", heading));
                int startRowFrom = String.IsNullOrEmpty(heading) ? 1 : 3;

                // Start adding the header
                var col = 1;
                workSheet.Cells[1, col].Value = "Customer ID";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Customer Name";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Order ID";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Order Date";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Product ID";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Product Name";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Price";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Unit";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                workSheet.Cells[1, col].Value = "Account Type";
                workSheet.Cells[1, col].Style.Font.Bold = true;
                workSheet.Cells[1, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                var row = 2;
                var id = 1;
                foreach (var r in raws)
                {
                    col = 1;
                    workSheet.Cells[row, col].Value = r.CustomerId;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.CustomerName;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.OrderId;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.OrderDate;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.ProductId;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.ProductName;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = string.Format("{0:#,##0.00}",r.Price);
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.Unit;
                    workSheet.Cells[row, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    workSheet.Cells[row, col].Value = r.AccountType;
                    workSheet.Cells[row++, col++].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                }

                // Set some document properties
                package.Workbook.Properties.Title = "Billing Raw File";

                // save our new workbook and we are done!
                result = package.GetAsByteArray();

                //-------- Now leaving the using statement
                // Outside the using statement

                return File(result, ExcelExportHelper.ExcelContentType, "Billing.xlsx");
            }
        }
        public void GenerateInvoice()
        {
            var custs = CustomerRepository.GetAll()?.ToList() ?? new List<Customer>();
            var ords = OrderRepository.GetAll()?.ToList() ?? new List<Order>();
            var cords = CustomerOrderRepository.GetAll()?.ToList() ?? new List<CustomerOrder>();
            var items = ItemRepository.GetAll()?.ToList() ?? new List<Product>();
            var accs = AccountRepository.GetAll()?.ToList() ?? new List<Account>();

            var model = new InvoiceViewModel();
            var id = 1;
            foreach (var cus in custs)
            {
                var acc = accs.Find(x => x.CustomerId == cus.Id);
                var cord = cords.FindAll(x => x.CustomerId == cus.Id);
                var orders = new List<Order>();
                var tamount = 0.00;
                foreach (var c in cord)
                {
                    var ord = ords.Find(x => x.Id == c.OrderId);
                    if (ord != null && ord.OrderItems.Count > 0)
                    {
                        orders.Add(ord);
                        tamount += ord.Total;
                    }
                }
                if (cus != null && acc != null && cord != null && cord.Count() > 0)
                {
                    model.Invoices.Add(new Invoice
                    {
                        InvoiceNo = string.Format("{0:000000}", id++),
                        InvoiceDate = DateTime.Now.ToString("dd MMM yyyy"),
                        BillingPeriod = DateTime.Now.AddMonths(-1).ToString("dd MMM") + " - " + DateTime.Now.ToString("dd MMM yyyy"),
                        Recipient = cus,
                        Orders = orders,
                        PaymentStatus = acc.AccountType == AccountTypeEnum.Prepaid ? "Success" : "Pending",
                        PaymentAmount = acc.AccountType == AccountTypeEnum.Prepaid ? tamount : 0.00,
                        PaymentDate = DateTime.Now.AddHours(-3),
                        PaymentMethod = acc.AccountType.ToString(),
                        TotalAmount = tamount
                    });
                }
            }
            
            string path = "Areas/ProcessVault/Documents/";
            string location = Server.MapPath("~/") + path;
           

            Directory.CreateDirectory(location);
            foreach (var m in model.Invoices)
            {
                byte[] result = ControllerContext.GeneratePdf(m, "InvoicePreview", (writer, document) =>
                {
                    document.SetPageSize(iTS.PageSize.A4);
                    document.NewPage();
                });

                var count = 0;
                var filename = "Invoice-" + m.InvoiceNo + ".pdf";
                while (System.IO.File.Exists(location + filename))
                {
                    count++;
                    filename = filename.Insert(filename.LastIndexOf('.'), "(" + count.ToString() + ")");
                }
                count = 0;
                using (FileStream outputFile = new FileStream(location + filename, FileMode.CreateNew))
                {
                    outputFile.Write(result, 0, result.Length);
                }
            }
        }
        public ActionResult PreviewInvoice()
        {
            var custs = CustomerRepository.GetAll()?.ToList() ?? new List<Customer>();
            var ords = OrderRepository.GetAll()?.ToList() ?? new List<Order>();
            var cords = CustomerOrderRepository.GetAll()?.ToList() ?? new List<CustomerOrder>();
            var items = ItemRepository.GetAll()?.ToList() ?? new List<Product>();
            var accs = AccountRepository.GetAll()?.ToList() ?? new List<Account>();

            var model = new InvoiceViewModel();
            var id = 1;
            foreach (var cus in custs)
            {
                var acc = accs.Find(x => x.CustomerId == cus.Id);
                var cord = cords.FindAll(x => x.CustomerId == cus.Id);
                var orders = new List<Order>();
                var tamount = 0.00;
                foreach (var c in cord)
                {
                    var ord = ords.Find(x => x.Id == c.OrderId);
                    if (ord != null && ord.OrderItems.Count > 0)
                    {
                        orders.Add(ord);
                        tamount += ord.Total;
                    }
                }
                if (cus != null && acc != null && cord != null && cord.Count() > 0)
                {
                    model.Invoices.Add(new Invoice
                    {
                        InvoiceNo = string.Format("{0:000000}", id++),
                        InvoiceDate = DateTime.Now.ToString("dd MMM yyyy"),
                        BillingPeriod = DateTime.Now.AddMonths(-1).ToString("dd MMM") + " - " + DateTime.Now.ToString("dd MMM yyyy"),
                        Recipient = cus,
                        Orders = orders,
                        PaymentStatus = acc.AccountType == AccountTypeEnum.Prepaid ? "Success" : "Pending",
                        PaymentAmount = acc.AccountType == AccountTypeEnum.Prepaid ? tamount : 0.00,
                        PaymentMethod = acc.AccountType.ToString(),
                        PaymentDate = DateTime.Now.AddHours(-3),
                        TotalAmount = tamount
                    });
                }
            }

            return View("InvoicePreview", model.Invoices[0]);
        }
    }
}