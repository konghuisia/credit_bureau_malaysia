﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseConnection.Utilities
{
    public class Utils
    {
        public static String ConnectionString(string type = "")
        {
            switch(type)
            {
                case "project":
                    return System.Configuration.ConfigurationManager.ConnectionStrings["ProjectDBConnectionString"].ConnectionString;
                case "kumfatt":
                    return System.Configuration.ConfigurationManager.ConnectionStrings["KumFattDBConnectionString"].ConnectionString;
                default:
                    return System.Configuration.ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            }
        }
    }
}
