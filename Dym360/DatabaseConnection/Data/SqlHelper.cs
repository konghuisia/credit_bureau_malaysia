﻿using Dapper;
using DatabaseConnection.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using ObjectClasses.KumFattWeb;
using ObjectClasses.ProductSpecificationRequest;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DatabaseConnection.Data
{
    public static class SqlHelper
    {
        public static T GetRecord<T>(string spName, List<ParameterInfo> parameters, string type = "")
        {
            T objRecord = default(T);
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                objRecord = SqlMapper.Query<T>(objConnection, spName, p, commandType: CommandType.StoredProcedure).FirstOrDefault();
                objConnection.Close();
            }
            return objRecord;
        }

        public static List<T> GetRecords<T>(string spName, List<ParameterInfo> parameters, string type = "")
        {
            List<T> recordList = new List<T>();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                recordList = SqlMapper.Query<T>(objConnection, spName, p, commandTimeout:60, commandType: CommandType.StoredProcedure).ToList();
                objConnection.Close();
            }
            return recordList;
        }

        //public static List<T> GetRecords<T, T1>(string spName, List<ParameterInfo> parameters, string type = "")
        //{
        //    List<T> recordList = new List<T>();
        //    using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
        //    {
        //        objConnection.Open();
        //        DynamicParameters p = new DynamicParameters();
        //        foreach (var param in parameters)
        //        {
        //            p.Add("@" + param.ParameterName, param.ParameterValue);
        //        }

        //        recordList = SqlMapper.Query<T, T1, T>(objConnection, spName, (t, t1) =>
        //        {
        //            propert
        //        }, p, commandType: CommandType.StoredProcedure).ToList();
        //        objConnection.Close();
        //    }
        //    return recordList;
        //}

        //public static List<ProjectSite> GetProjectSites(string spName, List<ParameterInfo> parameters, string type = "")
        //{
        //    List<ProjectSite> recordList = new List<ProjectSite>();
        //    using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
        //    {
        //        objConnection.Open();
        //        DynamicParameters p = new DynamicParameters();
        //        foreach (var param in parameters)
        //        {
        //            p.Add("@" + param.ParameterName, param.ParameterValue);
        //        }

        //        recordList = SqlMapper.Query<ProjectSite, Client, ProjectSite>(objConnection, spName, (ps, cl) =>
        //        {
        //            ps.Client = cl;
        //            return ps;
        //        }, p, splitOn: "ProjectId,ClientId", commandType: CommandType.StoredProcedure).ToList();


        //        objConnection.Close();
        //    }
        //    return recordList;
        //}

        public static List<Employee> GetEmployees(string spName, List<ParameterInfo> parameters, string type = "")
        {
            List<Employee> recordList = new List<Employee>();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                recordList = SqlMapper.Query<Employee, EmployeeDetail, ProfileImage, EmployeeWage, EmployeeQR, Employee>(objConnection, spName, (emp, empD, pi, ew, qr) =>
                {
                    emp.Detail = empD;
                    emp.ProfileImage = pi;
                    emp.Wage = ew;
                    emp.QR = qr;
                    return emp;
                }, p, splitOn: "EmployeeId,EmployeeDetailId,EmployeePictureId,EmployeeWageId,EmployeeQRId", commandType: CommandType.StoredProcedure).ToList();
                

                objConnection.Close();
            }
            return recordList;
        }

        public static Employee GetEmployee(string spName, List<ParameterInfo> parameters, string type = "")
        {
            Employee record = new Employee();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                record = SqlMapper.Query<Employee, EmployeeDetail, ProfileImage, EmployeeWage, EmployeeAllowance, EmployeeQR, AdvancePayment, Employee>(objConnection, spName, (emp, empD, pi, ew, ea, qr, ap) =>
                {
                    emp.Detail = empD;
                    emp.ProfileImage = pi;
                    emp.Wage = ew;
                    emp.Allowance = ea;
                    emp.QR = qr;
                    emp.AdvancePayment = ap;
                    return emp;
                }, p, splitOn: "EmployeeId,EmployeeDetailId,EmployeePictureId,EmployeeWageId,EmployeeId,EmployeeQRId,EmployeeLoanId", commandType: CommandType.StoredProcedure).Single();


                objConnection.Close();
            }
            return record;
        }

        public static List<ProjectSales> GetProjectSales(string spName, List<ParameterInfo> parameters)
        {
            List<ProjectSales> recordList = new List<ProjectSales>();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                recordList = SqlMapper.Query<ProjectSales, Customer, Project, Proposal, RequestState, ProjectSales>(objConnection, spName, (ps, cust, proj, props, state) =>
                {
                    ps.CustomerInfo = cust;
                    ps.ProjectInfo = proj;
                    ps.ProjectInfo.Proposal = props;
                    ps.RequestState = state;
                    return ps;
                }, p, splitOn: "CustomerId,ProjectId,ProposalId,State", commandType: CommandType.StoredProcedure).ToList();



                objConnection.Close();
            }
            return recordList;
        }

        public static ProjectSales GetProjectSale(string spName, List<ParameterInfo> parameters)
        {
            ProjectSales record = new ProjectSales();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                record = SqlMapper.Query<ProjectSales, Customer, Project, Proposal, RequestState, ProjectSales>(objConnection, spName, (ps, cust, proj, props, reqs) =>
                {
                    ps.CustomerInfo = cust;
                    ps.ProjectInfo = proj;
                    ps.ProjectInfo.Proposal = props;
                    ps.RequestState = reqs;
                    return ps;
                }, p, splitOn: "CustomerId,ProjectId,ProposalId,RequestStateId", commandType: CommandType.StoredProcedure).FirstOrDefault();



                objConnection.Close();
            }
            return record;
        }

        public static List<SpecificationRequest> GetProductSpacifications(string spName, List<ParameterInfo> parameters)
        {
            List<SpecificationRequest> records = new List<SpecificationRequest>();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                records = SqlMapper.Query<SpecificationRequest, Request, RequestState, Requester, SpecificationRequest>(objConnection, spName, (sr, req, reqs, reqt) =>
                {
                    req.RequestState = reqs;
                    req.Requester = reqt;
                    sr.Request = req;

                    return sr;
                }, p, splitOn: "ProductSpecificationId,RequestId,RequestStateId,UserId", commandType: CommandType.StoredProcedure).ToList();



                objConnection.Close();
            }
            return records;
        }

        public static List<ECNModel> GetECNModels(string spName, List<ParameterInfo> parameters)
        {
            List<ECNModel> records = new List<ECNModel>();
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                records = SqlMapper.Query<ECNModel, ECNDocument, ECNModel>(objConnection, spName, (ecn, doc) =>
                {
                    ecn.ECNDocument = doc;
                    return ecn;
                }, p, splitOn: "RequestId,ECNDocumentId", commandType: CommandType.StoredProcedure).ToList();



                objConnection.Close();
            }
            return records;
        }

        public static int GetIntRecord<T>(string spName, List<ParameterInfo> parameters)
        {
            int intRecord = 0;
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }

                using (var reader = SqlMapper.ExecuteReader(objConnection, spName, p, commandType: CommandType.StoredProcedure))
                {
                    if (reader != null && reader.Read())
                    {
                        intRecord = Convert.ToInt32(reader[0].ToString());
                    }
                }
                objConnection.Close();
            }
            return intRecord;
        }

        public static int ExecuteQuery(string spName, List<ParameterInfo> parameters, string type = "")
        {
            int success = 0;
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString(type)))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }
                success = SqlMapper.Execute(objConnection, spName, p, commandType: CommandType.StoredProcedure);
                objConnection.Close();
            }
            return success;
        }

        public static int ExecuteQueryWithIntOutputParam(string spName, List<ParameterInfo> parameters)
        {
            int success = 0;
            using (SqlConnection objConnection = new SqlConnection(Utils.ConnectionString()))
            {
                objConnection.Open();
                DynamicParameters p = new DynamicParameters();
                foreach (var param in parameters)
                {
                    p.Add("@" + param.ParameterName, param.ParameterValue);
                }
                success = SqlMapper.Execute(objConnection, spName, p, commandType: CommandType.StoredProcedure);
                objConnection.Close();
            }
            return success;
        }
        // This function takes arguments for 2 connection strings and commands to create a transaction 
        // involving two SQL Servers. It returns a value > 0 if the transaction is committed, 0 if the 
        // transaction is rolled back. To test this code, you can connect to two different databases 
        // on the same server by altering the connection string, or to another 3rd party RDBMS by 
        // altering the code in the connection2 code block.
        static public int CreateTransactionScope(
            string connectString1, string connectString2,
            string commandText1, string commandText2)
        {
            // Initialize the return value to zero and create a StringWriter to display results.
            int returnValue = 0;
            System.IO.StringWriter writer = new System.IO.StringWriter();

            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection connection1 = new SqlConnection(connectString1))
                    {
                        // Opening the connection automatically enlists it in the 
                        // TransactionScope as a lightweight transaction.
                        connection1.Open();

                        // Create the SqlCommand object and execute the first command.
                        SqlCommand command1 = new SqlCommand(commandText1, connection1);
                        returnValue = command1.ExecuteNonQuery();
                        writer.WriteLine("Rows to be affected by command1: {0}", returnValue);

                        // If you get here, this means that command1 succeeded. By nesting
                        // the using block for connection2 inside that of connection1, you
                        // conserve server and network resources as connection2 is opened
                        // only when there is a chance that the transaction can commit.   
                        using (SqlConnection connection2 = new SqlConnection(connectString2))
                        {
                            // The transaction is escalated to a full distributed
                            // transaction when connection2 is opened.
                            connection2.Open();

                            // Execute the second command in the second database.
                            returnValue = 0;
                            SqlCommand command2 = new SqlCommand(commandText2, connection2);
                            returnValue = command2.ExecuteNonQuery();
                            writer.WriteLine("Rows to be affected by command2: {0}", returnValue);
                        }
                    }

                    // The Complete method commits the transaction. If an exception has been thrown,
                    // Complete is not  called and the transaction is rolled back.
                    scope.Complete();

                }

            }
            catch (TransactionAbortedException ex)
            {
                writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
            }
            catch (ApplicationException ex)
            {
                writer.WriteLine("ApplicationException Message: {0}", ex.Message);
            }

            // Display messages.
            Console.WriteLine(writer.ToString());

            return returnValue;
        }
    }
}