﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Security.Claims;
using System.Security.Principal;

namespace IdentityManagement.Utilities
{
    public class Utils
    {
        public static bool IfUserAuthenticated()
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return true;
            }
            return false;
        }
        public static String GetDisplayName()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var name = identity.FindAll(m => m.Type == CustomClaimTypes.DisplayName);
            if (name.Count() > 0)
            {
                return name.First().Value;
            }

            throw new Exception("Display name not found");
        }
        public static String GetUserPosition()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var name = identity.FindAll(m => m.Type == CustomClaimTypes.Position);
            if (name.Count() > 0)
            {
                return name.First().Value;
            }

            throw new Exception("Position not found");
        }
        public static String GetUserGrad()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var name = identity.FindAll(m => m.Type == CustomClaimTypes.Grad);
            if (name.Count() > 0)
            {
                return name.First().Value;
            }

            throw new Exception("Position not found");
        }
        public static String GetUserReporting()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var reporting = identity.FindAll(m => m.Type == CustomClaimTypes.Reporting);
            if (reporting.Count() > 0)
            {
                return reporting.First().Value;
            }

            return "";
        }

        public static String GetLastLoginDate()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var name = identity.FindAll(m => m.Type == CustomClaimTypes.LastLoginDate);
            if (name.Count() > 0)
            {
                return name.First().Value;
            }
            return null;
        }
        public static String GetUsername()
        {
            return HttpContext.Current.User.Identity.Name;
        }
        public static String GetUserId()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var id = identity.FindAll(m => m.Type == CustomClaimTypes.UserId);
            if (id.Count() > 0)
            {
                return id.First().Value;
            }

            throw new Exception("User ID not found");
        }
        public static bool IfUserInRole(string roleName)
        {
            if (IfUserAuthenticated())
            {
                if (HttpContext.Current.User.IsInRole(roleName))
                {
                    return true;
                }
            }
            return false;
        }
        public static String GetUserDepartment()
        {
            var identity = (ClaimsIdentity)HttpContext.Current.User.Identity;
            var id = identity.FindAll(m => m.Type == CustomClaimTypes.Department);
            if (id.Count() > 0)
            {
                return id.First().Value;
            }

            throw new Exception("Department ID not found");
        }
    }
}