﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender
{
    public class EmailSender
    {
        public async Task SendEmails(List<string> emails, string link)
        {
            try
            {
                if (emails.Count > 0)
                {
                    foreach (var e in emails)
                    {
                        if (IsValidEmail(e))
                        {
                            MailAddress to = new MailAddress(e);

                            MailAddress from = new MailAddress("corporatepartnermail@gmail.com");

                            try
                            {
                                //await smtp.SendMailAsync(mail);
                                //Console.WriteLine("Email sent");


                                Thread email = new Thread(delegate ()
                                {
                                    //SendEmail(to.ToString(), from.ToString(), "CP##2019", "Attention", link);
                                });

                                email.IsBackground = true;
                                email.Start();
                            }

                            catch (Exception ex)
                            {
                                Console.WriteLine("Error {0} exception caught.", ex);
                            }
                            //mail.Attachments.Dispose();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }
        public bool SendClaimManagementEmail(string mailTo, string link, string subject, string body, string requestNo, string requestSubject, string username)
        {
            try
            {
                if (!string.IsNullOrEmpty(mailTo))
                {
                    if (IsValidEmail(mailTo))
                    {
                        MailAddress to = new MailAddress(mailTo);

                        MailAddress from = new MailAddress("dym360.2019@gmail.com");

                        try
                        {
                            //await smtp.SendMailAsync(mail);
                            //Console.WriteLine("Email sent");


                            Thread email = new Thread(delegate ()
                            {
                                SendEmail(to.ToString(), from.ToString(), "Dym#360!", subject, body, link, requestNo, requestSubject, username);
                            });

                            email.IsBackground = true;
                            email.Start();

                            return true;
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine("Error {0} exception caught.", ex);
                        }
                        //mail.Attachments.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return false;
        }
        public bool SendCustomerEmail(string mailTo, string link, string subject, string body, string requestNo, string requestSubject, string username, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(mailTo))
                {
                    if (IsValidEmail(mailTo))
                    {
                        MailAddress to = new MailAddress(mailTo);

                        MailAddress from = new MailAddress("dym360.2019@gmail.com");

                        try
                        {
                            //await smtp.SendMailAsync(mail);
                            //Console.WriteLine("Email sent");


                            Thread email = new Thread(delegate ()
                            {
                                SendCustomerEmail(to.ToString(), from.ToString(), "Dym#360!", subject, body, link, requestNo, requestSubject, username, password);
                            });

                            email.IsBackground = true;
                            email.Start();

                            return true;
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine("Error {0} exception caught.", ex);
                        }
                        //mail.Attachments.Dispose();
                    }
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            return false;
        }
        private void SendEmail(string to, string from, string password, string subject, string body, string link, string requestNo, string requestSubject, string username)
        {
            try
            {
                subject = subject.Replace("<Subject>", requestSubject);
                subject = subject.Replace("<Request Number>", requestNo);

                body = body.Replace("<User Name>", username);
                body = body.Replace("<Subject>", requestSubject);
                body = body.Replace("<Link>", "<a target='_blank' href='http://" + link + "'>http://" + link + "</a>");
                body = body.Replace("\n", "<br>");

                var fromAddress = new MailAddress(from, "DYM360");
                var toAddress = new MailAddress(to);

                using (MailMessage mm = new MailMessage(fromAddress, toAddress))
                {
                    mm.IsBodyHtml = true;
                    mm.Subject = subject;
                    mm.Body = "<html><span style='font-size:12.0pt;font-family:Arial'>" + body + "</span></html>";
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, password);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        private void SendCustomerEmail(string to, string from, string password, string subject, string body, string link, string requestNo, string requestSubject, string username, string reqPassword)
        {
            try
            {
                subject = subject.Replace("<Subject>", requestSubject);
                subject = subject.Replace("<Request Number>", requestNo);

                body = body.Replace("<Name>", username);
                body = body.Replace("<Subject>", requestSubject);
                body = body.Replace("<Link>", "<a target='_blank' href='http://" + link + "'>http://" + link + "</a>");
                body = body.Replace("\n", "<br>");
                if (reqPassword.Length > 10)
                {
                    body = body.Replace("<Password>", reqPassword.Substring(0, 10));
                }

                var fromAddress = new MailAddress(from, "DYM360");
                var toAddress = new MailAddress(to);

                using (MailMessage mm = new MailMessage(fromAddress, toAddress))
                {
                    mm.IsBodyHtml = true;
                    mm.Subject = subject;
                    mm.Body = "<html><span style='font-size:12.0pt;font-family:Arial'>" + body + "</span></html>";
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(from, password);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }

                //using (MailMessage mm = new MailMessage(fromAddress, toAddress))
                //{
                //    mm.IsBodyHtml = true;
                //    mm.Subject = subject;
                //    mm.Body = reqPassword.Substring(0, 10);
                //    SmtpClient smtp = new SmtpClient();
                //    smtp.Host = "smtp.gmail.com";
                //    smtp.EnableSsl = true;
                //    NetworkCredential NetworkCred = new NetworkCredential(from, password);
                //    smtp.UseDefaultCredentials = true;
                //    smtp.Credentials = NetworkCred;
                //    smtp.Port = 587;
                //    smtp.Send(mm);
                //}
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
