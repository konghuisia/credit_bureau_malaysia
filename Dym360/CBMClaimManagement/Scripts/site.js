﻿"use strict";
var sop;
var doc;
var comment;
var reviewers;
function formatDate(d) {
    var date = new Date(d);

    var hour = date.getHours();     // yields hours 
    var minute = date.getMinutes(); // yields minutes
    var second = date.getSeconds(); // yields seconds

    // After this construct a string with the above results as below
    return date.toShortFormat();
}

function formatDateTime(d) {
    var date = new Date(d);

    var hour = date.getHours().padLeft();     // yields hours 
    var minute = date.getMinutes().padLeft(); // yields minutes
    var second = date.getSeconds().padLeft(); // yields seconds

    // After this construct a string with the above results as below
    return date.toShortFormat() + ' ' + hour + ':' + minute + ':' + second;
}


Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
};
Date.prototype.toShortFormat = function () {

    var month_names = ["Jan", "Feb", "Mar",
        "Apr", "May", "Jun",
        "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec"];

    var day = this.getDate();
    var month_index = this.getMonth();
    var year = this.getFullYear();

    return "" + day + " " + month_names[month_index] + " " + year;
};
const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

function showToast(msg, type) {
    if (type === "success") {
        $.toast({
            heading: 'Success',
            text: msg,
            position: 'top-roght',
            stack: false
        });
    }
    else if (type === "error") {
        $.toast({
            heading: 'Error',
            text: msg,
            position: 'top-roght',
            icon: 'error'
        });
    }
}


function LoadPageView(elemId, url, data) {
    $(elemId).load(url, data);
}

function Search() {
    $('#tblAccount tbody tr').each(function () {
        var isHide = false;
        $(this).find('td').each(function () {
            var input = $('#inpt' + $(this).data('input')).val();

            if (input !== '' && input !== undefined && input !== 'all') {
                if ($(this).data('input') === "Status") {
                    if ($(this).text().trim() !== input) {
                        isHide = true;
                    }
                }
                else {
                    if ($(this).text().toLowerCase().indexOf(input.toLowerCase()) < 0) {
                        isHide = true;
                    }
                }
            }
        })

        if (isHide) {
            $(this).css('display', 'none');
        }
        else {
            $(this).css('display', 'table-row');
        }
    });
}
function ClearSearch() {
    $('#tblAccount tbody tr').prop('style', '');
    $('input').val('');
    $('select').val('all');
}

function sortTable(e) {
    var table, rows, switching, i, x, y, shouldSwitch, type, elemId, col;
    table = document.getElementById($(e).parent().parent().parent().parent().prop('id'));
    type = $(e).data('type');
    elemId = $(e).prop('id');
    col = $(e).parent().index();
    switching = true;
    /*Make a loop that will continue until
    no switching has been done:*/
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
        first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
            one from current row and one from the next:*/
            //rows[i].getElementsByTagName("td")[0].innerHTML = i;

            x = rows[i].getElementsByTagName("td")[col];
            y = rows[i + 1].getElementsByTagName("td")[col];
            //check if the two rows should switch place:
            if (type === 'asc') {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
            else {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    //if so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
            and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
    if (type === 'asc') {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="desc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-bottom"></span>');
    }
    else {
        $('#' + elemId).replaceWith('<span id="' + elemId + '" data-type="asc" onclick="sortTable(this)" class="glyphicon glyphicon-triangle-top"></span>');
    }
}

function PrintPreview() {
    $.ajax({
        url: window.location.origin + "/ProcessVault/ECN/PrintPreview",
        data: { id: "" },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

var wo;
function GetWO() {
    $.ajax({
        url: "https://dym360mobile.azurewebsites.net/Home/GetCompleteWorkOrder",
        cache: false,
        type: "POST",
        success: function (data, textStatus, XMLHttpRequest) {
            LoadSignature(data.Signature, data.WO);
        }
    });
}

function LoadSignature(imgData, jsData) {
    $.ajax({
        url: window.location.origin + "/ProcessVault/QMS/UploadFile",
        cache: false,
        type: "POST",
        data: { imageData: imgData, jsonData: jsData },
        success: function (data, textStatus, XMLHttpRequest) {

        }
    });
}

function InitQMSServiceReport() {
    var woCompletedDate;
    var woRespondedDate;
    var audits = wo.Audits;
    for (var i = 0; i < audits.length; i++) {
        var a = audits[i];
        if (a.Action === 'Responded') {
            woRespondedDate = formatDateTime(a.ActionDate);
        }
        if (a.Action === 'Completed') {
            woCompletedDate = formatDateTime(a.ActionDate);
        }
    }

    $.ajax({
        url: window.location.origin + "/ProcessVault/Home/LoadQMSReportList",
        data: {
            'qms': {
                'WONo': wo.WorkOrder.WONo,
                'ClinicName': wo.Clinic.ClinicName,
                'BENo': wo.Asset.BENo,
                'BECategory': wo.Asset.BECategory,
                'WRDate': formatDateTime(wo.RequestDate),
                'WODate': formatDateTime(wo.WorkOrder.WODate),
                'WOResponded': woRespondedDate,
                'WOCompleted': woCompletedDate,
                'ProblemCode': wo.WorkOrder.Detail.ProblemCode,
                'CorrectiveAction': wo.WorkOrder.Action.CorrectiveAction,
                'EngineerName': wo.WorkOrder.Detail.AssignTo
            }
        },
        cache: false,
        type: "POST",
        success: function (data, textStatus, XMLHttpRequest) {

        }
    });
}

function restrictDecimalInput(keydownEvent) {
    var e = keydownEvent;
    if ($(e.target).val().indexOf('.') > 0) {
        if (e.key === '.') {
            e.preventDefault();
        }
    }
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
        // Allow: Ctrl/cmd+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+C
        (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: Ctrl/cmd+X
        (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}