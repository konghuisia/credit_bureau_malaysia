﻿function init() {
    initInputRestriction();
    $('.currency').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.currency').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
        else {
            $(e.target).val('0.00');
        }
    });
}

function ClaimTypeChange(e) {
    if (e.value === "D0A1E5EF-6552-4B3A-8954-02886AE38CC3") {//Travelling
        $('#inptMileage').removeAttr('disabled');
        $('#inptClaimAmount').attr('disabled', true);
        $('#inptClaimAmount').val('');
    }
    else {
        $('#inptMileage').attr('disabled', true);
        $('#inptClaimAmount').removeAttr('disabled');
        $('#inptMileage').val('');
    }
}

function initInputRestriction() {
    $('#inptIdentity').keydown(function (e) {
        restrictAlphanumericInput(e);
    });
    $('#inptContactNo').keydown(function (e) {
        restrictIntegerInput(e);
    });
    $('.datepicker').keypress(function () {
        return false;
    });
    $('.decimal').keydown(function (e) {
        restrictDecimalInput(e);
    });
    $('.decimal').blur(function () {
        if ($(this).val().trim() !== "") {
            var t = $(this).val().replace(/\,/g, '');
            $(this).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
    });
    $('#inptMileage').blur(function () {
        var mileage = $(this).val();
        var times = 0.6;

        if ($(this).val() !== "0.00" && $(this).val() !== "") {
            $('#inptClaimAmount').val(numberWithCommas(parseFloat(mileage * times).toFixed(2)));
        }
    });
    $('#inptRemark').keydown(function (e) {
        if (e.which === 32 && !this.value.length) {
            e.preventDefault();
        }
        var inputValue = event.charCode;
        if (!(inputValue >= 65 && inputValue <= 120) && (inputValue !== 32 && inputValue !== 0)) {
            event.preventDefault();
        }   
    });
}

function bindAddApproverClick() {
        var a = $('#inptApprover option:selected');
        if (a.val() !== '0') {
            $('#tblApprover').append('<tr><td colspan="2"><p data-value="' + a.val() + '">' + a.text() + '</p></td>' +
                '<td style="text-align: center">' +
                '<a class="delete" onclick="removeApprover(this)" data-value="' + a.val() + '" title="Delete" data-toggle="tooltip"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td></tr>');
            a.addClass('hidden');
            $('#inptApprover').val(0);
        }
}

function removeApprover(e) {
    $.ajax({
        url: window.location.origin + "/CBMClaimManagement/Home/RemoveApprover",
        data: { "userId": $(e).data('value'), "requestId": $('#requestId').val() },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            $('#inptApprover [value=' + $(e).data('value') + ']').removeClass('hidden');
            $(e).parent().parent().remove();
        }
    });
}

function uploadClaim(e) {

    //if (e.parent().parent())
    var isValid = true;


    if ($('#inptDate').val().trim() === '') {
        if ($('#inptDate').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter date to proceed</p>').insertAfter($('#inptDate'));
        }
        isValid = false;
    }
    else {
        if ($('#inptDate').next().length > 0) {
            $('#inptDate').next().remove();
        }
    }

    if ($('#inptClaimTypeId').val() === 'D0A1E5EF-6552-4B3A-8954-02886AE38CC3' && $('#inptMileage').val() === '' || $('#inptMileage').val() === '0.00') {//Travelling
        if ($('#inptMileage').next().length < 1) {
            if ($('#inptMileage').val() === '') {
                $('<p class="invalid-alert" style="color:red;">Please enter mileage to proceed</p>').insertAfter($('#inptMileage'));
            }
            else if ($('#inptMileage').val() === '0.00') {
                $('<p class="invalid-alert" style="color:red;">Please enter a valid mileage to proceed</p>').insertAfter($('#inptMileage'));
            }
        }
        else {
            $('#inptMileage').next().remove();
            if ($('#inptMileage').val() === '') {
                $('<p class="invalid-alert" style="color:red;">Please enter mileage to proceed</p>').insertAfter($('#inptMileage'));
            }
            else if ($('#inptMileage').val() === '0.00') {
                $('<p class="invalid-alert" style="color:red;">Please enter a valid mileage to proceed</p>').insertAfter($('#inptMileage'));
            }
        }
        isValid = false;
    }
    else {
        if ($('#inptMileage').next().length > 0) {
            $('#inptMileage').next().remove();
        }
    }

    if ($('#inptClaimTypeId').val() !== 'D0A1E5EF-6552-4B3A-8954-02886AE38CC3' && $('#inptClaimAmount').val() === '' || $('#inptClaimAmount').val() === '0.00') {//Travelling
        if ($('#inptClaimAmount').next().length < 1) {
            if ($('#inptClaimAmount').val() === '') {
                $('<p class="invalid-alert" style="color:red;">Please enter claim to proceed</p>').insertAfter($('#inptClaimAmount'));
            }
            if ($('#inptClaimAmount').val() === '0.00') {
                $('<p class="invalid-alert" style="color:red;">Please enter a valid amount to proceed</p>').insertAfter($('#inptClaimAmount'));
            }
        }
        else {
            $('#inptClaimAmount').next().remove();
            if ($('#inptClaimAmount').val() === '') {
                $('<p class="invalid-alert" style="color:red;">Please enter claim to proceed</p>').insertAfter($('#inptClaimAmount'));
            }
            if ($('#inptClaimAmount').val() === '0.00') {
                $('<p class="invalid-alert" style="color:red;">Please enter a valid amount to proceed</p>').insertAfter($('#inptClaimAmount'));
            }
        }
        isValid = false;
    }
    else {
        if ($('#inptClaimAmount').next().length > 0) {
            $('#inptClaimAmount').next().remove();
        }
    }
    
    if (isValid === true) {

        var a = $(e).parent().parent().children();

        var formdata = new FormData(); //FormData object
        var fileInput;
        var row = "";

        if ($('#inptMileage').val() === "") {
            $('#inptMileage').val('0.00');
        }

        if ($('#inptClaimAmount').val() === "") {
            $('#inptClaimAmount').val('0.00');
        }

        formdata.append("RequestId", $('#requestId').val());

        fileInput = document.getElementById('inptClaim');
        formdata.append('ClaimTypeId', $('#inptClaimTypeId').val());
        formdata.append('Date', $('#inptDate').val());
        formdata.append('Remark', $('#inptRemark').val());
        formdata.append('Mileage', $('#inptMileage').val().trim().replace(/\,/g, ''));
        formdata.append('ClaimAmount', $('#inptClaimAmount').val().trim().replace(/\,/g, ''));

        if ($('#inptClaimTypeId').val() !== 'D0A1E5EF-6552-4B3A-8954-02886AE38CC3') {//Travelling
            $('#inptMileage').val('');
        }

        //Iterating through each files selected in fileInput
        if (fileInput !== null) {
            for (var i = 0; i < fileInput.files.length; i++) {
                //Appending each file to FormData object
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/CBMClaimManagement/Home/UploadClaims');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {

                $('.totalRow').remove();

                var r = $('#tblClaimInformation tbody tr').length + 1;
                
                var d = JSON.parse(xhr.response);

                var mileAge = numberWithCommas(parseFloat(d.Mileage).toFixed(2));
                if (d.ClaimTypeId !== "Travelling") {
                    mileAge = "";
                }

                row += '<tr>' +
                    '<td>' + r + '</td>' +
                    '<td>' + d.DateText + '</td> ' +
                    '<td>' + d.Remark + '</td>' +
                    '<td>' + d.ClaimTypeId + '</td> ' +
                    '<td style="text-align:right;">' + mileAge + '</td>' +
                    '<td class="count" style="text-align:right;">' + numberWithCommas(parseFloat(d.ClaimAmount).toFixed(2)) + '</td>' +
                    '<td>' + d.FileName + '</td>' +
                    '<td style="text-align:center;"><a class="delete" title="Delete" data-value="' + d.ClaimId + '" onclick="removeClaim(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>' +
                    '</tr>';


                $('#tblClaimInformation tbody').append(row);
                $('#inptClaim').val('');
                $('#inptRemark').val('');
                $('#inptDate').val('');
                $('#inptMileage').val('');
                $('#inptClaimAmount').val('');

                var all = $(".count").map(function () {
                    return this.innerHTML;
                }).get();

                var total = 0.00;
                $.each(all, function (key, value) {
                    total += parseFloat(value.replace(/,/g, ''));
                });

                var totalRow = '<tr class=totalRow>' +
                    '<td></td>' +
                    '<td></td> ' +
                    '<td></td> ' +
                    '<td></td>' +
                    '<td></td>' +
                    '<td style="text-align:right;"><div style="border-top: black 1px solid; border-bottom: black 1px solid;">' + numberWithCommas(parseFloat(total).toFixed(2)) + '</div></td>' +
                    '<td></td>' +
                    '<td></td>' +
                    '</tr>';
                $('#tblClaimInformation tbody').append(totalRow);
                
            }
        };
    }
}

function uploadDoc(type) {
    var formdata = new FormData(); //FormData object
    formdata.append("RequestId", $('#requestId').val());
    var fileInput;
    var control = '';
    if (type === "requester") {
        formdata.append("UploadBy", type);
        formdata.append("LastModifiedDate", $('#LastModifiedDate').val());
        fileInput = document.getElementById('fileSelectRequester');
    }

    //Iterating through each files selected in fileInput
    for (var i = 0; i < fileInput.files.length; i++) {
        //Appending each file to FormData object
        formdata.append(fileInput.files[i].name, fileInput.files[i]);
    }

    //Creating an XMLHttpRequest and sending
    var xhr = new XMLHttpRequest();
    xhr.open('POST', window.location.origin + '/CBMClaimManagement/Home/Upload');
    xhr.send(formdata);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var d = JSON.parse(xhr.response);

            var row = "";
            if (type === "requester") {
                $('#inptRequesterfile tbody')[0].innerHTML = '';
                for (var i = 0; i < d.length; i++) {
                    row += '<tr>' +
                        '<td>' + d[i].CreatedDate + '</td>' +
                        '<td>' + d[i].Name + '</td>' +
                        '<td>' + d[i].Size + '</td> ' +
                        '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                        '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                    '</tr>';
                }

                $('#inptRequesterfile').append(row);
                $('#fileSelectRequester').val('');
            }
        }
    };
}

function removeAttachment(e) {
    var attachmentId = $(e).parent().data('value');

    $.ajax({
        url: window.location.origin + "/CBMClaimManagement/Home/DeleteAttachment",
        data: { "attachmentId": attachmentId },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            showToast(d.Message, "success");
            $(e).parent().parent().remove();
        }
    });
}

function removeClaim(e) {
    var claimId = $(e).data('value');

    $.ajax({
        url: window.location.origin + "/CBMClaimManagement/Home/DeleteClaim",
        data: { "claimId": claimId },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            showToast(d.Message, "success");
            $(e).parent().parent().remove();

            $('.totalRow').remove();
            var all = $(".count").map(function () {
                return this.innerHTML;
            }).get();

            var total = 0.00;
            $.each(all, function (key, value) {
                total += parseFloat(value.replace(/,/g, ''));
            });

            var totalRow = '<tr class=totalRow>' +
                '<td></td>' +
                '<td></td> ' +
                '<td></td> ' +
                '<td></td>' +
                '<td></td>' +
                '<td style="text-align:right;"><div style="border-top: black 1px solid; border-bottom: black 1px solid;">' + numberWithCommas(parseFloat(total).toFixed(2)) + '</div></td>' +
                '<td></td>' +
                '</tr>';
            $('#tblClaimInformation tbody').append(totalRow);
        }
    });
}

function inputValidation(aid) {
    var isValid = true;

    if (aid === undefined || aid === null) {
        if ($('#tblApprover tbody tr').length < 1) {
            isValid = false;
            if ($('#tblApprover tfoot tr').next().length < 1) {
                $('#tblApprover tfoot').append('<tr><td colspan="3"><p class="invalid-alert" style="color:red; font-size:10px">Please select approver to proceed</p></td></tr>');
            }
        }
        else {
            if ($('#tblApprover tfoot tr').next().length > 0) {
                $('#tblApprover tfoot tr:last-child').remove();
            }
        }
    }

    //if ($('#inptSubject').val().trim() === '') {
    //    isValid = false;
    //    if ($('#inptSubject').next().length < 1) {
    //        $('<p style="color:red;">Please enter subject to proceed</p>').insertAfter($('#inptSubject'));
    //    }
    //}
    //else {
    //    if ($('#inptSubject').next().length > 0) {
    //        $('#inptSubject').next().remove();
    //    }
    //}

    if ($('#inptPeriod').val().trim() === '') {
        isValid = false;
        if ($('#inptPeriod').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter period to proceed</p>').insertAfter($('#inptPeriod'));
        }
    }
    else {
        if ($('#inptPeriod').next().length > 0) {
            $('#inptPeriod').next().remove();
        }
    }

    if ($('#lblStatus').text() === 'Pending Staff') {
        if ($('#inptComment').val().trim() === '') {
            isValid = false;
            if ($('#inptComment').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
            }
        }
        else {
            if ($('#inptComment').next().length > 0) {
                $('#inptComment').next().remove();
            }
        }
    }


    return isValid;
}

function SaveDraft(actionId) {

    if ($('#CanSubmit').val() === "true") {
        $('#CanSubmit').val('false');
    }
    else {
        return;
    }

    //if (inputValidation(actionId)) {
        var approvers = [];
        if ($('#lblStatus').text() === "Pending Staff" || $('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
            $('#tblApprover tbody tr').each(function () {
                approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
            });
        }
        
        var request = {
            "ActionId": actionId,
            "Period": $('#inptPeriod').val(),
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Approvers": approvers
        };
        $.ajax({
            url: window.location.origin + "/CBMClaimManagement/Home/SaveDraft",
            data: { "req": request },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    //}
}

function Submit(actionId) {
    if (inputValidation()) {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var approvers = [];
        if ($('#lblStatus').text() === "Pending Staff" || $('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
            $('#tblApprover tbody tr').each(function () {
                approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
            });
        }

        var request = {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Comment": $('#inptComment').val(),
            "Subject": $('#inptSubject').val(),
            "Period": $('#inptPeriod').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Approvers": approvers
        };
        $.ajax({
            url: window.location.origin + "/CBMClaimManagement/Home/Submit",
            data: { "req": request },
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Approve(actionId) {

    if ($('#CanSubmit').val() === "true") {
        $('#CanSubmit').val('false');
    }
    else {
        return;
    }

    var data = {
        "req": {
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "Subject": $('#inptSubject').val(),
            "Description": $('#inptDescription').val(),
            "ReferenceNo": $('#ReferenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    };
    $.ajax({
        url: window.location.origin + "/CBMClaimManagement/Home/Approve",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                $('#CanSubmit').val('true');
                showToast(d.Error, "error");
            }
        }
    });
}

function Reject(actionId) {
    if ($('#inptComment').val() !== "") {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var data = {
            "req": {
                "ActionId": actionId,
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "ReferenceNo": $('#ReferenceNo').val(),
                "Comment": $('#inptComment').val()
            }
        };
        $.ajax({
            url: window.location.origin + "/CBMClaimManagement/Home/Reject",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}

function Complete(actionId) {

    if ($('#CanSubmit').val() === "true") {
        $('#CanSubmit').val('false');
    }
    else {
        return;
    }

    var approvers = [];
    if ($('#lblStatus').text() === "Pending Staff" || $('#lblStatus').text() === "New" || $('#lblStatus').text() === "Draft") {
        $('#tblApprover tbody tr').each(function () {
            approvers.push({ "UserId": $(this).find('p').data('value'), "DeletedFlag": "N" });
        });
    }

    var request = {
        "ActionId": actionId,
        "RequestId": $('#requestId').val(),
        "Comment": $('#inptComment').val(),
        "Period": $('#inptPeriod').val(),
        "Subject": $('#inptSubject').val(),
        "Description": $('#inptDescription').val(),
        "ReferenceNo": $('#ReferenceNo').val(),
        "Approvers": approvers
    };
    $.ajax({
        url: window.location.origin + "/CBMClaimManagement/Home/Complete",
        data: { "req": request },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                setTimeout(function () {
                    window.location.href = window.location.origin + d.RedirectUrl;
                }, 2000);
            }
            else {
                $('#CanSubmit').val('true');
                showToast(d.Error, "error");
            }
        }
    });
}

function RequestMore(actionId) {
    if ($('#inptComment').val() !== "") {

        if ($('#CanSubmit').val() === "true") {
            $('#CanSubmit').val('false');
        }
        else {
            return;
        }

        var data = {
            "req": {
                "ActionId": actionId,
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "ReferenceNo": $('#ReferenceNo').val(),
                "Comment": $('#inptComment').val()
            }
        };
        $.ajax({
            url: window.location.origin + "/CBMClaimManagement/Home/RequestMore",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    $('#CanSubmit').val('true');
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}