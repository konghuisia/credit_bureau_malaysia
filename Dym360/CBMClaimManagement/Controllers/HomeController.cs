﻿using CBMClaimManagement.Models;
using DataAccessHelper.ClaimManagement.Store;
using DataAccessHelper.ConfigurationHelper.Store;
using DataAccessHelper.RequestHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.ClaimManagement;
using ObjectClasses.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using iTS = iTextSharp.text;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FileManager;
using MvcRazorToPdf;
using IronPdf;

namespace CBMClaimManagement.Controllers
{
    public class HomeController : Controller
    {
        private static string RoleId;
        const string ModuleId = "3D010C9F-28DA-4C57-A31F-0A18AD9ADF89";
        static Role CurRole;
        static User CurrentUser;

        UserStore userStore;
        RoleStore roleStore;
        RequestStore requestStore;
        ApprovalStore approvalStore;
        AttachmentStore AttachmentStore;
        UserAccessStore UserAccessStore;
        ClaimTypesStore ClaimTypesStore;
        ClaimManagementRequestStore ClaimManagementRequestStore;

        public HomeController()
        {
            userStore = DependencyResolver.Current.GetService<UserStore>();
            roleStore = DependencyResolver.Current.GetService<RoleStore>();
            requestStore = DependencyResolver.Current.GetService<RequestStore>();
            approvalStore = DependencyResolver.Current.GetService<ApprovalStore>();
            ClaimTypesStore = DependencyResolver.Current.GetService<ClaimTypesStore>();
            AttachmentStore = DependencyResolver.Current.GetService<AttachmentStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            ClaimManagementRequestStore = DependencyResolver.Current.GetService<ClaimManagementRequestStore>();
            CurrentUser = new User();
        }
        public async Task<ActionResult> Index()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            var vm = new IndexViewModel();
            vm.MyTasks = await requestStore.GetClaimRequestsList(Utils.GetUserId());
            vm.UserAccesses = await UserAccessStore.GetUserAccessByUserId(Utils.GetUserId());

            return View(vm);
        }

        public async Task<ActionResult> ClaimRequestForm(string requestId = null, string cameFrom = null)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            IList<User> users = new List<User>();
            var role = new Role();
            var config = new ClaimRequestViewModel();
            users = await userStore.GetAllUser();
            CurrentUser = await userStore.GetUser(Utils.GetUserId());
            ViewBag.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            role = await roleStore.GetRoleClaimManagementPOC("");
            config.ClaimTypes = await ClaimTypesStore.GetAllClaimTypes();

            RoleId = role.RoleId;
            if (string.IsNullOrEmpty(requestId))
            {
                config.Access = "Write";
                CurRole = await roleStore.GetRoleClaimManagementPOC("");

                config.Request = new Request()
                {
                    ReferenceNo = "HR-CM-" + DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                    RequestTypeId = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE",
                    ModuleId = ModuleId,
                    Requester = new Requester()
                    {
                        Name = CurrentUser.Name,
                        UserId = CurrentUser.UserId,
                        Position = CurrentUser.Position,
                        Department = CurrentUser.Department,
                        Reporting = CurrentUser.Reporting,
                        Id = int.Parse(CurrentUser.Id),
                    }
                };

                config.Request.CreatedDate = DateTime.Now;
                config.Request.RequestId = await requestStore.NewPreRequest(config.Request);

                ra = await roleStore.GetRoleActions("", CurRole.RoleId);
                apps = await approvalStore.GetAllApproval(config.Request.RequestId);

                var hods = users.ToList().FindAll(x => x.DepartmentId == CurrentUser.DepartmentId && x.UserId != CurrentUser.UserId && x.DeletedFlag == "N" && int.Parse(x.Grad) > int.Parse(CurrentUser.Grad));
                if (hods.Count > 0)
                {
                    var hod = hods.ToList().Aggregate((i1, i2) => int.Parse(i1.Grad) > int.Parse(i2.Grad) ? i1 : i2);
                    if (hod != null)
                    {
                        var approval = new Approval();
                        approval.UserId = hod.UserId;
                        approval.Name = hod.Name;

                        if (config.Request.HODs == null)
                        {
                            config.Request.HODs = new List<Approval>();

                        }
                        config.Request.HODs.Add(approval);
                    }
                }
                //config.Request.HODs = users.ToList().FindAll(x => x.DepartmentId == config.CurrentUser.DepartmentId && int.Parse(x.Grad) > int.Parse(config.CurrentUser.Grad) && x.DeletedFlag == "N");
                //config.Request.HODs = apps.ToList().FindAll(x => x.RoleId == "C7A9659F-5F1E-4584-8821-1CA39074FB6E" && x.UserId != CurrentUser.UserId);
            }
            else
            {
                config.Request = await requestStore.GetRequest(requestId);
                config.Request.Period = await ClaimManagementRequestStore.GetClaim(requestId);
                apps = await approvalStore.GetAllApproval(requestId);

                config.Request.HODs = apps.Where(x => x.Role == "HODS").ToList();
                config.Request.AdminTeam = apps.Where(x => x.Role == "ADMIN").ToList();
                config.Request.CEO = apps.ToList().Find(x => x.Role == "CEO");
                config.Request.FinanceTeam = apps.Where(x => x.Role == "FINANCE").ToList();
                config.ClaimItems = await ClaimManagementRequestStore.GetClaimItems(requestId);

                if (config.Request.Status == "Draft" || config.Request.Status == "Pending Staff")
                {
                    if (config.Request.Requester.UserId == Utils.GetUserId())
                    {
                        config.Access = "Write";
                        CurRole = await roleStore.GetRoleClaimManagementPOC("");
                        ra = await roleStore.GetRoleActions("", CurRole.RoleId);
                        RoleId = CurRole.RoleId;
                    }
                }
                else
                {
                    if (!config.Request.Status.ToLower().Contains("close"))
                    {
                        var isBlock = true;
                        var pends = apps.ToList().FindAll(x => x.Action == "PENDING");
                        foreach (var app in pends)
                        {
                            if (app.RoleId == config.Request.RequestState.RoleId && app.UserId == Utils.GetUserId())
                            {
                                isBlock = false;
                            }
                        }
                        if (!isBlock)
                        {
                            config.Access = "Write";
                            CurRole = await roleStore.GetRole(config.Request.RequestState.RoleId);
                            ra = await roleStore.GetRoleActions("", config.Request.RequestState.RoleId);
                            RoleId = CurRole.RoleId;
                        }
                    }
                }
            }
            config.cameFrom = cameFrom;
            config.CurrentUser = CurrentUser;
            config.RoleActions = ra.ToList();

            config.Approvers = users.ToList().FindAll(x => x.DepartmentId == config.CurrentUser.DepartmentId && int.Parse(x.Grad) > int.Parse(config.CurrentUser.Grad) && x.DeletedFlag == "N");

            if (config.Approvers.Count == 0)
            {
                config.Approvers = users.ToList().FindAll(x => int.Parse(x.Grad) > int.Parse(config.CurrentUser.Grad) && x.DeletedFlag == "N");
            }

            config.Users = users;
            return View("ClaimRequestForm", config);
        }

        [HttpPost]
        public async Task<JsonResult> RemoveApprover(string userId, string requestId)
        {
            var roleId = "C7A9659F-5F1E-4584-8821-1CA39074FB6E";
            var success = await approvalStore.RemoveReviewer(new Approval()
            {
                RoleId = roleId,
                UserId = userId
            }, requestId);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Approver removed";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }


        public async Task<JsonResult> SaveDraft(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Status = "Draft";
            var success = await ClaimManagementRequestStore.SaveRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request saved as draft";
                resp.RedirectUrl = "/CBMClaimManagement/Home/ClaimRequestForm?requestId=" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Submit(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            req.Requester.UserId = Utils.GetUserId();
            req.UserId = req.Requester.UserId;
            req.CreatedDate = DateTime.Now;
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/CBMClaimManagement/Home/ClaimRequestForm?requestId=" + req.RequestId + "";
            var success = await ClaimManagementRequestStore.SubmitRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request submitted";
                resp.RedirectUrl = "/CBMClaimManagement/Home";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }


        public async Task<JsonResult> Complete(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            req.Requester.UserId = Utils.GetUserId();
            req.UserId = req.Requester.UserId;
            req.CreatedDate = DateTime.Now;
            var success = await ClaimManagementRequestStore.SubmitRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request close complete";
                resp.RedirectUrl = "/CBMClaimManagement/Home";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> UploadClaims()
        {
            var reqId = Request.Params.GetValues("RequestId")[0];

            var c = new ClaimItems
            {
                //ClaimId = Request.Params.GetValues("ClaimId")[0],
                ClaimTypeId = Request.Params.GetValues("ClaimTypeId")[0],
                Date = DateTime.Parse(Request.Params.GetValues("Date")[0]),
                ClaimAmount = Convert.ToDecimal(Request.Params.GetValues("ClaimAmount")[0]),
                Remark = Request.Params.GetValues("Remark")[0],
                Mileage = Convert.ToDecimal(Request.Params.GetValues("Mileage")[0]),
            };

            var folder = "Attachments";

            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                                //Use the following properties to get file's name, size and MIMEType
                    int fileSize = file.ContentLength;
                    string fileName = file.FileName;
                    string ext = fileName.Substring(fileName.LastIndexOf('.'));
                    string mimeType = file.ContentType;
                    string path = "Areas/CBMClaimManagement/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + reqId + "/";
                    string location = Server.MapPath("~/") + path;
                    System.IO.Stream fileContent = file.InputStream;
                    System.IO.Directory.CreateDirectory(location);
                    //To save file, use SaveAs method
                    file.SaveAs(location + fileName); //File will be saved in application root

                    c.CreatedBy = Utils.GetUserId();
                    c.FileName = fileName;
                    c.Path = path;
                }
            }

            c = await ClaimManagementRequestStore.SaveClaimItem(c, reqId);
            var ct = await ClaimTypesStore.GetAllClaimTypes();
            if (Request.Files.Count == 0)
            {
                c.FileName = "";
            }
            c.ClaimTypeId = ct.Find(x => x.ClaimTypeId == c.ClaimTypeId).Name;
            c.DateText = c.Date.ToString("dd MMM yyyy");

            return Json(c);
        }

        public async Task<JsonResult> RequestMore(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/CBMClaimManagement/Home/ClaimRequestForm?requestId=" + req.RequestId + "";
            var success = await ClaimManagementRequestStore.RequestMoreRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request sent back for clarification";
                resp.RedirectUrl = "/CBMClaimManagement/Home";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Reject(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId; ;
            req.UserId = Utils.GetUserId();
            var success = await ClaimManagementRequestStore.RejectRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request rejected";
                resp.RedirectUrl = "/CBMClaimManagement/Home";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Approve(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/CBMClaimManagement/Home/ClaimRequestForm?requestId=" + req.RequestId + "";
            var success = await ClaimManagementRequestStore.ApproveRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request approved";
                resp.RedirectUrl = "/CBMClaimManagement/Home";
            }
            else
            {
                resp.Error = "Unable to poceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            request.Requester.UserId = Utils.GetUserId();
            request.RequestId = Request.Params.GetValues("RequestId")[0];
            var uploadBy = Request.Params.GetValues("UploadBy")[0];
            var classificationId = "";

            var folder = "";
            var AttachmentTypeId = "";
            if (uploadBy == "requester")
            {
                folder = "Attachments";
                AttachmentTypeId = "1459548D-2D14-46FB-8AF2-F348E1E32DC4";
            }
            else if (uploadBy == "finance")
            {
                folder = "Attachments";
                AttachmentTypeId = "01F6435C-5B8E-4221-B241-C1B2A2AA119F";
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/CBMClaimManagement/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    Path = path,
                    AbsolutePath = location,
                    ModifiedDate = Convert.ToDateTime(Request.Params.GetValues("LastModifiedDate")[i]).ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = classificationId
                });
            }

            request = await AttachmentStore.CreateAsync(request);

            foreach (var a in request.Attachments)
            {
                a.CreatedDate = Convert.ToDateTime(a.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            return Json(request.Attachments.FindAll(x => x.AttachmentTypeId == AttachmentTypeId));

        }

        [HttpPost]
        public async Task<JsonResult> DeleteClaim(string claimId)
        {
            await ClaimManagementRequestStore.DeleteClaim(claimId, Utils.GetUserId());
            return Json(new Response()
            {
                Message = "Claim removed"
            });
        }

        [HttpPost]
        public async Task<JsonResult> DeleteAttachment(string attachmentId)
        {
            await AttachmentStore.DeleteAttachment(attachmentId);
            return Json(new Response()
            {
                Message = "Attachment removed"
            });
        }

        public async Task<ActionResult> LoadMyTask()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            IList<MyTask> requests = await requestStore.GetClaimRequestsList(Utils.GetUserId());

            return View("MyTask", requests.ToList());
        }

        public async Task<ActionResult> ExportClaimRequest(string requestId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            if (!string.IsNullOrEmpty(requestId))
            {
                var users = await userStore.GetAllUser();
                var apps = await approvalStore.GetAllApproval(requestId);
                var CEO = apps.ToList().Find(x => x.Role == "CEO");
                var HODs = apps.Where(x => x.Role == "HODS").ToList();
                var AdminTeam = apps.Where(x => x.Role == "ADMIN").ToList();
                var FinanceTeam = apps.Where(x => x.Role == "FINANCE").ToList();
                var claimTypes = await ClaimTypesStore.GetAllClaimTypes();
                var claimRequest = await requestStore.GetRequest(requestId);
                var requestor = await userStore.GetUser(claimRequest.UserId);
                var claim = await ClaimManagementRequestStore.GetClaim(requestId);
                var claimItems = await ClaimManagementRequestStore.GetClaimItems(requestId);

                byte[] result = null;
                Image logo = Image.FromFile(@"C:\Users\owen_\Documents\CBM\Dym360\Dym360\Images\cbm_logo.png");
                //string fileName = @"\pcf.xlsx";
                //string tempPath = System.IO.Path.GetTempPath();
                using (ExcelPackage package = new ExcelPackage())
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets.Add(String.Format("{0}", "Personal Claim Form"));

                    workSheet.Cells.Style.Font.Size = 10;
                    workSheet.Cells.Style.Font.Name = "Roboto";


                    //Header
                    //workSheet.Row(1).Height = 30;

                    //workSheet.Cells[1, 1].Value = "PERSONAL CLAIMS FORM FOR REIMBURSEMENT";
                    //workSheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    //workSheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Bottom;
                    //workSheet.Cells[1, 1].Style.Font.Bold = true;
                    //workSheet.Cells[1, 1].Style.Font.Name = "Calibri";
                    //workSheet.Cells[1, 1].Style.Border.Left.Color.SetColor(Color.White);
                    //workSheet.Cells[1, 1].Style.Border.Right.Color.SetColor(Color.White);
                    //workSheet.Cells["A1:O1"].Merge = true;

                    //var picture = workSheet.Drawings.AddPicture("Logo", logo);
                    //picture.From.Column = 13;
                    //picture.From.Row = 0;
                    //picture.To.Column = 13;//end cell value
                    //picture.To.Row = 3;//end cell value
                    //picture.SetSize(, 179);
                    //Header End

                    //Name//
                    workSheet.Cells[1, 1].Value = "Name :";
                    workSheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //workSheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                    workSheet.Cells[1, 1].Style.Font.Bold = true;
                    workSheet.Cells["A1:B1"].Merge = true;

                    workSheet.Cells[1, 3].Value = requestor.Name;
                    workSheet.Cells[1, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[1, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[1, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[1, 3].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[1, 3].Style.Font.Bold = true;
                    workSheet.Cells["C1:I1"].Merge = true;

                    //Department//
                    workSheet.Cells[1, 10].Value = "Department :";
                    workSheet.Cells[1, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[1, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[1, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[1, 10].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[1, 10].Style.Font.Bold = true;
                    workSheet.Cells["J1:K1"].Merge = true;

                    workSheet.Cells[1, 12].Value = requestor.Department;
                    workSheet.Cells[1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[1, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[1, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[1, 12].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[1, 15].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[1, 15].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[1, 12].Style.Font.Bold = true;
                    workSheet.Cells["L1:O1"].Merge = true;

                    //Designation//
                    workSheet.Cells[2, 1].Value = "Designation :";
                    workSheet.Cells[2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[2, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    //workSheet.Cells[2, 1].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                    workSheet.Cells[2, 1].Style.Font.Bold = true;
                    workSheet.Cells["A2:B2"].Merge = true;

                    workSheet.Cells[2, 3].Value = requestor.Position;
                    workSheet.Cells[2, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[2, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[2, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[2, 3].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[2, 3].Style.Font.Bold = true;
                    workSheet.Cells["C2:I2"].Merge = true;

                    //Period//
                    workSheet.Cells[2, 10].Value = "Period :";
                    workSheet.Cells[2, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[2, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[2, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[2, 10].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[2, 10].Style.Font.Bold = true;
                    workSheet.Cells["J2:K2"].Merge = true;

                    workSheet.Cells[2, 12].Value = claim.HasValue ? Convert.ToDateTime(claim).ToString("MMMM yyyy") : "";
                    workSheet.Cells[2, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[2, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[2, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[2, 12].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[2, 15].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[2, 15].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[2, 12].Style.Font.Bold = true;
                    workSheet.Cells["L2:O2"].Merge = true;

                    //Staff Id//
                    workSheet.Cells[3, 1].Value = "Staff Id :";
                    workSheet.Cells[3, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[3, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[3, 1].Style.Font.Bold = true;
                    workSheet.Cells["A3:B3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A3:B3"].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["A3:B3"].Merge = true;
                    
                    workSheet.Cells[3, 3].Value = int.Parse(requestor.Id);
                    workSheet.Cells[3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[3, 3].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[3, 3].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[3, 3].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[3, 3].Style.Font.Bold = true;
                    workSheet.Cells["C3:I3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["C3:I3"].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["C3:I3"].Merge = true;
                    
                    workSheet.Cells[3, 10].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[3, 10].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells["J3:K3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["J3:K3"].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["J3:K3"].Merge = true;
                    
                    workSheet.Cells[3, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[3, 12].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[3, 15].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[3, 15].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[3, 12].Style.Font.Bold = true;
                    workSheet.Cells["L3:O3"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["L3:O3"].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["L3:O3"].Merge = true;

                    //New Header row
                    workSheet.Cells[5, 1].Value = "Date";
                    workSheet.Cells[5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[5, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[5, 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[5, 1].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[5, 1].Style.Font.Bold = true;
                    workSheet.Cells[7, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[7, 1].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["A5:A7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A5:A7"].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["A5:A7"].Merge = true;
                    workSheet.Column(1).Width = 12;

                    workSheet.Cells[5, 2].Value = "Description/Purpose";
                    workSheet.Cells[5, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[5, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[5, 2].Style.Font.Bold = true;
                    workSheet.Cells["B5:E5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["B5:E5"].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells["B7:E7"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["B7:E7"].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["B5:E7"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["B5:E7"].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["B5:E7"].Merge = true;
                    workSheet.Column(2).Width = 10;
                    workSheet.Column(3).Width = 10;
                    workSheet.Column(4).Width = 10;
                    workSheet.Column(5).Width = 10;


                    workSheet.Cells[5, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[5, 6].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[5, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[5, 6].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[6, 6].Value = "Mileage";
                    workSheet.Cells[6, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[6, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[6, 6].Style.Font.Bold = true;
                    workSheet.Cells[6, 6].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[6, 6].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[6, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[6, 6].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[7, 6].Value = "(KM)";
                    workSheet.Cells[7, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[7, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[7, 6].Style.Font.Bold = true;
                    workSheet.Cells[7, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[7, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[7, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[7, 6].Style.Border.Bottom.Color.SetColor(Color.Black);

                    var col = 7;
                    if (claimTypes.Count > 0)
                    {
                        foreach (var cts in claimTypes)
                        {
                            int a;
                            bool codeParseAble = int.TryParse(cts.ClaimTypeCode, out a);
                            if (codeParseAble)
                            {
                                workSheet.Cells[5, col].Value = int.Parse(cts.ClaimTypeCode);
                            }
                            else
                            {
                                workSheet.Cells[5, col].Value = cts.ClaimTypeCode;
                            }
                            workSheet.Cells[5, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            workSheet.Cells[5, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[5, col].Style.Font.Bold = true;
                            workSheet.Cells[5, col].Style.WrapText = true;
                            workSheet.Cells[5, col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[5, col].Style.Border.Top.Color.SetColor(Color.Black);
                            workSheet.Cells[5, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[5, col].Style.Border.Right.Color.SetColor(Color.Black);

                            workSheet.Cells[6, col].Value = cts.Name;
                            workSheet.Cells[6, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            workSheet.Cells[6, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[6, col].Style.Font.Bold = true;
                            workSheet.Cells[6, col].Style.WrapText = true;
                            workSheet.Cells[6, col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[6, col].Style.Border.Top.Color.SetColor(Color.Black);
                            workSheet.Cells[6, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[6, col].Style.Border.Right.Color.SetColor(Color.Black);

                            workSheet.Cells[7, col].Value = "(RM)";
                            workSheet.Cells[7, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            workSheet.Cells[7, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[7, col].Style.Font.Bold = true;
                            workSheet.Cells[7, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[7, col].Style.Border.Right.Color.SetColor(Color.Black);
                            workSheet.Cells[7, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[7, col].Style.Border.Bottom.Color.SetColor(Color.Black);

                            //if (cts.Name == "Travelling" || cts.Name == "Outpatient / Inpatient" || cts.Name == "Meal Allowance" || cts.Name == "Optical / Dental")
                            //{
                            //}

                            workSheet.Column(col).Width = 12;
                            col++;
                        }
                    }


                    workSheet.Cells[5, col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[5, col].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[5, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[5, col].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[6, col].Value = "Total Amount";
                    workSheet.Column(col).Width = 12;
                    workSheet.Cells[6, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[6, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[6, col].Style.Font.Bold = true;
                    workSheet.Cells[6, col].Style.WrapText = true;
                    workSheet.Cells[6, col].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[6, col].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[6, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[6, col].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[7, col].Value = "(RM)";
                    workSheet.Cells[7, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[7, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[7, col].Style.Font.Bold = true;
                    workSheet.Cells[7, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[7, col].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[7, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[7, col].Style.Border.Bottom.Color.SetColor(Color.Black);

                    var totalCol = col;
                    
                    //var grouppedClaims = claimItems.OrderBy(x => x.Date).GroupBy(x => x.Date);
                    var row = 8;
                    var mileageTotal = 0.00;
                    var travellingTotal = 0.00;
                    var parkingTotal = 0.00;
                    var mobileTotal = 0.00;
                    var outpatientTotal = 0.00;
                    var opticalTotal = 0.00;
                    var entertainmentTotal = 0.00;
                    var mealTotal = 0.00;
                    var otherTotal = 0.00;
                    var Total = 0.00;

                    foreach(var claims in claimItems)
                    {
                        workSheet.Column(1).Width = 11;
                        workSheet.Cells[row, 1].Value = claims.Date.ToString("dd.MM.yyyy");
                        workSheet.Cells[row, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[row, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[row, 1].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells[row, 1].Style.Border.Right.Color.SetColor(Color.Black);
                        workSheet.Cells[row, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells[row, 1].Style.Border.Bottom.Color.SetColor(Color.Black);

                        workSheet.Cells[row, 2].Value = claims.Remark;
                        workSheet.Cells[row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        workSheet.Cells["B" + row + ":E" + row + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells["B" + row + ":E" + row + ""].Style.Border.Bottom.Color.SetColor(Color.Black);
                        workSheet.Cells["B" + row + ":E" + row + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells["B" + row + ":E" + row + ""].Style.Border.Right.Color.SetColor(Color.Black);
                        workSheet.Cells["B" + row + ":E" + row + ""].Merge = true;
                        

                        if (claims.ClaimTypeId == "D0A1E5EF-6552-4B3A-8954-02886AE38CC3")//Travelling
                        {
                            workSheet.Cells[row, 6].Value = claims.Mileage;
                            workSheet.Cells[row, 6].Style.Numberformat.Format = "#,##0.00";
                            mileageTotal += decimal.ToDouble(claims.Mileage);
                        }
                        workSheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[row, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[row, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells[row, 6].Style.Border.Right.Color.SetColor(Color.Black);
                        workSheet.Cells[row, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        workSheet.Cells[row, 6].Style.Border.Bottom.Color.SetColor(Color.Black);

                        col = 7;
                        if (claimTypes.Count > 0)
                        {
                            var rowTotal = 0.00;
                            foreach (var cts in claimTypes)
                            {
                                if (cts.ClaimTypeId == claims.ClaimTypeId)
                                {
                                    if (cts.ClaimTypeId == "D0A1E5EF-6552-4B3A-8954-02886AE38CC3")//Travelling
                                    {
                                        travellingTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "6F924205-5DD8-4ADC-8A3B-090AF610E311")//Parking
                                    {
                                        parkingTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "01DA6A8E-EFDE-46CA-AD3D-5BA43D3F047F")//Mobile
                                    {
                                        mobileTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "63A283C7-C57E-4116-9643-13989450A696")//Outpatient
                                    {
                                        outpatientTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "272DC8B1-482E-474A-956E-3ED71DCFCE53")//Optical
                                    {
                                        opticalTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "32B1E19D-2815-43EA-90FD-F3D49E1640C9")//Entertainment
                                    {
                                        entertainmentTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else if (cts.ClaimTypeId == "DF5C48FD-42ED-4055-8107-60DCAA2E5980")//Meal
                                    {
                                        mealTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }
                                    else
                                    {
                                        otherTotal += decimal.ToDouble(claims.ClaimAmount);
                                    }

                                    workSheet.Cells[row, col].Value = claims.ClaimAmount;
                                    workSheet.Cells[row, col].Style.Numberformat.Format = "#,##0.00";
                                    Total += decimal.ToDouble(claims.ClaimAmount);
                                    rowTotal += decimal.ToDouble(claims.ClaimAmount);
                                }

                                workSheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                workSheet.Cells[row, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                workSheet.Cells[row, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                workSheet.Cells[row, col].Style.Border.Right.Color.SetColor(Color.Black);
                                workSheet.Cells[row, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                workSheet.Cells[row, col].Style.Border.Bottom.Color.SetColor(Color.Black);

                                col++;
                            }
                            workSheet.Cells[row, totalCol].Value = rowTotal;
                            workSheet.Cells[row, totalCol].Style.Numberformat.Format = "#,##0.00";
                            workSheet.Cells[row, totalCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            workSheet.Cells[row, totalCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[row, totalCol].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[row, totalCol].Style.Border.Right.Color.SetColor(Color.Black);
                            workSheet.Cells[row, totalCol].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[row, totalCol].Style.Border.Bottom.Color.SetColor(Color.Black);
                        }



                        row++;
                    }

                    
                    workSheet.Cells["A" + row + ":E" + row + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A" + row + ":E" + row + ""].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":E" + row + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A" + row + ":E" + row + ""].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":E" + row + ""].Merge = true;

                    
                    workSheet.Cells[row, 6].Value = mileageTotal;
                    workSheet.Cells[row, 6].Style.Numberformat.Format = "#,##0.00";
                    workSheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[row, 6].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[row, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[row, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[row, 6].Style.Border.Bottom.Color.SetColor(Color.Black);

                    col = 7;
                    if (claimTypes.Count > 0)
                    {

                        foreach (var cts in claimTypes)
                        {
                            if (cts.ClaimTypeId == "D0A1E5EF-6552-4B3A-8954-02886AE38CC3")//Travelling
                            {
                                workSheet.Cells[row, col].Value = travellingTotal;
                            }
                            else if (cts.ClaimTypeId == "6F924205-5DD8-4ADC-8A3B-090AF610E311")//Parking
                            {
                                workSheet.Cells[row, col].Value = parkingTotal;
                            }
                            else if (cts.ClaimTypeId == "01DA6A8E-EFDE-46CA-AD3D-5BA43D3F047F")//Mobile
                            {
                                workSheet.Cells[row, col].Value = mobileTotal;
                            }
                            else if (cts.ClaimTypeId == "63A283C7-C57E-4116-9643-13989450A696")//Outpatient
                            {
                                workSheet.Cells[row, col].Value = outpatientTotal;
                            }
                            else if (cts.ClaimTypeId == "272DC8B1-482E-474A-956E-3ED71DCFCE53")//Optical
                            {
                                workSheet.Cells[row, col].Value = opticalTotal;
                            }
                            else if (cts.ClaimTypeId == "32B1E19D-2815-43EA-90FD-F3D49E1640C9")//Entertainment
                            {
                                workSheet.Cells[row, col].Value = entertainmentTotal;
                            }
                            else if (cts.ClaimTypeId == "DF5C48FD-42ED-4055-8107-60DCAA2E5980")//Meal
                            {
                                workSheet.Cells[row, col].Value = mealTotal;
                            }
                            else
                            {
                                workSheet.Cells[row, col].Value = otherTotal;
                            }

                            workSheet.Cells[row, col].Style.Numberformat.Format = "#,##0.00";
                            workSheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            workSheet.Cells[row, col].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[row, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[row, col].Style.Border.Right.Color.SetColor(Color.Black);
                            workSheet.Cells[row, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[row, col].Style.Border.Bottom.Color.SetColor(Color.Black);

                            col++;
                        }
                    }

                    var grandTotalRow = row + 1;
                    workSheet.Cells[row + 1, 13].Value = "Grand Total";
                    workSheet.Cells[row + 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    workSheet.Cells[row + 1, 13].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[row + 1, 13].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[row + 1, 13].Style.Border.Left.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[row + 1, 14].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["M" + grandTotalRow + ":N" + grandTotalRow + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["M" + grandTotalRow + ":N" + grandTotalRow + ""].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 13].Style.Font.Bold = true;
                    workSheet.Cells["M" + grandTotalRow + ":N" + grandTotalRow + ""].Merge = true;

                    var totalRow = row + 1;
                    workSheet.Cells[row, totalCol].Value = Total;
                    workSheet.Cells[row, totalCol].Style.Font.Size = 11;
                    workSheet.Cells[row, totalCol].Style.Numberformat.Format = "#,##0.00";
                    workSheet.Cells[row, totalCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    workSheet.Cells[row, totalCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells["O" + row + ":O" + totalRow + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["O" + row + ":O" + totalRow + ""].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[totalRow, totalCol].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[totalRow, totalCol].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row, totalCol].Style.Font.Bold = true;
                    workSheet.Cells["O" + row + ":O" + totalRow + ""].Merge = true;


                    row += 3;
                    workSheet.Cells[row, 1].Value = "Note: For entertainment claims, please provide party/person(s) entertained.";
                    workSheet.Cells[row, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[row, 1].Style.Font.Bold = true;
                    workSheet.Row(row).Height = 15;
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells["A" + row + ":O" + row + ""].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":O" + row + ""].Merge = true;

                    //Approval Row
                    row += 1;
                    int bottomRightClaimTypeRow = row;

                    int plusTwo = row + 2;
                    int plusThree = row + 3;
                    int plusFour = row + 4;
                    workSheet.Cells[row, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 1, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 2, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 2, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 3, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 3, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 2, 1].Value = "SUBMITTED";
                    workSheet.Cells[row + 2, 1].Style.Font.Bold = true;
                    workSheet.Cells[row + 2, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[row + 2, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells["A" + plusTwo + ":C" + plusTwo + ""].Merge = true;
                    workSheet.Cells[row + 4, 1].Value = "Claimed By : " + requestor.Name;
                    workSheet.Cells["A" + plusFour + ":C" + plusFour + ""].Merge = true;
                    workSheet.Cells[row + 4, 1].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 1].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 2].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 2].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 3].Style.Border.Bottom.Color.SetColor(Color.Black);
                    
                    workSheet.Cells[row, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 1, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 2, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 2, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 3, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 3, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    var Checker = HODs.Find(x => x.Action == "Approve");
                    if (Checker != null)
                    {
                        workSheet.Cells[row + 2, 4].Value = "APPROVED";
                    }
                    workSheet.Cells[row + 2, 4].Style.Font.Bold = true;
                    workSheet.Cells[row + 2, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[row + 2, 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells["D" + plusTwo + ":F" + plusTwo + ""].Merge = true;
                    if (Checker != null)
                    {
                        workSheet.Cells[row + 4, 4].Value = "Checked By : " + Checker.Name;
                    }
                    else
                    {
                        workSheet.Cells[row + 4, 4].Value = "Checked By :";
                    }
                    workSheet.Cells["D" + plusFour + ":F" + plusFour + ""].Merge = true;
                    workSheet.Cells[row + 4, 4].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 4].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 5].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 6].Style.Border.Bottom.Color.SetColor(Color.Black);

                    workSheet.Cells[row, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 1, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 2, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 2, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 3, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 3, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    var Verified = AdminTeam.Find(x => x.Action == "Approve");
                    if (Verified != null)
                    {
                        workSheet.Cells[row + 2, 7].Value = "APPROVED";
                    }
                    workSheet.Cells[row + 2, 7].Style.Font.Bold = true;
                    workSheet.Cells["G" + plusTwo + ":I" + plusTwo + ""].Merge = true;
                    workSheet.Cells[row + 2, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[row + 2, 7].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    if (Verified != null)
                    {
                        workSheet.Cells[row + 4, 7].Value = "Verified By : " + Verified.Name;
                    }
                    else
                    {
                        workSheet.Cells[row + 4, 7].Value = "Verified By :";
                    }
                    workSheet.Cells["G" + plusFour + ":I" + plusFour + ""].Merge = true;
                    workSheet.Cells[row + 4, 7].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 7].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 8].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 9].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 9].Style.Border.Bottom.Color.SetColor(Color.Black);

                    workSheet.Cells[row, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 1, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 1, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 2, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 2, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 3, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 3, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    if (CEO.Action == "Approve")
                    {
                        workSheet.Cells[row + 2, 10].Value = "APPROVED";
                    }
                    workSheet.Cells[row + 2, 10].Style.Font.Bold = true;
                    workSheet.Cells["J" + plusTwo + ":L" + plusTwo + ""].Merge = true;
                    workSheet.Cells[row + 2, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[row + 2, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    if (CEO.Action == "Approve")
                    {
                        workSheet.Cells[row + 4, 10].Value = "Approved By : " + CEO.Name;
                    }
                    else
                    {
                        workSheet.Cells[row + 4, 10].Value = "Approved By :";
                    }
                    workSheet.Cells["J" + plusFour + ":L" + plusFour + ""].Merge = true;
                    workSheet.Cells[row + 4, 10].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 10].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 11].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 11].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[row + 4, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row + 4, 12].Style.Border.Bottom.Color.SetColor(Color.Black);

                    row += 5;
                    int plusOne = row + 1;
                    workSheet.Cells[row, 1].Value = "Claimant";
                    workSheet.Cells[row, 1].Style.Font.Bold = true;
                    workSheet.Cells["A" + row + ":C" + row + ""].Merge = true;
                    workSheet.Cells[row, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 3].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[plusOne, 1].Value = "Date : " + claimRequest.CreatedDate.ToString("dd.MM.yyyy");
                    workSheet.Cells[plusOne, 3].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[plusOne, 3].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + plusOne + ":C" + plusOne + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["A" + plusOne + ":C" + plusOne + ""].Style.Border.Bottom.Color.SetColor(Color.Black);

                    workSheet.Cells[row, 4].Value = "Immediate Superior";
                    workSheet.Cells[row, 4].Style.Font.Bold = true;
                    workSheet.Cells["D" + row + ":F" + row + ""].Merge = true;
                    workSheet.Cells[row, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 6].Style.Border.Right.Color.SetColor(Color.Black);

                    if (Checker != null)
                    {
                        workSheet.Cells[plusOne, 4].Value = "Date : " + Checker.ActionDate.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        workSheet.Cells[plusOne, 4].Value = "Date : ";
                    }
                    workSheet.Cells[plusOne, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[plusOne, 6].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["D" + plusOne + ":F" + plusOne + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["D" + plusOne + ":F" + plusOne + ""].Style.Border.Bottom.Color.SetColor(Color.Black);

                    workSheet.Cells[row, 7].Value = "Admin Dept";
                    workSheet.Cells[row, 7].Style.Font.Bold = true;
                    workSheet.Cells["G" + row + ":I" + row + ""].Merge = true;
                    workSheet.Cells[row, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 9].Style.Border.Right.Color.SetColor(Color.Black);

                    if (Verified != null)
                    {
                        workSheet.Cells[plusOne, 7].Value = "Date : " + Verified.ActionDate.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        workSheet.Cells[plusOne, 7].Value = "Date : ";
                    }
                    workSheet.Cells[plusOne, 9].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[plusOne, 9].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["G" + plusOne + ":I" + plusOne + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["G" + plusOne + ":I" + plusOne + ""].Style.Border.Bottom.Color.SetColor(Color.Black);

                    workSheet.Cells[row, 10].Value = "CEO";
                    workSheet.Cells[row, 10].Style.Font.Bold = true;
                    workSheet.Cells["J" + row + ":L" + row + ""].Merge = true;
                    workSheet.Cells[row, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[row, 12].Style.Border.Right.Color.SetColor(Color.Black);

                    if (CEO.Action == "Approve")
                    {
                        workSheet.Cells[plusOne, 10].Value = "Date : " + CEO.ActionDate.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        workSheet.Cells[plusOne, 10].Value = "Date : ";
                    }
                    workSheet.Cells[plusOne, 12].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[plusOne, 12].Style.Border.Right.Color.SetColor(Color.Black);
                    workSheet.Cells["J" + plusOne + ":L" + plusOne + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["J" + plusOne + ":L" + plusOne + ""].Style.Border.Bottom.Color.SetColor(Color.Black);

                    row += 2;
                    plusOne = row + 1;
                    workSheet.Cells[row, 1].Value = "Verified By Finance Department";
                    workSheet.Cells[row, 1].Style.Font.Bold = true;
                    workSheet.Cells[row, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[row, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells["A" + row + ":C" + plusOne].Style.WrapText = true;
                    workSheet.Cells["A" + row + ":C" + plusOne].Merge = true;
                    workSheet.Cells["A" + row + ":C" + plusOne].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["A" + row + ":C" + plusOne].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["A" + row + ":C" + plusOne].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["A" + row + ":C" + plusOne].Style.Border.Right.Color.SetColor(Color.Black);

                    //Finance
                    var Verify = FinanceTeam.Find(x => x.Action == "Complete");
                    if (Verify != null) {
                        workSheet.Cells[row, 4].Value = "Verified By : " + Verify.Name;
                    }
                    else
                    {
                        workSheet.Cells[row, 4].Value = "Verified By :";
                    }
                    workSheet.Cells["D" + row + ":L" + row].Merge = true;
                    workSheet.Cells["D" + row + ":L" + row].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["D" + row + ":L" + row].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["D" + row + ":L" + row].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["D" + row + ":L" + row].Style.Border.Right.Color.SetColor(Color.Black);
                    
                    if (Verify != null)
                    {
                        workSheet.Cells[plusOne, 4].Value = "Date : " + Verify.ActionDate.ToString("dd.MM.yyyy");
                    }
                    else
                    {
                        workSheet.Cells[plusOne, 4].Value = "Date :";
                    }
                    workSheet.Cells["D" + plusOne + ":L" + plusOne].Merge = true;
                    workSheet.Cells["D" + plusOne + ":L" + plusOne].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["D" + plusOne + ":L" + plusOne].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells["D" + plusOne + ":L" + plusOne].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["D" + plusOne + ":L" + plusOne].Style.Border.Right.Color.SetColor(Color.Black);
                    //END finance


                    //Bottom Right Claim Type
                    if (claimTypes.Count > 0)
                    {
                        foreach (var cts in claimTypes)
                        {
                            var type = "";
                            var typeTotal = 0.00;

                            if (cts.ClaimTypeId == "D0A1E5EF-6552-4B3A-8954-02886AE38CC3")//Travelling
                            {
                                type = cts.Name;
                                typeTotal = travellingTotal;
                            }
                            else if (cts.ClaimTypeId == "6F924205-5DD8-4ADC-8A3B-090AF610E311")//Parking
                            {
                                type = cts.Name;
                                typeTotal = parkingTotal;
                            }
                            else if (cts.ClaimTypeId == "01DA6A8E-EFDE-46CA-AD3D-5BA43D3F047F")//Mobile
                            {
                                type = cts.Name;
                                typeTotal = mobileTotal;
                            }
                            else if (cts.ClaimTypeId == "63A283C7-C57E-4116-9643-13989450A696")//Outpatient
                            {
                                type = cts.Name;
                                typeTotal = outpatientTotal;
                            }
                            else if (cts.ClaimTypeId == "272DC8B1-482E-474A-956E-3ED71DCFCE53")//Optical
                            {
                                type = cts.Name;
                                typeTotal = opticalTotal;
                            }
                            else if (cts.ClaimTypeId == "32B1E19D-2815-43EA-90FD-F3D49E1640C9")//Entertainment
                            {
                                type = cts.Name;
                                typeTotal = entertainmentTotal;
                            }
                            else if (cts.ClaimTypeId == "DF5C48FD-42ED-4055-8107-60DCAA2E5980")//Meal
                            {
                                type = cts.Name;
                                typeTotal = mealTotal;
                            }
                            else
                            {
                                type = cts.Name;
                                typeTotal = otherTotal;
                            }

                            workSheet.Cells[bottomRightClaimTypeRow, 13].Value = type;
                            workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Merge = true;
                            workSheet.Cells[bottomRightClaimTypeRow, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            workSheet.Cells[bottomRightClaimTypeRow, 14].Style.Border.Right.Color.SetColor(Color.Black);

                            workSheet.Cells[bottomRightClaimTypeRow, 15].Value = typeTotal;
                            workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Numberformat.Format = "#,##0.00";
                            workSheet.Cells[bottomRightClaimTypeRow, 15].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            workSheet.Cells[bottomRightClaimTypeRow, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                            workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Right.Color.SetColor(Color.Black);

                            bottomRightClaimTypeRow++;
                        }
                    }


                    workSheet.Cells[bottomRightClaimTypeRow, 13].Value = "Total Claim";
                    workSheet.Cells[bottomRightClaimTypeRow, 13].Style.Font.Bold = true;
                    workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Merge = true;
                    workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells["M" + bottomRightClaimTypeRow + ":N" + bottomRightClaimTypeRow + ""].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[bottomRightClaimTypeRow, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    workSheet.Cells[bottomRightClaimTypeRow, 14].Style.Border.Right.Color.SetColor(Color.Black);

                    workSheet.Cells[bottomRightClaimTypeRow, 15].Value = Total;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Numberformat.Format = "#,##0.00";
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Font.Bold = true;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Top.Color.SetColor(Color.Black);
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Bottom.Color.SetColor(Color.Black);
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    workSheet.Cells[bottomRightClaimTypeRow, 15].Style.Border.Right.Color.SetColor(Color.Black);
                    

                    workSheet.PrinterSettings.TopMargin = (decimal)2.3 / 2.54M;
                    workSheet.PrinterSettings.HeaderMargin = (decimal)0.3 / 2.54M;
                    workSheet.PrinterSettings.RightMargin = (decimal)0.3 / 2.54M;
                    workSheet.PrinterSettings.FooterMargin = (decimal)0.8 / 2.54M;
                    workSheet.PrinterSettings.BottomMargin = (decimal)1 / 2.54M;
                    workSheet.PrinterSettings.LeftMargin = (decimal)0.3 / 2.54M;


                    workSheet.PrinterSettings.RepeatRows = new ExcelAddress("$1:$7");
                    workSheet.HeaderFooter.OddHeader.CenteredText = "PERSONAL CLAIMS FORM FOR REIMBURSEMENT";
                    workSheet.HeaderFooter.OddHeader.InsertPicture(logo, PictureAlignment.Right);
                    workSheet.PrinterSettings.Scale = 84;
                    //workSheet.Column(14).PageBreak = true;
                    workSheet.PrinterSettings.Orientation = eOrientation.Landscape;
                    workSheet.PrinterSettings.PaperSize = ePaperSize.A4;
                    package.Workbook.Properties.Title = "Personal Claim Form";
                    package.Workbook.Properties.Company = "© Copyright " + DateTime.Now.ToString("yyyy") + " Credit Bureau Malaysia Sdn Bhd.";

                    // save our new workbook and we are done!
                    result = package.GetAsByteArray();
                    //package.SaveAs(new FileInfo(tempPath + fileName));

                    //-------- Now leaving the using statement
                } // Outside the using statement
            //    Workbook workbook = new Workbook();
                
            //workbook.LoadFromFile(tempPath + fileName, ExcelVersion.Version2010);
                
            //workbook.SaveToFile(KnownFolders.Downloads.Path + "\\result.pdf", Spire.Xls.FileFormat.PDF);


            //    return File(result, "Application/pdf", "Personal Claim Form.pdf");
                return File(result, ExcelExportHelper.ExcelContentType, "Personal Claim Form.xlsx");
            }
            return RedirectToAction("Index", "Home", new { area = "CBMClaimManagement" });
        }

        public async Task<FileContentResult> ExportClaimRequestToPdf(string requestId)
        {
            var report = new Models.ExportClaimRequestViewModel();

            report.users = await userStore.GetAllUser();
            report.apps = await approvalStore.GetAllApproval(requestId);
            report.CEO = report.apps.ToList().Find(x => x.Role == "CEO");
            report.HODs = report.apps.Where(x => x.Role == "HODS").ToList();
            report.AdminTeam = report.apps.Where(x => x.Role == "ADMIN").ToList();
            report.FinanceTeam = report.apps.Where(x => x.Role == "FINANCE").ToList();
            report.claimTypes = await ClaimTypesStore.GetAllClaimTypes();
            report.claimRequest = await requestStore.GetRequest(requestId);
            report.requestor = await userStore.GetUser(report.claimRequest.UserId);
            report.claim = await ClaimManagementRequestStore.GetClaim(requestId);
            report.claimItems = await ClaimManagementRequestStore.GetClaimItems(requestId);
            report.Logo = Server.MapPath("~/Image/cbm_logo.png");


            //var Renderer = new IronPdf.HtmlToPdf();
            //Renderer.PrintOptions.PaperSize = PdfPrintOptions.PdfPaperSize.A4;
            //Renderer.PrintOptions.PaperOrientation = PdfPrintOptions.PdfPaperOrientation.Landscape;
            //Renderer.PrintOptions.Title = "Personal_Claim_Form";
            //Renderer.PrintOptions.Footer = new HtmlHeaderFooter()
            //{
            //    Height = 15,
            //    HtmlFragment = "<div style='float:right;'><i>{page} of {total-pages}<i></div>",
            //    DrawDividerLine = true
            //};
            //Renderer.PrintOptions.FirstPageNumber = 1; //use 2 if a cover page will be appended

            //var PDF = Renderer.RenderHtmlAsPdf(RenderRazorViewToString("ExportClaimRequestToPdf", report));
            //var contentLength = PDF.BinaryData.Length;
            //Response.AppendHeader("Content-Length", contentLength.ToString());
            //Response.AppendHeader("Content-Disposition", "inline; filename=Personal_Claim_Form.pdf");

            byte[] result = ControllerContext.GeneratePdf(report, "Personal_Claim_Form", (writer, document) =>
            {
                document.SetPageSize(iTS.PageSize.A4.Rotate());
                document.NewPage();
                document.AddTitle("Personal_Claim_Form");
            });
            return File(result, "application/pdf");
            //return File(PDF.BinaryData, "application/pdf");
        }

        public string RenderRazorViewToString (string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}