﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObjectClasses;
using ObjectClasses.ClaimManagement;
using ObjectClasses.Configuration;

namespace CBMClaimManagement.Models
{
    public class ExportClaimRequestViewModel
    {
        public IList<User> users { get; set; }
        public IList<Approval> apps { get; set; }
        public List<ClaimTypes> claimTypes { get; set; }
        public Request claimRequest { get; set; }
        public User requestor { get; set; }
        public DateTime? claim { get; set; }
        public List<ClaimItems> claimItems { get; set; }

        public Approval CEO { get; set; }
        public List<Approval> HODs { get; set; }
        public List<Approval> AdminTeam { get; set; }
        public List<Approval> FinanceTeam { get; set; }
        public string Logo { get; internal set; }
    }
}