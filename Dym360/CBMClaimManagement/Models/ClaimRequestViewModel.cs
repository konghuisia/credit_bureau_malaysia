﻿using ObjectClasses;
using ObjectClasses.ClaimManagement;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CBMClaimManagement.Models
{
    public class ClaimRequestViewModel
    {
        public ClaimRequestViewModel()
        {
            Approvers = new List<User>();
            ClaimRequest = new ClaimRequest();
            ClaimItems = new List<ClaimItems>();
            ClaimTypes = new List<ClaimTypes>();
        }
        public string Access { get; set; }
        public string cameFrom { get; set; }
        public Request Request { get; set; }
        public User CurrentUser { get; set; }
        public IList<User> Users { get; set; }
        public List<User> Approvers { get; set; }
        public ClaimRequest ClaimRequest { get; set; }
        public List<ClaimTypes> ClaimTypes { get; set; }
        public List<ClaimItems> ClaimItems { get; set; }
        public List<RoleAction> RoleActions { get; set; }
    }
}