﻿using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CBMClaimManagement.Models
{
    public class IndexViewModel
    {
        public List<MyTask> MyTasks { get; set; }
        public List<UserAccess> UserAccesses { get; set; }
    }
}