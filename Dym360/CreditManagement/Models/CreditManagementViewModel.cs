﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CreditManagement.Models
{
    public class CreditManagementViewModel
    {
        public CreditManagementViewModel()
        {
            Account = new Account();
        }

        public Request Request { get; set; }
        public string BaseUrl { get; set; }
        public string Access { get; set; }
        public List<DocumentType> DocumentTypes { get; set; }
        public List<RequestType> RequestTypes { get; set; }
        public List<RoleAction> RoleActions { get; set; }
        public List<Department> Departments { get; set; }
        public List<Location> Locations { get; set; }
        public List<User> Users { get; set; }
        public List<HOD> HODs { get; set; }
        public List<Classification> Classifications { get; set; }
        public bool Print { get; set; }
        public Account Account { get; set; }
        public string CameFrom { get; set; }
    }
}