﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CreditManagement.Models
{
    public class SubscriptionRequestViewModel
    {
        public SubscriptionRequestViewModel()
        {
            Approvers = new List<User>();
            SubscriptionRequest = new SubscriptionRequest();
        }
        public string Access { get; set; }
        public string cameFrom { get; set; }
        public Request Request { get; set; }
        public User CurrentUser { get; set; }
        public IList<User> Users { get; set; }
        public List<User> Approvers { get; set; }
        public SubscriptionRequest SubscriptionRequest { get; set; }
        public List<RoleAction> RoleActions { get; set; }
    }
}