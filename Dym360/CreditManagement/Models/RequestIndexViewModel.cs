﻿using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CreditManagement.Models
{
    public class RequestIndexViewModel
    {
        public List<UserAccess> UserAccesses { get; set; }
        public List<Account> Accounts { get; set; }
        public List<AccountRequest> AccountRequests { get; set; }
        public List<MyTask> MyTasks { get; set; }
        public List<DepartmentAccess> CurrentUserDepartmentAccess { get; internal set; }
    }
}