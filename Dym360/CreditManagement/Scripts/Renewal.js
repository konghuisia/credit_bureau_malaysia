﻿"use strict";

function init() {
    $('.currency').keydown(function (e) {
        restrictDecimalInput(e);
    });

    $('.currency').blur(function (e) {
        if ($(e.target).val().trim() !== "") {
            var t = $(e.target).val().replace(/\,/g, '');
            $(e.target).val(numberWithCommas(parseFloat(t).toFixed(2)));
        }
        else {
            $(e.target).val('0.00');
        }
    });
}


function InputValidate(status) {
    var isValid = true;
    if (status === "New" || status === "Draft" || status === "Pending Requester") {
        if ($('#inptSubject').val() === "") {
            if ($('#inptSubject').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please enter subject to proceed</p>').insertAfter('#inptSubject');
            }
            isValid = false;
        }
        else {
            $('#inptSubject').next().remove();
        }

        if ($('#inptCredit').val() === "") {
            if ($('#inptCredit').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please enter credit to proceed</p>').insertAfter('#inptCredit');
            }
            isValid = false;
        }
        else {
            $('#inptCredit').next().remove();
        }
        
    }

    if (status === "Pending Requester" || (status === "Pending Finance Team" && $('#previousState').val() === "REQUEST_MORE")) {
        if ($('#inptComment').val() === "") {
            if ($('#inptComment').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter('#inptComment');
            }
            isValid = false;
        }
        else {
            $('#inptComment').next().remove();
        }
    }

    if (status === "Pending Finance Team") {
        if ($('#inptFinancefile tbody tr').length === 0) {
            if ($('#inptFinancefile').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please attach invoice to proceed</p>').insertAfter('#inptFinancefile');
            }
            isValid = false;
        }
        else {
            $('#inptFinancefile').next().remove();
        }
    }

    if (status === "Pending CS Team") {
        if ($('#inptCsfile tbody tr').length === 0) {
            if ($('#inptCsfile').next().length < 1) {
                $('<p class="invalid-alert" style="color:red;">Please attach remittance slip to proceed</p>').insertAfter('#inptCsfile');
            }
            isValid = false;
        }
        else {
            $('#inptCsfile').next().remove();
        }
    }

    return isValid;
}

function SetRequest() {
    if ($('#inptCommentRemark').val() !== "" && $('#inptCommentRemark').val() !== undefined) {
        comment = {
            "Id": "",
            "Description": $('#inptCommentRemark').val()
        };
    }
}

function Submit(actionId) {
    if (InputValidate($('#lblStatus').text())) {
        
        var data = {
            "req": {
                "AccountId": $('#accountId').val(),
                "ActionId": actionId,
                "RoleId": $('#roleId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "RequestTypeId": $('input[name="requesttype"]:checked').val(),
                "Comment": $('#inptComment').val(),
                "Credit": parseFloat($('#inptCredit').val().replace(/,/g, '')),
                "OldBalance": $('#oldBalance').val()
            }
        };

        $.ajax({
            url: window.location.origin + "/CreditManagement/Renewal/Submit",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Complete(actionId) {
    if (InputValidate($('#lblStatus').text())) {

        var data = {
            "req": {
                "AccountId": $('#accountId').val(),
                "ActionId": actionId,
                "RoleId": $('#roleId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "RequestTypeId": $('input[name="requesttype"]:checked').val(),
                "Comment": $('#inptComment').val(),
                "Credit": parseFloat($('#inptCredit').val().replace(/,/g, '')),
                "OldBalance": $('#oldBalance').val()
            }
        };

        $.ajax({
            url: window.location.origin + "/CreditManagement/Renewal/Complete",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function SaveDraft(actionId) {
    if (InputValidate()) {

        var data = {
            "req": {
                "AccountId": $('#accountId').val(),
                "ActionId": actionId,
                "RequestId": $('#requestId').val(),
                "ReferenceNo": $('#referenceNo').val(),
                "Subject": $('#inptSubject').val(),
                "Description": $('#inptDescription').val(),
                "RequestTypeId": $('input[name="requesttype"]:checked').val(),
                "Comment": $('#inptComment').val(),
                "Credit": parseFloat($('#inptCredit').val().replace(/,/g, '')),
                "OldBalance": $('#oldBalance').val()
            }
        };

        $.ajax({
            url: window.location.origin + "/CreditManagement/Renewal/SaveDraft",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
}

function Approve(actionId) {
    var data = {
        "req": {
            "AccountId": $('#accountId').val(),
            "ActionId": actionId,
            "RequestId": $('#requestId').val(),
            "ReferenceNo": $('#referenceNo').val(),
            "Comment": $('#inptComment').val()
        }
    }
    $.ajax({
        url: window.location.origin + "/CreditManagement/Renewal/Approve",
        data: data,
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            if (d.Error === null) {
                showToast(d.Message, "success");
                if (d.RedirectUrl !== null) {
                    setTimeout(function () {
                        window.location.href = window.location.origin + d.RedirectUrl;
                    }, 2000);
                }
            }
            else {
                showToast(d.Error, "error");
            }
        }
    });
}

function Reject(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "AccountId": $('#accountId').val(),
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        }
        $.ajax({
            url: window.location.origin + "/CreditManagement/Renewal/Reject",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
    }
}

function RequestMore(actionId) {
    if ($('#inptComment').val() !== "") {
        var data = {
            "req": {
                "AccountId": $('#accountId').val(),
                "ActionId": actionId,
                "ReferenceNo": $('#referenceNo').val(),
                "RequestId": $('#requestId').val(),
                "Comment": $('#inptComment').val()
            }
        }
        $.ajax({
            url: window.location.origin + "/CreditManagement/Renewal/RequestMore",
            data: data,
            cache: false,
            type: "POST",
            dataType: "html",

            success: function (data, textStatus, XMLHttpRequest) {
                var d = JSON.parse(data);
                if (d.Error === null) {
                    showToast(d.Message, "success");
                    if (d.RedirectUrl !== null) {
                        setTimeout(function () {
                            window.location.href = window.location.origin + d.RedirectUrl;
                        }, 2000);
                    }
                }
                else {
                    showToast(d.Error, "error");
                }
            }
        });
    }
    else {
        if ($('#inptComment').next().length < 1) {
            $('<p class="invalid-alert" style="color:red;">Please enter comment to proceed</p>').insertAfter($('#inptComment'));
        }
    }
}

function uploadDoc(type) {
    if ($('#fileSelectRequester').val() !== "" || $('#fileSelectPex').val() !== "") {
        var formdata = new FormData(); //FormData object
        formdata.append("RequestId", $('#requestId').val());
        formdata.append("UploadBy", type);
        var fileInput;
        if (type === "requester") {
            fileInput = document.getElementById('fileSelectRequester');
        }
        else if (type === "finance") {
            fileInput = document.getElementById('fileSelectFinance');
        }
        else if (type === "cs") {
            fileInput = document.getElementById('fileSelectCs');
        }
        //Iterating through each files selected in fileInput
        for (var i = 0; i < fileInput.files.length; i++) {
            //Appending each file to FormData object
            formdata.append("LastModifiedDate", formatDateTime(fileInput.files[i].lastModifiedDate));
            formdata.append(fileInput.files[i].name, fileInput.files[i]);
        }

        //Creating an XMLHttpRequest and sending
        var xhr = new XMLHttpRequest();
        xhr.open('POST', window.location.origin + '/CreditManagement/Renewal/Upload');
        xhr.send(formdata);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var d = JSON.parse(xhr.response);

                var r = $('#inptRequesterfile tbody tr').length + 1;

                var row = "";
                if (type === "requester") {
                    $('#inptRequesterfile tbody')[0].innerHTML = '';
                    for (var i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';
                    }

                    $('#inptRequesterfile').append(row);
                    $('#fileSelectRequester').val('');
                }
                else if (type === "finance") {
                    $('#inptFinancefile tbody')[0].innerHTML = '';
                    for (i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td>' + d[i].UploadedBy + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';
                    }

                    $('#inptFinancefile').append(row);
                    $('#fileSelectFinance').val('');

                    if ($('#inptFinancefile tbody tr').length > 0) {
                        $('#financeFileUpload').toggle();
                    }
                    else {
                        $('#financeFileUpload').toggle();
                    }
                }
                else if (type === "cs") {
                    $('#inptCsfile tbody')[0].innerHTML = '';
                    for (i = 0; i < d.length; i++) {
                        row += '<tr>' +
                            '<td>' + d[i].CreatedDate + '</td>' +
                            '<td>' + d[i].Name + '</td>' +
                            '<td>' + d[i].UploadedBy + '</td> ' +
                            '<td>' + d[i].Size + '</td> ' +
                            '<td>' + d[i].Name.substring(d[i].Name.lastIndexOf('.')) + '</td> ' +
                            '<td data-value="' + d[i].AttachmentId + '" style="text-align:center;"><a class="delete" title="Delete" onclick="removeAttachment(this)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>'
                        '</tr>';
                    }

                    $('#inptCsfile').append(row);
                    $('#fileSelectCs').val('');

                    if ($('#inptCsfile tbody tr').length > 0) {
                        $('#csFileUpload').toggle();
                    }
                    else {
                        $('#csFileUpload').toggle();
                    }
                }
            }
        };
    }
}

function removeAttachment(e) {
    var attachmentId = $(e).parent().data('value');

    $.ajax({
        url: window.location.origin + "/CreditManagement/Renewal/DeleteAttachment",
        data: { "attachmentId": attachmentId },
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            showToast(d.Message, "success");
            $(e).parent().parent().remove();

            if ($('#inptCsfile tbody tr').length === 0) {
                $('#csFileUpload').toggle();
            }

            if ($('#inptFinancefile tbody tr').length === 0) {
                $('#financeFileUpload').toggle();
            }
        }
    });
}

function CloseForm() {
    $.ajax({
        url: window.location.origin + "/CreditManagement/Home/Index",
        data: "",
        cache: false,
        type: "POST",
        dataType: "html",

        success: function (data, textStatus, XMLHttpRequest) {
            var d = JSON.parse(data);
            //setTimeout(function () {
            window.location.href = window.location.origin + d.RedirectUrl;
            //}, 2000);
        }
    });
}

$('#impt :checkbox').change(function () {
    // this will contain a reference to the checkbox   
    if (this.checked) {
        // the checkbox is now checked 
    } else {
        // the checkbox is now no longer checked
    }
});

//function ExportPDF() {
//    $.ajax({
//        url: window.location.origin + "/CreditManagement/Renewal/PdfSharpConvert",
//        data: { "html": document.documentElement.innerHTML },
//        cache: false,
//        type: "POST",
//        dataType: "html",

//        success: function (data, textStatus, XMLHttpRequest) {

//        }
//    });
//}