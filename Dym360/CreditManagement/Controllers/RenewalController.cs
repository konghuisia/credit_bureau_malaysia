﻿using DataAccessHelper.ConfigurationHelper.Store;
using DataAccessHelper.CreditManagement.Store;
using DataAccessHelper.RequestHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CreditManagement.Controllers
{
    public class RenewalController : Controller
    {
        UserStore UserStore;
        RoleStore RoleStore;
        AccountStore AccountStore;
        RequestStore RequestStore;
        ApprovalStore ApprovalStore;
        AttachmentStore AttachmentStore;
        DepartmentStore DepartmentStore;
        CreditManagementRequestStore CreditManagementRequestStore;

        // GET: Renewal
        private static string RoleId;
        private static string ModuleId = "A14CF95F-6EE7-4800-B570-87D62B9FC882";//Credit Management Module Id
        private static List<RoleAction> RoleActions = new List<RoleAction>();

        public RenewalController()
        {
            RoleStore = DependencyResolver.Current.GetService<RoleStore>();
            UserStore = DependencyResolver.Current.GetService<UserStore>();
            AccountStore = DependencyResolver.Current.GetService<AccountStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            ApprovalStore = DependencyResolver.Current.GetService<ApprovalStore>();
            AttachmentStore = DependencyResolver.Current.GetService<AttachmentStore>();
            DepartmentStore = DependencyResolver.Current.GetService<DepartmentStore>();
            CreditManagementRequestStore = DependencyResolver.Current.GetService<CreditManagementRequestStore>();
        }

        public async Task<ActionResult> Renewal(string accountId)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            var vm = new Models.CreditManagementViewModel();
            IList<RoleAction> ra = new List<RoleAction>();
            var role = new Role();
            vm.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            var user = await UserStore.GetUser(Utils.GetUserId());
            role = await RoleStore.GetRoleCreditManagementPOC("");

            RoleId = role.RoleId;

            vm.Request = new Request()
            {
                ReferenceNo = "CS-" + DateTime.Now.ToString("yyyyMMdd-hhmmss"),
                RequestTypeId = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE",
                ModuleId = ModuleId,
                Requester = new Requester()
                {
                    Name = user.Name,
                    UserId = user.UserId,
                    Position = user.Position,
                    Department = user.Department,
                    Reporting = user.Reporting,
                },
                CreatedDate = DateTime.Now
            };

            vm.Request.RequestId = await RequestStore.NewCreditManagementPreRequest(vm.Request);
            vm.Access = "Write";
            ra = await RoleStore.GetRoleActions("", RoleId);

            IList<RequestType> requestTypes = new List<RequestType>();
            requestTypes = await RequestStore.GetAllRequestType();

            IList<Department> departments = new List<Department>();
            departments = await DepartmentStore.GetAllDepartments();

            IList<User> users = new List<User>();
            users = await UserStore.GetAllUser();

            vm.RequestTypes = requestTypes.ToList();
            vm.RoleActions = ra.ToList();
            RoleActions = ra.ToList();
            vm.Departments = departments.ToList();
            vm.Users = users.ToList().FindAll(x => x.UserId != Utils.GetUserId());
            vm.Account = await AccountStore.GetAccountByAccountId(accountId);

            return View(vm);
        }

        public async Task<ActionResult> EditRenewal(string requestId, string cameFrom)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            var vm = new Models.CreditManagementViewModel();
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            var role = new Role();
            vm.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            var user = await UserStore.GetUser(Utils.GetUserId());
            role = await RoleStore.GetRoleCreditManagementPOC("");

            vm.Request = await RequestStore.GetRequest(requestId);
            apps = await ApprovalStore.GetAllApproval(requestId);
            vm.Request.FinanceTeam = apps.ToList().FindAll(x => x.RoleId == "7CB5B81B-50F2-479A-A27D-3DA421500C49");
            vm.Request.CSTeam = apps.ToList().FindAll(x => x.RoleId == "F3B404A5-DF2C-46AA-A687-DFA360E96CA7");
            vm.Request.AdminTeam = apps.ToList().FindAll(x => x.RoleId == "DC04F2AA-66CA-4466-8A4A-F54056ECA8BC");

            if (vm.Request.Status == "Draft" || vm.Request.Status == "Pending Requester")
            {
                RoleId = role.RoleId;
                if (vm.Request.Requester.UserId == Utils.GetUserId())
                {
                    vm.Access = "Write";
                    ra = await RoleStore.GetRoleActions("", RoleId);
                }
            }
            else
            {
                if (!vm.Request.Status.ToLower().Contains("close"))
                {
                    var isBlock = true;
                    var pends = apps.ToList().FindAll(x => x.Action == "PENDING");
                    foreach (var app in pends)
                    {
                        if (app.RoleId == vm.Request.RequestState.RoleId && app.UserId == Utils.GetUserId())
                        {
                            isBlock = false;
                        }
                    }
                    if (!isBlock)
                    {
                        vm.Access = "Write";
                        ra = await RoleStore.GetRoleActions("", vm.Request.RequestState.RoleId);
                    }
                }
                else if (!vm.Request.Status.ToLower().Contains("reject"))
                {
                    vm.Print = true;
                }
                RoleId = vm.Request.RequestState.RoleId;
            }


            IList<RequestType> requestTypes = new List<RequestType>();
            requestTypes = await RequestStore.GetAllRequestType();

            IList<Department> departments = new List<Department>();
            departments = await DepartmentStore.GetAllDepartments();

            IList<User> users = new List<User>();
            users = await UserStore.GetAllUser();

            IList<Classification> classifications = new List<Classification>();
            classifications = await AttachmentStore.GetClassifications();


            vm.RequestTypes = requestTypes.ToList();
            vm.RoleActions = ra.ToList();
            RoleActions = ra.ToList();
            vm.Departments = departments.ToList();
            vm.Users = users.ToList().FindAll(x => x.UserId != Utils.GetUserId());
            vm.Classifications = classifications.ToList();
            vm.Account = await AccountStore.GetAccountByRequestId(requestId);
            vm.CameFrom = cameFrom;

            return View(vm);
        }

        public async Task<ActionResult> RenewalPreview(string requestId, string cameFrom)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            var vm = new Models.CreditManagementViewModel();
            IList<RoleAction> ra = new List<RoleAction>();
            IList<Approval> apps = new List<Approval>();
            var role = new Role();
            vm.BaseUrl = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "");
            var user = await UserStore.GetUser(Utils.GetUserId());
            role = await RoleStore.GetRole("");

            vm.Request = await RequestStore.GetRequest(requestId);
            apps = await ApprovalStore.GetAllApproval(requestId);
            vm.Request.FinanceTeam = apps.ToList().FindAll(x => x.RoleId == "7CB5B81B-50F2-479A-A27D-3DA421500C49");
            vm.Request.CSTeam = apps.ToList().FindAll(x => x.RoleId == "F3B404A5-DF2C-46AA-A687-DFA360E96CA7");
            vm.Request.AdminTeam = apps.ToList().FindAll(x => x.RoleId == "DC04F2AA-66CA-4466-8A4A-F54056ECA8BC");

            IList<RequestType> requestTypes = new List<RequestType>();
            requestTypes = await RequestStore.GetAllRequestType();

            IList<Department> departments = new List<Department>();
            departments = await DepartmentStore.GetAllDepartments();

            IList<User> users = new List<User>();
            users = await UserStore.GetAllUser();

            IList<Classification> classifications = new List<Classification>();
            classifications = await AttachmentStore.GetClassifications();

            vm.RequestTypes = requestTypes.ToList();
            vm.RoleActions = ra.ToList();
            RoleActions = ra.ToList();
            vm.Departments = departments.ToList();
            vm.Users = users.ToList().FindAll(x => x.UserId != Utils.GetUserId());
            vm.Classifications = classifications.ToList();
            vm.Account = await AccountStore.GetAccountByRequestId(requestId);
            vm.CameFrom = cameFrom;

            return View(vm);
        }

        public async Task<JsonResult> SaveDraft(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Status = "Draft";
            var success = await CreditManagementRequestStore.SaveRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request saved as draft";
                resp.RedirectUrl = "/CreditManagement/Renewal/EditRenewal?requestId=" + req.RequestId;
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Submit(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/CreditManagement/Renewal/EditRenewal?requestId=" + req.RequestId + "";
            var success = await CreditManagementRequestStore.SubmitRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request submitted";
                resp.RedirectUrl = "/CreditManagement/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> Complete(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = !string.IsNullOrEmpty(RoleId) ? RoleId : req.RoleId;
            req.UserId = Utils.GetUserId();
            req.Requester.UserId = req.UserId;
            var success = await CreditManagementRequestStore.SubmitRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request closed complete";
                resp.RedirectUrl = "/CreditManagement/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        public async Task<JsonResult> RequestMore(Request req)
        {
            req.ModuleId = ModuleId;
            req.RoleId = RoleId;
            req.UserId = Utils.GetUserId();
            req.Link = System.Web.HttpContext.Current.Request.Url.Authority + "/CreditManagement/Renewal/EditRenewal?requestId=" + req.RequestId + "";
            var success = await CreditManagementRequestStore.RequestMoreRequest(req);

            var resp = new Response();
            if (success)
            {
                resp.Message = "Request sent back for clarification";
                resp.RedirectUrl = "/CreditManagement/Home/";
            }
            else
            {
                resp.Error = "Unable to proceed, please try again later";
            }
            return Json(resp);
        }

        [HttpPost]
        public async Task<JsonResult> Upload()
        {
            var request = new Request();
            request.Attachments = new List<Attachment>();
            request.Requester.UserId = Utils.GetUserId();
            request.RequestId = Request.Params.GetValues("RequestId")[0];
            var uploadBy = Request.Params.GetValues("UploadBy")[0];
            var classificationId = "";

            var folder = "";
            var AttachmentTypeId = "";
            if (uploadBy == "requester")
            {
                folder = "Attachments";
                AttachmentTypeId = "D88A3D13-707A-46D0-B4CC-6EAF6832C3E3";
            }
            else if (uploadBy == "finance")
            {
                folder = "Attachments";
                AttachmentTypeId = "24EECF8D-EB43-44E8-8417-BE44AD12922D";
            }
            else if (uploadBy == "cs")
            {
                folder = "Attachments";
                AttachmentTypeId = "21CF1EA0-5069-41F3-B58F-62595FEB1F56";
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                            //Use the following properties to get file's name, size and MIMEType
                int fileSize = file.ContentLength;
                string fileName = file.FileName;
                string ext = fileName.Substring(fileName.LastIndexOf('.'));
                string mimeType = file.ContentType;
                string path = "Areas/CreditManagement/" + folder + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + request.RequestId + "/";
                string location = Server.MapPath("~/") + path;
                System.IO.Stream fileContent = file.InputStream;
                System.IO.Directory.CreateDirectory(location);
                //To save file, use SaveAs method
                file.SaveAs(location + fileName); //File will be saved in application root

                request.Attachments.Add(new Attachment()
                {
                    AttachmentTypeId = AttachmentTypeId,
                    Name = fileName,
                    Size = file.ContentLength.ToString(),
                    Path = path,
                    AbsolutePath = location,
                    ModifiedDate = Convert.ToDateTime(Request.Params.GetValues("LastModifiedDate")[i]).ToString("dd MMM yyyy HH:mm:ss"),
                    ClassificationId = classificationId
                });
            }

            request = await AttachmentStore.CreateAsync(request);

            foreach (var a in request.Attachments)
            {
                a.CreatedDate = Convert.ToDateTime(a.CreatedDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            return Json(request.Attachments.FindAll(x => x.AttachmentTypeId == AttachmentTypeId));

        }

        [HttpPost]
        public async Task<JsonResult> DeleteAttachment(string attachmentId)
        {
            await AttachmentStore.DeleteAttachment(attachmentId);
            return Json(new Response()
            {
                Message = "Attachment removed"
            });
        }
    }
}