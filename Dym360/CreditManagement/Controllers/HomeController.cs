﻿using DataAccessHelper.ConfigurationHelper.Store;
using DataAccessHelper.CreditManagement.Store;
using DataAccessHelper.RequestHelper.Store;
using IdentityManagement.Utilities;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CreditManagement.Controllers
{
    public class HomeController : Controller
    {
        AccountStore AccountStore;
        RequestStore RequestStore;
        UserAccessStore UserAccessStore;
        AccountRequestStore AccountRequestStore;
        DepartmentAccessStore DepartmentAccessStore;

        public HomeController()
        {
            AccountStore = DependencyResolver.Current.GetService<AccountStore>();
            RequestStore = DependencyResolver.Current.GetService<RequestStore>();
            UserAccessStore = DependencyResolver.Current.GetService<UserAccessStore>();
            AccountRequestStore = DependencyResolver.Current.GetService<AccountRequestStore>();
            DepartmentAccessStore = DependencyResolver.Current.GetService<DepartmentAccessStore>();
        }

        public async Task<ActionResult> Index()
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            return View(new Models.RequestIndexViewModel() {
                CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment()),
                Accounts = await AccountStore.GetAccounts(),
                AccountRequests = await AccountRequestStore.GetAccountRequest(),
                MyTasks = await RequestStore.GetRenewalRequestsList(Utils.GetUserId())
            });
        }

        public async Task<ActionResult> LoadMyTask()
        {
            IList<MyTask> requests = await RequestStore.GetRenewalRequests(Utils.GetUserId());

            return View("MyTask", requests.ToList());
        }

        public async Task<ActionResult> LoadPageView(string type)
        {
            if (!Utils.IfUserAuthenticated())
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }

            var vm = new Models.RequestIndexViewModel();
            vm.CurrentUserDepartmentAccess = await DepartmentAccessStore.GetDepartmentAccessByDepartmentId(Utils.GetUserDepartment());

            switch (type)
            {
                case "active":
                default:
                    vm.Accounts = await AccountStore.GetAccounts();
                    vm.AccountRequests = await AccountRequestStore.GetAccountRequest();
                    vm.MyTasks = await RequestStore.GetRenewalRequestsList(Utils.GetUserId());

                    return PartialView("_Active", vm);
                case "inactive":
                    vm.Accounts = await AccountStore.GetAccounts();

                    return PartialView("_Inactive", vm);
            }
        }
    }
}