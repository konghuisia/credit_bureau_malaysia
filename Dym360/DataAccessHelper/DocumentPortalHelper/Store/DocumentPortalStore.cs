﻿using DataAccessHelper.DocumentPortalHelper.DAL;
using DataAccessHelper.ProjectSalesHelper.DAL;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.DocumentPortalHelper.Store
{
    public class DocumentPortalStore
    {
        public Task<bool> SubmitRequest(CompanyDocument sop, Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return DocumentPortalControl.NewRequest(sop,req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> SaveRequest(CompanyDocument sop, Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return DocumentPortalControl.SaveRequest(sop, req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RejectRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return DocumentPortalControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> ApproveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return DocumentPortalControl.ApproveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMoreRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return DocumentPortalControl.RequestMoreRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task DeleteAsync(Project project)
        {
            if (project != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    ProjectSalesControl.DeleteProjectSale(project);
                });
            }
            throw new ArgumentNullException("project");
        }

        public void Dispose()
        {

        }

        public Task<IList<CompanyDocument>> GetAllCompanyDocuments()
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentPortalControl.GetCompanyDocuments();
            });
        }
        public Task<CompanyDocument> GetCompanyDocument(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentPortalControl.GetCompanyDocument(requestId);
            });
        }

        public Task<ProjectSales> GetProjectSale(string projectId = null)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProjectSale(projectId);
            });
        }

        public Task<int> GetProposalSeqNo()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProposalNoSeq();
            });
        }

        public Task<ProjectSales> Save(ProjectSales projectSales, ProjectSalesAction projectSalesAction, bool isUpdate = false, bool isGetProposalNo = false)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.Save(projectSales, projectSalesAction, isUpdate, isGetProposalNo);
            });
        }

        public Task SubmitProjectSale(ProjectSales projectSales, ProjectSalesAction projectSalesAction)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.SubmitProjectSale(projectSales,projectSalesAction);
            });
        }

        public Task ActionTakenProjectSale(ProjectSalesAction projectSalesAction)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.ActionTakenProjectSale(projectSalesAction);
            });
        }
    }
}
