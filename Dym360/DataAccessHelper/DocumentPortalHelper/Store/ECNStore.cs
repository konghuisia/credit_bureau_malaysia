﻿using DataAccessHelper.DocumentPortalHelper.DAL;
using ObjectClasses;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.DocumentPortalHelper.Store
{
    public class ECNStore
    {
        public Task<bool> Assign(ECNDocument ecnDoc, Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.Assign(ecnDoc, req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Save(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.SaveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Submit(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.Submit(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Approve(Request req, string baseUrl)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.ApproveRequest(req, baseUrl);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Start(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.StartRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Reject(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Complete(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.CompleteRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> RequestMore(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ECNControl.RequestMoreRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<ECNDocument> GetECNDocument(string reqId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ECNControl.GetECNDocument(reqId);
            });
        }

        public Task<IList<ECNModel>> GetECNDocuments()
        {
            return Task.Factory.StartNew(() =>
            {
                return ECNControl.GetECNDocuments();
            });
        }
    }
}
