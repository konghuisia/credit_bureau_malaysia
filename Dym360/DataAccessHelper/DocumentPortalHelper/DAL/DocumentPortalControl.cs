﻿using DatabaseConnection.Data;
using ObjectClasses.ProjectSalesManagement.Entities;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses;
using ObjectClasses.DocumentPortal;

namespace DataAccessHelper.DocumentPortalHelper.DAL
{
    public static class DocumentPortalControl
    {
        public static bool NewRequest(CompanyDocument sop, Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            if (req.Approvers != null)
            {
                foreach (var a in req.Approvers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (sop != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId }
                };
                var companyDocument = SqlHelper.GetRecord<CompanyDocument>("dp.CompanyDocument_Get", parameters);

                if (companyDocument == null)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "DepartmentApplicability", ParameterValue = sop.DepartmentApplicability },
                        new ParameterInfo() { ParameterName = "LocationApplicability", ParameterValue = sop.LocationApplicability },
                        new ParameterInfo() { ParameterName = "SOPName", ParameterValue = sop.SOPName },
                        new ParameterInfo() { ParameterName = "SOPVersion", ParameterValue = sop.SOPVersion },
                        new ParameterInfo() { ParameterName = "Validity", ParameterValue = sop.Validity },
                        new ParameterInfo() { ParameterName = "UserOwnerId", ParameterValue = sop.UserOwnerId },
                        new ParameterInfo() { ParameterName = "DepartmentOwnerId", ParameterValue = sop.DepartmentOwnerId },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = sop.Code },
                        new ParameterInfo() { ParameterName = "SubmittedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("dp.CompanyDocument_New", parameters);
                }
                else
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "DepartmentApplicability", ParameterValue = sop.DepartmentApplicability },
                        new ParameterInfo() { ParameterName = "LocationApplicability", ParameterValue = sop.LocationApplicability },
                        new ParameterInfo() { ParameterName = "SOPName", ParameterValue = sop.SOPName },
                        new ParameterInfo() { ParameterName = "SOPVersion", ParameterValue = sop.SOPVersion },
                        new ParameterInfo() { ParameterName = "Validity", ParameterValue = sop.Validity },
                        new ParameterInfo() { ParameterName = "UserOwnerId", ParameterValue = sop.UserOwnerId },
                        new ParameterInfo() { ParameterName = "DepartmentOwnerId", ParameterValue = sop.DepartmentOwnerId },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = sop.Code }
                    };
                    success = SqlHelper.ExecuteQuery("dp.CompanyDocument_Update", parameters);
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool SaveRequest(CompanyDocument sop, Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (sop != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId }
                };
                var companyDocument = SqlHelper.GetRecord<CompanyDocument>("dp.CompanyDocument_Get", parameters);

                if (companyDocument == null)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "DepartmentApplicability", ParameterValue = sop.DepartmentApplicability },
                        new ParameterInfo() { ParameterName = "LocationApplicability", ParameterValue = sop.LocationApplicability },
                        new ParameterInfo() { ParameterName = "SOPName", ParameterValue = sop.SOPName },
                        new ParameterInfo() { ParameterName = "SOPVersion", ParameterValue = sop.SOPVersion },
                        new ParameterInfo() { ParameterName = "Validity", ParameterValue = sop.Validity },
                        new ParameterInfo() { ParameterName = "UserOwnerId", ParameterValue = sop.UserOwnerId },
                        new ParameterInfo() { ParameterName = "DepartmentOnerId", ParameterValue = sop.DepartmentOwnerId },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = sop.Code },
                        new ParameterInfo() { ParameterName = "SubmittedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("dp.CompanyDocument_New", parameters);
                }
                else
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "DepartmentApplicability", ParameterValue = sop.DepartmentApplicability },
                        new ParameterInfo() { ParameterName = "LocationApplicability", ParameterValue = sop.LocationApplicability },
                        new ParameterInfo() { ParameterName = "SOPName", ParameterValue = sop.SOPName },
                        new ParameterInfo() { ParameterName = "SOPVersion", ParameterValue = sop.SOPVersion },
                        new ParameterInfo() { ParameterName = "Validity", ParameterValue = sop.Validity },
                        new ParameterInfo() { ParameterName = "UserOwnerId", ParameterValue = sop.UserOwnerId },
                        new ParameterInfo() { ParameterName = "DepartmentOnerId", ParameterValue = sop.DepartmentOwnerId },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = sop.Code }
                    };
                    success = SqlHelper.ExecuteQuery("dp.CompanyDocument_Update", parameters);
                }
            }

            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40" },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool RejectRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Rejected" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ApproveRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;

                if (req.Status == "Close Completed")
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    };
                    var dp = SqlHelper.GetRecord<CompanyDocument>("dp.CompanyDocument_Get", parameters);

                    if (dp != null)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = "A542CF4A-3986-4548-8AD0-8CA67291DC3B" },
                        };
                        var att = SqlHelper.GetRecord<Attachment>("req.Attachment_Get_ByType", parameters);

                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "CompanyDocumentId", ParameterValue = dp.CompanyDocumentId },
                            new ParameterInfo() { ParameterName = "Name", ParameterValue = att.Name.Replace(att.Name.Substring(att.Name.LastIndexOf(".")),".pdf")},
                            new ParameterInfo() { ParameterName = "Description", ParameterValue = att.Description },
                            new ParameterInfo() { ParameterName = "Path", ParameterValue = att.Path.Replace("Attachments","Documents")},
                            new ParameterInfo() { ParameterName = "Size", ParameterValue = att.Size },
                            new ParameterInfo() { ParameterName = "ModifiedDate", ParameterValue = Convert.ToDateTime(att.ModifiedDate).ToString("yyyy-MM-dd hh:mm:ss tt") },
                            new ParameterInfo() { ParameterName = "ClassificationId", ParameterValue = att.ClassificationId }
                        };
                        success = SqlHelper.ExecuteQuery("dp.Document_New", parameters);
                    }
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Approved" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool RequestMoreRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Requested more information" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IList<CompanyDocument> GetCompanyDocuments()
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            var companyDocuments = SqlHelper.GetRecords<CompanyDocument>("dp.CompanyDocuments_Get", new List<ParameterInfo>());

            foreach (var cd in companyDocuments)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CompanyDocumentId", ParameterValue = cd.CompanyDocumentId }
                };
                var doc = SqlHelper.GetRecord<Document>("dp.Document_Get", parameters);
                cd.Document = doc;
            }
            return companyDocuments.FindAll(x => x.Document != null);
        }

        public static CompanyDocument GetCompanyDocument(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };

            return SqlHelper.GetRecord<CompanyDocument>("dp.CompanyDocument_Get", parameters);
        }
    }
}
