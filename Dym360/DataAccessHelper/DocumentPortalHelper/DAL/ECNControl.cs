﻿using DatabaseConnection.Data;
using FileManager;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.DocumentPortalHelper.DAL
{
    public class ECNControl
    {
        public static bool SaveRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40" },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            return success > 0 ? true : false;
        }
        public static bool Submit(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflows = SqlHelper.GetRecords<Workflow>("cfg.Workflow_Get", parameters);
            var workflow = new Workflow();

            if (req.Reviewers != null)
            {
                workflow = workflows.Find(x => x.NextWorkflowProcess == "Reviewer");
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            else
            {
                if (workflows.Count > 1)
                {
                    workflow = workflows.Find(x => x.NextWorkflowProcess == "Engineer");
                }
                else
                {
                    workflow = workflows[0];
                }
            }


            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            return success > 0 ? true : false;
        }
        public static bool Assign(ECNDocument ecnDoc, Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (ecnDoc != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId }
                };
                var ecnDocument = SqlHelper.GetRecord<ECNDocument>("ecn.ECNDocument_Get", parameters);

                if (ecnDocument == null)
                {
                    ecnDocument = new ECNDocument();

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "CompletionDate", ParameterValue = ecnDoc.CompletionDate },
                        new ParameterInfo() { ParameterName = "DocumentName", ParameterValue = ecnDoc.DocumentName },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = ecnDoc.Code },
                        new ParameterInfo() { ParameterName = "Remark", ParameterValue = ecnDoc.Remark },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    ecnDocument.ECNDocumentId = SqlHelper.GetRecord<string>("ecn.ECNDocument_New", parameters);
                }
                else
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "CompletionDate", ParameterValue = ecnDoc.CompletionDate },
                        new ParameterInfo() { ParameterName = "DocumentName", ParameterValue = ecnDoc.DocumentName },
                        new ParameterInfo() { ParameterName = "Code", ParameterValue = ecnDoc.Code },
                        new ParameterInfo() { ParameterName = "Remark", ParameterValue = ecnDoc.Remark },
                        new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("ecn.ECNDocument_Update", parameters);
                }

                foreach (var scp in ecnDoc.ECNTaskScopes)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "ECNDocumentId", ParameterValue = ecnDocument.ECNDocumentId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue =  scp.UserId}
                    };
                    success = SqlHelper.ExecuteQuery("ecn.ECNTaskScope_New", parameters);
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            if (req.Approvers != null)
            {
                foreach (var a in req.Approvers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (req.FinalApprovers != null)
            {
                foreach (var a in req.FinalApprovers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "23CB39F4-19F8-4998-9F1E-2B0E996433D9" },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Assigned" }
            };

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Assigned" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool RejectRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Rejected" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ApproveRequest(Request req, string baseUrl)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;

                if (req.Status == "Close Completed")
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    };
                    var ecn = SqlHelper.GetRecord<ECNDocument>("ecn.ECNDocument_Get", parameters);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    };
                    var atts = SqlHelper.GetRecords<Attachment>("dbo.Documents_Get", parameters);

                    var path = "";
                    var files = new List<string>();
                    foreach (var att in atts)
                    {
                        if (att.AttachmentTypeId == "A542CF4A-3986-4548-8AD0-8CA67291DC3B")
                        {
                            files.Add(baseUrl + att.Path + att.Name);
                            if (path == "")
                            {
                                path = att.Path;
                            }
                        }
                    }
                    var publish_doc = baseUrl + path + "/" + ecn.DocumentName + ".pdf";

                    MergePDF.MergeASPDFs(publish_doc, files.ToArray());
                    if (ecn != null)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = "A542CF4A-3986-4548-8AD0-8CA67291DC3B" },
                        };
                        var att = SqlHelper.GetRecord<Attachment>("req.Attachment_Get_ByType", parameters);

                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "ECNDocumentId", ParameterValue = ecn.ECNDocumentId },
                            new ParameterInfo() { ParameterName = "Path", ParameterValue = path },
                            new ParameterInfo() { ParameterName = "Size", ParameterValue = 0 },
                            new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("ecn.ECNDocument_Publish", parameters);
                    }
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Approved" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CompleteRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null && workflow.workflowApprovals.Count > 0)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
                else
                {
                    parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" }
                        };
                    var requestApprovers = SqlHelper.GetRecords<User>("req.RequestApproval_Get", parameters);

                    foreach (var a in requestApprovers)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Completed" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool RequestMoreRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Requested more information" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool StartRequest(Request req)
        {
            var parameters = new List<ParameterInfo>();
            var success = 0;
            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Started" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            return success == 0 ? false : true;
        }
        public static ECNDocument GetECNDocument(string reqId)
        {
            var ecnDoc = new ECNDocument();
            var parameters = new List<ParameterInfo> {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = reqId }
            };
            ecnDoc = SqlHelper.GetRecord<ECNDocument>("ecn.ECNDocument_Get", parameters);
            if (ecnDoc != null)
            {
                parameters = new List<ParameterInfo> {
                    new ParameterInfo() { ParameterName = "ECNDocumentId", ParameterValue = ecnDoc.ECNDocumentId }
                };
                ecnDoc.ECNTaskScopes = SqlHelper.GetRecords<ECNTaskScope>("ecn.ECNTaskScopes_Get", parameters);

                parameters = new List<ParameterInfo> {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = reqId }
                };
                ecnDoc.Documents = SqlHelper.GetRecords<Attachment>("dbo.Documents_Get", parameters);
            }

            return ecnDoc;
        }
        public static IList<ECNModel> GetECNDocuments()
        {
            IList<ECNModel> ecnmL = new List<ECNModel>();
            ecnmL = SqlHelper.GetECNModels("ecn.ECNDocuments_Get", new List<ParameterInfo>());

            return ecnmL;
        }
    }
}
