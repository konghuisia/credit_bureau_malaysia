﻿using DataAccessHelper.ClaimManagement.DAL;
using ObjectClasses;
using ObjectClasses.ClaimManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ClaimManagement.Store
{
    public class ClaimManagementRequestStore
    {
        public Task<bool> SubmitRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.NewRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> SaveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.SaveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RejectRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> ApproveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.ApproveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMoreRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.RequestMoreRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public void Dispose()
        {

        }

        public Task<ClaimItems> SaveClaimItem(ClaimItems c, string reqId)
        {
            if (c != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.SaveClaimItem(c, reqId);
                });
            }
            throw new ArgumentNullException("claims");
        }

        public Task<DateTime?> GetClaim(string requestId)
        {
            if (!string.IsNullOrEmpty(requestId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.GetClaim(requestId);
                });
            }
            throw new ArgumentNullException("getclaim");
        }

        public Task<List<ClaimItems>> GetClaimItems(string requestId)
        {
            if (!string.IsNullOrEmpty(requestId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.GetClaimItems(requestId).ToList() ?? new List<ClaimItems>();
                });
            }
            throw new ArgumentNullException("getclaims");
        }

        public Task<bool> DeleteClaim(string claimId, string userid)
        {
            if (!string.IsNullOrEmpty(claimId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ClaimManagementRequestControl.DeleteClaim(claimId, userid);
                });
            }
            throw new ArgumentNullException("claim");
        }
    }
}
