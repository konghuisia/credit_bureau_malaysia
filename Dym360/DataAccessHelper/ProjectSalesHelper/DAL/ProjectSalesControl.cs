﻿using DatabaseConnection.Data;
using ObjectClasses.ProjectSalesManagement.Entities;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses;

namespace DataAccessHelper.ProjectSalesHelper.DAL
{
    public static class ProjectSalesControl
    {
        public static ProjectSales NewProjectSale(ProjectSales projectSale)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = projectSale.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSale.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSale.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);
            if (workflow.workflowApprovals != null && workflow.workflowApprovals.Count > 0)
            {
                foreach (var a in workflow.workflowApprovals)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSale.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSale.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = "412BA7CC-2BEA-49EC-80E4-72B676F5CD07" },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSale.Requester.UserId },
                new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = projectSale.ReferenceNo },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "1B9D0B8F-3EEF-450A-86A7-75CE3725C3EE" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "Project Sale Request" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            projectSale.Status = "New";
            parameters.Add(new ParameterInfo() { ParameterName = "Status", ParameterValue = projectSale.Status });
            var requestId = SqlHelper.GetRecord<string>("req.Request_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSale.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            if (!string.IsNullOrEmpty(requestId))
            {
                projectSale.RequestId = requestId;
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = projectSale.ReferenceNo },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = "Project Sales " + projectSale.ReferenceNo },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "Project Sales Proposal" },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSale.Requester.UserId }
                };
                projectSale.ProjectInfo.ProjectId = SqlHelper.GetRecord<string>("prj.Project_New", parameters);
            }
            return projectSale;
        }

        public static int DeleteProjectSale(Project projectSale)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSale.ProjectId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Project_Delete", parameters);
            return success;
        }

        public static IList<ProjectSales> GetProjectSales(string userId)
        {
            List<ProjectSales> oProjectSales = new List<ProjectSales>();
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            var p = SqlHelper.GetProjectSales("prj.ProjectSales_Get_Pending", parameters);
            oProjectSales.AddRange(p);

            foreach (var ps in oProjectSales)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = ps.CustomerInfo.CustomerId }
                };
                var cps = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);

                if (cps != null && cps.Count > 0)
                {
                    ps.CustomerInfo.ContactPersons = cps;
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "userId", ParameterValue = userId }
            };
            var c = SqlHelper.GetProjectSales("prj.ProjectSales_Get_Closed", parameters);
            oProjectSales.AddRange(c);

            foreach (var ps in oProjectSales)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = ps.CustomerInfo.CustomerId }
                };
                var cps = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);

                if (cps != null && cps.Count > 0)
                {
                    ps.CustomerInfo.ContactPersons = cps;
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "userId", ParameterValue = userId }
            };
            var o = SqlHelper.GetProjectSales("prj.ProjectSales_Get_Outbox", parameters);
            oProjectSales.AddRange(o);

            foreach (var ps in oProjectSales)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = ps.CustomerInfo.CustomerId }
                };
                var cps = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);

                if (cps != null && cps.Count > 0)
                {
                    ps.CustomerInfo.ContactPersons = cps;
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "userId", ParameterValue = userId }
            };
            var d = SqlHelper.GetProjectSales("prj.ProjectSales_Get_Draft", parameters);
            oProjectSales.AddRange(d);

            foreach (var ps in oProjectSales)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = ps.CustomerInfo.CustomerId }
                };
                var cps = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);

                if (cps != null && cps.Count > 0)
                {
                    ps.CustomerInfo.ContactPersons = cps;
                }
            }

            return oProjectSales;
        }
        public static IList<ProjectSales> GetProjectSales()
        {
            var c = SqlHelper.GetProjectSales("prj.ProjectSales_Get_Closed", new List<ParameterInfo>());
            return c;
        }

        public static ProjectSales GetProjectSale(string projectId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            var oProjectSale = SqlHelper.GetRecord<ProjectSales>("prj.ProjectSale_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = oProjectSale.RequestId }
            };
            oProjectSale.RequestState = SqlHelper.GetRecord<RequestState>("req.RequestState_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            oProjectSale.ProjectInfo = SqlHelper.GetRecord<Project>("prj.ProjectDetail_Get", parameters);

            if (oProjectSale.ProjectInfo != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
                };
                oProjectSale.ProjectInfo.Proposal = SqlHelper.GetRecord<Proposal>("prj.ProposalNo_Get", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            oProjectSale.CustomerInfo = SqlHelper.GetRecord<Customer>("cust.Customer_Get", parameters);

            if (oProjectSale.CustomerInfo != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = oProjectSale.CustomerInfo.CustomerId }
                };
                oProjectSale.CustomerInfo.ContactPersons = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);
            }
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = oProjectSale.RequestId }
            };
            oProjectSale.Histories = SqlHelper.GetRecords<History>("req.RequestHistory_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = oProjectSale.RequestId }
            };
            oProjectSale.Attachments = SqlHelper.GetRecords<Attachment>("req.Attachments_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = oProjectSale.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = oProjectSale.UserId }
            };
            oProjectSale.CommentRemarks = SqlHelper.GetRecords<CommentRemark>("req.Comment_Get", parameters);

            return oProjectSale;
        }

        public static int GetProposalNoSeq()
        {
            string seqNo = SqlHelper.GetRecord<string>("prj.ProposalNoSeq_Get", new List<ParameterInfo>());
            return string.IsNullOrEmpty(seqNo) ? 1 : Convert.ToInt32(seqNo);
        }

        public static ProjectSales Save(ProjectSales projectSales, ProjectSalesAction projectSalesAction, bool isUpdate, bool isGetProposalNo)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            int success = 0;

            if (isGetProposalNo)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId },
                    new ParameterInfo() { ParameterName = "ProposalNo", ParameterValue = projectSales.ProjectInfo.Proposal.ProposalNo },
                    new ParameterInfo() { ParameterName = "ProposalNoSeq", ParameterValue = projectSales.ProjectInfo.Proposal.ProposalNoSeq },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSales.ProjectInfo.Proposal.CreatedBy }
                };
                success = SqlHelper.ExecuteQuery("prj.ProposalNo_New", parameters);
            }

            if (projectSales.Attachments != null)
            {
                foreach (var a in projectSales.Attachments)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = a.AttachmentTypeId },
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSales.RequestId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSales.Requester.UserId },
                        new ParameterInfo() { ParameterName = "Description", ParameterValue = a.Description },
                        new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = a.AttachmentId }
                    };
                    success = SqlHelper.ExecuteQuery("req.Attachment_Update", parameters);
                }
            }

            if (isUpdate)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId },
                    new ParameterInfo() { ParameterName = "PlantType", ParameterValue = projectSales.ProjectInfo.PlantType },
                    new ParameterInfo() { ParameterName = "PaymentTermId", ParameterValue = projectSales.ProjectInfo.PaymentTermId },
                    new ParameterInfo() { ParameterName = "DeliveryDate", ParameterValue = Convert.ToDateTime(projectSales.ProjectInfo.DeliveryDate).ToString("yyyy/MM/dd") },
                    new ParameterInfo() { ParameterName = "PriceEstimation", ParameterValue = projectSales.ProjectInfo.PriceEstimation.ToString().Replace(",", "") },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = projectSales.ProjectInfo.Description }
                };
                success = SqlHelper.ExecuteQuery("prj.ProjectDetail_Update", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = projectSales.CustomerInfo.CustomerId },
                    new ParameterInfo() { ParameterName = "CountryId", ParameterValue = projectSales.CustomerInfo.CountryId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = projectSales.CustomerInfo.Name },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = projectSales.CustomerInfo.Email },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = projectSales.CustomerInfo.ContactNo },
                    new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = projectSales.CustomerInfo.FaxNo },
                    new ParameterInfo() { ParameterName = "Address", ParameterValue = projectSales.CustomerInfo.Address },
                    new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = projectSales.Requester.UserId }
                };
                success = SqlHelper.ExecuteQuery("cust.Customer_Update", parameters);

                foreach (var p in projectSales.CustomerInfo.ContactPersons)
                {
                    if (string.IsNullOrEmpty(p.ContactPersonId))
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = projectSales.CustomerInfo.CustomerId },
                            new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                            new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                            new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSales.Requester.UserId }
                        };
                        SqlHelper.GetRecord<ContactPerson>("cust.CustomerContactPerson_New", parameters);
                    }
                }

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = projectSales.CustomerInfo.CustomerId }
                };
                projectSales.CustomerInfo.ContactPersons = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + projectSalesAction.ReferenceNo + "]" }
                };
                var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

                if (!string.IsNullOrWhiteSpace(requestHistoryId))
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                        new ParameterInfo() { ParameterName = "Comment", ParameterValue = projectSalesAction.Comment }
                    };
                    success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
                }
            }
            else
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId },
                    new ParameterInfo() { ParameterName = "PlantType", ParameterValue = projectSales.ProjectInfo.PlantType },
                    new ParameterInfo() { ParameterName = "PaymentTermId", ParameterValue = projectSales.ProjectInfo.PaymentTermId },
                    new ParameterInfo() { ParameterName = "DeliveryDate", ParameterValue = Convert.ToDateTime(projectSales.ProjectInfo.DeliveryDate).ToString("yyyy/MM/dd") },
                    new ParameterInfo() { ParameterName = "PriceEstimation", ParameterValue = projectSales.ProjectInfo.PriceEstimation.ToString().Replace(",", "") },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = projectSales.ProjectInfo.Description },
                    new ParameterInfo() { ParameterName = "Round", ParameterValue = 1 }
                };
                success = SqlHelper.ExecuteQuery("prj.ProjectDetail_New", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId },
                    new ParameterInfo() { ParameterName = "CountryId", ParameterValue = projectSales.CustomerInfo.CountryId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = projectSales.CustomerInfo.Name },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = projectSales.CustomerInfo.Email },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = projectSales.CustomerInfo.ContactNo },
                    new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = projectSales.CustomerInfo.FaxNo },
                    new ParameterInfo() { ParameterName = "Address", ParameterValue = projectSales.CustomerInfo.Address },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSales.Requester.UserId }
                };
                success = SqlHelper.ExecuteQuery("cust.Customer_New", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId }
                };
                var customer = SqlHelper.GetRecord<Customer>("cust.Customer_Get", parameters); ;

                foreach (var p in projectSales.CustomerInfo.ContactPersons)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = customer.CustomerId },
                        new ParameterInfo() { ParameterName = "Name", ParameterValue = p.Name },
                        new ParameterInfo() { ParameterName = "Email", ParameterValue = p.Email },
                        new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = p.ContactNo },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSales.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("cust.CustomerContactPerson_New", parameters);
                }

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = customer.CustomerId }
                };
                projectSales.CustomerInfo.ContactPersons = SqlHelper.GetRecords<ContactPerson>("cust.CustomerContactPerson_Get", parameters);
            }
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSales.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = projectSales.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            return projectSales;
        }

        public static int SubmitProjectSale(ProjectSales projectSales, ProjectSalesAction projectSalesAction)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = "412BA7CC-2BEA-49EC-80E4-72B676F5CD07" },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSalesAction.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            var status = "";
            if (workflow.SequenceNo == 0)
            {
                status = workflow.NextWorkflowProcess;
            }
            else
            {
                status = "Pending " + workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);
            if (workflow.workflowApprovals != null && workflow.workflowApprovals.Count > 0)
            {
                foreach (var a in workflow.workflowApprovals)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSalesAction.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }


            if (projectSales.Attachments != null)
            {
                foreach (var a in projectSales.Attachments)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = a.AttachmentTypeId },
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSales.RequestId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSales.Requester.UserId },
                        new ParameterInfo() { ParameterName = "Description", ParameterValue = a.Description },
                        new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = a.AttachmentId }
                    };
                    success = SqlHelper.ExecuteQuery("req.Attachment_Update", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectSales.ProjectInfo.ProjectId },
                new ParameterInfo() { ParameterName = "PlantType", ParameterValue = projectSales.ProjectInfo.PlantType },
                new ParameterInfo() { ParameterName = "PaymentTermId", ParameterValue = projectSales.ProjectInfo.PaymentTermId },
                new ParameterInfo() { ParameterName = "DeliveryDate", ParameterValue = Convert.ToDateTime(projectSales.ProjectInfo.DeliveryDate).ToString("yyyy/MM/dd") },
                new ParameterInfo() { ParameterName = "PriceEstimation", ParameterValue = projectSales.ProjectInfo.PriceEstimation.ToString().Replace(",", "") },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = projectSales.ProjectInfo.Description }
            };
            success = SqlHelper.ExecuteQuery("prj.ProjectDetail_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = projectSales.CustomerInfo.CustomerId },
                new ParameterInfo() { ParameterName = "CountryId", ParameterValue = projectSales.CustomerInfo.CountryId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = projectSales.CustomerInfo.Name },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = projectSales.CustomerInfo.Email },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = projectSales.CustomerInfo.ContactNo },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = projectSales.CustomerInfo.FaxNo },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = projectSales.CustomerInfo.Address },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = projectSales.Requester.UserId }
            };
            var customerId = SqlHelper.GetRecord<string>("cust.Customer_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSalesAction.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = projectSalesAction.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSalesAction.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "Project Sales Submitted: [" + projectSalesAction.ReferenceNo + "]" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (!string.IsNullOrWhiteSpace(requestHistoryId))
            {
                parameters = new List<ParameterInfo>
                {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSales.Requester.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = projectSalesAction.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            return success;
        }

        public static int ActionTakenProjectSale(ProjectSalesAction projectSalesAction)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = "412BA7CC-2BEA-49EC-80E4-72B676F5CD07" },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSalesAction.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            var status = "";
            if (workflow.SequenceNo == 0)
            {
                status = workflow.NextWorkflowProcess;
            }
            else
            {
                status = "Pending " + workflow.NextWorkflowProcess;
            }
            
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSalesAction.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            var success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = projectSalesAction.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = projectSalesAction.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.WorkflowProcessId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSalesAction.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = projectSalesAction.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }
            
            if (!string.IsNullOrEmpty(projectSalesAction.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = projectSalesAction.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = projectSalesAction.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = projectSalesAction.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "Action taken on " + projectSalesAction.ReferenceNo + " project sales proposal" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = projectSalesAction.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            

            return success;
        }
    }
}
