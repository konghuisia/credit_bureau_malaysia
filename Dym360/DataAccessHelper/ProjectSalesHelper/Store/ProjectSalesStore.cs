﻿using DataAccessHelper.ProjectSalesHelper.DAL;
using ObjectClasses.Configuration;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ProjectSalesHelper.Store
{
    public class ProjectSalesStore
    {
        public Task<ProjectSales> CreateAsync(ProjectSales projectSales)
        {
            if (projectSales != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectSalesControl.NewProjectSale(projectSales);
                });
            }
            throw new ArgumentNullException("project");
        }

        public Task DeleteAsync(Project project)
        {
            if (project != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    ProjectSalesControl.DeleteProjectSale(project);
                });
            }
            throw new ArgumentNullException("project");
        }

        public void Dispose()
        {

        }
        public Task<IList<ProjectSales>> GetAllProjectSale(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProjectSales(userId);
            });
        }

        public Task<IList<ProjectSales>> GetAllProjectSale()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProjectSales();
            });
        }

        public Task<ProjectSales> GetProjectSale(string projectId = null)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProjectSale(projectId);
            });
        }

        public Task<int> GetProposalSeqNo()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.GetProposalNoSeq();
            });
        }

        public Task<ProjectSales> Save(ProjectSales projectSales, ProjectSalesAction projectSalesAction, bool isUpdate = false, bool isGetProposalNo = false)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.Save(projectSales, projectSalesAction, isUpdate, isGetProposalNo);
            });
        }

        public Task SubmitProjectSale(ProjectSales projectSales, ProjectSalesAction projectSalesAction)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.SubmitProjectSale(projectSales,projectSalesAction);
            });
        }

        public Task ActionTakenProjectSale(ProjectSalesAction projectSalesAction)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSalesControl.ActionTakenProjectSale(projectSalesAction);
            });
        }
    }
}
