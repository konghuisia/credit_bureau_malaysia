﻿using DatabaseConnection.Data;
using FileManager;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DataAccessHelper.RequestHelperHelper.DAL
{
    public class AttachmentControl
    {
        public static Request NewAttachment(Request request)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            foreach (var a in request.Attachments)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = a.AttachmentTypeId },
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.Requester.UserId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = a.Name },
                    new ParameterInfo() { ParameterName = "ModifiedDate", ParameterValue = a.ModifiedDate },
                    new ParameterInfo() { ParameterName = "Path", ParameterValue = a.Path },
                    new ParameterInfo() { ParameterName = "Size", ParameterValue = a.Size },
                    new ParameterInfo() { ParameterName = "ClassificationId", ParameterValue = a.ClassificationId }
                };
                var attachmentId = SqlHelper.GetRecord<string>("req.Attachment_New", parameters);

                new Thread(() =>
                {
                    var doc = FileUtils.SaveAttachmentAsDocument(a.AbsolutePath + a.Name);

                    var path = a.Path.Replace("Attachments", "Documents");
                    var ext = "";
                    if (doc.LastIndexOf('.') > -1)
                    {
                        ext = doc.Substring(doc.LastIndexOf('.')).ToLower();
                    }
                    var filename = doc.Substring(doc.LastIndexOf("/") + 1);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = attachmentId },
                        new ParameterInfo() { ParameterName = "Name", ParameterValue = filename },
                        new ParameterInfo() { ParameterName = "Path", ParameterValue = path },
                        new ParameterInfo() { ParameterName = "Extension", ParameterValue = ext },
                        new ParameterInfo() { ParameterName = "Size", ParameterValue = a.Size },
                        new ParameterInfo() { ParameterName = "ClassificationId", ParameterValue = a.ClassificationId }
                    };
                    var success = SqlHelper.ExecuteQuery("dbo.Document_New", parameters);
                }).Start();
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
            };
            request.Attachments = SqlHelper.GetRecords<Attachment>("req.Attachments_Get", parameters);

            return request;
        }

        public static bool DeletePersonInCharge(string personInChargeId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = personInChargeId }
            };
            return SqlHelper.GetRecord<bool>("sub.Delete_PersonInCharge", parameters);
        }

        public static Request NewPersonInChargeAttachment(Request request)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            foreach (var a in request.Attachments)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = a.AttachmentTypeId },
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.Requester.UserId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = a.Name },
                    new ParameterInfo() { ParameterName = "ModifiedDate", ParameterValue = a.ModifiedDate },
                    new ParameterInfo() { ParameterName = "Path", ParameterValue = a.Path },
                    new ParameterInfo() { ParameterName = "Size", ParameterValue = a.Size },
                    new ParameterInfo() { ParameterName = "ClassificationId", ParameterValue = a.ClassificationId }
                };
                var attachmentId = SqlHelper.GetRecord<string>("req.Attachment_New", parameters);

                if (!string.IsNullOrEmpty(attachmentId))
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId },
                        new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = attachmentId },
                        new ParameterInfo() { ParameterName = "AttachmentTypeId", ParameterValue = a.AttachmentTypeId },
                        new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = request.PersonInChargeId },
                    };
                    SqlHelper.ExecuteQuery("sub.Create_PersonInChargeAttachment", parameters);
                }
            }

            return request;
        }

        public static List<Attachment> GetAttachmentByRequestId(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            return SqlHelper.GetRecords<Attachment>("req.Attachments_Get", parameters);
        }

        public static IList<AttachmentType> GetAttachmentTypes()
        {
            List<AttachmentType> attachmentTypes = SqlHelper.GetRecords<AttachmentType>("cfg.AttachmentType_Get", new List<ParameterInfo>());
            return attachmentTypes;
        }

        public static IList<Classification> GetClassifications()
        {
            List<Classification> classifications = SqlHelper.GetRecords<Classification>("cfg.Classifications_Get", new List<ParameterInfo>());
            return classifications;
        }

        public static bool DeleteAttachment(string attachmentId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = attachmentId }
            };
            int success = SqlHelper.ExecuteQuery("req.Attachment_Delete", parameters);
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public static IList<Project> GetProjects()
        //{
        //    List<Project> oProjects = SqlHelper.GetRecords<Project>("cfg.Projects_Get", new List<ParameterInfo>());
        //    return oProjects;
        //}
    }
}
