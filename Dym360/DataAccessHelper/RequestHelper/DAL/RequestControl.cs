﻿using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;

namespace DataAccessHelper.RequestHelperHelper.DAL
{
    public class RequestControl
    {
        public static string NewPreRequest(Request request)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.Requester.UserId },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = request.ModuleId },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = request.RequestTypeId },
                new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = request.ReferenceNo },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = request.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = request.Description },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = request.Status }
            };
            var req = SqlHelper.GetRecord<string>("req.Request_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = request.ModuleId }
            };
            var wapps = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApprovals_Get", parameters);
            var success = 0;
            if (wapps != null)
            {
                foreach (var w in wapps)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = w.RoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = w.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = /*string.Equals(w.RoleId, "C7A9659F-5F1E-4584-8821-1CA39074FB6E") ? "PENDING" : */"N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = request.Requester.UserId },
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            
            return req;
        }

        public static bool NewRequest(Request req)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            var success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = workflow.WorkflowId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApprovals_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int UpdateRequest(Request request)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = request.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = request.Description },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.Requester.UserId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = request.Status }
            };
            int success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            return success;
        }


        public static IList<RequestType> GetRequestTypes()
        {
            List<RequestType> requestTypes = SqlHelper.GetRecords<RequestType>("cfg.RequestTypes_Get", new List<ParameterInfo>());
            return requestTypes;
        }

        public static IList<MyTask> GetRequests(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            List<MyTask> requests = SqlHelper.GetRecords<MyTask>("req.Requests_Get", parameters);
            return requests;
        }

        public static IList<MyTask> GetRequestsByModule(string moduleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = moduleId }
            };
            List<MyTask> requests = SqlHelper.GetRecords<MyTask>("req.Requests_Get_ByModule", parameters);
            return requests;
        }

        public static Request GetRequest(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            var request = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            request.RequestState = SqlHelper.GetRecord<RequestState>("req.RequestState_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.UserId }
            };
            request.Requester = SqlHelper.GetRecord<Requester>("acc.User_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            request.Attachments = SqlHelper.GetRecords<Attachment>("req.Attachments_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            request.CommentRemarks = SqlHelper.GetRecords<CommentRemark>("req.Comments_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
            };
            request.Remarks = SqlHelper.GetRecords<Remark>("req.Remarks_Get", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            request.Histories = SqlHelper.GetRecords<History>("req.RequestHistory_Get", parameters);

            return request;
        }

        public static Request GetRequestByReferenceNo(string referenceNo)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = referenceNo }
            };
            var request = SqlHelper.GetRecord<Request>("req.Request_Get_ByReferenceNo", parameters);

            if (request != null)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
                };
                request.RequestState = SqlHelper.GetRecord<RequestState>("req.RequestState_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.UserId }
                };
                request.Requester = SqlHelper.GetRecord<Requester>("acc.User_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
                };
                request.Attachments = SqlHelper.GetRecords<Attachment>("req.Attachments_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
                };
                request.CommentRemarks = SqlHelper.GetRecords<CommentRemark>("req.Comments_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
                };
                request.Remarks = SqlHelper.GetRecords<Remark>("req.Remarks_Get", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = request.RequestId }
                };
                request.Histories = SqlHelper.GetRecords<History>("req.RequestHistory_Get", parameters);
            }
            return request;
        }

        public static string NewCreditManagementPreRequest(Request request)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = request.Requester.UserId },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = request.ModuleId },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = request.RequestTypeId },
                new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = request.ReferenceNo },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = request.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = request.Description },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = request.Status }
            };
            var req = SqlHelper.GetRecord<string>("req.Request_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = request.ModuleId }
            };
            var wapps = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApprovals_Get", parameters);
            var success = 0;
            if (wapps != null)
            {
                foreach (var w in wapps)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = w.RoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = w.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = request.Requester.UserId },
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            return req;
        }

        #region CBM Credit Management
        public static IList<MyTask> GetRenewalRequests(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            return SqlHelper.GetRecords<MyTask>("req.Get_RenewalRequests", parameters);
        }
        public static List<MyTask> GetRenewalRequestsList(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            return SqlHelper.GetRecords<MyTask>("req.Get_RenewalRequests", parameters);
        }
        #endregion

        #region Claim Managemen
        public static List<MyTask> GetClaimRequestsList(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            return SqlHelper.GetRecords<MyTask>("req.Get_ClaimRequests", parameters);
        }
        #endregion

        #region Subscription Management
        internal static List<MyTask> GetSubscriptionRequestsList(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            return SqlHelper.GetRecords<MyTask>("req.Get_SubscriptionRequests", parameters);
        }
        #endregion
    }
}
