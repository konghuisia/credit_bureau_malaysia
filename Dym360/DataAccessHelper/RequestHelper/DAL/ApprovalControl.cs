﻿using DatabaseConnection.Data;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.RequestHelper.DAL
{
    public class ApprovalControl
    {
        public static IList<Approval> GetApprovals(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            IList<Approval> apps = SqlHelper.GetRecords<Approval>("req.RequestApprovals_Get", parameters);
            return apps;
        }

        public static bool RemoveReviewer(Approval reviewer, string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = reviewer.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = reviewer.UserId }
            };
            var success = SqlHelper.ExecuteQuery("req.RequestApproval_Delete", parameters);
            
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
