﻿using DataAccessHelper.RequestHelperHelper.DAL;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.RequestHelper.Store
{
    public class RequestStore
    {
        public Task<string> NewPreRequest(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return RequestControl.NewPreRequest(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> NewRequest(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return RequestControl.NewRequest(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task UpdateRequest(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    RequestControl.UpdateRequest(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        public void Dispose()
        {

        }

        public Task<IList<RequestType>> GetAllRequestType()
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequestTypes();
            });
        }

        public Task<IList<MyTask>> GetAllRequest(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequests(userId);
            });
        }

        public Task<IList<MyTask>> GetAllRequestByModule(string module)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequestsByModule(module);
            });
        }

        public Task<Request> GetRequest(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequest(requestId);
            });
        }

        public Task<List<MyTask>> GetRenewalRequestsList()
        {
            throw new NotImplementedException();
        }

        public Task<Request> GetRequestByReferenceNo(string referenceNo)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRequestByReferenceNo(referenceNo);
            });
        }

        public Task<string> NewCreditManagementPreRequest(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return RequestControl.NewCreditManagementPreRequest(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        #region CBM Credit Management
        public Task<IList<MyTask>> GetRenewalRequests(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRenewalRequests(userId);
            });
        }

        public Task<List<MyTask>> GetRenewalRequestsList(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetRenewalRequestsList(userId).ToList() ?? new List<MyTask>();
            });
        }
        #endregion

        #region CBM Claim Management
        public Task<List<MyTask>> GetClaimRequestsList(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetClaimRequestsList(userId).ToList() ?? new List<MyTask>();
            });
        }
        #endregion

        #region CBM Subscription Management
        public Task<List<MyTask>> GetSubscriptionRequestsList(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestControl.GetSubscriptionRequestsList(userId).ToList() ?? new List<MyTask>();
            });
        }
        #endregion
    }
}
