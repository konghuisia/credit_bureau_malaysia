﻿using DataAccessHelper.RequestHelper.DAL;
using DatabaseConnection.Data;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.RequestHelper.Store
{
    public class ApprovalStore
    {
        public Task<IList<Approval>> GetAllApproval(string requestid)
        {
            return Task.Factory.StartNew(() =>
            {
                return ApprovalControl.GetApprovals(requestid);
            });
        }

        public Task<bool> RemoveReviewer(Approval reviewer,string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ApprovalControl.RemoveReviewer(reviewer, requestId);
            });
        }
    }
}
