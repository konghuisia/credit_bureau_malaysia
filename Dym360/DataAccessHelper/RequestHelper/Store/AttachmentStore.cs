﻿using DataAccessHelper.RequestHelperHelper.DAL;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.RequestHelper.Store
{
    public class AttachmentStore
    {
        public Task<Request> CreateAsync(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return AttachmentControl.NewAttachment(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<Request> NewPersonInChargeAttachment(Request request)
        {
            if (request != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return AttachmentControl.NewPersonInChargeAttachment(request);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<IList<AttachmentType>> GetAttachmentTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return AttachmentControl.GetAttachmentTypes();
            });
        }

        public Task<IList<Classification>> GetClassifications()
        {
            return Task.Factory.StartNew(() =>
            {
                return AttachmentControl.GetClassifications();
            });
        }

        public void Dispose()
        {

        }

        public Task<bool> DeleteAttachment(string attachmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AttachmentControl.DeleteAttachment(attachmentId);
            });
            throw new ArgumentNullException("attachmentId");
        }

        public Task<List<Attachment>> GetAttachmentByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AttachmentControl.GetAttachmentByRequestId(requestId)?.ToList() ?? new List<Attachment>();
            });
        }

        public Task<bool> DeletePersonInCharge(string personInChargeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AttachmentControl.DeletePersonInCharge(personInChargeId);
            });
            throw new ArgumentNullException("personInChargeId");
        }
    }
}
