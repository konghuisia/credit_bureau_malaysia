﻿using DataAccessHelper.CreditManagement.DAL;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.Store
{
    public class AccountStore
    {
        public Task<List<Account>> GetAccountByActive(bool Active)
        {
            return Task.Factory.StartNew(() =>
            {
                return AccountControl.GetAccountByActive(Active).ToList() ?? new List<Account>();
            });
        }

        public Task<Account> GetAccountByAccountId(string accountId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AccountControl.GetAccountByAccountId(accountId);
            });
        }

        public Task<Account> GetAccountByRequestId(string requestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return AccountControl.GetAccountByRequestId(requestId);
            });
        }

        public Task<List<Account>> GetAccounts()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccountControl.GetAccounts().ToList() ?? new List<Account>();
            });
        }
    }
}
