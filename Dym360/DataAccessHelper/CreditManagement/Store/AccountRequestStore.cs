﻿using DataAccessHelper.CreditManagement.DAL;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.Store
{
    public class AccountRequestStore
    {
        public Task<List<AccountRequest>> GetAccountRequest()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccountRequestControl.GetAccountRequest().ToList() ?? new List<AccountRequest>();
            });
        }
    }
}
