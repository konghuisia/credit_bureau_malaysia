﻿using DataAccessHelper.CreditManagement.DAL;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.Store
{
    public class CreditManagementRequestStore
    {
        public Task<bool> SubmitRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CreditManagementRequestControl.NewRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> SaveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CreditManagementRequestControl.SaveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RejectRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CreditManagementRequestControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> ApproveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CreditManagementRequestControl.ApproveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMoreRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CreditManagementRequestControl.RequestMoreRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public void Dispose()
        {

        }
    }
}
