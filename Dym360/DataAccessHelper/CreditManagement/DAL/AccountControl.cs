﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.DAL
{
    public class AccountControl
    {
        public static List<Account> GetAccountByActive(bool Active)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Active", ParameterValue = Active }
            };
            return SqlHelper.GetRecords<Account>("sub.Get_AccountByActive", parameters);
        }

        public static Account GetAccountByAccountId(string accountId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AccountId", ParameterValue = accountId }
            };
            return SqlHelper.GetRecord<Account>("sub.Get_AccountByAccountId", parameters);
        }

        public static Account GetAccountByRequestId(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId }
            };
            return SqlHelper.GetRecord<Account>("sub.Get_AccountByRequestId", parameters);
        }

        public static List<Account> GetAccounts()
        {
            return SqlHelper.GetRecords<Account>("sub.Get_Accounts", new List<ParameterInfo>());
        }
    }
}
