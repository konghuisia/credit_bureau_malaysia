﻿using DatabaseConnection.Data;
using ObjectClasses.CreditManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.DAL
{
    public class AccountRequestControl
    {
        public static List<AccountRequest> GetAccountRequest()
        {
            return SqlHelper.GetRecords<AccountRequest>("sub.Get_AccountRequest", new List<ParameterInfo>());
        }
    }
}
