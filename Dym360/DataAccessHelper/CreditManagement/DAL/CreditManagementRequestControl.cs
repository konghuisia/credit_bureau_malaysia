﻿using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CreditManagement.DAL
{
    public class CreditManagementRequestControl
    {
        public static bool NewRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            if (req.Approvers != null)
            {
                foreach (var a in req.Approvers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "61C2FDF2-447A-4D03-92C3-55A76BF0FB94" }
                    };
                    var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

                    if ((EmailTemplate != null) && EmailTemplate.Active)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId }
                        };
                        var User = SqlHelper.GetRecord<User>("acc.Get_UserByUserId", parameters);

                        if ((User != null) && !string.IsNullOrEmpty(User.Email))
                        {
                            var Sender = new EmailSender.EmailSender();

                            Sender.SendClaimManagementEmail(User.Email, req.Link, EmailTemplate.Subject, EmailTemplate.Body, req.ReferenceNo, req.Subject, User.Name);
                        }

                    }

                }
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId) && workflow.NextWorkflowProcessId != "546E5CA1-10AC-447F-A61A-265B21E47918")
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "AccountId", ParameterValue = req.AccountId },
                    new ParameterInfo() { ParameterName = "Active", ParameterValue = true },
                    new ParameterInfo() { ParameterName = "Credit", ParameterValue = req.Credit },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("sub.Update_Account", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "AccountId", ParameterValue = req.AccountId },
                new ParameterInfo() { ParameterName = "Credit", ParameterValue = req.Credit },
                new ParameterInfo() { ParameterName = "OldBalance", ParameterValue = req.OldBalance },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var AccountRequestId = SqlHelper.ExecuteQuery("sub.Create_AccountRequest", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool SaveRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);
            
            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40" },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "AccountId", ParameterValue = req.AccountId },
                new ParameterInfo() { ParameterName = "Credit", ParameterValue = req.Credit },
                new ParameterInfo() { ParameterName = "OldBalance", ParameterValue = req.OldBalance },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var AccountRequestId = SqlHelper.ExecuteQuery("sub.Create_AccountRequest", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool RejectRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Rejected" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ApproveRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Approved" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool RequestMoreRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Requested more information" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "61C2FDF2-447A-4D03-92C3-55A76BF0FB94" }
            };
            var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId }
            };
            var Request = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);

            if ((EmailTemplate != null) && EmailTemplate.Active)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = Request.UserId }
                };
                var User = SqlHelper.GetRecord<User>("acc.Get_UserByUserId", parameters);

                if ((User != null) && !string.IsNullOrEmpty(User.Email))
                {
                    var Sender = new EmailSender.EmailSender();

                    Sender.SendClaimManagementEmail(User.Email, req.Link, EmailTemplate.Subject, EmailTemplate.Body, req.ReferenceNo, req.Subject, User.Name);
                }
            }

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
