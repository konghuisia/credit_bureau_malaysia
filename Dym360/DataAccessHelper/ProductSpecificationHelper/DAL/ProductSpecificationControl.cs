﻿using DatabaseConnection.Data;
using ObjectClasses.ProjectSalesManagement.Entities;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses;
using ObjectClasses.DocumentPortal;
using ObjectClasses.ProductSpecificationRequest;

namespace DataAccessHelper.ProductSpecificationHelper.DAL
{
    public static class ProductSpecificationControl
    {
        public static bool Submit(SpecificationRequest sr)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = sr.Request.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId }
            };
            var workflows = SqlHelper.GetRecords<Workflow>("cfg.Workflow_Get", parameters);

            var workflow = new Workflow();

            if (sr.Request.Reviewers != null)
            {
                workflow = workflows.Find(x => x.NextWorkflowProcess == "Reviewer");
                foreach (var r in sr.Request.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            else
            {
                if (workflows.Count > 1)
                {
                    workflow = workflows.Find(x => x.NextWorkflowProcess == "PEx Team");
                }
                else
                {
                    workflow = workflows[0];
                }
            }
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = sr.Request.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);


            if (string.IsNullOrEmpty(sr.ProductSpecificationId))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = sr.CustomerId },
                    new ParameterInfo() { ParameterName = "Product", ParameterValue = sr.Product },
                    new ParameterInfo() { ParameterName = "Reference", ParameterValue = sr.Reference }
                };
                success = SqlHelper.ExecuteQuery("psr.ProductSpecification_New", parameters);
            }
            else
            {
                if (workflow.NextWorkflowProcess == "Final Approver")
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "Proposal", ParameterValue = sr.Proposal },
                        new ParameterInfo() { ParameterName = "SpecDate", ParameterValue = sr.SpecDate },
                        new ParameterInfo() { ParameterName = "Recommendation", ParameterValue = sr.Recommendation }
                    };
                    success = SqlHelper.ExecuteQuery("psr.ProductSpecification_Update_QC", parameters);
                }
                else
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = sr.CustomerId },
                        new ParameterInfo() { ParameterName = "Product", ParameterValue = sr.Product },
                        new ParameterInfo() { ParameterName = "Reference", ParameterValue = sr.Reference }
                    };
                    success = SqlHelper.ExecuteQuery("psr.ProductSpecification_Update_Req", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = sr.Request.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "PENDING" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                sr.Request.Status = "Pending " + (workflow.NextWorkflowProcess == "PEx Team" ? "QC" : workflow.NextWorkflowProcess);
            }
            else
            {
                sr.Request.Status = workflow.NextWorkflowProcess;
            }

            var desc = "Request submitted";
            if (workflow.NextWorkflowProcess == "Final Approver")
            {
                desc = "Proposal submitted";
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = desc }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = sr.Request.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool SaveRequest(SpecificationRequest sr)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            if (sr.Request.Reviewers != null)
            {
                foreach (var r in sr.Request.Reviewers)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "F5EBCECF-BEC3-418D-8FF5-2E27A034EF40" },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            if (string.IsNullOrEmpty(sr.ProductSpecificationId))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = sr.CustomerId },
                    new ParameterInfo() { ParameterName = "Product", ParameterValue = sr.Product },
                    new ParameterInfo() { ParameterName = "Reference", ParameterValue = sr.Reference }
                };
                sr.ProductSpecificationId = SqlHelper.GetRecord<string>("psr.ProductSpecification_New", parameters);
            }
            else
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = sr.CustomerId },
                    new ParameterInfo() { ParameterName = "Product", ParameterValue = sr.Product },
                    new ParameterInfo() { ParameterName = "Reference", ParameterValue = sr.Reference }
                };
                success = SqlHelper.ExecuteQuery("psr.ProductSpecification_Update_Req", parameters);
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = sr.Request.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool RejectRequest(SpecificationRequest sr)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = sr.Request.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                sr.Request.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                sr.Request.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = sr.Request.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "Request rejected" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = sr.Request.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = sr.Request.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ApproveRequest(SpecificationRequest sr)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = sr.Request.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                sr.Request.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                sr.Request.Status = workflow.NextWorkflowProcess;
            }

            var condition = "PROCEED";
            var SpecCode = "";
            if (workflow.NextWorkflowProcess == "Close Completed")
            {
                condition = "";
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = condition }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = sr.Request.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                if (workflow.NextWorkflowProcess == "Close Completed")
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "ModuleId", ParameterValue = sr.Request.ModuleId }
                    };
                    var reference = SqlHelper.GetRecord<Reference>("cfg.Reference_Get", parameters);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "ReferenceId", ParameterValue = reference.ReferenceId }
                    };
                    var seqNo = SqlHelper.GetIntRecord<int>("cfg.ReferenceSequence_Get", parameters);
                    seqNo++;
                    SpecCode = reference.Prefix + string.Format("{0:00000}", seqNo);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "ReferenceId", ParameterValue = reference.ReferenceId },
                        new ParameterInfo(){ ParameterName = "SequenceNo", ParameterValue = seqNo }

                    };
                    success = SqlHelper.ExecuteQuery("cfg.ReferenceSequence_New", parameters);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo(){ ParameterName = "Code", ParameterValue = SpecCode }

                    };
                    success = SqlHelper.ExecuteQuery("psr.ProductSpecification_Update_Code", parameters);
                }
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    var state = "";
                    if (workflow.NextWorkflowProcess == "Final Approver")
                    {
                        state = "PENDING";
                    }
                    foreach (var a in workflow.workflowApprovals)
                    {

                        parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = state },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.UserId }
                    };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                    }
                }
            }

            if (!string.IsNullOrEmpty(sr.Request.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = sr.Request.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "Request approved" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = sr.Request.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool RequestMoreRequest(SpecificationRequest sr)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = sr.Request.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                sr.Request.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                sr.Request.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = sr.Request.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = sr.Request.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.WorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = sr.Request.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = sr.Request.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = sr.Request.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = sr.Request.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "Request sent back for more information" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = sr.Request.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = sr.Request.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IList<CompanyDocument> GetCompanyDocuments()
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            var companyDocuments = SqlHelper.GetRecords<CompanyDocument>("dp.CompanyDocuments_Get", new List<ParameterInfo>());

            foreach (var cd in companyDocuments)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CompanyDocumentId", ParameterValue = cd.CompanyDocumentId }
                };
                var doc = SqlHelper.GetRecord<Document>("dp.Document_Get", parameters);
                cd.Document = doc;
            }
            return companyDocuments.FindAll(x => x.Document != null);
        }

        public static SpecificationRequest GetSpecificationRequest(string reqId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = reqId }
            };

            return SqlHelper.GetRecord<SpecificationRequest>("psr.ProductSpecification_Get", parameters);
        }

        public static IList<SpecificationRequest> GetSpecificationRequests()
        {
            return SqlHelper.GetProductSpacifications("psr.ProductSpecifications_Get", new List<ParameterInfo>());
        }
    }
}
