﻿using DataAccessHelper.DocumentPortalHelper.DAL;
using DataAccessHelper.ProductSpecificationHelper.DAL;
using DataAccessHelper.ProjectSalesHelper.DAL;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.DocumentPortal;
using ObjectClasses.ProductSpecificationRequest;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ProductSpecificationHelper.Store
{
    public class ProductSpecificationStore
    {
        public Task<bool> SubmitRequest(SpecificationRequest sr)
        {
            if (sr != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.Submit(sr);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> SaveRequest(SpecificationRequest sr)
        {
            if (sr != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.SaveRequest(sr);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RejectRequest(SpecificationRequest sr)
        {
            if (sr != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.RejectRequest(sr);
                });
            }
            throw new ArgumentNullException("sr");
        }

        public Task<bool> ApproveRequest(SpecificationRequest sr)
        {
            if (sr != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.ApproveRequest(sr);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMoreRequest(SpecificationRequest sr)
        {
            if (sr != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.RequestMoreRequest(sr);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task DeleteAsync(Project project)
        {
            if (project != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    ProjectSalesControl.DeleteProjectSale(project);
                });
            }
            throw new ArgumentNullException("project");
        }

        public void Dispose()
        {

        }

        public Task<SpecificationRequest> GetSpecificationRequest(string reqId)
        {
            if (!string.IsNullOrEmpty(reqId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProductSpecificationControl.GetSpecificationRequest(reqId);
                });
            }
            throw new ArgumentNullException("reqId");
        }

        public Task<IList<SpecificationRequest>> GetSpecificationRequests()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProductSpecificationControl.GetSpecificationRequests();
            });
        }

    }
}
