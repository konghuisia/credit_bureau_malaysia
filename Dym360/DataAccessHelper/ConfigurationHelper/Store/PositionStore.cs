﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.Configuration.Store
{
    public class PositionStore
    {
        public Task CreateAsync(Position position)
        {
            if (position != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    PositionControl.NewPosition(position);
                });
            }
            throw new ArgumentNullException("position");
        }

        public Task DeleteAsync(Position position)
        {
            if (position != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    PositionControl.DeletePosition(position);
                });
            }
            throw new ArgumentNullException("position");
        }

        public void Dispose()
        {

        }

        public Task<IList<Position>> GetAllPosition()
        {
            return Task.Factory.StartNew(() =>
            {
                return PositionControl.GetPositions();
            });
        }
    }
}
