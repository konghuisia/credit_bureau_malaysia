﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class UserActionStore
    {
        public Task<bool> CreateUserAction(UserAction ua)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserActionControl.CreateUserAction(ua);
            });
        }
    }
}
