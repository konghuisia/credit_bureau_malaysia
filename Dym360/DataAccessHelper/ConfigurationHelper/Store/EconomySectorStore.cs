﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class EconomySectorStore
    {
        public Task<List<EconomySector>> GetEconomySector()
        {
            return Task.Factory.StartNew(() =>
            {
                return EconomySectorControl.GetEconomySector()?.ToList() ?? new List<EconomySector>();
            });
        }
    }
}
