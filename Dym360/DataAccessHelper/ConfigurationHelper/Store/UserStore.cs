﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class UserStore
    {
        public Task<IList<User>> GetAllUser(string position = "", string grad = "")
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetUsers(position, grad);
            });
        }
        public Task<User> GetUser(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetUser(userId);
            });
        }
        public Task<IList<HOD>> GetAllHOD()
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetHODs();
            });
        }

        public Task UpdateLoginDate(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdateLoginDate(userId);
            });
        }

        public void UpdateLoginAttempt(string userId)
        {
            Task.Factory.StartNew(() =>
            {
                UserControl.UpdateLoginAttempt(userId);
            });
        }

        public Task<bool> UpdatePassword(User user)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdatePassword(user);
            });
        }

        public Task<bool> ChangePassword(User user, string pass)
        {
            try
            {
                return Task.Factory.StartNew(() =>
                {
                    return UserControl.ChangePassword(user, pass);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Task<bool> ResetPassword(string userId, string accessId, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.ResetPassword(userId, accessId, createdBy);
            });
        }

        public Task<bool> DeleteUser(string userId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.DeleteUser(userId, id);
            });
        }

        public Task<bool> CheckUserName(string username)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.CheckUserName(username);
            });
        }

        public Task<string> CreateNewUser(User user, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.CreateNewUser(user, id);
            });
        }

        public Task<string> UpdateUser(User user, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdateUser(user, id);
            });
        }
    }
}
