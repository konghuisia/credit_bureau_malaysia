﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class TitleStore
    {
        public Task<List<Title>> GetTitle()
        {
            return Task.Factory.StartNew(() =>
            {
                return TitleControl.GetTitle()?.ToList() ?? new List<Title>();
            });
        }
    }
}
