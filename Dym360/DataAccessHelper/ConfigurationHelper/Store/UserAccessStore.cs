﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class UserAccessStore
    {
        public Task<List<UserAccess>> GetUserAccessByUserId(string UserId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserAccessControl.GetUserAccessByUserId(UserId).ToList() ?? new List<UserAccess>();
            });
        }
    }
}
