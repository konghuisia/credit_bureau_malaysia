﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.ClaimManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class ClaimTypesStore
    {
        public Task<List<ClaimTypes>> GetAllClaimTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return ClaimTypesControl.GetAllClaimTypes()?.ToList() ?? new List<ClaimTypes>();
            });
        }
    }
}
