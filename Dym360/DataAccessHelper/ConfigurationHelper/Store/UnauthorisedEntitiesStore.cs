﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class UnauthorisedEntitiesStore
    {
        public Task<List<UnauthorisedEntities>> GetUnauthorisedEntities()
        {
            return Task.Factory.StartNew(() =>
            {
                return UnauthorisedEntitiesControl.GetUnauthorisedEntities()?.ToList() ?? new List<UnauthorisedEntities>();
            });
        }
        public Task<string> CreateUnauthorisedEntities(UnauthorisedEntities ue, string CreatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UnauthorisedEntitiesControl.CreateUnauthorisedEntities(ue, CreatedBy);
            });
        }
        public Task<string> UpdateUnauthorisedEntities(UnauthorisedEntities ue, string UpdatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UnauthorisedEntitiesControl.UpdateUnauthorisedEntities(ue, UpdatedBy);
            });
        }
        public Task<bool> DeleteUnauthorisedEntities(string UnauthorisedEntitiesId, string DeletedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UnauthorisedEntitiesControl.DeleteUnauthorisedEntities(UnauthorisedEntitiesId, DeletedBy);
            });
        }
    }
}
