﻿
using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class DepartmentAccessStore
    {
        public Task<List<DepartmentAccess>> GetDepartmentAccessByDepartmentId(string DepartmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentAccessControl.GetDepartmentAccessByDepartmentId(DepartmentId).ToList() ?? new List<DepartmentAccess>();
            });
        }

        public Task<bool> UpdateDepartmentAccess(List<DepartmentAccess> access, string id, string DepartmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentAccessControl.UpdateDepartmentAccess(access, id, DepartmentId);
            });
        }

        public Task<bool> CreateDepartmentAccess(List<DepartmentAccess> access, string id, string DepartmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentAccessControl.CreateDepartmentAccess(access, id, DepartmentId);
            });
        }
    }
}
