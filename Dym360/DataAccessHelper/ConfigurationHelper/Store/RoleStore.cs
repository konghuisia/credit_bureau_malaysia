﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class RoleStore
    {
        #region Role
        public Task<IList<Role>> GetRoles(string roleTypeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoles(roleTypeId);
            });
        }
        public Task<Role> GetRole(string RoleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRole(RoleId);
            });
        }
        #endregion

        #region  RoleType
        public Task CreateAsync(RoleType roleType)
        {
            if (roleType != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    RoleControl.NewRoleType(roleType);
                });
            }
            throw new ArgumentNullException("roleType");
        }

        public Task DeleteAsync(RoleType roleType)
        {
            if (roleType != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    RoleControl.DeleteRoleType(roleType);
                });
            }
            throw new ArgumentNullException("roleType");
        }

        public void Dispose()
        {

        }

        public Task<IList<RoleType>> GetAllRoleType()
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoleTypes();
            });
        }
        #endregion

        #region RoleAction
        public Task<IList<RoleAction>> GetRoleActions(string userId = null, string roleId = null)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoleActions(userId, roleId);
            });
        }
        #endregion

        #region CBM Credit Management
        public Task<Role> GetRoleCreditManagementPOC(string RoleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoleCreditManagementPOC(RoleId);
            });
        }

        public Task<Role> GetRoleClaimManagementPOC(string RoleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoleClaimManagementPOC(RoleId);
            });
        }

        public Task<Role> GetRoleSubscriptionManagement(string RoleId, string DepartmentId, string PositionId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RoleControl.GetRoleSubscriptionManagement(RoleId, DepartmentId, PositionId);
            });
        }
        #endregion
    }
}
