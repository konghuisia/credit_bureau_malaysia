﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class LegalConstitutionTypeStore
    {
        public Task<List<LegalConstitutionType>> GetLegalConstitutionTypeControl()
        {
            return Task.Factory.StartNew(() =>
            {
                return LegalConstitutionTypeControl.GetLegalConstitutionTypeControl()?.ToList() ?? new List<LegalConstitutionType>();
            });
        }
    }
}
