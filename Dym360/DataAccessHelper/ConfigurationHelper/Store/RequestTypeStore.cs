﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class RequestTypeStore
    {

        public Task<IList<RequestType>> GetRequestTypeByModule(string moduleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return RequestTypeControl.GetRequestTypes(moduleId);
            });
        }
    }
}
