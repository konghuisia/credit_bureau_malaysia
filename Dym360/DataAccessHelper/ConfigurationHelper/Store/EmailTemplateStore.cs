﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class EmailTemplateStore
    {
        public Task<List<EmailTemplate>> GetEmailTemplates()
        {
            return Task.Factory.StartNew(() =>
            {
                return EmailTemplateControl.GetEmailTemplates()?.ToList() ?? new List<EmailTemplate>();
            });
        }

        public Task<EmailTemplate> GetEmailTemplateByEmailTemplateId(string emailTemplateId)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmailTemplateControl.GetEmailTemplateByEmailTemplateId(emailTemplateId);
            });
        }

        public Task<EmailTemplate> GetEmailTemplateByModuleId(string ModuleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmailTemplateControl.GetEmailTemplateByModuleId(ModuleId);
            });
        }

        public Task<string> UpdateEmailTemplate(EmailTemplate emailTemplate, string updatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmailTemplateControl.UpdateEmailTemplate(emailTemplate, updatedBy);
            });
        }
    }
}
