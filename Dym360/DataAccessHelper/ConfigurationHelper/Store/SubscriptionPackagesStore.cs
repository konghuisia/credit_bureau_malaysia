﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class SubscriptionPackagesStore
    {
        public Task<List<SubscriptionPackages>> GetSubscriptionPackages()
        {
            return Task.Factory.StartNew(() =>
            {
                return SubscriptionPackagesControl.GetSubscriptionPackages()?.ToList() ?? new List<SubscriptionPackages>();
            });
        }
    }
}
