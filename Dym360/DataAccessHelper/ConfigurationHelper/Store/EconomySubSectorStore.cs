﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class EconomySubSectorStore
    {
        public Task<List<EconomySubSector>> GetEconomySubSector()
        {
            return Task.Factory.StartNew(() =>
            {
                return EconomySubSectorControl.GetEconomySubSector()?.ToList() ?? new List<EconomySubSector>();
            });
        }
    }
}
