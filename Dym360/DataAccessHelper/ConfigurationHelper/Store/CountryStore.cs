﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class CountryStore
    {
        public Task<IList<Country>> GetAllCountry()
        {
            return Task.Factory.StartNew(() =>
            {
                return CountryControl.GetCountries();
            });
        }
    }
}
