﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class PurposeOfUseStore
    {
        public Task<List<PurposeOfUse>> GetPurposeOfUse()
        {
            return Task.Factory.StartNew(() =>
            {
                return PurposeOfUseControl.GetPurposeOfUse()?.ToList() ?? new List<PurposeOfUse>();
            });
        }
    }
}
