﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class ReferenceStore
    {
        public Task<Reference> GetReference(string moduleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReferenceControl.GetReference(moduleId);
            });
        }

        public Task<bool> NewReference(Reference reference)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReferenceControl.NewReference(reference);
            });
        }


        public Task<string> GetReferenceSequence(string referenceId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReferenceControl.GetReferenceSequence(referenceId);
            });
        }
        public Task<bool> NewReferenceSequence(ReferenceSequence refSeq)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReferenceControl.NewReferenceSequence(refSeq);
            });
        }
    }
}
