﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class PaymentTermStore
    {
        public Task<IList<PaymentTerm>> GetAllPaymentTerm()
        {
            return Task.Factory.StartNew(() =>
            {
                return PaymentTermControl.GetPaymentTerms();
            });
        }
    }
}
