﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.Configuration.Store
{
    public class ModuleStore
    {
        public Task CreateAsync(Module module)
        {
            if (module != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    ModuleControl.NewModule(module);
                });
            }
            throw new ArgumentNullException("module");
        }

        public Task DeleteAsync(Module module)
        {
            if (module != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    ModuleControl.DeleteModule(module);
                });
            }
            throw new ArgumentNullException("module");
        }

        public void Dispose()
        {

        }

        public Task<IList<Module>> GetAllModule()
        {
            return Task.Factory.StartNew(() =>
            {
                return ModuleControl.GetModules();
            });
        }
    }
}
