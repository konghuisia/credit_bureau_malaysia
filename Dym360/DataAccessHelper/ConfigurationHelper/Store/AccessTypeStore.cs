﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.Configuration.Store
{
    public class AccessTypeStore
    {
        public Task CreateAsync(AccessType accessType)
        {
            if (accessType != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    AccessTypeControl.NewAccessType(accessType);
                });
            }
            throw new ArgumentNullException("accessType");
        }

        public Task DeleteAsync(AccessType accessType)
        {
            if (accessType != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    AccessTypeControl.DeleteAccessType(accessType);
                });
            }
            throw new ArgumentNullException("accessType");
        }

        public void Dispose()
        {

        }

        public Task<IList<AccessType>> GetAllAccessType()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessTypeControl.GetAccessTypes();
            });
        }
    }
}
