﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class WorkflowStore
    {
        public Task<IList<Workflow>> GetAllWorkflow(string moduleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return WorkflowControl.GetWorkflows(moduleId);
            });
        }

        public Task<bool> UpdateWorkflow(Workflow workflow)
        {
            return Task.Factory.StartNew(() =>
            {
                return WorkflowControl.UpdateWorkflow(workflow);
            });
        }

        public Task<IList<WorkflowProcess>> GetWorkflowProcesses(string moduleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return WorkflowControl.GetWorkflowProcesses(moduleId);
            });
        }
    }
}
