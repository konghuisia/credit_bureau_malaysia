﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class LocationStore
    {
        public Task<IList<Location>> GetAllLocations()
        {
            return Task.Factory.StartNew(() =>
            {
                return LocationControl.GetLocations();
            });
        }
    }
}
