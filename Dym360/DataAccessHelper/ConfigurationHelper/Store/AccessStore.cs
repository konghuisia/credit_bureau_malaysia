﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class AccessStore
    {
        public Task<List<Access>> GetAllAccesses()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.GetAllAccesses()?.ToList() ?? new List<Access>();
            });
        }
    }
}
