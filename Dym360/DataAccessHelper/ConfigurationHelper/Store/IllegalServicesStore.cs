﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class IllegalServicesStore
    {
        public Task<List<IllegalServices>> GetIllegalServices()
        {
            return Task.Factory.StartNew(() =>
            {
                return IllegalServicesControl.GetIllegalServices()?.ToList() ?? new List<IllegalServices>();
            });
        }
        public Task<string> CreateIllegalServices(IllegalServices ue, string CreatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return IllegalServicesControl.CreateIllegalServices(ue, CreatedBy);
            });
        }
        public Task<string> UpdateIllegalServices(IllegalServices ue, string UpdatedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return IllegalServicesControl.UpdateIllegalServices(ue, UpdatedBy);
            });
        }
        public Task<bool> DeleteIllegalServices(string IllegalServicesId, string DeletedBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return IllegalServicesControl.DeleteIllegalServices(IllegalServicesId, DeletedBy);
            });
        }
    }
}
