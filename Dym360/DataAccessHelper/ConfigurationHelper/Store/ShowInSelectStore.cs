﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class ShowInSelectStore
    {
        public Task<List<ShowInSelect>> GetShowInSelectByModuleId(string ModuleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ShowInSelectControl.GetShowInSelectByModuleId(ModuleId)?.ToList() ?? new List<ShowInSelect>();
            });
        }
    }
}
