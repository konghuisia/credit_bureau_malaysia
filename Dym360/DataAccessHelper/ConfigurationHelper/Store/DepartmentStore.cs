﻿using DataAccessHelper.ConfigurationHelper.DAL;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.Store
{
    public class DepartmentStore
    {
        public Task<List<Department>> GetAllDepartments()
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentControl.GetDepartments()?.ToList() ?? new List<Department>();
            });
        }

        public Task<Department> GetDepartmentByDepartmentId(string departmentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentControl.GetDepartmentByDepartmentId(departmentId);
            });
        }

        public Task<Department> CreateNewDepartment(Department department, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentControl.CreateNewDepartment(department, id);
            });
        }

        public Task<Department> UpdateDepartment(Department department, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentControl.UpdateDepartment(department, id);
            });
        }
        public Task<bool> DeleteDepartment(string DepartmentId, string id)
        {
            return Task.Factory.StartNew(() =>
            {
                return DepartmentControl.DeleteDepartment(DepartmentId, id);
            });
        }
    }
}
