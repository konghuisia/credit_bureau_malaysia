﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class AccessControl
    {
        public static List<Access> GetAllAccesses()
        {
            return SqlHelper.GetRecords<Access>("cfg.Access_Get_All", new List<ParameterInfo>());
        }
    }
}
