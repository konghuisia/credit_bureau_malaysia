﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class ReferenceControl
    {
        public static Reference GetReference(string moduleId)
        {
            var parameterInfo = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ModuleId", ParameterValue = moduleId }
            };
            var reference = SqlHelper.GetRecord<Reference>("cfg.Reference_Get", parameterInfo);
            return reference;
        }

        public static bool NewReference(Reference re)
        {
            var parameterInfo = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ModuleId", ParameterValue = re.ModuleId },
                new ParameterInfo(){ ParameterName = "Prefix", ParameterValue = re.Prefix },
                new ParameterInfo(){ ParameterName = "Length", ParameterValue = re.Length },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = re.CreatedBy }

            };
            var success = SqlHelper.ExecuteQuery("cfg.Reference_New", parameterInfo);
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetReferenceSequence(string referenceId)
        {
            var parameterInfo = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ReferenceId", ParameterValue = referenceId }
            };
            var seqNo = SqlHelper.GetRecord<string>("cfg.ReferenceSequence_Get", parameterInfo);
            return seqNo;
        }

        public static bool NewReferenceSequence(ReferenceSequence rs)
        {
            var parameterInfo = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ReferenceId", ParameterValue = rs.ReferenceId },
                new ParameterInfo(){ ParameterName = "SequenceNo", ParameterValue = rs.SequenceNo }

            };
            var success = SqlHelper.ExecuteQuery("cfg.ReferenceSequenceNo_New", parameterInfo);
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
