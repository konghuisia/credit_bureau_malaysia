﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class DepartmentAccessControl
    {
        public static List<DepartmentAccess> GetDepartmentAccessByDepartmentId(string DepartmentId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DepartmentId", ParameterValue = DepartmentId }
            };
            return SqlHelper.GetRecords<DepartmentAccess>("cfg.Get_DepartmentAccessByDepartmentId", parameters);
        }
        public static bool UpdateDepartmentAccess(List<DepartmentAccess> access, string id, string DepartmentId)
        {
            try
            {
                foreach (var a in access)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "DepartmentId", ParameterValue = DepartmentId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
                    };

                    SqlHelper.ExecuteQuery("cfg.Create_DepartmentAccess", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool CreateDepartmentAccess(List<DepartmentAccess> access, string id, string DepartmentId)
        {
            try
            {
                foreach (var a in access)
                {
                    var parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "DepartmentId", ParameterValue = DepartmentId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = id }
                    };

                    SqlHelper.ExecuteQuery("cfg.Create_DepartmentAccess", parameters);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
