﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class WorkflowControl
    {
        public static int NewModule(Module module)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = module.Name },
                new ParameterInfo() { ParameterName = "Abbreviation", ParameterValue = module.Abbreviation },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = module.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = module.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Module_New", parameters);
            return success;
        }

        public static bool UpdateWorkflow(Workflow workflow)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = workflow.WorkflowId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = workflow.ActionId },
                new ParameterInfo() { ParameterName = "NextRoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "Percentate", ParameterValue = workflow.Percentage },
                new ParameterInfo() { ParameterName = "Sequence", ParameterValue = workflow.SequenceNo }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Workflow_Update", parameters);
            
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IList<Workflow> GetWorkflows(string moduleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = moduleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = "" }
            };
            var workflows = SqlHelper.GetRecords<Workflow>("cfg.Workflow_Get", parameters);
            return workflows;
        }
        
        public static IList<WorkflowProcess> GetWorkflowProcesses(string moduleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = moduleId }
            };
            var results = SqlHelper.GetRecords<WorkflowProcess>("cfg.WorkflowProcesses_Get", parameters);

            return results;
        }
    }
}
