﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class LegalConstitutionTypeControl
    {
        public static List<LegalConstitutionType> GetLegalConstitutionTypeControl()
        {
            return SqlHelper.GetRecords<LegalConstitutionType>("cfg.Get_LegalConstitutionType", new List<ParameterInfo>());
        }
    }
}
