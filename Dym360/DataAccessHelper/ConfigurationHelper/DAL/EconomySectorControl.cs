﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class EconomySectorControl
    {
        public static List<EconomySector> GetEconomySector()
        {
            return SqlHelper.GetRecords<EconomySector>("cfg.Get_EconomySector", new List<ParameterInfo>());
        }
    }
}
