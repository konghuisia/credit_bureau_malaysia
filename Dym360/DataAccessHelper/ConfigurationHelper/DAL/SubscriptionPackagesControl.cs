﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class SubscriptionPackagesControl
    {
        public static List<SubscriptionPackages> GetSubscriptionPackages()
        {
            return SqlHelper.GetRecords<SubscriptionPackages>("cfg.Get_SubscriptionPackages", new List<ParameterInfo>());
        }
    }
}
