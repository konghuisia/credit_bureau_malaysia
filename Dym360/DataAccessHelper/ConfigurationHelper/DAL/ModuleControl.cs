﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public static class ModuleControl
    {
        public static int NewModule(Module module)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = module.Name },
                new ParameterInfo() { ParameterName = "Abbreviation", ParameterValue = module.Abbreviation },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = module.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = module.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Module_New", parameters);
            return success;
        }

        public static int DeleteModule(Module module)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = module.ModuleId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Module_Delete", parameters);
            return success;
        }

        public static IList<Module> GetModules()
        {
            List<Module> oModules = SqlHelper.GetRecords<Module>("cfg.Modules_Get", new List<ParameterInfo>());
            return oModules;
        }
    }
}
