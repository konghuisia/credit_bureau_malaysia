﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class UserControl
    {
        private const string defaultPass = "Cbm#2019";
        public static IList<User> GetUsers(string position, string grad)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Grad", ParameterValue = grad },
                new ParameterInfo() { ParameterName = "Position", ParameterValue = position }
            };
            List<User> users = SqlHelper.GetRecords<User>("dbo.GetAllUser", parameters);
            return users;
        }

        public static User GetUser(string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            var user = SqlHelper.GetRecord<User>("acc.User_Get", parameters);
            return user;
        }

        public static IList<HOD> GetHODs ()
        {
            var hods = SqlHelper.GetRecords<HOD>("cfg.DepartmentHODs_Get", new List<ParameterInfo>());
            return hods;
        }

        public static bool UpdateLoginDate(string userId)
        {
            return SqlHelper.ExecuteQuery("acc.User_Update_LastLoginDate", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } }) > 0 ? true : false;
        }

        public static void UpdateLoginAttempt(string userId)
        {
            SqlHelper.ExecuteQuery("acc.User_Update_FailedLogin", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } });
        }

        public static bool UpdatePassword(User user)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
            };
            
            return SqlHelper.ExecuteQuery("acc.User_Update_Password", parameters) > 0 ? true : false;
        }

        public static bool CheckUserName(string username)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Username", ParameterValue = username }
            };
            return SqlHelper.GetRecord<bool>("acc.Check_Username", parameters);
        }

        public static string CreateNewUser(User user, string id)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, defaultPass);
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo() { ParameterName = "Username", ParameterValue = user.Username },
                new ParameterInfo() { ParameterName = "Position", ParameterValue = user.Position },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo() { ParameterName = "Department", ParameterValue = user.Department },
                //new ParameterInfo() { ParameterName = "Initial", ParameterValue = user.Initial },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo() { ParameterName = "Source", ParameterValue = user.Source }
            };
            return SqlHelper.GetRecord<string>("acc.Create_User", parameters);
        }

        public static string UpdateUser(User user, string id)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, defaultPass);
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo() { ParameterName = "Username", ParameterValue = user.Username },
                new ParameterInfo() { ParameterName = "Position", ParameterValue = user.Position },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo() { ParameterName = "Department", ParameterValue = user.Department },
                //new ParameterInfo() { ParameterName = "Initial", ParameterValue = user.Initial },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo() { ParameterName = "Source", ParameterValue = user.Source }
            };
            return SqlHelper.GetRecord<string>("acc.Update_User", parameters);
        }

        public static bool DeleteUser(string userId, string id)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<bool>("acc.User_Delete", parameters);
        }

        public static bool ChangePassword(User user, string pass)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId }
            };
            var u = SqlHelper.GetRecord<User>("acc.User_Get_By_UserId", parameters);

            try
            {
                if (!HashSalt.VerifyPassword(user.Password, u.Password, u.Salt))
                {
                    throw new Exception("Current password incorrect.");
                }
                else
                {
                    HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, pass);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                        new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                        new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                    };
                    
                    return SqlHelper.ExecuteQuery("acc.User_Update_Password", parameters) > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }
        public static bool ResetPassword(string userId, string accessId, string createdBy)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, defaultPass);

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("acc.User_Reset_Password", parameters) > 0 ? true : false;
        }

        public static bool NewUser(User user)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserName", ParameterValue = user.Username }
            };

            if (!string.IsNullOrEmpty(SqlHelper.GetRecord<string>("GetUserByUsername", parameters)))
            {
                //throw new DuplicateWaitObjectException("User ID already exist.");
                return false;
            }

            user.Password = string.IsNullOrEmpty(user.Password) ? defaultPass : user.Password;

            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo(){ ParameterName = "UserName", ParameterValue = user.Username },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo(){ ParameterName = "Source", ParameterValue = user.Source },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo(){ ParameterName = "AccountTypeId", ParameterValue = user.AccountTypeId },
            };

            user.UserId = SqlHelper.GetRecord<string>("acc.User_New", parameters);
            
            var result = true;
            if (string.IsNullOrEmpty(user.UserId))
            {
                result = false;
            }
            
            return result;
        }

        public static bool UpdateUser(User user)
        {
            HashSalt hashSalt = new HashSalt();
            if (!string.IsNullOrEmpty(user.Password))
            {
                hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);
            }

            var parameters = new List<ParameterInfo>();
            
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo(){ ParameterName = "AccountTypeId", ParameterValue = user.AccountTypeId },
            };
            
            return SqlHelper.ExecuteQuery("acc.User_Update", parameters) > 0 ? true : false;
        }

    }
}
