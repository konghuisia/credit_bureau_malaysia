﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class RoleControl
    {
        #region Role
        public static IList<Role> GetRoles(string roleTypeId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleTypeId", ParameterValue = roleTypeId }
            };
            List<Role> roles = SqlHelper.GetRecords<Role>("cfg.Roles_Get", parameters);
            return roles;
        }
        public static Role GetRole(string roleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = roleId }
            };
            var role = SqlHelper.GetRecord<Role>("cfg.Role_Get", parameters);
            return role;
        }
        #endregion

        #region RoleType
        public static int NewRoleType(RoleType roleType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = roleType.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = roleType.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = roleType.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.RoleType_New", parameters);
            return success;
        }

        public static int DeleteRoleType(RoleType roleType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleTypeId", ParameterValue = roleType.RoleTypeId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.RoleType_Delete", parameters);
            return success;
        }

        public static IList<RoleType> GetRoleTypes()
        {
            List<RoleType> oroleTypes = SqlHelper.GetRecords<RoleType>("cfg.RoleTypes_Get", new List<ParameterInfo>());
            return oroleTypes;
        }
        #endregion

        #region RoleAction
        public static IList<RoleAction> GetRoleActions(string userId = null, string roleId = null)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = (roleId == null ? DBNull.Value.ToString() : roleId) },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = (userId == null ? DBNull.Value.ToString() : userId) }
            };
            List<RoleAction> roleActions = SqlHelper.GetRecords<RoleAction>("cfg.RoleAction_Get", parameters);
            return roleActions;
        }
        #endregion

        #region CBM Credit Management
        public static Role GetRoleCreditManagementPOC(string roleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = roleId }
            };
            return SqlHelper.GetRecord<Role>("cfg.Get_Role_CreditManagementPOC", parameters);
        }
        #endregion

        #region CBM Claim Management
        public static Role GetRoleClaimManagementPOC(string roleId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = roleId }
            };
            return SqlHelper.GetRecord<Role>("cfg.Get_Role_ClaimManagementPOC", parameters);
        }
        #endregion

        #region CBM Subscription Management
        public static Role GetRoleSubscriptionManagement(string roleId, string departmentId, string positionId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = roleId },
                new ParameterInfo() { ParameterName = "DepartmentId", ParameterValue = departmentId },
                new ParameterInfo() { ParameterName = "PositionId", ParameterValue = positionId }
            };
            return SqlHelper.GetRecord<Role>("cfg.Get_Role_SubscriptionManagement", parameters);
        }
        #endregion
    }
}
