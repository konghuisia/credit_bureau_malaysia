﻿using DatabaseConnection.Data;
using ObjectClasses.ClaimManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class ClaimTypesControl
    {
        public static List<ClaimTypes> GetAllClaimTypes()
        {
            return SqlHelper.GetRecords<ClaimTypes>("cfg.ClaimTypes_Get_All", new List<ParameterInfo>());
        }
    }
}
