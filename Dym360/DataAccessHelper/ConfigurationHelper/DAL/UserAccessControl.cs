﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class UserAccessControl
    {
        public static List<UserAccess> GetUserAccessByUserId(string UserId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = UserId }
            };
            return SqlHelper.GetRecords<UserAccess>("acc.Get_UserAccessByUserId", parameters);
        }
    }
}
