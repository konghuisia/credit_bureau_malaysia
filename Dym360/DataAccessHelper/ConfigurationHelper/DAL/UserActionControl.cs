﻿using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using DatabaseConnection.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class UserActionControl
    {
        public static bool CreateUserAction(UserAction ua)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = ua.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = ua.CurrentRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = ua.ActionId }
            };
            var NextWorkFlow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);
            
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = ua.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = ua.ExpectedRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = ua.ActionId }
            };
            var ExpectedWorkFlow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (ua.ModuleId == "F43ED0E5-F3AE-4FC1-AB86-EA925FF80E9F" && ua.CurrentRoleId == "A4CA6DFC-0E59-43CA-95E4-A7D332F7D863" && ua.ActionId == "8F80B844-4391-4CA2-834F-DF625E2FE6CA" && NextWorkFlow == null)
            {
                NextWorkFlow = new Workflow()
                {
                    NextWorkflowProcess = "Draft",
                };
            }
            
            if (ua.ModuleId == "F43ED0E5-F3AE-4FC1-AB86-EA925FF80E9F" && ua.ExpectedRoleId == "A4CA6DFC-0E59-43CA-95E4-A7D332F7D863" && ua.ActionId == "8F80B844-4391-4CA2-834F-DF625E2FE6CA" && ExpectedWorkFlow == null)
            {
                ExpectedWorkFlow = new Workflow()
                {
                    NextWorkflowProcess = "Draft",
                };
            }

            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = ua.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = ua.Status },
                new ParameterInfo() { ParameterName = "NextStatus", ParameterValue = NextWorkFlow != null ? ExpectedWorkFlow.NextWorkflowProcess : null },
                new ParameterInfo() { ParameterName = "ExpectedStatus", ParameterValue = ExpectedWorkFlow != null ? ExpectedWorkFlow.NextWorkflowProcess : null },
                new ParameterInfo() { ParameterName = "CurrentRoleId", ParameterValue = ua.CurrentRoleId },
                new ParameterInfo() { ParameterName = "ExpectedRoleId", ParameterValue = ua.ExpectedRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = ua.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = ua.UserId },
                new ParameterInfo() { ParameterName = "ButtonName", ParameterValue = ua.ButtonName },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = ua.CreatedBy },
            };
            return SqlHelper.GetRecord<bool>("cfg.Create_UserAction", parameter);
        }
    }
}
