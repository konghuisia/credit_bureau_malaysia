﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System.Collections.Generic;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class EconomySubSectorControl
    {
        public static List<EconomySubSector> GetEconomySubSector()
        {
            return SqlHelper.GetRecords<EconomySubSector>("cfg.Get_EconomySubSector", new List<ParameterInfo>());
        }
    }
}
