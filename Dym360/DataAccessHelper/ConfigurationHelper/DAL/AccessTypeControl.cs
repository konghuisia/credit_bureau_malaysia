﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class AccessTypeControl
    {
        public static int NewAccessType(AccessType accessType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = accessType.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = accessType.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = accessType.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.AccessType_New", parameters);
            return success;
        }

        public static int DeleteAccessType(AccessType accessType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "AccessTypeId", ParameterValue = accessType.AccessTypeId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.AccessType_Delete", parameters);
            return success;
        }

        public static IList<AccessType> GetAccessTypes()
        {
            List<AccessType> oAccessTypes = SqlHelper.GetRecords<AccessType>("cfg.AccessTypes_Get", new List<ParameterInfo>());
            return oAccessTypes;
        }
    }
}
