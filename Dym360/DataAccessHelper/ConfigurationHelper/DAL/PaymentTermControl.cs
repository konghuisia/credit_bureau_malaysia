﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class PaymentTermControl
    {
        public static IList<PaymentTerm> GetPaymentTerms()
        {
            var paymentTerms = SqlHelper.GetRecords<PaymentTerm>("cfg.PaymentTerm_Get", new List<ParameterInfo>());
            return paymentTerms;
        }
    }
}
