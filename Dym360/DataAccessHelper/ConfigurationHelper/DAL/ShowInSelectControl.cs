﻿using DatabaseConnection.Data;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class ShowInSelectControl
    {
        public static List<ShowInSelect> GetShowInSelectByModuleId(string moduleId)
        {
            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ModuleId" , ParameterValue = moduleId }
            };
            return SqlHelper.GetRecords<ShowInSelect>("cfg.Get_ShowInSelectByModuleId", parameter);
        }
    }
}
