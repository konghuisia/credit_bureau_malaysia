﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class UnauthorisedEntitiesControl
    {
        public static List<UnauthorisedEntities> GetUnauthorisedEntities()
        {
            return SqlHelper.GetRecords<UnauthorisedEntities>("cfg.Get_UnauthorisedEntities", new List<ParameterInfo>());
        }

        public static string CreateUnauthorisedEntities(UnauthorisedEntities ue, string CreatedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "Name", ParameterValue = ue.Name },
                new ParameterInfo () { ParameterName = "Website", ParameterValue = ue.Website },
                new ParameterInfo () { ParameterName = "DateAddedToAlertList", ParameterValue = ue.DateAddedToAlertList },
                new ParameterInfo () { ParameterName = "CreatedBy", ParameterValue = CreatedBy }
            };
            return SqlHelper.GetRecord<string>("cfg.Create_UnauthorisedEntities", parameters);
        }

        public static string UpdateUnauthorisedEntities(UnauthorisedEntities ue, string UpdatedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "UnauthorisedEntitiesId", ParameterValue = ue.UnauthorisedEntitiesId },
                new ParameterInfo () { ParameterName = "Name", ParameterValue = ue.Name },
                new ParameterInfo () { ParameterName = "Website", ParameterValue = ue.Website },
                new ParameterInfo () { ParameterName = "DateAddedToAlertList", ParameterValue = ue.DateAddedToAlertList },
                new ParameterInfo () { ParameterName = "UpdatedBy", ParameterValue = UpdatedBy }
            };
            return SqlHelper.GetRecord<string>("cfg.Update_UnauthorisedEntities", parameters);
        }

        public static bool DeleteUnauthorisedEntities(string UnauthorisedEntitiesId, string DeletedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "UnauthorisedEntitiesId", ParameterValue = UnauthorisedEntitiesId },
                new ParameterInfo () { ParameterName = "DeletedBy", ParameterValue = DeletedBy }
            };
            return SqlHelper.GetRecord<bool>("cfg.Delete_UnauthorisedEntities", parameters);
        }
    }
}
