﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class IllegalServicesControl
    {
        public static List<IllegalServices> GetIllegalServices()
        {
            return SqlHelper.GetRecords<IllegalServices>("cfg.Get_IllegalServices", new List<ParameterInfo>());
        }

        public static string CreateIllegalServices(IllegalServices ue, string CreatedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "Name", ParameterValue = ue.Name },
                new ParameterInfo () { ParameterName = "Address", ParameterValue = ue.Address },
                new ParameterInfo () { ParameterName = "DateOfWarningLetterSent", ParameterValue = ue.DateOfWarningLetterSent },
                new ParameterInfo () { ParameterName = "CreatedBy", ParameterValue = CreatedBy }
            };
            return SqlHelper.GetRecord<string>("cfg.Create_IllegalServices", parameters);
        }

        public static string UpdateIllegalServices(IllegalServices ue, string UpdatedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "IllegalServicesId", ParameterValue = ue.IllegalServicesId },
                new ParameterInfo () { ParameterName = "Name", ParameterValue = ue.Name },
                new ParameterInfo () { ParameterName = "Address", ParameterValue = ue.Address },
                new ParameterInfo () { ParameterName = "DateOfWarningLetterSent", ParameterValue = ue.DateOfWarningLetterSent },
                new ParameterInfo () { ParameterName = "UpdatedBy", ParameterValue = UpdatedBy }
            };
            return SqlHelper.GetRecord<string>("cfg.Update_IllegalServices", parameters);
        }

        public static bool DeleteIllegalServices(string IllegalServicesId, string DeletedBy)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo () { ParameterName = "IllegalServicesId", ParameterValue = IllegalServicesId },
                new ParameterInfo () { ParameterName = "DeletedBy", ParameterValue = DeletedBy }
            };
            return SqlHelper.GetRecord<bool>("cfg.Delete_IllegalServices", parameters);
        }
    }
}
