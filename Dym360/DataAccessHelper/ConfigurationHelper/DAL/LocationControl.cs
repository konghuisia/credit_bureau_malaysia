﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class LocationControl
    {
        public static IList<Location> GetLocations()
        {
            var locations = SqlHelper.GetRecords<Location>("cfg.Locations_Get", new List<ParameterInfo>());
            return locations;
        }
    }
}
