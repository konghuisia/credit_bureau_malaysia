﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class TitleControl
    {
        public static List<Title> GetTitle()
        {
            return SqlHelper.GetRecords<Title>("cfg.Get_Title", new List<ParameterInfo>());
        }
    }
}
