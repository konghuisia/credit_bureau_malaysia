﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class PurposeOfUseControl
    {
        public static List<PurposeOfUse> GetPurposeOfUse()
        {
            return SqlHelper.GetRecords<PurposeOfUse>("cfg.Get_PurposeOfUse", new List<ParameterInfo>());
        }
    }
}
