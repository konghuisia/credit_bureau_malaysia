﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class DepartmentControl
    {
        public static List<Department> GetDepartments()
        {
            return SqlHelper.GetRecords<Department>("cfg.Departments_Get", new List<ParameterInfo>());
        }

        public static Department GetDepartmentByDepartmentId(string departmentId)
        {
            List<ParameterInfo> parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "DepartmentId", ParameterValue = departmentId }
            };
            return SqlHelper.GetRecord<Department>("cfg.Department_Get_By_DepartmentId", parameter);
        }

        public static bool DeleteDepartment(string DepartmentId, string id)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "DepartmentId", ParameterValue = DepartmentId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<bool>("cfg.Delete_Department", parameters);
        }

        public static Department UpdateDepartment(Department Department, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "DepartmentId", ParameterValue = Department.DepartmentId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = Department.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = Department.Description },
                new ParameterInfo() { ParameterName = "Abbreviation", ParameterValue = Department.Abbreviation },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<Department>("cfg.Update_Department", parameters);
        }

        public static Department CreateNewDepartment(Department Department, string id)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = Department.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = Department.Description },
                new ParameterInfo() { ParameterName = "Abbreviation", ParameterValue = Department.Abbreviation },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = id }
            };
            return SqlHelper.GetRecord<Department>("cfg.Create_Department", parameters);
        }
    }
}
