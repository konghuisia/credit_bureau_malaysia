﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class PositionControl
    {
        public static int NewPosition(Position position)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Grad", ParameterValue = position.Grad },
                new ParameterInfo() { ParameterName = "Title", ParameterValue = position.Title },
                new ParameterInfo() { ParameterName = "Abbreviation", ParameterValue = position.Abbreviation },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = position.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Position_New", parameters);
            return success;
        }

        public static int DeletePosition(Position position)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PositionId", ParameterValue = position.PositionId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.Module_Delete", parameters);
            return success;
        }

        public static IList<Position> GetPositions()
        {
            List<Position> oPositions = SqlHelper.GetRecords<Position>("cfg.Positions_Get", new List<ParameterInfo>());
            return oPositions;
        }
    }
}
