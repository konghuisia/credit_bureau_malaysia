﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class EmailTemplateControl
    {
        public static List<EmailTemplate> GetEmailTemplates()
        {
            return SqlHelper.GetRecords<EmailTemplate>("cfg.Get_EmailTemplates", new List<ParameterInfo>());
        }

        public static EmailTemplate GetEmailTemplateByEmailTemplateId(string emailTemplateId)
        {
            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = emailTemplateId }
            };
            return SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameter);
        }

        public static EmailTemplate GetEmailTemplateByModuleId(string moduleId)
        {
            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = moduleId }
            };
            return SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByModuleId", parameter);
        }

        public static string UpdateEmailTemplate(EmailTemplate emailTemplate, string updatedBy)
        {
            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = emailTemplate.EmailTemplateId },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = emailTemplate.ModuleId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = emailTemplate.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = emailTemplate.Description },
                new ParameterInfo() { ParameterName = "Active", ParameterValue = emailTemplate.Active },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = !string.IsNullOrEmpty(emailTemplate.Subject) ? Uri.UnescapeDataString(emailTemplate.Subject) : "" },
                new ParameterInfo() { ParameterName = "Body", ParameterValue = !string.IsNullOrEmpty(emailTemplate.Body) ? Uri.UnescapeDataString(emailTemplate.Body) : "" },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = updatedBy }
            };
            return SqlHelper.GetRecord<string>("cfg.Update_EmailTemplate", parameter);
        }
    }
}
