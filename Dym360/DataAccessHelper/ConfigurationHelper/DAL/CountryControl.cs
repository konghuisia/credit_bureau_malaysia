﻿using DatabaseConnection.Data;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class CountryControl
    {
        public static IList<Country> GetCountries()
        {
            var countries = SqlHelper.GetRecords<Country>("cfg.Country_Get", new List<ParameterInfo>());
            return countries;
        }
    }
}
