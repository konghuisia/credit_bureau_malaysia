﻿using DatabaseConnection.Data;
using ObjectClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ConfigurationHelper.DAL
{
    public class RequestTypeControl
    {
        public static int NewRequestType(RequestType requestType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = requestType.Name },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = requestType.Description },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = requestType.CreatedBy }
            };
            int success = SqlHelper.ExecuteQuery("cfg.RequestType_New", parameters);
            return success;
        }

        public static int DeleteRequestType(RequestType requestType)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = requestType.RequestTypeId }
            };
            int success = SqlHelper.ExecuteQuery("cfg.RequestType_Delete", parameters);
            return success;
        }

        public static IList<RequestType> GetRequestTypes(string ModuleId)
        {

            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = ModuleId }
            };
            List<RequestType> oRequestTypes = SqlHelper.GetRecords<RequestType>("cfg.RequestTypes_Get_ByModule", parameters);
            return oRequestTypes;
        }
    }
}
