﻿using DataAccessHelper.CustomerRelationshipHelper.DAL;
using ObjectClasses;
using ObjectClasses.CustomerRelationshipManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CustomerRelationshipHelper.Store
{
    public class CustomerRelationshipStore
    {
        public Task<bool> NewPublicGeneralRequest(Request req, Sender sender)
        {
            if (req != null && sender != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.NewRequest(req, sender);
                });
            }
            throw new ArgumentNullException("request & sender");
        }

        public Task<bool> Assign(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.Assign(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> Start(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.StartRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> Complete(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.CompleteRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> Approve(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.ApproveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }
        public Task<bool> Reject(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMore(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.RequestMoreRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<Sender> GetSender(string reqId)
        {
            if (!string.IsNullOrEmpty(reqId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return CustomerRelationshipControl.GetSender(reqId);
                });
            }
            throw new ArgumentNullException("reqId");
        }
    }
}
