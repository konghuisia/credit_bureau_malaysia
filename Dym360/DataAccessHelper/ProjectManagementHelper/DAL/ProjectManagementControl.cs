﻿using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.ProjectManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ProjectManagementHelper.DAL
{
    public class ProjectManagementControl
    {
        public static string CreateProject(Project project, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = project.Name },
                new ParameterInfo() { ParameterName = "ReferenceNo", ParameterValue = project.ReferenceNo },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = project.Description },
                new ParameterInfo() { ParameterName = "StartDate", ParameterValue = project.StartDate },
                new ParameterInfo() { ParameterName = "EndDate", ParameterValue = project.EndDate },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId },
                new ParameterInfo() { ParameterName = "SalesProposalId", ParameterValue = project.SalesProposalId }
            };
            project.ProjectId = SqlHelper.GetRecord<string>("dbo.Project_New", parameters, "project");

            return project.ProjectId;
        }

        public static bool UpdateProject(Project project)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = project.ProjectId },
                new ParameterInfo() { ParameterName = "ActualEndDate", ParameterValue = project.ActualEndDate },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = project.Status },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = project.Remark }
            };
            var success = SqlHelper.ExecuteQuery("dbo.Project_Update", parameters, "project");

            return success > 0 ? true : false;
        }

        public static bool CreateProjectTasks(List<ProjectPlanTask> taskList, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            foreach (var task in taskList)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TaskId", ParameterValue = task.TaskId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = task.Name },
                    new ParameterInfo() { ParameterName = "StartDate", ParameterValue = task.StartDate },
                    new ParameterInfo() { ParameterName = "EndDate", ParameterValue = task.EndDate },
                    new ParameterInfo() { ParameterName = "Duration", ParameterValue = task.Duration },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = task.UserId },
                    new ParameterInfo() { ParameterName = "ParentId", ParameterValue = taskList.Find(x => x.Id == task.ParentId)?.TaskId },
                    new ParameterInfo() { ParameterName = "Depth", ParameterValue = task.Depth },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = task.Description },
                    new ParameterInfo() { ParameterName = "Sequence", ParameterValue = task.Sequence },
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = task.ProjectId }
                };
                task.TaskId = SqlHelper.GetRecord<string>("dbo.ProjectTask_New", parameters, "project");
            }

            return taskList.Where(x => x.TaskId == null).Count() > 0 ? false : true;
        }

        public static bool ExecuteProjectPlan(Project project, List<ProjectPlanTask> taskList, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            foreach (var task in taskList)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TaskId", ParameterValue = task.TaskId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = task.Name },
                    new ParameterInfo() { ParameterName = "StartDate", ParameterValue = task.StartDate },
                    new ParameterInfo() { ParameterName = "EndDate", ParameterValue = task.EndDate },
                    new ParameterInfo() { ParameterName = "Duration", ParameterValue = task.Duration },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = task.UserId },
                    new ParameterInfo() { ParameterName = "ParentId", ParameterValue = taskList.Find(x => x.Id == task.ParentId)?.TaskId },
                    new ParameterInfo() { ParameterName = "Depth", ParameterValue = task.Depth },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = task.Description },
                    new ParameterInfo() { ParameterName = "Sequence", ParameterValue = task.Sequence },
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = task.ProjectId }
                };
                task.TaskId = SqlHelper.GetRecord<string>("dbo.ProjectTask_New", parameters, "project");
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = project.ProjectId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = project.Status }
            };
            var success = SqlHelper.ExecuteQuery("dbo.Project_Update_Status", parameters, "project");

            return success > 0 ? true : false;
        }

        public static bool UpdateProjectTasks(List<ProjectPlanTask> taskList, string userId)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            foreach (var task in taskList)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TaskId", ParameterValue = task.TaskId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = task.Name },
                    new ParameterInfo() { ParameterName = "StartDate", ParameterValue = task.StartDate },
                    new ParameterInfo() { ParameterName = "EndDate", ParameterValue = task.EndDate },
                    new ParameterInfo() { ParameterName = "Duration", ParameterValue = task.Duration },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = task.UserId },
                    new ParameterInfo() { ParameterName = "ParentId", ParameterValue = taskList.Find(x => x.Id == task.ParentId)?.TaskId },
                    new ParameterInfo() { ParameterName = "Depth", ParameterValue = task.Depth },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId },
                    new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = task.ProjectId }
                };
                task.TaskId = SqlHelper.GetRecord<string>("dbo.ProjectTask_Update", parameters, "project");
            }

            return success > 0 ? true : false;
        }

        public static ProjectPlanTask UpdateTaskProgress(ProjectPlanTask task, string userId)
        {
            var result = new ProjectPlanTask();
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TaskId", ParameterValue = task.TaskId }
            };
            result = SqlHelper.GetRecord<ProjectPlanTask>("dbo.ProjectTask_Get_ByTaskId", parameters, "project");

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TaskId", ParameterValue = task.TaskId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = task.UserId },
                new ParameterInfo() { ParameterName = "State", ParameterValue = task.State },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = task.Remark },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId }
            };
            result.Remarks = SqlHelper.GetRecords<Remark>("dbo.ProjectTask_Update_Progress", parameters, "project");

            return result;
        }

        public static Project GetProject(string projectId)
        {
            var project = new Project();
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            return SqlHelper.GetRecord<Project>("dbo.Project_Get", parameters, "project");
        }

        public static IList<Project> GetProjects()
        {
            var projects = new List<Project>();

            projects = SqlHelper.GetRecords<Project>("dbo.Projects_Get", new List<ParameterInfo>(), "project");

            foreach(var p in projects)
            {
                p.Tasks = GetTasks(p.ProjectId).ToList();
            }
            return projects;
        }
        public static IList<ProjectPlanTask> GetTasks(string projectId)
        {
            var taskList = new List<ProjectPlanTask>();
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            taskList =  SqlHelper.GetRecords<ProjectPlanTask>("dbo.ProjectTasks_Get", parameters, "project");

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId }
            };
            var remarks = SqlHelper.GetRecords<Remark>("dbo.TaskRemarks_Get", parameters, "project");

            foreach (var r in remarks)
            {
                r.ActionDate = Convert.ToDateTime(r.ActionDate).ToString("dd MMM yyyy HH:mm:ss");
            }

            foreach (var t in taskList)
            {
                t.Remarks = remarks.FindAll(x => x.Id == t.TaskId);
            }
            return taskList;
        }
    }
}
