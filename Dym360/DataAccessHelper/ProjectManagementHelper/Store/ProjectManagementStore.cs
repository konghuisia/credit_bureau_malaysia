﻿using DataAccessHelper.ProjectManagementHelper.DAL;
using ObjectClasses.ProjectManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.ProjectManagementHelper.Store
{
    public class ProjectManagementStore
    {
        public Task<string> CreateProject(Project project, string userId)
        {
            if (project != null && userId != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.CreateProject(project, userId);
                });
            }
            throw new ArgumentNullException("project");
        }
        public Task<bool> UpdateProject(Project project)
        {
            if (project != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.UpdateProject(project);
                });
            }
            throw new ArgumentNullException("project");
        }
        public Task<bool> SaveProjectTasks(List<ProjectPlanTask> taskList, string userId)
        {
            if (taskList != null && userId != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.CreateProjectTasks(taskList, userId);
                });
            }
            throw new ArgumentNullException("taskList");
        }
        public Task<bool> ExecuteProject(Project project, List<ProjectPlanTask> taskList, string userId)
        {
            project.Status = "Executed";
            if (taskList != null && userId != null && project != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.ExecuteProjectPlan(project, taskList, userId);
                });
            }
            throw new ArgumentNullException("taskList");
        }
        public Task<Project> GetProject(string projectId)
        {
            if (!string.IsNullOrEmpty(projectId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.GetProject(projectId);
                });
            }
            throw new ArgumentNullException("projectId");
        }
        public Task<bool> UpdateProjectTasks(List<ProjectPlanTask> taskList, string userId)
        {
            if (taskList != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.UpdateProjectTasks(taskList, userId);
                });
            }
            throw new ArgumentNullException("taskList");
        }
        public Task<ProjectPlanTask> UpdateTaskProgress(ProjectPlanTask task, string userId)
        {
            if (task != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.UpdateTaskProgress(task, userId);
                });
            }
            throw new ArgumentNullException("taskList");
        }
        public Task<IList<Project>> GetProjects()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectManagementControl.GetProjects();
            });
        }
        public Task<IList<ProjectPlanTask>> GetTasks(string projectId)
        {
            if (!string.IsNullOrEmpty(projectId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return ProjectManagementControl.GetTasks(projectId);
                });
            }
            throw new ArgumentNullException("projectId");
        }
    }
}
