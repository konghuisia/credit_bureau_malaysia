﻿using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.ConsentForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class ConsentFormPeopleControl
    {
        public static List<ConsentFormPeople> GetConsentFormPeopleByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecords<ConsentFormPeople>("sub.Get_ConsentFormPeople", parameters) ?? new List<ConsentFormPeople>();
        }

        public static string UpdateConsentFormPeople(ConsentFormPeople ConsentFormPeople)
        {
            if (ConsentFormPeople.IsSubmit)
            {

                var parameter = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = ConsentFormPeople.RequestId },
                };
                SqlHelper.ExecuteQuery("sub.Update_SubmitConsentFormPeople", parameter);
            }

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = ConsentFormPeople.RequestId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = ConsentFormPeople.Name },
                new ParameterInfo() { ParameterName = "NRIC_Passport", ParameterValue = ConsentFormPeople.NRIC_Passport },
                new ParameterInfo() { ParameterName = "Designation", ParameterValue = ConsentFormPeople.Designation },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = ConsentFormPeople.Type }
            };
            return SqlHelper.GetRecord<string>("sub.Update_ConsentFormPeople", parameters);
        }

        public static bool DeleteConsentFormPeople(string ConsentFormPeopleId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ConsentFormPeopleId", ParameterValue = ConsentFormPeopleId },
            };
            return SqlHelper.GetRecord<bool>("sub.Delete_ConsentFormPeople", parameters);
        }
    }
}
