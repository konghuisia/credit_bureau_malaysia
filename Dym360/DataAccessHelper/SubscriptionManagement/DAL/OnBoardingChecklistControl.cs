﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class OnBoardingChecklistControl
    {
        public static OnBoardingChecklist GetOnBoardingChecklistByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecord<OnBoardingChecklist>("sub.Get_OnBoardingChecklistByRequestId", parameters) ?? new OnBoardingChecklist();
        }

        public static string UpdateOnBoardingChecklist(OnBoardingChecklist OnBoardingChecklist)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = OnBoardingChecklist.RequestId },
                new ParameterInfo() { ParameterName = "Corporate", ParameterValue = OnBoardingChecklist.Corporate },
                new ParameterInfo() { ParameterName = "FI", ParameterValue = OnBoardingChecklist.FI },
                new ParameterInfo() { ParameterName = "MMSDA", ParameterValue = OnBoardingChecklist.MMSDA },
                new ParameterInfo() { ParameterName = "Fintech", ParameterValue = OnBoardingChecklist.Fintech },
                new ParameterInfo() { ParameterName = "Moneylender", ParameterValue = OnBoardingChecklist.Moneylender },
                new ParameterInfo() { ParameterName = "P2PLender", ParameterValue = OnBoardingChecklist.P2PLender },
                new ParameterInfo() { ParameterName = "CreditLeasing", ParameterValue = OnBoardingChecklist.CreditLeasing },
                new ParameterInfo() { ParameterName = "SME", ParameterValue = OnBoardingChecklist.SME },
                new ParameterInfo() { ParameterName = "SubscriptionAgreement", ParameterValue = OnBoardingChecklist.SubscriptionAgreement },
                new ParameterInfo() { ParameterName = "PackagePricing", ParameterValue = OnBoardingChecklist.PackagePricing },
                new ParameterInfo() { ParameterName = "DirectorICCopy", ParameterValue = OnBoardingChecklist.DirectorICCopy },
                new ParameterInfo() { ParameterName = "SecurityAdminForm", ParameterValue = OnBoardingChecklist.SecurityAdminForm },
                new ParameterInfo() { ParameterName = "SecurityAdminICCopy", ParameterValue = OnBoardingChecklist.SecurityAdminICCopy },
                new ParameterInfo() { ParameterName = "KYCDeclaration", ParameterValue = OnBoardingChecklist.KYCDeclaration },
                new ParameterInfo() { ParameterName = "CopyOfCBMConsentForm", ParameterValue = OnBoardingChecklist.CopyOfCBMConsentForm },
                new ParameterInfo() { ParameterName = "PaymentSlip", ParameterValue = OnBoardingChecklist.PaymentSlip },
                new ParameterInfo() { ParameterName = "CopyOfLicense", ParameterValue = OnBoardingChecklist.CopyOfLicense },
                new ParameterInfo() { ParameterName = "LicenseExpiryDate", ParameterValue = OnBoardingChecklist.LicenseExpiryDate },
                new ParameterInfo() { ParameterName = "PromotionalLetter", ParameterValue = OnBoardingChecklist.PromotionalLetter },
                new ParameterInfo() { ParameterName = "CMSServices", ParameterValue = OnBoardingChecklist.CMSServices },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = OnBoardingChecklist.UpdatedBy }
            };
            return SqlHelper.GetRecord<string>("sub.Update_OnBoardingChecklist", parameters);
        }
    }
}
