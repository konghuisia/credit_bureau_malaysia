﻿using DataAccessHelper.ConfigurationHelper;
using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class SubscriptionManagementRequestControl
    {
        public static bool NewRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);


            if (/*workflow.NextWorkflowProcess == "First Interest" || */workflow.NextWorkflowProcess == "Website Demo" || workflow.NextWorkflowProcess == "Package Selection")
            {
                var aps = new List<Approval>();
                aps.Add(new Approval()
                {
                    UserId = req.Requester.UserId,
                });
                req.Approvers = aps;
            }

            if (workflow.NextWorkflowProcess == "KIV")
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "DeletedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestApproval_DeleteAll", parameters);
            }

            if (req.Reviewers != null)
            {
                foreach (var r in req.Reviewers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = r.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }
            if (req.Approvers != null)
            {
                foreach (var a in req.Approvers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                    SendEmail(req.ModuleId, a.UserId, req.Link, req.ReferenceNo, req.Subject);
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.SubscriptionManagementWorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                    SendEmail(req.ModuleId, a.UserId, req.Link, req.ReferenceNo, req.Subject);
                    //parameters = new List<ParameterInfo>
                    //{
                    //    new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId }
                    //};
                    //var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByModuleId", parameters);

                    //if ((EmailTemplate != null) && EmailTemplate.Active)
                    //{
                    //    parameters = new List<ParameterInfo>
                    //    {
                    //        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId }
                    //    };
                    //    var User = SqlHelper.GetRecord<User>("acc.Get_UserByUserId", parameters);

                    //    if ((User != null) && !string.IsNullOrEmpty(User.Email))
                    //    {
                    //        var Sender = new EmailSender.EmailSender();

                    //        Sender.SendClaimManagementEmail(User.Email, req.Link, EmailTemplate.Subject, EmailTemplate.Body, req.ReferenceNo, req.Subject, User.Name);
                    //    }

                    //}
                }
            }


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId) && workflow.NextWorkflowProcess != "KIV")
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            if (workflow.NextWorkflowProcess == "Form Submission" || workflow.NextWorkflowProcess == "Payment Slip")
            {
                SendCustomerEmail(req.ModuleId, req.CustomerLink, req.ReferenceNo, req.Subject, subReq.Email, subReq.Name, req.RequestId);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Submitted" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);


            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, req.RequestId);
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "BusinessName", ParameterValue =  subReq.BusinessName },
                new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = subReq.BusinessRegNo },
                new ParameterInfo() { ParameterName = "Name", ParameterValue =  subReq.Name },
                new ParameterInfo() { ParameterName = "Designation", ParameterValue = subReq.Designation },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue =  subReq.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = subReq.Email },
                new ParameterInfo() { ParameterName = "PackageId", ParameterValue = subReq.PackageId },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "PurposeOfUse", ParameterValue = subReq.PurposeOfUse },
                new ParameterInfo() { ParameterName = "FrequencyOfUse", ParameterValue = subReq.FrequencyOfUse },
                new ParameterInfo() { ParameterName = "LegalRecommendAction", ParameterValue = subReq.LegalRecommendAction },
                new ParameterInfo() { ParameterName = "Title", ParameterValue = subReq.Title != "0" ? subReq.Title : null },
                new ParameterInfo() { ParameterName = "ConstitutionType", ParameterValue = subReq.ConstitutionType },
                new ParameterInfo() { ParameterName = "Reason", ParameterValue = subReq.Reason },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var SubscriptionRequestId = SqlHelper.ExecuteQuery("sub.Create_SubscriptionRequest", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "BusinessAddress", ParameterValue = cusDet.BusinessAddress },
                new ParameterInfo() { ParameterName = "BusinessName", ParameterValue = cusDet.BusinessName },
                new ParameterInfo() { ParameterName = "BusinessRegDate", ParameterValue = cusDet.BusinessRegDate },
                new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = cusDet.BusinessRegNo },
                new ParameterInfo() { ParameterName = "CountryOfRegistration", ParameterValue = cusDet.CountryOfRegistration },
                new ParameterInfo() { ParameterName = "EconomySector", ParameterValue = cusDet.EconomySector },
                new ParameterInfo() { ParameterName = "EconomySubSector", ParameterValue = cusDet.EconomySubSector },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = cusDet.EmailAddress },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cusDet.FaxNo },
                new ParameterInfo() { ParameterName = "LegalConstitution", ParameterValue = cusDet.LegalConstitution },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = cusDet.PostalCode },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "TelephoneNo", ParameterValue = cusDet.TelephoneNo },
                new ParameterInfo() { ParameterName = "Website", ParameterValue = cusDet.Website },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var CustomerId = SqlHelper.ExecuteQuery("sub.Create_CompanyDetail", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CustomerSubmitRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow.NextWorkflowProcess == "Admin")
            {
                workflow.NextWorkflowProcess = "Document Verification";
            }
            
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.SubscriptionManagementWorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                    SendEmail(req.ModuleId, a.UserId, req.Link, req.ReferenceNo, req.Subject);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
            };
            var re = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);


            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = "Customer" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + re.ReferenceNo + "] - Submitted" }
            };

            if (workflow != null && !string.IsNullOrEmpty(workflow.NextRoleId))
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = "Customer" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + re.ReferenceNo + "] - Submitted" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "PurposeOfUse", ParameterValue = subReq.PurposeOfUse },
                new ParameterInfo() { ParameterName = "FrequencyOfUse", ParameterValue = subReq.FrequencyOfUse },
            };
            var SubscriptionRequestId = SqlHelper.ExecuteQuery("sub.Update_SubscriptionRequest_Customer", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "BusinessRegDate", ParameterValue = cusDet.BusinessRegDate },
                new ParameterInfo() { ParameterName = "LegalConstitution", ParameterValue = cusDet.LegalConstitution },
                new ParameterInfo() { ParameterName = "CountryOfRegistration", ParameterValue = cusDet.CountryOfRegistration },
                new ParameterInfo() { ParameterName = "EconomySector", ParameterValue = cusDet.EconomySector },
                new ParameterInfo() { ParameterName = "EconomySubSector", ParameterValue = cusDet.EconomySubSector },
                new ParameterInfo() { ParameterName = "BusinessAddress", ParameterValue = cusDet.BusinessAddress },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = cusDet.PostalCode },
                new ParameterInfo() { ParameterName = "TelephoneNo", ParameterValue = cusDet.TelephoneNo },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cusDet.FaxNo },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = cusDet.EmailAddress },
                new ParameterInfo() { ParameterName = "Website", ParameterValue = cusDet.Website },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
            };
            var CustomerId = SqlHelper.ExecuteQuery("sub.Update_CompanyDetail_Customer", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidatePassword(string requestId, string password)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
            };
            var sr = SqlHelper.GetRecord<SubscriptionRequest>("sub.Get_SubscriptionRequest", parameters);

            if (sr != null)
            {
                if (sr.Password.Substring(0, 10) == password)
                {
                    return true;
                }
                return false;
            }

            return false;
        }

        public static void SendEmail(string ModuleId, string UserId, string Link, string ReferenceNo, string Subject)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "F140E3CE-593D-42AF-AB31-46A3F4F23F99" }
            };
            var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

            if ((EmailTemplate != null) && EmailTemplate.Active)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = UserId }
                };
                var User = SqlHelper.GetRecord<User>("acc.Get_UserByUserId", parameters);

                if ((User != null) && !string.IsNullOrEmpty(User.Email))
                {
                    var Sender = new EmailSender.EmailSender();

                    Sender.SendClaimManagementEmail(User.Email, Link, EmailTemplate.Subject, EmailTemplate.Body, ReferenceNo, Subject, User.Name);
                }
            }
        }

        public static void SendCustomerEmail(string ModuleId, string Link, string ReferenceNo, string Subject, string Email, string Name, string reqId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "6883873E-8051-4DE4-AD5B-A12342E51247" }
            };
            var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

            if ((EmailTemplate != null) && EmailTemplate.Active)
            {
                var Sender = new EmailSender.EmailSender();

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = reqId },
                };
                var sr =  SqlHelper.GetRecord<SubscriptionRequest>("sub.Get_SubscriptionRequest", parameters);

                if (sr != null)
                {
                    Sender.SendCustomerEmail(Email, Link, EmailTemplate.Subject, EmailTemplate.Body, ReferenceNo, Subject, Name, sr.Password);
                }
            }
        }

        public static void SubscriptionRequestCompleteSendCustomerEmail(string ModuleId, string Link, string ReferenceNo, string Subject, string Email, string Name, string reqId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "B96F4121-1E4A-4A34-81AA-B1768484D314" }
            };
            var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

            if ((EmailTemplate != null) && EmailTemplate.Active)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = reqId }
                };
                var SA = SqlHelper.GetRecords<PersonInCharge>("sub.Get_PersonInChargeByRequestId", parameters);

                if (SA.Count > 0 && SA != null)
                {
                    foreach (var sa in SA)
                    {
                        if (sa.Type == "SA" && !string.IsNullOrEmpty(sa.EmailAddress))
                        {
                            var Sender = new EmailSender.EmailSender();

                            Sender.SendCustomerEmail(sa.EmailAddress, Link, EmailTemplate.Subject, EmailTemplate.Body, ReferenceNo, Subject, sa.Name, "");
                        }
                    }
                }
            }
        }

        public static SubscriptionRequest GetSubscriptionRequest(string requestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
            };
            return SqlHelper.GetRecord<SubscriptionRequest>("sub.Get_SubscriptionRequest", parameters);
        }

        public static bool SaveRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (req.Approvers != null)
            {
                foreach (var a in req.Approvers)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = "397B8335-B903-431C-91B4-E4515A4C44BE" },//Sales Pitch role id
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "N/A" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = req.RequestTypeId },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = req.Subject },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = req.Description }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, req.RequestId);
            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "BusinessName", ParameterValue =  subReq.BusinessName },
                new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = subReq.BusinessRegNo },
                new ParameterInfo() { ParameterName = "Name", ParameterValue =  subReq.Name },
                new ParameterInfo() { ParameterName = "Designation", ParameterValue = subReq.Designation },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue =  subReq.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = subReq.Email },
                new ParameterInfo() { ParameterName = "PackageId", ParameterValue = subReq.PackageId },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo() { ParameterName = "PurposeOfUse", ParameterValue = subReq.PurposeOfUse },
                new ParameterInfo() { ParameterName = "FrequencyOfUse", ParameterValue = subReq.FrequencyOfUse },
                new ParameterInfo() { ParameterName = "LegalRecommendAction", ParameterValue = subReq.LegalRecommendAction },
                new ParameterInfo() { ParameterName = "Title", ParameterValue = subReq.Title != "0" ? subReq.Title : null },
                new ParameterInfo() { ParameterName = "ConstitutionType", ParameterValue = subReq.ConstitutionType },
                new ParameterInfo() { ParameterName = "Reason", ParameterValue = subReq.Reason },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var SubscriptionRequestId = SqlHelper.ExecuteQuery("sub.Create_SubscriptionRequest", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "BusinessAddress", ParameterValue = cusDet.BusinessAddress },
                new ParameterInfo() { ParameterName = "BusinessName", ParameterValue = cusDet.BusinessName },
                new ParameterInfo() { ParameterName = "BusinessRegDate", ParameterValue = cusDet.BusinessRegDate },
                new ParameterInfo() { ParameterName = "BusinessRegNo", ParameterValue = cusDet.BusinessRegNo },
                new ParameterInfo() { ParameterName = "CountryOfRegistration", ParameterValue = cusDet.CountryOfRegistration },
                new ParameterInfo() { ParameterName = "EconomySector", ParameterValue = cusDet.EconomySector },
                new ParameterInfo() { ParameterName = "EconomySubSector", ParameterValue = cusDet.EconomySubSector },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = cusDet.EmailAddress },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cusDet.FaxNo },
                new ParameterInfo() { ParameterName = "LegalConstitution", ParameterValue = cusDet.LegalConstitution },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = cusDet.PostalCode },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "TelephoneNo", ParameterValue = cusDet.TelephoneNo },
                new ParameterInfo() { ParameterName = "Website", ParameterValue = cusDet.Website },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId },
            };
            var CustomerId = SqlHelper.ExecuteQuery("sub.Create_CompanyDetail", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CustomerSaveRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            var success = 0;
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "PurposeOfUse", ParameterValue = subReq.PurposeOfUse },
                new ParameterInfo() { ParameterName = "FrequencyOfUse", ParameterValue = subReq.FrequencyOfUse },
            };
            var SubscriptionRequestId = SqlHelper.ExecuteQuery("sub.Update_SubscriptionRequest_Customer", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "BusinessRegDate", ParameterValue = cusDet.BusinessRegDate },
                new ParameterInfo() { ParameterName = "LegalConstitution", ParameterValue = cusDet.LegalConstitution },
                new ParameterInfo() { ParameterName = "CountryOfRegistration", ParameterValue = cusDet.CountryOfRegistration },
                new ParameterInfo() { ParameterName = "EconomySector", ParameterValue = cusDet.EconomySector },
                new ParameterInfo() { ParameterName = "EconomySubSector", ParameterValue = cusDet.EconomySubSector },
                new ParameterInfo() { ParameterName = "BusinessAddress", ParameterValue = cusDet.BusinessAddress },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = cusDet.PostalCode },
                new ParameterInfo() { ParameterName = "TelephoneNo", ParameterValue = cusDet.TelephoneNo },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cusDet.FaxNo },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = cusDet.EmailAddress },
                new ParameterInfo() { ParameterName = "Website", ParameterValue = cusDet.Website },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
            };
            var CustomerId = SqlHelper.ExecuteQuery("sub.Update_CompanyDetail_Customer", parameters);

            return true;
        }

        public static bool CustomerContinueRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            var success = 0;
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "PurposeOfUse", ParameterValue = subReq.PurposeOfUse },
                new ParameterInfo() { ParameterName = "FrequencyOfUse", ParameterValue = subReq.FrequencyOfUse },
            };
            var SubscriptionRequestId = SqlHelper.ExecuteQuery("sub.Update_SubscriptionRequest_Customer", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
            };
            SqlHelper.ExecuteQuery("sub.Update_SubscriptionRequest_EformStage", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "BusinessRegDate", ParameterValue = cusDet.BusinessRegDate },
                new ParameterInfo() { ParameterName = "LegalConstitution", ParameterValue = cusDet.LegalConstitution },
                new ParameterInfo() { ParameterName = "CountryOfRegistration", ParameterValue = cusDet.CountryOfRegistration },
                new ParameterInfo() { ParameterName = "EconomySector", ParameterValue = cusDet.EconomySector },
                new ParameterInfo() { ParameterName = "EconomySubSector", ParameterValue = cusDet.EconomySubSector },
                new ParameterInfo() { ParameterName = "BusinessAddress", ParameterValue = cusDet.BusinessAddress },
                new ParameterInfo() { ParameterName = "PostalCode", ParameterValue = cusDet.PostalCode },
                new ParameterInfo() { ParameterName = "TelephoneNo", ParameterValue = cusDet.TelephoneNo },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cusDet.FaxNo },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = cusDet.EmailAddress },
                new ParameterInfo() { ParameterName = "Website", ParameterValue = cusDet.Website },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
            };
            var CustomerId = SqlHelper.ExecuteQuery("sub.Update_CompanyDetail_Customer", parameters);

            return true;
        }

        public static bool RejectRequest(Request req)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Rejected" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ApproveRequest(Request req)
        {
            var success = 0;
            var action = "Approved";
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;

                if (workflow.NextWorkflowProcess == "Legal" || workflow.NextWorkflowProcess == "Close Completed")
                {
                    action = "Complete";
                    req.ActionId = "FE288856-C8DD-4F6B-B87A-D5968C4ACD3B";//Complete Action ID

                    if (workflow.NextWorkflowProcess == "Close Completed")
                    {
                        req.Status = workflow.NextWorkflowProcess;

                        SubscriptionRequestCompleteSendCustomerEmail(req.ModuleId, req.Link, req.ReferenceNo, req.Subject, null, null, req.RequestId);
                    }
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.SubscriptionManagementWorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                        SendEmail(req.ModuleId, a.UserId, req.Link, req.ReferenceNo, req.Subject);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - " + action }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Management(Request req, string Action)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            var isProceed = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                    new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
                };
                success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);


                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
                };
                workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.SubscriptionManagementWorkflowApproval_Get", parameters);

                if (workflow.workflowApprovals != null)
                {
                    foreach (var a in workflow.workflowApprovals)
                    {
                        parameters = new List<ParameterInfo>
                        {
                            new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                            new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                            new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                            new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                            new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                        };
                        success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);

                        SendEmail(req.ModuleId, a.UserId, req.Link, req.ReferenceNo, req.Subject);
                    }
                }
            }

            if (!string.IsNullOrEmpty(req.Comment))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                    new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
                };
                success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - " + Action }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            if (isProceed > 0)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                    new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                    new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                    new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
                };
                success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);
            }
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool RequestMoreRequest(Request req, SubscriptionRequest subReq)
        {
            var success = 0;
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "ModuleId", ParameterValue = req.ModuleId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId }
            };
            var workflow = SqlHelper.GetRecord<Workflow>("cfg.Workflow_Get", parameters);

            if (workflow != null && workflow.SequenceNo > 0)
            {
                req.Status = "Pending " + workflow.NextWorkflowProcess;
            }
            else
            {
                req.Status = workflow.NextWorkflowProcess;
            }

            if (workflow.NextWorkflowProcess == "Form Submission" || workflow.NextWorkflowProcess == "Payment Slip")
            {
                SendCustomerEmail(req.ModuleId, req.CustomerLink, req.ReferenceNo, req.Subject, subReq.Email, subReq.Name, req.RequestId);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "Condition", ParameterValue = "PROCEED" }
            };
            success = SqlHelper.ExecuteQuery("req.RequestApproval_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = req.RoleId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UpdatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId }
            };
            workflow.workflowApprovals = SqlHelper.GetRecords<WorkflowApproval>("cfg.SubscriptionManagementWorkflowApproval_Get", parameters);

            if (workflow.workflowApprovals != null)
            {
                foreach (var a in workflow.workflowApprovals)
                {

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                        new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                        new ParameterInfo() { ParameterName = "UserId", ParameterValue = a.UserId },
                        new ParameterInfo() { ParameterName = "State", ParameterValue = "" },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.Requester.UserId }
                    };
                    success = SqlHelper.ExecuteQuery("req.RequestApproval_New", parameters);
                }
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "RoleId", ParameterValue = workflow.NextRoleId },
                new ParameterInfo() { ParameterName = "WorkflowProcessId", ParameterValue = workflow.NextWorkflowProcessId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = req.UserId }
            };
            success = SqlHelper.ExecuteQuery("req.RequestState_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Comment", ParameterValue = req.Comment }
            };
            success = SqlHelper.ExecuteQuery("req.Comment_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "ActionId", ParameterValue = req.ActionId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = req.UserId },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "[" + req.ReferenceNo + "] - Requested more information" }
            };
            var requestHistoryId = SqlHelper.GetRecord<string>("req.RequestHistory_New", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = req.Status },
                new ParameterInfo() { ParameterName = "RequestTypeId", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Subject", ParameterValue = "" },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = "" }
            };
            success = SqlHelper.ExecuteQuery("req.Request_Update", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "F140E3CE-593D-42AF-AB31-46A3F4F23F99" }
            };
            var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = req.RequestId }
            };
            var Request = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);

            //if ((EmailTemplate != null) && EmailTemplate.Active)
            //{
            //    parameters = new List<ParameterInfo>
            //    {
            //        new ParameterInfo() { ParameterName = "UserId", ParameterValue = Request.UserId }
            //    };
            //    var User = SqlHelper.GetRecord<User>("acc.Get_UserByUserId", parameters);

            //    if ((User != null) && !string.IsNullOrEmpty(User.Email))
            //    {
            //        var Sender = new EmailSender.EmailSender();

            //        Sender.SendClaimManagementEmail(User.Email, req.Link, EmailTemplate.Subject, EmailTemplate.Body, req.ReferenceNo, req.Subject, User.Name);
            //    }
            //}

            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
