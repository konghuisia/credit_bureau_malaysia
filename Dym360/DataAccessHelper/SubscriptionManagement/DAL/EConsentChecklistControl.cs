﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement.EConsentChecklist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class EConsentChecklistControl
    {
        public static EConsentChecklist GetEConsentChecklistByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecord<EConsentChecklist>("sub.Get_EConsentChecklist", parameters) ?? new EConsentChecklist();
        }

        public static string UpdateEConsentChecklist(EConsentChecklist EConsentChecklist)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = EConsentChecklist.RequestId },
                new ParameterInfo() { ParameterName = "Consent", ParameterValue = EConsentChecklist.Consent },
                new ParameterInfo() { ParameterName = "IdentityVerification", ParameterValue = EConsentChecklist.IdentityVerification },
                new ParameterInfo() { ParameterName = "ElectronicSignature", ParameterValue = EConsentChecklist.ElectronicSignature },
                new ParameterInfo() { ParameterName = "DocumentMustBeOriginal", ParameterValue = EConsentChecklist.DocumentMustBeOriginal },
                new ParameterInfo() { ParameterName = "RetentionOfDocument", ParameterValue = EConsentChecklist.RetentionOfDocument }
            };
            return SqlHelper.GetRecord<string>("sub.Update_EConsentChecklist", parameters);
        }
    }
}
