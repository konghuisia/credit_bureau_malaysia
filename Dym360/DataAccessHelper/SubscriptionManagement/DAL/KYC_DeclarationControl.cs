﻿using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class KYC_DeclarationControl
    {
        public static KYC_DeclarationViewModel GetKYC_DeclarationByRequestId(string RequestId)
        {
            var KYC_Declaration = new KYC_DeclarationViewModel();
            
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            KYC_Declaration.Request = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);
            KYC_Declaration.KYC_DealOn = SqlHelper.GetRecord<KYC_DealOn>("sub.Get_KYC_DealOnByRequestId", parameters);
            KYC_Declaration.KYC_CompanyType = SqlHelper.GetRecord<KYC_CompanyType>("sub.Get_KYC_CompanyTypeByRequestId", parameters);
            KYC_Declaration.KYC_CompanyPolicy = SqlHelper.GetRecord<KYC_CompanyPolicy>("sub.Get_KYC_CompanyPolicyByRequestId", parameters);
            KYC_Declaration.KYC_ProductService = SqlHelper.GetRecord<KYC_ProductServices>("sub.Get_KYC_ProductServicesByRequestId", parameters);
            KYC_Declaration.KYC_ProtectionOfData = SqlHelper.GetRecord<KYC_ProtectionOfData>("sub.Get_KYC_ProtectionOfDataByRequestId", parameters);
            KYC_Declaration.KYC_ConsentProcurement = SqlHelper.GetRecord<KYC_ConsentProcurement>("sub.Get_KYC_ConsentProcurementByRequestId", parameters);
            KYC_Declaration.KYC_TargetedClientMarket = SqlHelper.GetRecord<KYC_TargetedClientMarket>("sub.Get_KYC_TargetedClientMarketByRequestId", parameters);
            KYC_Declaration.KYC_PurposeOfSubscription = SqlHelper.GetRecord<KYC_PurposeOfSubscription>("sub.Get_KYC_PurposeOfSubscriptionByRequestId", parameters);
            KYC_Declaration.KYC_SubscriberDeclaration = SqlHelper.GetRecord<KYC_SubscriberDeclaration>("sub.Get_KYC_SubscriberDeclarationByRequestId", parameters) ?? new KYC_SubscriberDeclaration();

            return KYC_Declaration;
        }

        public static bool UpdateKYC_DeclarationByRequestId(string RequestId, KYC_DeclarationViewModel kYC_Declaration)
        {
            var parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = kYC_Declaration.KYC_SubscriberDeclaration.Name },
                new ParameterInfo() { ParameterName = "IC", ParameterValue = kYC_Declaration.KYC_SubscriberDeclaration.IC },
                new ParameterInfo() { ParameterName = "Director", ParameterValue = kYC_Declaration.KYC_SubscriberDeclaration.Director },
                new ParameterInfo() { ParameterName = "AuthorisedOfficer", ParameterValue = kYC_Declaration.KYC_SubscriberDeclaration.AuthorisedOfficer },
                new ParameterInfo() { ParameterName = "AuthorisedOfficerText", ParameterValue = kYC_Declaration.KYC_SubscriberDeclaration.AuthorisedOfficerText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_SubscriberDeclaration.SubscriberDeclarationId = SqlHelper.GetRecord<string>("sub.Update_KYC_SubscriberDeclaration", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Wholesaler", ParameterValue = kYC_Declaration.KYC_CompanyType.Wholesaler },
                new ParameterInfo() { ParameterName = "Distributor", ParameterValue = kYC_Declaration.KYC_CompanyType.Distributor },
                new ParameterInfo() { ParameterName = "Retailer", ParameterValue = kYC_Declaration.KYC_CompanyType.Retailer },
                new ParameterInfo() { ParameterName = "Manufacturer", ParameterValue = kYC_Declaration.KYC_CompanyType.Manufacturer },
                new ParameterInfo() { ParameterName = "Others", ParameterValue = kYC_Declaration.KYC_CompanyType.Others },
                new ParameterInfo() { ParameterName = "OthersText", ParameterValue = kYC_Declaration.KYC_CompanyType.OthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_CompanyType.CompanyTypeId = SqlHelper.GetRecord<string>("sub.Update_KYC_CompanyType", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Advisory", ParameterValue = kYC_Declaration.KYC_ProductService.Advisory },
                new ParameterInfo() { ParameterName = "AdvisoryText", ParameterValue = kYC_Declaration.KYC_ProductService.AdvisoryText },
                new ParameterInfo() { ParameterName = "Services", ParameterValue = kYC_Declaration.KYC_ProductService.Services },
                new ParameterInfo() { ParameterName = "Legal", ParameterValue = kYC_Declaration.KYC_ProductService.Legal },
                new ParameterInfo() { ParameterName = "Accounting", ParameterValue = kYC_Declaration.KYC_ProductService.Accounting },
                new ParameterInfo() { ParameterName = "Auditing", ParameterValue = kYC_Declaration.KYC_ProductService.Auditing },
                new ParameterInfo() { ParameterName = "Taxation", ParameterValue = kYC_Declaration.KYC_ProductService.Taxation },
                new ParameterInfo() { ParameterName = "Engineering", ParameterValue = kYC_Declaration.KYC_ProductService.Engineering },
                new ParameterInfo() { ParameterName = "SoftwareImplementation", ParameterValue = kYC_Declaration.KYC_ProductService.SoftwareImplementation },
                new ParameterInfo() { ParameterName = "DataProcessing", ParameterValue = kYC_Declaration.KYC_ProductService.DataProcessing },
                new ParameterInfo() { ParameterName = "RealEstate", ParameterValue = kYC_Declaration.KYC_ProductService.RealEstate },
                new ParameterInfo() { ParameterName = "Advertising", ParameterValue = kYC_Declaration.KYC_ProductService.Advertising },
                new ParameterInfo() { ParameterName = "Transportation", ParameterValue = kYC_Declaration.KYC_ProductService.Transportation },
                new ParameterInfo() { ParameterName = "Agriculture", ParameterValue = kYC_Declaration.KYC_ProductService.Agriculture },
                new ParameterInfo() { ParameterName = "Communication", ParameterValue = kYC_Declaration.KYC_ProductService.Communication },
                new ParameterInfo() { ParameterName = "Education", ParameterValue = kYC_Declaration.KYC_ProductService.Education },
                new ParameterInfo() { ParameterName = "InsuranceRelated", ParameterValue = kYC_Declaration.KYC_ProductService.InsuranceRelated },
                new ParameterInfo() { ParameterName = "BankingAndFinance", ParameterValue = kYC_Declaration.KYC_ProductService.BankingAndFinance },
                new ParameterInfo() { ParameterName = "HealthRelated", ParameterValue = kYC_Declaration.KYC_ProductService.HealthRelated },
                new ParameterInfo() { ParameterName = "OtherServices", ParameterValue = kYC_Declaration.KYC_ProductService.OtherServices },
                new ParameterInfo() { ParameterName = "OtherServicesText", ParameterValue = kYC_Declaration.KYC_ProductService.OtherServicesText },
                new ParameterInfo() { ParameterName = "ServicesText", ParameterValue = kYC_Declaration.KYC_ProductService.ServicesText },
                new ParameterInfo() { ParameterName = "Provision", ParameterValue = kYC_Declaration.KYC_ProductService.Provision },
                new ParameterInfo() { ParameterName = "ProvisionText", ParameterValue = kYC_Declaration.KYC_ProductService.ProvisionText },
                new ParameterInfo() { ParameterName = "Others", ParameterValue = kYC_Declaration.KYC_ProductService.Others },
                new ParameterInfo() { ParameterName = "OthersText", ParameterValue = kYC_Declaration.KYC_ProductService.OthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_ProductService.ProductServicesId = SqlHelper.GetRecord<string>("sub.Update_KYC_ProductService", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Wholesaler", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.Wholesaler },
                new ParameterInfo() { ParameterName = "Distributor", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.Distributor },
                new ParameterInfo() { ParameterName = "Retailer", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.Retailer },
                new ParameterInfo() { ParameterName = "Manufacturer", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.Manufacturer },
                new ParameterInfo() { ParameterName = "Others", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.Others },
                new ParameterInfo() { ParameterName = "OthersText", ParameterValue = kYC_Declaration.KYC_TargetedClientMarket.OthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_TargetedClientMarket.TargetedClientMarketId = SqlHelper.GetRecord<string>("sub.Update_KYC_TargetedClientMarket", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "CreditTerm", ParameterValue = kYC_Declaration.KYC_DealOn.CreditTerm },
                new ParameterInfo() { ParameterName = "CashTerm", ParameterValue = kYC_Declaration.KYC_DealOn.CashTerm },
                new ParameterInfo() { ParameterName = "Both", ParameterValue = kYC_Declaration.KYC_DealOn.Both },
                new ParameterInfo() { ParameterName = "CreditTermPerc", ParameterValue = kYC_Declaration.KYC_DealOn.CreditTermPerc },
                new ParameterInfo() { ParameterName = "CashTermPerc", ParameterValue = kYC_Declaration.KYC_DealOn.CashTermPerc },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_DealOn.DealOnId = SqlHelper.GetRecord<string>("sub.Update_KYC_DealOn", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Purpose", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.Purpose },
                new ParameterInfo() { ParameterName = "LoanApplication_FinancingFacility", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.LoanApplication_FinancingFacility },
                new ParameterInfo() { ParameterName = "PeriodicReviewOfLoan", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.PeriodicReviewOfLoan },
                new ParameterInfo() { ParameterName = "OpeningAnAccount", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.OpeningAnAccount },
                new ParameterInfo() { ParameterName = "Investment", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.Investment },
                new ParameterInfo() { ParameterName = "Insurance", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.Insurance },
                new ParameterInfo() { ParameterName = "FinancialGurantee", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.FinancialGurantee },
                new ParameterInfo() { ParameterName = "Employment", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.Employment },
                new ParameterInfo() { ParameterName = "HirePurchase", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.HirePurchase },
                new ParameterInfo() { ParameterName = "CrimeFraudDetectionAndPrevention", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.CrimeFraudDetectionAndPrevention },
                new ParameterInfo() { ParameterName = "InvestigationByAuditFirm_ComplianceUnits_LawFirms", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.InvestigationByAuditFirm_ComplianceUnits_LawFirms },
                new ParameterInfo() { ParameterName = "RentalContract", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.RentalContract },
                new ParameterInfo() { ParameterName = "DebtRecovery", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.DebtRecovery },
                new ParameterInfo() { ParameterName = "Others", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.Others },
                new ParameterInfo() { ParameterName = "OthersText", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.OthersText },
                new ParameterInfo() { ParameterName = "No", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.No },
                new ParameterInfo() { ParameterName = "TransferCreditReportToThirdParty", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.TransferCreditReportToThirdParty },
                new ParameterInfo() { ParameterName = "YesOthers", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.YesOthers },
                new ParameterInfo() { ParameterName = "YesOthersText", ParameterValue = kYC_Declaration.KYC_PurposeOfSubscription.YesOthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_PurposeOfSubscription.PurposeOfSubscriptionId = SqlHelper.GetRecord<string>("sub.Update_KYC_PurposeOfSubscription", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "DataUserYes", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.DataUserYes },
                new ParameterInfo() { ParameterName = "DataUserNo", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.DataUserNo },
                new ParameterInfo() { ParameterName = "AnswerYes", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.AnswerYes },
                new ParameterInfo() { ParameterName = "AnswerNo", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.AnswerNo },
                new ParameterInfo() { ParameterName = "ExplainToDataSubject", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.ExplainToDataSubject },
                new ParameterInfo() { ParameterName = "ObtainIcCopy", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.ObtainIcCopy },
                new ParameterInfo() { ParameterName = "ObtainDataSubjectConsent", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.ObtainDataSubjectConsent },
                new ParameterInfo() { ParameterName = "BeforeCreditInfoOthers", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.BeforeCreditInfoOthers },
                new ParameterInfo() { ParameterName = "BeforeCreditInfoOthersText", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.BeforeCreditInfoOthersText },
                new ParameterInfo() { ParameterName = "ProvideReportToThirdParty", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.ProvideReportToThirdParty },
                new ParameterInfo() { ParameterName = "ProvideReportToDataSubject", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.ProvideReportToDataSubject },
                new ParameterInfo() { ParameterName = "AfterCreditInfoOthers", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.AfterCreditInfoOthers },
                new ParameterInfo() { ParameterName = "AfterCreditInfoOthersText", ParameterValue = kYC_Declaration.KYC_CompanyPolicy.AfterCreditInfoOthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_CompanyPolicy.CompanyPolicyId = SqlHelper.GetRecord<string>("sub.Update_KYC_CompanyPolicy", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "Instinct", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.Instinct },
                new ParameterInfo() { ParameterName = "OweMoney", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.OweMoney },
                new ParameterInfo() { ParameterName = "BadReputation", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.BadReputation },
                new ParameterInfo() { ParameterName = "CreditReview", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.CreditReview },
                new ParameterInfo() { ParameterName = "Others", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.Others },
                new ParameterInfo() { ParameterName = "OthersText", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.OthersText },
                new ParameterInfo() { ParameterName = "PoliciesAndProcedures", ParameterValue = kYC_Declaration.KYC_ConsentProcurement.PoliciesAndProcedures },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_ConsentProcurement.ConsentProcurementId = SqlHelper.GetRecord<string>("sub.Update_KYC_ConsentProcurement", parameters);

            parameters = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "StoreCreditReportInElectronicDevice", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.StoreCreditReportInElectronicDevice },
                new ParameterInfo() { ParameterName = "PrintCreditReportForFilling", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.PrintCreditReportForFilling },
                new ParameterInfo() { ParameterName = "ProvideDataSubjectHisReport", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ProvideDataSubjectHisReport },
                new ParameterInfo() { ParameterName = "AfterOthers", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AfterOthers },
                new ParameterInfo() { ParameterName = "AfterOthersText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AfterOthersText },
                new ParameterInfo() { ParameterName = "UsingMachineShredder", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.UsingMachineShredder },
                new ParameterInfo() { ParameterName = "AccessToOfficeComputerIsEncrypted", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AccessToOfficeComputerIsEncrypted },
                new ParameterInfo() { ParameterName = "AccessToCBMSystemIsLimited", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AccessToCBMSystemIsLimited },
                new ParameterInfo() { ParameterName = "SharingOrDisplayingPassword", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.SharingOrDisplayingPassword },
                new ParameterInfo() { ParameterName = "UsingFirewall", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.UsingFirewall },
                new ParameterInfo() { ParameterName = "FirewallText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.FirewallText },
                new ParameterInfo() { ParameterName = "UsingAntivirus", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.UsingAntivirus },
                new ParameterInfo() { ParameterName = "AntivirusText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AntivirusText },
                new ParameterInfo() { ParameterName = "EnsureOthers", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.EnsureOthers },
                new ParameterInfo() { ParameterName = "EnsureOthersText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.EnsureOthersText },
                new ParameterInfo() { ParameterName = "ParentCompany", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ParentCompany },
                new ParameterInfo() { ParameterName = "SubsidiaryCompany", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.SubsidiaryCompany },
                new ParameterInfo() { ParameterName = "AllEmployee", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.AllEmployee },
                new ParameterInfo() { ParameterName = "ThirdParty ", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ThirdParty },
                new ParameterInfo() { ParameterName = "Director_CEO_Management", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.Director_CEO_Management },
                new ParameterInfo() { ParameterName = "People_CompanyOthers", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.People_CompanyOthers },
                new ParameterInfo() { ParameterName = "People_CompanyOthersText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.People_CompanyOthersText },
                new ParameterInfo() { ParameterName = "RetainDataYes", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.RetainDataYes },
                new ParameterInfo() { ParameterName = "RetainDataYesText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.RetainDataYesText },
                new ParameterInfo() { ParameterName = "RetainDataNo", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.RetainDataNo },
                new ParameterInfo() { ParameterName = "ElectronicCopy", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ElectronicCopy },
                new ParameterInfo() { ParameterName = "PhysicalFilling", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.PhysicalFilling },
                new ParameterInfo() { ParameterName = "CloudSystem", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.CloudSystem },
                new ParameterInfo() { ParameterName = "InternetStorageProvider", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.InternetStorageProvider },
                new ParameterInfo() { ParameterName = "PersonalDatabase", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.PersonalDatabase },
                new ParameterInfo() { ParameterName = "StoreOthers", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.StoreOthers },
                new ParameterInfo() { ParameterName = "StoreOthersText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.StoreOthersText },
                new ParameterInfo() { ParameterName = "CreditInformationOnlyForInternalUsage", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.CreditInformationOnlyForInternalUsage },
                new ParameterInfo() { ParameterName = "OnlyForSecurityAdministrator", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.OnlyForSecurityAdministrator },
                new ParameterInfo() { ParameterName = "ReportWillNotBePrinted", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ReportWillNotBePrinted },
                new ParameterInfo() { ParameterName = "ReportWillNotBeRetained", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ReportWillNotBeRetained },
                new ParameterInfo() { ParameterName = "ControlOthers", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ControlOthers },
                new ParameterInfo() { ParameterName = "ControlOthersText", ParameterValue = kYC_Declaration.KYC_ProtectionOfData.ControlOthersText },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId },
            };
            kYC_Declaration.KYC_ProtectionOfData.ProtectionOfDataId = SqlHelper.GetRecord<string>("sub.Update_KYC_ProtectionOfData", parameters);

            return true;
        }
    }
}
