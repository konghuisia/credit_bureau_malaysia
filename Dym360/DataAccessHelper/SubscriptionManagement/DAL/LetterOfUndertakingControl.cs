﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class LetterOfUndertakingControl
    {
        public static LetterOfUndertaking GetLetterOfUndertakingByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecord<LetterOfUndertaking>("sub.Get_LetterOfUndertakingByRequestId", parameters) ?? new LetterOfUndertaking();
        }

        public static string UpdateLetterOfUndertaking(LetterOfUndertaking LetterOfUndertaking)
        {
            if (LetterOfUndertaking.IsSubmit)
            {
                var parameter = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = LetterOfUndertaking.RequestId },
                };
                SqlHelper.ExecuteQuery("sub.Update_SubmitLetterOfUndertaking", parameter);
            }

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = LetterOfUndertaking.RequestId },
                new ParameterInfo() { ParameterName = "Consented", ParameterValue = LetterOfUndertaking.Consented },
            };
            return SqlHelper.GetRecord<string>("sub.Update_LetterOfUndertaking", parameters);
        }
    }
}
