﻿using DataAccessHelper.ConfigurationHelper;
using DatabaseConnection.Data;
using ObjectClasses;
using ObjectClasses.Configuration;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class PersonInChargeControl
    {
        public static List<PersonInCharge> GetPersonInChargeByRequestId(string RequestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecords<PersonInCharge>("sub.Get_PersonInChargeByRequestId", parameters);
        }

        public static List<PersonInChargeAttachment> GetPersonInChargeAttachmentByRequestId(string RequestId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecords<PersonInChargeAttachment>("sub.Get_PersonInChargeAttachmentByRequestId", parameters);
        }

        public static string CreatePersonInCharge(PersonInCharge pic, string link)
        {
            string PersonInChargeId = "";
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, pic.EmailAddress);
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = pic.Name },
                new ParameterInfo() { ParameterName = "Type", ParameterValue = pic.Type },
                new ParameterInfo() { ParameterName = "IcNumber", ParameterValue = pic.IcNumber },
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = pic.RequestId },
                new ParameterInfo() { ParameterName = "IsPrimary", ParameterValue = pic.IsPrimary },
                new ParameterInfo() { ParameterName = "Designation", ParameterValue = pic.Designation },
                new ParameterInfo() { ParameterName = "EmailAddress", ParameterValue = pic.EmailAddress },
                new ParameterInfo() { ParameterName = "TelephoneNumber", ParameterValue = pic.TelephoneNumber },
                new ParameterInfo() { ParameterName = "Password", ParameterValue = hashSalt.Hash },
            };
            PersonInChargeId = SqlHelper.GetRecord<string>("sub.Create_PersonInCharge", parameters);

            if (!string.IsNullOrEmpty(PersonInChargeId) && pic.Type == "SA")
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "EmailTemplateId", ParameterValue = "6883873E-8051-4DE4-AD5B-A12342E51247" }
                };
                var EmailTemplate = SqlHelper.GetRecord<EmailTemplate>("cfg.Get_EmailTemplateByEmailTemplateId", parameters);

                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = pic.RequestId },
                };
                var Request = SqlHelper.GetRecord<Request>("req.Request_Get", parameters);

                if ((EmailTemplate != null) && EmailTemplate.Active && Request != null)
                {
                    if (!string.IsNullOrEmpty(pic.EmailAddress))
                    {
                        var Sender = new EmailSender.EmailSender();

                        Sender.SendCustomerEmail(pic.EmailAddress, link + PersonInChargeId + "&RequestId=" + Request.RequestId, EmailTemplate.Subject, EmailTemplate.Body, Request.ReferenceNo, Request.Subject, pic.Name, hashSalt.Hash);
                    }
                }
            }

            return PersonInChargeId;
        }

        public static PersonInCharge GetPersonInChargeByPersonInChargeId(string personInChargeId)
        {
            var parameter = new List<ParameterInfo>()
            {
                new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = personInChargeId },
            };
            return SqlHelper.GetRecord<PersonInCharge>("sub.Get_PersonInChargeByPersonInChargeId", parameter);
        }

        public static string CreatePersonInChargeAttachment(string requestId, string attachmentId, string personInChargeId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = requestId },
                new ParameterInfo() { ParameterName = "AttachmentId", ParameterValue = attachmentId },
                new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = personInChargeId },
            };
            return SqlHelper.GetRecord<string>("sub.Create_PersonInChargeAttachment", parameters);
        }

        public static bool UpdatePersonInChargeConsent(PersonInCharge personInCharge)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = personInCharge.PersonInChargeId },
                new ParameterInfo() { ParameterName = "Consented", ParameterValue = personInCharge.Consented },
            };
            return SqlHelper.GetRecord<bool>("sub.Update_PersonInChargeConsent", parameters);
        }

        public static bool ValidatePassword(string personInChargeId, string password)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "PersonInChargeId", ParameterValue = personInChargeId },
            };
            var sr = SqlHelper.GetRecord<PersonInCharge>("sub.Get_PersonInChargeByPersonInChargeId", parameters);

            if (sr != null)
            {
                if (sr.Password.Substring(0, 10) == password)
                {
                    return true;
                }
                return false;
            }

            return false;
        }
    }
}
