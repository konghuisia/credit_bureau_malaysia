﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class LegalRecommendActionControl
    {
        public static List<LegalRecommendAction> GetLegalRecommendActions()
        {
            return SqlHelper.GetRecords<LegalRecommendAction>("sub.Get_LegalRecommendActions", new List<ParameterInfo>());
        }
    }
}
