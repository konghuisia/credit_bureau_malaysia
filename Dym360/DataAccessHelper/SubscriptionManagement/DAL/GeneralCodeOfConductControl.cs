﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement.GeneralCodeOfConduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class GeneralCodeOfConductControl
    {
        public static GeneralCodeOfConduct GetGeneralCodeOfConductByRequestId(string RequestId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = RequestId }
            };
            return SqlHelper.GetRecord<GeneralCodeOfConduct>("sub.Get_GeneralCodeOfConduct", parameters) ?? new GeneralCodeOfConduct();
        }

        public static string UpdateGeneralCodeOfConduct(GeneralCodeOfConduct GeneralCodeOfConduct)
        {
            if (GeneralCodeOfConduct.IsSubmit)
            {

                var parameter = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "RequestId", ParameterValue = GeneralCodeOfConduct.RequestId },
                };
                SqlHelper.ExecuteQuery("sub.Update_SubmitGeneralCodeOfConduct", parameter);
            }

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "RequestId", ParameterValue = GeneralCodeOfConduct.RequestId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = GeneralCodeOfConduct.Name },
                new ParameterInfo() { ParameterName = "NRIC", ParameterValue = GeneralCodeOfConduct.NRIC },
                new ParameterInfo() { ParameterName = "Designation", ParameterValue = GeneralCodeOfConduct.Designation }
            };
            return SqlHelper.GetRecord<string>("sub.Update_GeneralCodeOfConduct", parameters);
        }
    }
}
