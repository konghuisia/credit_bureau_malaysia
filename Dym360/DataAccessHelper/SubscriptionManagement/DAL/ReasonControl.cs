﻿using DatabaseConnection.Data;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.DAL
{
    public class ReasonControl
    {
        public static List<Reason> GetReasons()
        {
            return SqlHelper.GetRecords<Reason>("sub.Get_Reasons", new List<ParameterInfo>());
        }
    }
}
