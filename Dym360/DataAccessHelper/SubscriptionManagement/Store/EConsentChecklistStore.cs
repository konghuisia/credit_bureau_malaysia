﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement.EConsentChecklist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class EConsentChecklistStore
    {
        public Task<EConsentChecklist> GetEConsentChecklistByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return EConsentChecklistControl.GetEConsentChecklistByRequestId(RequestId);
            });
        }

        public Task<string> UpdateConsentFormPeople(EConsentChecklist EConsentChecklist)
        {
            return Task.Factory.StartNew(() =>
            {
                return EConsentChecklistControl.UpdateEConsentChecklist(EConsentChecklist);
            });
        }
    }
}
