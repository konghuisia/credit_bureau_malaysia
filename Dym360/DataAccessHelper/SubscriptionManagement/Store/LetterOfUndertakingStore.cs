﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class LetterOfUndertakingStore
    {
        public Task<LetterOfUndertaking> GetLetterOfUndertakingByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return LetterOfUndertakingControl.GetLetterOfUndertakingByRequestId(RequestId) ?? new LetterOfUndertaking();
            });
        }

        public Task<string> UpdateLetterOfUndertaking(LetterOfUndertaking LetterOfUndertaking)
        {
            return Task.Factory.StartNew(() =>
            {
                return LetterOfUndertakingControl.UpdateLetterOfUndertaking(LetterOfUndertaking);
            });
        }
    }
}
