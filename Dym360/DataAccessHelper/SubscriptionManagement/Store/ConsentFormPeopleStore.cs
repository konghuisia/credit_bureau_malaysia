﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.ConsentForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class ConsentFormPeopleStore
    {
        public Task<List<ConsentFormPeople>> GetConsentFormPeopleByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ConsentFormPeopleControl.GetConsentFormPeopleByRequestId(RequestId) ?? new List<ConsentFormPeople>();
            });
        }

        public Task<string> UpdateConsentFormPeople(ConsentFormPeople ConsentFormPeople)
        {
            return Task.Factory.StartNew(() =>
            {
                return ConsentFormPeopleControl.UpdateConsentFormPeople(ConsentFormPeople);
            });
        }

        public Task<bool> DeleteConsentFormPeople(string ConsentFormPeopleId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ConsentFormPeopleControl.DeleteConsentFormPeople(ConsentFormPeopleId);
            });
        }
    }
}
