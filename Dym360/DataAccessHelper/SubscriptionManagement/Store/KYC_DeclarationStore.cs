﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class KYC_DeclarationStore
    {
        public Task<KYC_DeclarationViewModel> GetKYC_DeclarationByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return KYC_DeclarationControl.GetKYC_DeclarationByRequestId(RequestId);
            });
        }

        public Task<bool> UpdateKYC_DeclarationByRequestId(string RequestId, KYC_DeclarationViewModel kYC_Declaration)
        {
            return Task.Factory.StartNew(() =>
            {
                return KYC_DeclarationControl.UpdateKYC_DeclarationByRequestId(RequestId, kYC_Declaration);
            });
        }
    }
}
