﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class CustomerDetailStore
    {
        public Task<CustomerDetails> GetCustomerDetailsByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CustomerDetailControl.GetCustomerDetailsByRequestId(RequestId);
            });
        }
    }
}
