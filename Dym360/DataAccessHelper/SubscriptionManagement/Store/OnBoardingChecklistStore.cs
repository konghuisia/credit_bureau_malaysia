﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class OnBoardingChecklistStore
    {
        public Task<OnBoardingChecklist> GetOnBoardingChecklistByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return OnBoardingChecklistControl.GetOnBoardingChecklistByRequestId(RequestId) ?? new OnBoardingChecklist();
            });
        }

        public Task<string> UpdateOnBoardingChecklist(OnBoardingChecklist OnBoardingChecklist)
        {
            return Task.Factory.StartNew(() =>
            {
                return OnBoardingChecklistControl.UpdateOnBoardingChecklist(OnBoardingChecklist);
            });
        }
    }
}
