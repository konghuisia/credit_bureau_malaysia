﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class LegalRecommendActionStore
    {
        public Task<List<LegalRecommendAction>> GetLegalRecommendActions()
        {
            return Task.Factory.StartNew(() =>
            {
                return LegalRecommendActionControl.GetLegalRecommendActions()?.ToList() ?? new List<LegalRecommendAction>();
            });
        }
    }
}
