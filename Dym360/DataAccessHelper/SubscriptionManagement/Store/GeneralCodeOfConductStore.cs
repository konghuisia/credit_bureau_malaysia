﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement.GeneralCodeOfConduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class GeneralCodeOfConductStore
    {
        public Task<GeneralCodeOfConduct> GetGeneralCodeOfConductByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return GeneralCodeOfConductControl.GetGeneralCodeOfConductByRequestId(RequestId) ?? new GeneralCodeOfConduct();
            });
        }

        public Task<string> UpdateGeneralCodeOfConduct(GeneralCodeOfConduct GeneralCodeOfConduct)
        {
            return Task.Factory.StartNew(() =>
            {
                return GeneralCodeOfConductControl.UpdateGeneralCodeOfConduct(GeneralCodeOfConduct);
            });
        }
    }
}
