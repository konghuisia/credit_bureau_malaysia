﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class PersonInChargeStore
    {
        public Task<List<PersonInCharge>> GetPersonInChargeByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.GetPersonInChargeByRequestId(RequestId).ToList() ?? new List<PersonInCharge>();
            });
        }
        public Task<List<PersonInChargeAttachment>> GetPersonInChargeAttachmentByRequestId(string RequestId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.GetPersonInChargeAttachmentByRequestId(RequestId).ToList() ?? new List<PersonInChargeAttachment>();
            });
        }

        public Task<string> CreatePersonInCharge(PersonInCharge pic, string link)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.CreatePersonInCharge(pic, link);
            });
        }

        public Task<string> CreatePersonInChargeAttachment(string requestId, string attachmentId, string personInChargeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.CreatePersonInChargeAttachment(requestId, attachmentId, personInChargeId);
            });
        }

        public Task<PersonInCharge> GetPersonInChargeByPersonInChargeId(string personInChargeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.GetPersonInChargeByPersonInChargeId(personInChargeId) ?? new PersonInCharge();
            });
        }

        public Task<bool> UpdatePersonInChargeConsent(PersonInCharge PersonInCharge)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.UpdatePersonInChargeConsent(PersonInCharge);
            });
        }

        public Task<bool> ValidatePassword(string PersonInChargeId, string Password)
        {
            return Task.Factory.StartNew(() =>
            {
                return PersonInChargeControl.ValidatePassword(PersonInChargeId, Password);
            });
        }
    }
}
