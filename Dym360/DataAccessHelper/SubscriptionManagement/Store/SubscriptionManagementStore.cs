﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses;
using ObjectClasses.SubscriptionManagement;
using ObjectClasses.SubscriptionManagement.KYC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class SubscriptionManagementStore
    {
        public Task<bool> ValidatePassword(string requestId, string password)
        {
            if (!string.IsNullOrEmpty(requestId) && !string.IsNullOrEmpty(password))
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.ValidatePassword(requestId, password);
                });
            }
            throw new ArgumentNullException("validate");
        }

        public Task<bool> SubmitRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.NewRequest(req, subReq, cusDet);
                });
            }
            throw new ArgumentNullException("request");
        }


        public Task<bool> CustomerSubmitRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.CustomerSubmitRequest(req, subReq, cusDet);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> SaveRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.SaveRequest(req, subReq, cusDet);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> CustomerSaveRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.CustomerSaveRequest(req, subReq, cusDet);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> CustomerContinueRequest(Request req, SubscriptionRequest subReq, CustomerDetails cusDet)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.CustomerContinueRequest(req, subReq, cusDet);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RejectRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.RejectRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> ApproveRequest(Request req)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.ApproveRequest(req);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> Management(Request req, string action)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.Management(req, action);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> RequestMoreRequest(Request req, SubscriptionRequest subRequest)
        {
            if (req != null)
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.RequestMoreRequest(req, subRequest);
                });
            }
            throw new ArgumentNullException("request");
        }

        public void Dispose()
        {

        }

        public Task<SubscriptionRequest> GetSubscriptionRequest(string requestId)
        {
            if (!string.IsNullOrEmpty(requestId))
            {
                return Task.Factory.StartNew(() =>
                {
                    return SubscriptionManagementRequestControl.GetSubscriptionRequest(requestId);
                });
            }
            throw new ArgumentNullException("request");
        }

        public Task<bool> ValidatePassword(string requestId, object password)
        {
            throw new NotImplementedException();
        }
    }
}
