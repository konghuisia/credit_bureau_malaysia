﻿using DataAccessHelper.SubscriptionManagement.DAL;
using ObjectClasses.SubscriptionManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.SubscriptionManagement.Store
{
    public class ReasonStore
    {
        public Task<List<Reason>> GetReasons()
        {
            return Task.Factory.StartNew(() =>
            {
                return ReasonControl.GetReasons()?.ToList() ?? new List<Reason>();
            });
        }
    }
}
