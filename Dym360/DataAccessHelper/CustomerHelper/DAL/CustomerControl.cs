﻿using DatabaseConnection.Data;
using ObjectClasses.ProjectSalesManagement.Entities;
using ObjectClasses.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CustomerHelper.DAL
{
    public static class CustomerControl
    {
        public static Customer NewCustomer(Customer cust, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "Name", ParameterValue = cust.Name },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = cust.Email },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = cust.ContactNo },
                new ParameterInfo() { ParameterName = "CountryId", ParameterValue = cust.CountryId },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = cust.FaxNo },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = cust.Address },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId }
            };
            var custId = SqlHelper.GetRecord<string>("cust.Customer_New_G", parameters);

            cust.CustomerId = custId;

            return cust;

        }

        public static bool DeleteCustomer(string custId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = custId }
            };
            var success = SqlHelper.ExecuteQuery("cust.Customer_Delete", parameters);
            
            if (success > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IList<Customer> GetCustomers()
        {
            return SqlHelper.GetRecords<Customer>("cust.Customers_Get", new List<ParameterInfo>());
        }

        public static int NewContactPersons(Customer customer, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            var success = 0;
            foreach (var c in customer.ContactPersons)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "CustomerId", ParameterValue = customer.CustomerId },
                    new ParameterInfo() { ParameterName = "Name", ParameterValue = c.Name },
                    new ParameterInfo() { ParameterName = "Email", ParameterValue = c.Email },
                    new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = c.ContactNo },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId }
                };
                success = SqlHelper.ExecuteQuery("cust.CustomerContactPerson_New", parameters);
            }

            return success;
        }

        public static int DeleteContactPerson(string contactPersonId, string userId)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ContactPersonId", ParameterValue = contactPersonId },
                new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId }
            };
            var success = SqlHelper.ExecuteQuery("cust.CustomerContactPerson_Delete", parameters);

            return success;
        }

    }
}
