﻿
using DataAccessHelper.CustomerHelper.DAL;
using ObjectClasses.ProjectSalesManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.CustomerHelper.Store
{
    public class CustomerStore
    {
        public Task DeleteContactPerson(string contactPersonId, string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CustomerControl.DeleteContactPerson(contactPersonId, userId);
            });
        }

        public Task<IList<Customer>> GetCustomers()
        {
            return Task.Factory.StartNew(() =>
            {
                return CustomerControl.GetCustomers();
            });
        }

        public Task<Customer> NewCustomer(Customer customer, string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CustomerControl.NewCustomer(customer, userId);
            });
        }

        public Task<bool> DeleteCustomer(string custId)
        {
            return Task.Factory.StartNew(() =>
            {
                return CustomerControl.DeleteCustomer(custId);
            });
        }


    }
}
