﻿using DataAccessHelper.KumFattWebHelper.Employees.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Employees.Store
{
    public class EmployeeStore
    {
        public Task<string> InitNewEmployee(string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.InitNewEmployee(createdBy);
            });
        }

        public Task<bool> NewEmployee(Employee employee)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.NewEmployee(employee);
            });
        }

        public Task<bool> UpdateEmployee(Employee employee)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.UpdateEmployee(employee);
            });
        }

        public Task<bool> DeleteEmployee(Employee employee)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.DeleteEmployee(employee);
            });
        }

        public Task<Employee> GetEmployeeById(string employeeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.GetEmployeeById(employeeId);
            });
        }

        public Task<List<Employee>> EmployeeList()
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.EmployeeList()?.ToList() ?? new List<Employee>();
            });
        }
        public Task<List<Employee>> EmployeesWithoutTeam()
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.EmployeesWithoutTeam()?.ToList() ?? new List<Employee>();
            });
        }
        public Task<List<Employee>> EmployeesWithoutAccount()
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeControl.EmployeesWithoutAccount()?.ToList() ?? new List<Employee>();
            });
        }

    }
}
