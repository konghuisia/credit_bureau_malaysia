﻿using DataAccessHelper.KumFattWebHelper.Employees.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Employees.Store
{
    public class EmployeeDocumentStore
    {
        public Task NewProfileImage(ProfileImage image)
        {
            return Task.Factory.StartNew(() =>
            {
                EmployeeDocumentControl.NewProfileImage(image);
            });
        }

        public Task<EmployeeDocument> NewDocument(EmployeeDocument eDoc)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeDocumentControl.NewDocument(eDoc);
            });
        }
        public Task<bool> DeleteDocument(string documentId)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeDocumentControl.DeleteDocument(documentId);
            });
        }
    }
}
