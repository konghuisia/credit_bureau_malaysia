﻿using DatabaseConnection.Data;
using System;
using System.Collections.Generic;
using ObjectClasses.KumFattWeb;

namespace DataAccessHelper.KumFattWebHelper.Employees.DAL
{
    public static class EmployeeControl
    {
        private const string confType = "kumfatt";
        public static string InitNewEmployee(string createdBy)
        {
            return SqlHelper.GetRecord<string>("emp.Employee_New_Temp", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = createdBy } }, confType);
        }

        public static bool NewEmployee(Employee emp)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = emp.Name },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = emp.Email },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = emp.ContactNo },
                new ParameterInfo() { ParameterName = "OfficeContactNo", ParameterValue = emp.OfficeContactNo },
                new ParameterInfo() { ParameterName = "EmployeeTypeId", ParameterValue = emp.EmployeeTypeId },
                new ParameterInfo() { ParameterName = "Nationality", ParameterValue = emp.Detail.Nationality },
                new ParameterInfo() { ParameterName = "IdentityNo", ParameterValue = emp.Detail.IdentityNo },
                new ParameterInfo() { ParameterName = "DOB", ParameterValue = emp.Detail.DOB },
                new ParameterInfo() { ParameterName = "JoinDate", ParameterValue = emp.Detail.JoinDate },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = emp.Detail.Address },
                new ParameterInfo() { ParameterName = "HomeAddress", ParameterValue = emp.Detail.HomeAddress },
                new ParameterInfo() { ParameterName = "AllowanceTypeId", ParameterValue = emp.Allowance.AllowanceTypeId },
                new ParameterInfo() { ParameterName = "AllowanceAmount", ParameterValue = emp.Allowance.Amount },
                new ParameterInfo() { ParameterName = "AllowanceHourlyRate", ParameterValue = emp.Allowance.HourlyRate },
                new ParameterInfo() { ParameterName = "WageTypeId", ParameterValue = emp.Wage.WageTypeId },
                new ParameterInfo() { ParameterName = "WageAmount", ParameterValue = emp.Wage.Amount },
                new ParameterInfo() { ParameterName = "WageHourlyRate", ParameterValue = emp.Wage.HourlyRate },
                new ParameterInfo() { ParameterName = "IsEpfSocso", ParameterValue = emp.Wage.IsEpfSocso },
                new ParameterInfo() { ParameterName = "MonthlyWageTypeId", ParameterValue = emp.Wage.MonthlyWageTypeId },
                new ParameterInfo() { ParameterName = "IsActive", ParameterValue = emp.IsActive },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy },
                new ParameterInfo() { ParameterName = "MaritalStatus", ParameterValue = (int)emp.Detail.MaritalStatus },
                new ParameterInfo() { ParameterName = "ResignationDate", ParameterValue = emp.Detail.ResignationDate },
                new ParameterInfo() { ParameterName = "RunawayDate", ParameterValue = emp.Detail.RunawayDate },
                new ParameterInfo() { ParameterName = "JobTitleId", ParameterValue = emp.JobTitleId },
                new ParameterInfo() { ParameterName = "EMPName", ParameterValue = emp.EMPName },
                new ParameterInfo() { ParameterName = "EMPContactNo", ParameterValue = emp.EMPContactNo },
                new ParameterInfo() { ParameterName = "EMPRelationshipId", ParameterValue = emp.RelationshipId },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = emp.Detail.Remark },
                new ParameterInfo() { ParameterName = "AdvanceDate", ParameterValue = emp.AdvancePayment.AdvanceDate },
                new ParameterInfo() { ParameterName = "AdvanceAmount", ParameterValue = emp.AdvancePayment.AdvanceAmount },
                new ParameterInfo() { ParameterName = "RepaymentEffDate", ParameterValue = emp.AdvancePayment.RepaymentEffDate },
                new ParameterInfo() { ParameterName = "MonthlyRepaymentAmount", ParameterValue = emp.AdvancePayment.MonthlyRepaymentAmount },
                new ParameterInfo() { ParameterName = "MonthlyRepaymentType", ParameterValue = emp.AdvancePayment.MonthlyRepaymentType },
                new ParameterInfo() { ParameterName = "AgencyId", ParameterValue = emp.AgencyId },
                new ParameterInfo() { ParameterName = "OtherAgency", ParameterValue = emp.OtherAgency }
            };

            var employeeId = SqlHelper.GetRecord<string>("emp.Employee_New", parameters, confType);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = emp.QR.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = emp.QR.Path },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy }
            };

            SqlHelper.ExecuteQuery("emp.EmployeeQR_New", parameters, confType);

            foreach (var i in emp.Incentives)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                    new ParameterInfo() { ParameterName = "IncentiveTypeId", ParameterValue = i.IncentiveTypeId },
                    new ParameterInfo() { ParameterName = "Rate", ParameterValue = i.Rate },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.EmployeeIncentive_New", parameters, confType);
            }

            LoggerControl.LogAction(emp.AccessId, emp.EmployeeId, "Create", "Added new employee.", emp.CreatedBy);
            
            return string.IsNullOrEmpty(employeeId) ? false : true;
        }
        public static bool UpdateEmployee(Employee emp)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = emp.Name },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = emp.Email },
                new ParameterInfo() { ParameterName = "OfficeContactNo", ParameterValue = emp.OfficeContactNo },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = emp.ContactNo },
                new ParameterInfo() { ParameterName = "EmployeeTypeId", ParameterValue = emp.EmployeeTypeId },
                new ParameterInfo() { ParameterName = "Nationality", ParameterValue = emp.Detail.Nationality },
                new ParameterInfo() { ParameterName = "IdentityNo", ParameterValue = emp.Detail.IdentityNo },
                new ParameterInfo() { ParameterName = "DOB", ParameterValue = emp.Detail.DOB },
                new ParameterInfo() { ParameterName = "JoinDate", ParameterValue = emp.Detail.JoinDate },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = emp.Detail.Address },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = emp.Detail.Address },
                new ParameterInfo() { ParameterName = "HomeAddress", ParameterValue = emp.Detail.HomeAddress },
                new ParameterInfo() { ParameterName = "AllowanceTypeId", ParameterValue = emp.Allowance.AllowanceTypeId },
                new ParameterInfo() { ParameterName = "AllowanceAmount", ParameterValue = emp.Allowance.Amount },
                new ParameterInfo() { ParameterName = "AllowanceHourlyRate", ParameterValue = emp.Allowance.HourlyRate },
                new ParameterInfo() { ParameterName = "WageTypeId", ParameterValue = emp.Wage.WageTypeId },
                new ParameterInfo() { ParameterName = "WageAmount", ParameterValue = emp.Wage.Amount },
                new ParameterInfo() { ParameterName = "WageHourlyRate", ParameterValue = emp.Wage.HourlyRate },
                new ParameterInfo() { ParameterName = "IsEpfSocso", ParameterValue = emp.Wage.IsEpfSocso },
                new ParameterInfo() { ParameterName = "MonthlyWageTypeId", ParameterValue = emp.Wage.MonthlyWageTypeId },
                new ParameterInfo() { ParameterName = "IsActive", ParameterValue = emp.IsActive },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy },
                new ParameterInfo() { ParameterName = "MaritalStatus", ParameterValue = (int)emp.Detail.MaritalStatus },
                new ParameterInfo() { ParameterName = "ResignationDate", ParameterValue = emp.Detail.ResignationDate },
                new ParameterInfo() { ParameterName = "RunawayDate", ParameterValue = emp.Detail.RunawayDate },
                new ParameterInfo() { ParameterName = "JobTitleId", ParameterValue = emp.JobTitleId },
                new ParameterInfo() { ParameterName = "EMPName", ParameterValue = emp.EMPName },
                new ParameterInfo() { ParameterName = "EMPContactNo", ParameterValue = emp.EMPContactNo },
                new ParameterInfo() { ParameterName = "EMPRelationshipId", ParameterValue = emp.RelationshipId },
                new ParameterInfo() { ParameterName = "Remark", ParameterValue = emp.Detail.Remark },
                new ParameterInfo() { ParameterName = "AdvanceDate", ParameterValue = emp.AdvancePayment.AdvanceDate },
                new ParameterInfo() { ParameterName = "AdvanceAmount", ParameterValue = emp.AdvancePayment.AdvanceAmount },
                new ParameterInfo() { ParameterName = "RepaymentEffDate", ParameterValue = emp.AdvancePayment.RepaymentEffDate },
                new ParameterInfo() { ParameterName = "MonthlyRepaymentAmount", ParameterValue = emp.AdvancePayment.MonthlyRepaymentAmount },
                new ParameterInfo() { ParameterName = "MonthlyRepaymentType", ParameterValue = emp.AdvancePayment.MonthlyRepaymentType },
                new ParameterInfo() { ParameterName = "AgencyId", ParameterValue = emp.AgencyId },
                new ParameterInfo() { ParameterName = "OtherAgency", ParameterValue = emp.OtherAgency }
            };

            var employeeId = SqlHelper.GetRecord<string>("emp.Employee_Update", parameters, confType);

            foreach (var i in emp.Incentives)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                    new ParameterInfo() { ParameterName = "IncentiveTypeId", ParameterValue = i.IncentiveTypeId },
                    new ParameterInfo() { ParameterName = "Rate", ParameterValue = i.Rate },
                    new ParameterInfo() { ParameterName = "DeletedFlag", ParameterValue = i.DeletedFlag },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.EmployeeIncentive_Update", parameters, confType);
            }

            foreach (var d in emp.Documents)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "EmployeeDocumentId", ParameterValue = d.EmployeeDocumentId }
                };
                SqlHelper.ExecuteQuery("emp.EmployeeDocument_Delete", parameters, confType);
            }

            LoggerControl.LogAction(emp.AccessId, emp.EmployeeId, "Update", "Edited existing employee.", emp.CreatedBy);

            return string.IsNullOrEmpty(employeeId) ? false : true;
        }
        public static bool DeleteEmployee(Employee emp)
        {
            var parameters = new List<ParameterInfo> {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = emp.EmployeeId },
                 new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = emp.CreatedBy }
            };

            LoggerControl.LogAction(emp.AccessId, emp.EmployeeId, "Delete", "Removed existing employee.", emp.CreatedBy);

            return SqlHelper.ExecuteQuery("emp.Employee_Delete", parameters, confType) > 0 ? true : false;
        }

        public static Employee GetEmployeeById(string employeeId)
        {
            var parameters = new List<ParameterInfo> { new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = employeeId } };
            var employee = new Employee();

            employee = SqlHelper.GetEmployee("emp.Employee_Get_By_Id", parameters, confType);
            employee.Team = SqlHelper.GetRecord<Team>("emp.Team_Get_By_EmployeeId", parameters, confType);
            employee.Documents = SqlHelper.GetRecords<EmployeeDocument>("emp.EmployeeDocument_Get_All", parameters, confType);
            employee.Incentives = SqlHelper.GetRecords<EmployeeIncentive>("emp.EmployeeIncentive_Get_By_EmployeeId", parameters, confType);
            return employee;
        }
        public static IList<Employee> EmployeeList()
        {
            return SqlHelper.GetEmployees("emp.Employee_Get_All", new List<ParameterInfo>(), confType);
        }

        public static IList<Employee> EmployeesWithoutTeam()
        {
            return SqlHelper.GetEmployees("emp.Employee_Get_Without_Team", new List<ParameterInfo>(), confType);
        }
        public static IList<Employee> EmployeesWithoutAccount()
        {
            return SqlHelper.GetRecords<Employee>("emp.Employee_Get_Without_Account", new List<ParameterInfo>(), confType);
        }
    }
}
