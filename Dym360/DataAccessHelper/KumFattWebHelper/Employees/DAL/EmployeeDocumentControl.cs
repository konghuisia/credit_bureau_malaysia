﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Employees.DAL
{
    public static class EmployeeDocumentControl
    {
        private const string confType = "kumfatt";
        public static void NewProfileImage(ProfileImage image)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = image.EmployeeId },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = image.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = image.Path },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = image.CreatedBy }
            };
            SqlHelper.GetRecord<string>("emp.EmployeePicture_New", parameters, confType);
        }
        public static EmployeeDocument NewDocument(EmployeeDocument eDoc)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = eDoc.EmployeeId },
                new ParameterInfo() { ParameterName = "DocumentTypeId", ParameterValue = eDoc.DocumentTypeId },
                new ParameterInfo() { ParameterName = "DocumentName", ParameterValue = eDoc.Name },
                new ParameterInfo() { ParameterName = "DocumentNo", ParameterValue = eDoc.No },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = eDoc.Path },
                new ParameterInfo() { ParameterName = "ExpiryDate", ParameterValue = eDoc.ExpiryDate },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = eDoc.CreatedBy }
            };
            eDoc.EmployeeDocumentId = SqlHelper.GetRecord<string>("emp.EmployeeDocument_New", parameters, confType);

            return eDoc;
        }

        public static bool DeleteDocument(string documentId)
        {
            return SqlHelper.ExecuteQuery("emp.EmployeeDocument_Delete", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "EmployeeDocumentId", ParameterValue = documentId } }, confType) > 0 ? true : false;
        }
    }
}
