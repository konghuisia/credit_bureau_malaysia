﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class IncentiveTypeStore
    {
        public Task<List<IncentiveType>> GetIncentiveTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return IncentiveTypeControl.GetAll()?.ToList() ?? new List<IncentiveType>();
            });
        }
    }
}
