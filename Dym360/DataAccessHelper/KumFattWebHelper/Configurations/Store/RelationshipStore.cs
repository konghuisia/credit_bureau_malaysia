﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses.KumFattWeb;
using DataAccessHelper.KumFattWebHelper.Configurations.DAL;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class RelationshipStore
    {
        public Task<List<Relationship>> RelationshipList()
        {
            return Task.Factory.StartNew(() =>
            {
                return RelationshipControl.GetAll()?.ToList() ?? new List<Relationship>();
            });
        }
        
        public Task<bool> NewRelationship(Relationship r, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return RelationshipControl.NewRelationship(r, createdBy);
            });
        }

        public Task<bool> UpdateRelationship(Relationship r, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return RelationshipControl.UpdateRelationship(r, createdBy);
            });
        }

        public Task<bool> DeleteRelationship(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return RelationshipControl.DeleteRelationship(Id, createdBy);
            });
        }

    }
}
