﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class DocumentTypeStore
    {
        public Task<List<DocumentType>> GetDocumentTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.GetAll()?.ToList() ?? new List<DocumentType>();
            });
        }

        public Task<bool> NewDocumentType(DocumentType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.NewDocumentType(e, createdBy);
            });
        }

        public Task<bool> UpdateDocumentType(DocumentType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.UpdateDocumentType(e, createdBy);
            });
        }
        public Task<bool> DeleteDocumentType(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return DocumentTypeControl.DeleteDocumentType(Id, createdBy);
            });
        }
    }
}
