﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class AccessStore
    {
        public Task<List<Access>> AccessList()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.AccessList()?.ToList() ?? new List<Access>();
            });
        }

        public Task<List<AccountType>> AccountTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return AccessControl.AccountTypes()?.ToList() ?? new List<AccountType>();
            });
        }
    }
}
