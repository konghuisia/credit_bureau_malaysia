﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class ProjectStatusStore
    {
        public Task<List<ProjectStatus>> ProjectStatuses()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectStatusControl.ProjectStatuses()?.ToList() ?? new List<ProjectStatus>();
            });
        }
        public Task<bool> NewProjectStatus(ProjectStatus e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectStatusControl.NewProjectStatus(e, createdBy);
            });
        }
        //public Task<bool> UpdateCountry(Country e, string createdBy)
        //{
        //    return Task.Factory.StartNew(() =>
        //    {
        //        return CountryControl.UpdateCountry(e, createdBy);
        //    });
        //}
        public Task<bool> DeleteProjectStatus(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectStatusControl.DeleteProjectStatus(Id, createdBy);
            });
        }
    }
}
