﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class SourceTypeStore
    {
        public Task<List<SourceType>> GetSourceTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return SourceTypeControl.GetAll()?.ToList() ?? new List<SourceType>();
            });
        }
    }
}
