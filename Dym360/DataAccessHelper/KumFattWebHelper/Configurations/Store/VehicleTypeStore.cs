﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class VehicleTypeStore
    {
        public Task<List<VehicleType>> GetVehicleTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return VehicleTypeControl.GetAll()?.ToList() ?? new List<VehicleType>();
            });
        }

        public Task<bool> NewVehicleType(VehicleType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return VehicleTypeControl.NewVehicleType(e, createdBy);
            });
        }

        public Task<bool> UpdateVehicleType(VehicleType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return VehicleTypeControl.UpdateVehicleType(e, createdBy);
            });
        }

        public Task<bool> DeleteVehicleType(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return VehicleTypeControl.DeleteVehicleType(Id, createdBy);
            });
        }
    }
}
