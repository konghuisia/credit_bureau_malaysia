﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses.KumFattWeb;
using DataAccessHelper.KumFattWebHelper.Configurations.DAL;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class JobTitleStore
    {
        public Task<List<JobTitle>> JobTitleList()
        {
            return Task.Factory.StartNew(() =>
            {
                return JobTitleControl.GetAll()?.ToList() ?? new List<JobTitle>();
            });
        }

        public Task<bool> NewJobTitle(JobTitle r, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return JobTitleControl.NewJobTitle(r, createdBy);
            });
        }

        public Task<bool> UpdateJobTitle(JobTitle r, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return JobTitleControl.UpdateJobTitle(r, createdBy);
            });
        }

        public Task<bool> DeleteJobTitle(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return JobTitleControl.DeleteJobTitle(Id, createdBy);
            });
        }
    }
}
