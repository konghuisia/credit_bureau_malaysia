﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class AllowanceTypeStore
    {
        public Task<List<AllowanceType>> GetAllowanceTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return AllowanceTypeControl.GetAll()?.ToList() ?? new List<AllowanceType>();
            });
        }
    }
}
