﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class AgencyStore
    {
        public Task<List<Agency>> AgencyList()
        {
            return Task.Factory.StartNew(() =>
            {
                return AgencyControl.AgencyList()?.ToList() ?? new List<Agency>();
            });
        }

        public Task<bool> NewAgency(Agency ag, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return AgencyControl.NewAgency(ag, createdBy);
            });
        }

        public Task<bool> UpdateAgency(Agency ag, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return AgencyControl.UpdateAgency(ag, createdBy);
            });
        }

        public Task<bool> DeleteAgency(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return AgencyControl.DeleteAgency(Id, createdBy);
            });
        }
    }
}
