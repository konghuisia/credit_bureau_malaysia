﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class EmployeeTypeStore
    {
        public Task<List<EmployeeType>> GetEmployeeTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeTypeControl.GetAll().ToList() ?? new List<EmployeeType>();
            });
        }
        public Task<bool> NewEmployeeType(EmployeeType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeTypeControl.NewEmployeeType(e, createdBy);
            });
        }

        public Task<bool> UpdateEmployeeType(EmployeeType e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeTypeControl.UpdateEmployeeType(e, createdBy);
            });
        }
        public Task<bool> DeleteEmployeeType(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return EmployeeTypeControl.DeleteEmployeeType(Id, createdBy);
            });
        }
    }
}
