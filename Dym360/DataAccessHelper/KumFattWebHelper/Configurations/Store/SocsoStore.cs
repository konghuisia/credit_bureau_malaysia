﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class SocsoStore
    {
        public Task<List<Socso>> SocsoList()
        {
            return Task.Factory.StartNew(() =>
            {
                return SocsoControl.SocsoList()?.ToList() ?? new List<Socso>();
            });
        }
        public Task<bool> NewSocso(Socso e)
        {
            return Task.Factory.StartNew(() =>
            {
                return SocsoControl.NewSocso(e);
            });
        }
        public Task<bool> UpdateSocso(Socso e)
        {
            return Task.Factory.StartNew(() =>
            {
                return SocsoControl.UpdateSocso(e);
            });
        }
        public Task<bool> DeleteSocso(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return SocsoControl.DeleteSocso(Id, createdBy);
            });
        }
    }
}
