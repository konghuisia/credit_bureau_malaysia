﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class MonthlyWageTypeStore
    {
        public Task<List<MonthlyWageType>> GetMonthlyWageTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return MonthlyWageTypeControl.GetAll()?.ToList() ?? new List<MonthlyWageType>();
            });
        }
    }
}
