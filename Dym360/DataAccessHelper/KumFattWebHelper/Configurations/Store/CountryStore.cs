﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class CountryStore
    {
        public Task<List<Country>> CountryList()
        {
            return Task.Factory.StartNew(() =>
            {
                return CountryControl.CountryList()?.ToList() ?? new List<Country>();
            });
        }
        public Task<bool> NewCountry(Country e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return CountryControl.NewCountry(e, createdBy);
            });
        }
        public Task<bool> UpdateCountry(Country e, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return CountryControl.UpdateCountry(e, createdBy);
            });
        }
        public Task<bool> DeleteCountry(string Id, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return CountryControl.DeleteCountry(Id, createdBy);
            });
        }
    }
}
