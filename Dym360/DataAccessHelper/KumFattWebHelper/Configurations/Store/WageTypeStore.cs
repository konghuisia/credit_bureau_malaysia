﻿using DataAccessHelper.KumFattWebHelper.Configurations.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.Store
{
    public class WageTypeStore
    {
        public Task<List<WageType>> GetWageTypes()
        {
            return Task.Factory.StartNew(() =>
            {
                return WageTypeControl.GetAll()?.ToList() ?? new List<WageType>();
            });
        }
    }
}
