﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public class RelationshipControl
    {private const string confType = "kumfatt";
        public static IList<Relationship> GetAll()
        {
            return SqlHelper.GetRecords<Relationship>("cfg.Relationship_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewRelationship(Relationship r, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = r.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = r.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = r.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Relationship_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateRelationship(Relationship r, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = r.RelationshipId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = r.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = r.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = r.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Relationship_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteRelationship(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Relationship_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
