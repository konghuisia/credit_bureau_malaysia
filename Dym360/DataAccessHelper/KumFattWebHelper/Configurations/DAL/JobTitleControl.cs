﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public class JobTitleControl
    {
        private const string confType = "kumfatt";
        public static IList<JobTitle> GetAll()
        {
            return SqlHelper.GetRecords<JobTitle>("cfg.JobTitle_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewJobTitle(JobTitle jt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = jt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = jt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = jt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.JobTitle_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateJobTitle(JobTitle jt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = jt.JobTitleId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = jt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = jt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = jt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.JobTitle_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteJobTitle(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.JobTitle_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
