﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class DocumentTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<DocumentType> GetAll()
        {
            return SqlHelper.GetRecords<DocumentType>("cfg.DocumentType_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewDocumentType(DocumentType dt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = dt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = dt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = dt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.DocumentType_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateDocumentType(DocumentType dt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = dt.DocumentTypeId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = dt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = dt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = dt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.DocumentType_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteDocumentType(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.DocumentType_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
