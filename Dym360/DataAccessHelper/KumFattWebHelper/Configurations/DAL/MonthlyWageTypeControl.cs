﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public class MonthlyWageTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<MonthlyWageType> GetAll()
        {
            return SqlHelper.GetRecords<MonthlyWageType>("cfg.MonthlyWageType_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
