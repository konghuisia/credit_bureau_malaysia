﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class EmployeeTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<EmployeeType> GetAll()
        {
            return SqlHelper.GetRecords<EmployeeType>("cfg.EmployeeType_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewEmployeeType(EmployeeType et, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = et.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = et.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = et.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.EmployeeType_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateEmployeeType(EmployeeType et, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = et.EmployeeTypeId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = et.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = et.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = et.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.EmployeeType_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteEmployeeType(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.EmployeeType_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
