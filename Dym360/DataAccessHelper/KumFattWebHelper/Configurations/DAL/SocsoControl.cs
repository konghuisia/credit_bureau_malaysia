﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class SocsoControl
    {
        private const string confType = "kumfatt";
        public static IList<Socso> SocsoList()
        {
            return SqlHelper.GetRecords<Socso>("cfg.Socso_Get_All", new List<ParameterInfo>(), confType);
        }
        public static bool NewSocso(Socso c)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = c.Name },
                new ParameterInfo(){ ParameterName = "Greater", ParameterValue = c.Greater },
                new ParameterInfo(){ ParameterName = "Lesser", ParameterValue = c.Lesser },
                new ParameterInfo(){ ParameterName = "Rate", ParameterValue = c.Rate },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = c.CreatedBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Socso_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateSocso(Socso c)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = c.SocsoId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = c.Name },
                new ParameterInfo(){ ParameterName = "Greater", ParameterValue = c.Greater },
                new ParameterInfo(){ ParameterName = "Lesser", ParameterValue = c.Lesser },
                new ParameterInfo(){ ParameterName = "Rate", ParameterValue = c.Rate },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = c.CreatedBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Socso_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteSocso(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Socso_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
