﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class IncentiveTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<IncentiveType> GetAll()
        {
            return SqlHelper.GetRecords<IncentiveType>("cfg.IncentiveType_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
