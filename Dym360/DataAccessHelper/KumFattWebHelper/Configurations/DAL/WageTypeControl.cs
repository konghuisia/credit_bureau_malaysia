﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class WageTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<WageType> GetAll()
        {
            return SqlHelper.GetRecords<WageType>("cfg.WageType_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
