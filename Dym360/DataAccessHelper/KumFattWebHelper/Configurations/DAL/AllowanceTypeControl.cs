﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public class AllowanceTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<AllowanceType> GetAll()
        {
            return SqlHelper.GetRecords<AllowanceType>("cfg.AllowanceType_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
