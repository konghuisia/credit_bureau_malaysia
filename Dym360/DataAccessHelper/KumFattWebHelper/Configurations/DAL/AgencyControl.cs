﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public class AgencyControl
    {
        private const string confType = "kumfatt";
        public static IList<Agency> AgencyList()
        {
            return SqlHelper.GetRecords<Agency>("cfg.Agency_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewAgency(Agency ag, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = ag.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = ag.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = ag.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Agency_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateAgency(Agency ag, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = ag.AgencyId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = ag.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = ag.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = ag.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Agency_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteAgency(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Agency_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
