﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class SourceTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<SourceType> GetAll()
        {
            return SqlHelper.GetRecords<SourceType>("cfg.SourceType_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
