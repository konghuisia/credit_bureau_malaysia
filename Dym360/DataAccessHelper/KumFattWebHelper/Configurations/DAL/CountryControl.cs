﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class CountryControl
    {
        private const string confType = "kumfatt";
        public static IList<Country> CountryList()
        {
            return SqlHelper.GetRecords<Country>("cfg.Country_Get_All", new List<ParameterInfo>(), confType);
        }
        public static bool NewCountry(Country c, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = c.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = c.Keyword },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Country_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateCountry(Country c, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = c.CountryId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = c.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = c.Keyword },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Country_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteCountry(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.Country_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
