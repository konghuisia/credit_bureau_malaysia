﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class VehicleTypeControl
    {
        private const string confType = "kumfatt";
        public static IList<VehicleType> GetAll()
        {
            return SqlHelper.GetRecords<VehicleType>("cfg.VehicleType_Get_All", new List<ParameterInfo>(), confType);
        }

        public static bool NewVehicleType(VehicleType vt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = vt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = vt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = vt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.VehicleType_New", parameters, confType) > 0 ? true : false;
        }

        public static bool UpdateVehicleType(VehicleType vt, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = vt.VehicleTypeId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = vt.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = vt.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = vt.Description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.VehicleType_Update", parameters, confType) > 0 ? true : false;
        }

        public static bool DeleteVehicleType(string Id, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Id", ParameterValue = Id },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.ExecuteQuery("cfg.VehicleType_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
