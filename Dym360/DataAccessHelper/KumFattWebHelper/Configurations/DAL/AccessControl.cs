﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Configurations.DAL
{
    public static class AccessControl
    {
        public static IList<Access> AccessList()
        {
            return SqlHelper.GetRecords<Access>("cfg.Access_Get_All", new List<ParameterInfo>());
        }
        public static IList<AccountType> AccountTypes()
        {
            return SqlHelper.GetRecords<AccountType>("acc.AccountTypes_Get", new List<ParameterInfo>());
        }
    }
}
