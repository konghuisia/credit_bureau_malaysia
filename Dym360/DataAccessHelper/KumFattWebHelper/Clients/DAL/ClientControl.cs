﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Clients.DAL
{
    public static class ClientControl
    {
        private const string confType = "kumfatt";

        public static IList<Client> ClientList()
        {
            return SqlHelper.GetRecords<Client>("cus.Client_Get_All", new List<ParameterInfo>(), confType);
        }

        public static string NewClient(Client client)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>{
                new ParameterInfo() { ParameterName = "Name", ParameterValue = client.Name },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = client.FaxNo },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = client.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = client.Email },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = client.Address },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = client.CreatedBy },
            };

            client.ClientId = SqlHelper.GetRecord<string>("cus.Client_New", parameters, confType);

            LoggerControl.LogAction(client.AccessId, client.ClientId, "Create", "Added new client.", client.CreatedBy);

            return client.ClientId;
        }

        public static bool UpdateClient(Client client)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>{
                 new ParameterInfo() { ParameterName = "ClientId", ParameterValue = client.ClientId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = client.Name },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = client.FaxNo },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = client.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = client.Email },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = client.Address },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = client.CreatedBy },
            };
            
            LoggerControl.LogAction(client.AccessId, client.ClientId, "Update", "Edited existing client.", client.CreatedBy);

            return SqlHelper.ExecuteQuery("cus.Client_Update", parameters, confType) > 0 ? true : false ;
        }

        public static Client GetClientById(string clientId)
        {
            var cl = new Client();

            cl = SqlHelper.GetRecord<Client>("cus.Client_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ClientId", ParameterValue = clientId } }, confType);
          
            return cl;
        }
        
        public static bool DeleteClient(Client cl)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ClientId", ParameterValue = cl.ClientId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = cl.CreatedBy }
            };
            LoggerControl.LogAction(cl.AccessId, cl.ClientId, "Delete", "Removed existing client.", cl.CreatedBy);

            return SqlHelper.ExecuteQuery("cus.Client_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
