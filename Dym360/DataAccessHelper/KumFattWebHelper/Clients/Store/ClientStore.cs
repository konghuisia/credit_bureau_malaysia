﻿using DataAccessHelper.KumFattWebHelper.Clients.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Clients.Store
{
    public class ClientStore
    {
        public Task<List<Client>> ClientList()
        {
            return Task.Factory.StartNew(() =>
            {
                return ClientControl.ClientList()?.ToList() ?? new List<Client>();
            });
        }

        public Task<string> NewClient(Client client)
        {
            return Task.Factory.StartNew(() =>
            {
                return ClientControl.NewClient(client);
            });
        }

        public Task<bool> UpdateClient(Client client)
        {
            return Task.Factory.StartNew(() =>
            {
                return ClientControl.UpdateClient(client);
            });
        }

        public Task<bool> DeleteClient(Client client)
        {
            return Task.Factory.StartNew(() =>
            {
                return ClientControl.DeleteClient(client);
            });
        }

        public Task<Client> GetClientById(string clientId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ClientControl.GetClientById(clientId);
            });
        }
    }
}
