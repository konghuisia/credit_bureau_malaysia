﻿using DataAccessHelper.KumFattWebHelper.Contractors.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Contractors.Store
{
    public class ContractorStore
    {
        public Task<List<Contractor>> ContractorList()
        {
            return Task.Factory.StartNew(() =>
            {
                return ContractorControl.ContractorList()?.ToList() ?? new List<Contractor>();
            });
        }

        public Task<string> NewContractor(Contractor Contractor)
        {
            return Task.Factory.StartNew(() =>
            {
                return ContractorControl.NewContractor(Contractor);
            });
        }

        public Task<bool> UpdateContractor(Contractor Contractor)
        {
            return Task.Factory.StartNew(() =>
            {
                return ContractorControl.UpdateContractor(Contractor);
            });
        }

        public Task<bool> DeleteContractor(Contractor Contractor)
        {
            return Task.Factory.StartNew(() =>
            {
                return ContractorControl.DeleteContractor(Contractor);
            });
        }

        public Task<Contractor> GetContractorById(string ContractorId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ContractorControl.GetContractorById(ContractorId);
            });
        }
    }
}
