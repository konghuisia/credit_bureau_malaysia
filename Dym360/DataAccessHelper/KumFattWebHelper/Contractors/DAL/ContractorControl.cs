﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Contractors.DAL
{
    public static class ContractorControl
    {
        private const string confType = "kumfatt";

        public static IList<Contractor> ContractorList()
        {
            return SqlHelper.GetRecords<Contractor>("cus.Contractor_Get_All", new List<ParameterInfo>(), confType);
        }

        public static string NewContractor(Contractor Contractor)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>{
                new ParameterInfo() { ParameterName = "Name", ParameterValue = Contractor.Name },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = Contractor.FaxNo },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = Contractor.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = Contractor.Email },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = Contractor.Address },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = Contractor.CreatedBy },
            };

            Contractor.ContractorId = SqlHelper.GetRecord<string>("cus.Contractor_New", parameters, confType);

            LoggerControl.LogAction(Contractor.AccessId, Contractor.ContractorId, "Create", "Added new Contractor.", Contractor.CreatedBy);

            return Contractor.ContractorId;
        }

        public static bool UpdateContractor(Contractor Contractor)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>{
                 new ParameterInfo() { ParameterName = "ContractorId", ParameterValue = Contractor.ContractorId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = Contractor.Name },
                new ParameterInfo() { ParameterName = "FaxNo", ParameterValue = Contractor.FaxNo },
                new ParameterInfo() { ParameterName = "ContactNo", ParameterValue = Contractor.ContactNo },
                new ParameterInfo() { ParameterName = "Email", ParameterValue = Contractor.Email },
                new ParameterInfo() { ParameterName = "Address", ParameterValue = Contractor.Address },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = Contractor.CreatedBy },
            };
            
            LoggerControl.LogAction(Contractor.AccessId, Contractor.ContractorId, "Update", "Edited existing Contractor.", Contractor.CreatedBy);

            return SqlHelper.ExecuteQuery("cus.Contractor_Update", parameters, confType) > 0 ? true : false ;
        }

        public static Contractor GetContractorById(string ContractorId)
        {
            var cl = new Contractor();

            cl = SqlHelper.GetRecord<Contractor>("cus.Contractor_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ContractorId", ParameterValue = ContractorId } }, confType);
          
            return cl;
        }
        
        public static bool DeleteContractor(Contractor cl)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ContractorId", ParameterValue = cl.ContractorId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = cl.CreatedBy }
            };
            LoggerControl.LogAction(cl.AccessId, cl.ContractorId, "Delete", "Removed existing Contractor.", cl.CreatedBy);

            return SqlHelper.ExecuteQuery("cus.Contractor_Delete", parameters, confType) > 0 ? true : false;
        }
    }
}
