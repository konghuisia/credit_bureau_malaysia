﻿using DatabaseConnection.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper
{
    public static class LoggerControl
    {
        private const string confType = "kumfatt";
        public static void LogAction(string accessId, string referenceId, string action, string description, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = accessId },
                new ParameterInfo(){ ParameterName = "ReferenceId", ParameterValue = referenceId },
                new ParameterInfo(){ ParameterName = "Action", ParameterValue = action },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = description },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            SqlHelper.ExecuteQuery("cfg.Logger_New", parameters, confType);
        }
    }
}
