﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System.Collections.Generic;

namespace DataAccessHelper.KumFattWebHelper.ProjectSites.DAL
{
    public static class ProjectSiteControl
    {
        private const string confType = "kumfatt";

        public static string InitNewProjectSite(string createdBy)
        {
            return SqlHelper.GetRecord<string>("prj.Project_New_Temp", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = createdBy } }, confType);
        }

        public static bool NewProjectSite(ProjectSite prj)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = prj.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = prj.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = prj.Description },
                new ParameterInfo(){ ParameterName = "ClientId", ParameterValue = prj.ClientId },
                new ParameterInfo(){ ParameterName = "PersonInCharge", ParameterValue = prj.PersonInCharge },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = prj.Status },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = prj.Remark },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
            };
            SqlHelper.ExecuteQuery("prj.Project_New", parameters, confType);
            
            foreach (var t in prj.Teams)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "TeamId", ParameterValue = t.TeamId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectTeam_New", parameters, confType);
            }
            foreach (var c in prj.Contractors)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "ContractorId", ParameterValue = c.ContractorId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectTeam_New", parameters, confType);
            }

            foreach (var e in prj.Engineers)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "EmployeeId", ParameterValue = e.EmployeeId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectEngineer_New", parameters, confType);
            }

            LoggerControl.LogAction(prj.AccessId, prj.ProjectId, "Create", "Added new project site.", prj.CreatedBy);

            return string.IsNullOrEmpty(prj.ProjectId) ? false : true;
        }

        public static bool UpdateProjectSite(ProjectSite prj)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            foreach (var e in prj.Engineers)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "EmployeeId", ParameterValue = e.EmployeeId },
                    new ParameterInfo(){ ParameterName = "DeletedFlag", ParameterValue = e.DeletedFlag },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectEngineer_Update", parameters, confType);
            }

            foreach (var t in prj.Teams)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "TeamId", ParameterValue = t.TeamId },
                    new ParameterInfo(){ ParameterName = "DeletedFlag", ParameterValue = t.DeletedFlag },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectTeam_Update", parameters, confType);
            }
            foreach (var c in prj.Contractors)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                    new ParameterInfo(){ ParameterName = "ContractorId", ParameterValue = c.ContractorId },
                    new ParameterInfo(){ ParameterName = "DeletedFlag", ParameterValue = c.DeletedFlag },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.ProjectContractor_Update", parameters, confType);
            }
            foreach (var q in prj.Quotations)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "QuotationId", ParameterValue = q.QuotationId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.Quotation_Delete", parameters, confType);
            }

            foreach (var p in prj.PurchaseOrders)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "PurchaseOrderId", ParameterValue = p.PurchaseOrderId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.PurchaseOrder_Delete", parameters, confType);
            }

            foreach (var l in prj.LetterOfClaims)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "LetterOfClaimId", ParameterValue = l.LetterOfClaimId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.LetterOfClaim_Delete", parameters, confType);
            }

            foreach (var i in prj.TaxInvoices)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "TaxInvoiceId", ParameterValue = i.TaxInvoiceId },
                    new ParameterInfo(){ ParameterName = "Status", ParameterValue = i.Status },
                    new ParameterInfo(){ ParameterName = "DeletedFlag", ParameterValue = i.DeletedFlag },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
                };
                SqlHelper.ExecuteQuery("prj.TaxInvoice_Update", parameters, confType);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = prj.Name },
                new ParameterInfo(){ ParameterName = "Keyword", ParameterValue = prj.Keyword },
                new ParameterInfo(){ ParameterName = "Description", ParameterValue = prj.Description },
                new ParameterInfo(){ ParameterName = "ClientId", ParameterValue = prj.ClientId },
                new ParameterInfo(){ ParameterName = "PersonInCharge", ParameterValue = prj.PersonInCharge },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = prj.Status },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = prj.Remark },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
            };

            LoggerControl.LogAction(prj.AccessId, prj.ProjectId, "Update", "Edited existing project site.", prj.CreatedBy);

            return SqlHelper.ExecuteQuery("prj.Project_Update", parameters, confType) > 0 ? true : false;
        }

        public static Document NewDocument(Document prjDoc, string projectId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = prjDoc.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = prjDoc.Path },
                new ParameterInfo() { ParameterName = "FileSize", ParameterValue = prjDoc.FileSize },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = prjDoc.CreatedBy }
            };
            prjDoc.ProjectDocumentId = SqlHelper.GetRecord<string>("prj.ProjectDocument_New", parameters, confType);
            return prjDoc;
        }

        public static ProjectSite GetProjectSiteById(string projectId)
        {
            var ps = new ProjectSite();

            ps = SqlHelper.GetRecord<ProjectSite>("prj.Project_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);

            ps.Teams = SqlHelper.GetRecords<Team>("prj.ProjectTeam_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.Contractors = SqlHelper.GetRecords<Contractor>("prj.ProjectContractor_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.Quotations = SqlHelper.GetRecords<Quotation>("prj.Quotation_Get_By_ProjectId", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.PurchaseOrders = SqlHelper.GetRecords<PurchaseOrder>("prj.PurchaseOrder_Get_By_ProjectId", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.LetterOfClaims = SqlHelper.GetRecords<LetterOfClaim>("prj.LetterOfClaim_Get_By_ProjectId", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.TaxInvoices = SqlHelper.GetRecords<TaxInvoice>("prj.TaxInvoice_Get_By_ProjectId", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.Documents = SqlHelper.GetRecords<Document>("prj.ProjectDocument_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            ps.Engineers = SqlHelper.GetRecords<ProjectEngineer>("prj.ProjectEngineer_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = projectId } }, confType);
            return ps;
        }

        public static IList<ProjectSite> ProjectSiteList()
        {
            var ps = SqlHelper.GetRecords<ProjectSite>("prj.Project_Get_All", new List<ParameterInfo>(), confType);

            foreach (var p in ps)
            {
                p.Contractors = SqlHelper.GetRecords<Contractor>("prj.ProjectContractor_Get_By_Id", new List<ParameterInfo> { new ParameterInfo() { ParameterName="ProjectId", ParameterValue=p.ProjectId } }, confType);
            }
            return ps;
        }

        public static bool DeleteProjectSite(ProjectSite prj)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = prj.ProjectId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = prj.CreatedBy }
            };
            LoggerControl.LogAction(prj.AccessId, prj.ProjectId, "Delete", "Removed existing project site.", prj.CreatedBy);

            return SqlHelper.ExecuteQuery("prj.Project_Delete", parameters, confType) > 0 ? true : false;
        }

    }
}
