﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.ProjectSites.DAL
{
    public static class ProjectSiteDocumentControl
    {
        private const string confType = "kumfatt";

        public static string NewQuotation(Quotation quo)
        {
            var parameters = new List<ParameterInfo>
                {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = quo.ProjectSiteId },
                    new ParameterInfo() { ParameterName = "DateOfIssue", ParameterValue = quo.DateOfIssue },
                    new ParameterInfo() { ParameterName = "No", ParameterValue = quo.No },
                    new ParameterInfo() { ParameterName = "Amount", ParameterValue = quo.Amount },
                    new ParameterInfo() { ParameterName = "FileName", ParameterValue = quo.FileName },
                    new ParameterInfo() { ParameterName = "Path", ParameterValue = quo.Path },
                    new ParameterInfo() { ParameterName = "FileSize", ParameterValue = quo.FileSize },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = quo.CreatedBy }
                };

            return SqlHelper.GetRecord<string>("prj.Quotation_New", parameters, confType);
        }

        public static string NewPurchaseOrder(PurchaseOrder po)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = po.ProjectSiteId },
                new ParameterInfo() { ParameterName = "DateOfIssue", ParameterValue = po.DateOfIssue },
                new ParameterInfo() { ParameterName = "No", ParameterValue = po.No },
                new ParameterInfo() { ParameterName = "Amount", ParameterValue = po.Amount },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = po.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = po.Path },
                new ParameterInfo() { ParameterName = "FileSize", ParameterValue = po.FileSize },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = po.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("prj.PurchaseOrder_New", parameters, confType);
        }

        public static string NewLetterOfClaim(LetterOfClaim loc)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = loc.ProjectSiteId },
                new ParameterInfo() { ParameterName = "DateOfIssue", ParameterValue = loc.DateOfIssue },
                new ParameterInfo() { ParameterName = "No", ParameterValue = loc.No },
                new ParameterInfo() { ParameterName = "Amount", ParameterValue = loc.Amount },
                new ParameterInfo() { ParameterName = "ClaimType", ParameterValue = loc.ClaimType },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = loc.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = loc.Path },
                new ParameterInfo() { ParameterName = "FileSize", ParameterValue = loc.FileSize },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = loc.CreatedBy }
            };
            return SqlHelper.GetRecord<string>("prj.LetterOfClaim_New", parameters, confType);
        }
        public static string NewTaxInvoice(TaxInvoice ti)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "ProjectId", ParameterValue = ti.ProjectSiteId },
                new ParameterInfo() { ParameterName = "DateOfIssue", ParameterValue = ti.DateOfIssue },
                new ParameterInfo() { ParameterName = "No", ParameterValue = ti.No },
                new ParameterInfo() { ParameterName = "Amount", ParameterValue = ti.Amount },
                new ParameterInfo() { ParameterName = "ClaimType", ParameterValue = ti.ClaimType },
                new ParameterInfo() { ParameterName = "FileName", ParameterValue = ti.FileName },
                new ParameterInfo() { ParameterName = "Path", ParameterValue = ti.Path },
                new ParameterInfo() { ParameterName = "FileSize", ParameterValue = ti.FileSize },
                new ParameterInfo() { ParameterName = "Status", ParameterValue = ti.Status },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = ti.CreatedBy }
            };

            return SqlHelper.GetRecord<string>("prj.TaxInvoice_New", parameters, confType);
        }

        public static bool DeleteDocument(string documentId, string userId, string documentType)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = userId }
            };
            
            if (documentType == "quotation")
            {
                parameters.Add(new ParameterInfo() { ParameterName = "QuotationId", ParameterValue = documentId });
                return SqlHelper.ExecuteQuery("prj.Quotation_Delete", parameters, confType) > 0 ? true : false; ;
            }
            else if (documentType == "purchaseorder")
            {
                parameters.Add(new ParameterInfo() { ParameterName = "PurchaseOrderId", ParameterValue = documentId });
                return SqlHelper.ExecuteQuery("prj.PurchaseOrder_Delete", parameters, confType) > 0 ? true : false; ;
            }
            else if (documentType == "letterofclaim")
            {
                parameters.Add(new ParameterInfo() { ParameterName = "LetterOfClaimId", ParameterValue = documentId });
                return SqlHelper.ExecuteQuery("prj.LetterOfClaim_Delete", parameters, confType) > 0 ? true : false; ;
            }
            else if (documentType == "taxinvoice")
            {
                parameters.Add(new ParameterInfo() { ParameterName = "TaxInvoiceId", ParameterValue = documentId });
                return SqlHelper.ExecuteQuery("prj.TaxInvoice_Delete", parameters, confType) > 0 ? true : false; ;
            }
            else
            {
                parameters.Add(new ParameterInfo() { ParameterName = "ProjectDocumentId", ParameterValue = documentId });
                return SqlHelper.ExecuteQuery("prj.ProjectDocument_Delete", parameters, confType) > 0 ? true : false; ;
            }
        }
    }
}
