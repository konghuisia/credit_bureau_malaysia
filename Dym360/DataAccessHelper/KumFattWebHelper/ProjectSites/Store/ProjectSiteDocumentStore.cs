﻿using DataAccessHelper.KumFattWebHelper.ProjectSites.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.ProjectSites.Store
{
    public class ProjectSiteDocumentStore
    {
        public Task<string> NewQuotation(Quotation quo)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteDocumentControl.NewQuotation(quo);
            });
        }

        public Task<string> NewPurchaseOrder(PurchaseOrder po)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteDocumentControl.NewPurchaseOrder(po);
            });
        }

        public Task<string> NewLetterOfClaim(LetterOfClaim loc)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteDocumentControl.NewLetterOfClaim(loc);
            });
        }

        public Task<string> NewTaxInvoice(TaxInvoice ti)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteDocumentControl.NewTaxInvoice(ti);
            });
        }

        public Task<bool> DeleteDocument(string documentId, string userId, string documentType)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteDocumentControl.DeleteDocument(documentId, userId, documentType);
            });
        }
    }
}
