﻿using DataAccessHelper.KumFattWebHelper.ProjectSites.DAL;
using ObjectClasses.KumFattWeb;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.ProjectSites.Store
{
    public class ProjectSiteStore
    {
        public Task<string> InitNewProjectSit(string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.InitNewProjectSite(createdBy);
            });
        }
        public Task<bool> NewProjectSite(ProjectSite prj)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.NewProjectSite(prj);
            });
        }

        public Task<bool> UpdateProjectSite(ProjectSite prj)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.UpdateProjectSite(prj);
            });
        }

        public Task<Document> NewDocument(Document prjDoc, string projectId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.NewDocument(prjDoc, projectId);
            });
        }


        public Task<ProjectSite> GetProjectSiteById(string projectId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.GetProjectSiteById(projectId);
            });
        }
        public Task<List<ProjectSite>> ProjectList()
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.ProjectSiteList()?.ToList() ?? new List<ProjectSite>();
            });
        }

        public Task<bool> DeleteProjectSite(ProjectSite prj)
        {
            return Task.Factory.StartNew(() =>
            {
                return ProjectSiteControl.DeleteProjectSite(prj);
            });
        }
    }
}
