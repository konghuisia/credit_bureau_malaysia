﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Users.DAL
{
    public static class UserControl
    {
        private const string defaultPass = "kfc12345";
        public static bool NewUser(User user)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserName", ParameterValue = user.Username }
            };

            if (!string.IsNullOrEmpty(SqlHelper.GetRecord<string>("GetUserByUsername", parameters)))
            {
                //throw new DuplicateWaitObjectException("User ID already exist.");
                return false;
            }

            user.Password = string.IsNullOrEmpty(user.Password) ? defaultPass : user.Password;

            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo(){ ParameterName = "UserName", ParameterValue = user.Username },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo(){ ParameterName = "Source", ParameterValue = user.Source },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo(){ ParameterName = "AccountTypeId", ParameterValue = user.AccountTypeId },
            };

            user.UserId = SqlHelper.GetRecord<string>("acc.User_New", parameters);

            if (!string.IsNullOrEmpty(user.EmployeeId) && user.EmployeeId != "0" && string.IsNullOrEmpty(user.UserId))
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "EmployeeId", ParameterValue = user.EmployeeId },
                    new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.EmployeeAccount_New", parameters, "kumfatt");
            }

            var result = true;
            if (!string.IsNullOrEmpty(user.UserId))
            {
                foreach (var a in user.Accesses)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                    };

                    result = SqlHelper.ExecuteQuery("acc.UserAccess_New", parameters) > 0 ? true : false;
                }
            }
            else
            {
                result = false;
            }


            LoggerControl.LogAction(user.AccessId, user.UserId, "Create", "Added new account.", user.CreatedBy);


            return result;
        }

        public static bool UpdateUser(User user)
        {
            HashSalt hashSalt = new HashSalt();
            if (!string.IsNullOrEmpty(user.Password))
            {
                hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);
            }

            var parameters = new List<ParameterInfo>();

            foreach (var a in user.Accesses)
            {
                parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                        new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                        new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                        new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                        new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                        new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                    };

                SqlHelper.ExecuteQuery("acc.UserAccess_Update", parameters);
            }
            
            if (!string.IsNullOrEmpty(user.EmployeeId) && user.EmployeeId != "0")
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "EmployeeId", ParameterValue = user.EmployeeId },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.EmployeeAccount_Update", parameters, "kumfatt");
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo(){ ParameterName = "Name", ParameterValue = user.Name },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "Email", ParameterValue = user.Email },
                new ParameterInfo(){ ParameterName = "Remark", ParameterValue = user.Remark },
                new ParameterInfo(){ ParameterName = "Status", ParameterValue = user.Status },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy },
                new ParameterInfo(){ ParameterName = "AccountTypeId", ParameterValue = user.AccountTypeId },
            };
            
            LoggerControl.LogAction(user.AccessId, user.UserId, "Update", "Edited existing account.", user.CreatedBy);


            return SqlHelper.ExecuteQuery("acc.User_Update", parameters) > 0 ? true : false;
        }

        public static bool UpdatePassword(User user)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, user.Password);

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
            };
            
            LoggerControl.LogAction(user.AccessId, user.UserId, "Update", "Edited existing account password.", user.CreatedBy);


            return SqlHelper.ExecuteQuery("acc.User_Update_Password", parameters) > 0 ? true : false;
        }

        public static bool ChangePassword(User user, string pass)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId }
            };
            var u = SqlHelper.GetRecord<User>("acc.User_Get_By_UserId", parameters);

            try
            {
                if (!HashSalt.VerifyPassword(user.Password, u.Password, u.Salt))
                {
                    throw new Exception("Current password incorrect.");
                }
                else
                {
                    HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, pass);

                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                        new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                        new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                        new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                    };



                    LoggerControl.LogAction(user.AccessId, user.UserId, "Update", "Changed existing account password.", user.CreatedBy);


                    return SqlHelper.ExecuteQuery("acc.User_Update_Password", parameters) > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }
        public static bool ResetPassword(string userId, string accessId, string createdBy)
        {
            HashSalt hashSalt = HashSalt.GenerateSaltedHash(200, defaultPass);

            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo(){ ParameterName = "Password", ParameterValue = hashSalt.Hash },
                new ParameterInfo(){ ParameterName = "Salt", ParameterValue = hashSalt.Salt },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };
            
            LoggerControl.LogAction(accessId, userId, "Update", "Reset to default password.", createdBy);


            return SqlHelper.ExecuteQuery("acc.User_Reset_Password", parameters) > 0 ? true : false;
        }

        public static bool DeleteUser(string userId, string accessId, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "UserId", ParameterValue = userId },
                new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            LoggerControl.LogAction(accessId, userId, "Delete", "Removed existing account.", createdBy);


            return SqlHelper.ExecuteQuery("acc.User_Delete", parameters) > 0 ? true : false;
        }


        public static bool UpdateLoginDate(string userId)
        {
            return SqlHelper.ExecuteQuery("acc.User_Update_LastLoginDate", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } }) > 0 ? true : false;
        }

        public static void UpdateLoginAttempt(string userId)
        {
            SqlHelper.ExecuteQuery("acc.User_Update_FailedLogin", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } });
        }

        public static User GetUserByUserId(string userId)
        {
            var user = SqlHelper.GetRecord<User>("acc.User_Get_By_UserId", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } });
            user.Accesses = SqlHelper.GetRecords<UserAccess>("acc.UserAccess_Get_By_UserId", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } });
            user.EmployeeId = SqlHelper.GetRecord<string>("emp.EmployeeAccount_Get_By_UserId", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "UserId", ParameterValue = userId } }, "kumfatt");
            return user;
        }

        public static IList<User> UserList()
        {
            return SqlHelper.GetRecords<User>("acc.User_Get_All", new List<ParameterInfo>() { new ParameterInfo() { ParameterName = "Source", ParameterValue = "KumFatt" } });
        }

        public static bool UpdateUserAccess(User user)
        {
            var result = false;
            foreach (var a in user.Accesses)
            {
                var parameters = new List<ParameterInfo>
                {
                    new ParameterInfo(){ ParameterName = "UserId", ParameterValue = user.UserId },
                    new ParameterInfo(){ ParameterName = "AccessId", ParameterValue = a.AccessId },
                    new ParameterInfo(){ ParameterName = "IsCreate", ParameterValue = a.IsCreate },
                    new ParameterInfo(){ ParameterName = "IsRead", ParameterValue = a.IsRead },
                    new ParameterInfo(){ ParameterName = "IsUpdate", ParameterValue = a.IsUpdate },
                    new ParameterInfo(){ ParameterName = "IsDelete", ParameterValue = a.IsDelete },
                    new ParameterInfo(){ ParameterName = "CreatedBy", ParameterValue = user.CreatedBy }
                };

                result = SqlHelper.ExecuteQuery("acc.UserAccess_Update", parameters) > 0 ? true : false;

            }

            return result;
        }
    }
}
