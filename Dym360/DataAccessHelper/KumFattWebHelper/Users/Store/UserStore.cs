﻿using DataAccessHelper.KumFattWebHelper.Users.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Users.Store
{
    public class UserStore
    {
        public Task<bool> NewUser(User user)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.NewUser(user);
            });
        }

        public Task<bool> UpdateUser(User user)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdateUser(user);
            });
        }

        public Task<bool> UpdatePassword(User user)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdatePassword(user);
            });
        }

        public Task<bool> ChangePassword(User user, string pass)
        {
            try
            {
                return Task.Factory.StartNew(() =>
                {
                    return UserControl.ChangePassword(user, pass);
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public Task<bool> ResetPassword(string userId, string accessId, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.ResetPassword(userId, accessId, createdBy);
            });
        }

        public Task<bool> DeleteUser(string userId, string accessId, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.DeleteUser(userId, accessId, createdBy);
            });
        }

        public Task<User> GetUserByUserId(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.GetUserByUserId(userId);
            });
        }

        public Task UpdateLoginDate(string userId)
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UpdateLoginDate(userId);
            });
        }

        public void UpdateLoginAttempt(string userId)
        {
            Task.Factory.StartNew(() =>
            {
                UserControl.UpdateLoginAttempt(userId);
            });
        }

        public Task<List<User>> UserList()
        {
            return Task.Factory.StartNew(() =>
            {
                return UserControl.UserList()?.ToList() ?? new List<User>();
            });
        }
    }
}
