﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses.KumFattWeb;
using DataAccessHelper.KumFattWebHelper.Reports.DAL;

namespace DataAccessHelper.KumFattWebHelper.Reports.Store
{
    public class ReportStore
    {
        public Task<List<TeamWageAttendance>> GetTeamWageReports(string teamId, int month, int year)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReportControl.TeamWagesReport(teamId, month, year)?.ToList() ?? new List<TeamWageAttendance>();
            });
        }
        public Task<List<MonthlyIndividualRecord>> GetMonthlyIndividualReport(string employeeId, int month, int year, string monthlyWageTypeId)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReportControl.MonthlyIndividualReport(employeeId, month, year, monthlyWageTypeId)?.ToList() ?? new List<MonthlyIndividualRecord>();
            });
        }
        public Task<MonthlyWageRecord> GetMonthlyWageReport(string projectId, int month, int year)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReportControl.MonthlyWageReport(projectId, month, year);
            });
        }
        public Task<MonthlyWageRecord> GetMonthlySocsoReport(string agencyId, int month, int year)
        {
            return Task.Factory.StartNew(() =>
            {
                return ReportControl.MonthlySocsoReport(agencyId, month, year);
            });
        }
    }
}
