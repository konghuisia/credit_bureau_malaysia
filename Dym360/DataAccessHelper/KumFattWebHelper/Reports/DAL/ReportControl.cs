﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ObjectClasses.KumFattWeb;
using DatabaseConnection.Data;

namespace DataAccessHelper.KumFattWebHelper.Reports.DAL
{
    public class ReportControl
    {

        private const string confType = "kumfatt";
        public static IList<TeamWageAttendance> TeamWagesReport(string teamId, int month, int year)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "TeamId", ParameterValue = teamId },
                new ParameterInfo(){ ParameterName = "Month", ParameterValue = month },
                new ParameterInfo(){ ParameterName = "Year", ParameterValue = year }
            };
            
            return SqlHelper.GetRecords<TeamWageAttendance>("TeamWageAttendance_Get", parameters, confType);
        }

        public static IList<MonthlyIndividualRecord> MonthlyIndividualReport(string employeeId, int month, int year, string monthlyWageTypeId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "EmployeeId", ParameterValue = employeeId },
                new ParameterInfo(){ ParameterName = "Month", ParameterValue = month },
                new ParameterInfo(){ ParameterName = "Year", ParameterValue = year },
                new ParameterInfo(){ ParameterName = "MonthlyWageTypeId", ParameterValue = monthlyWageTypeId }
            };

            return SqlHelper.GetRecords<MonthlyIndividualRecord>("MonthlyIndividualReport_Get", parameters, confType);
        }

        public static MonthlyWageRecord MonthlyWageReport(string projectId, int month, int year)
        {
            var report = new MonthlyWageRecord();
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "ProjectId", ParameterValue = projectId },
                new ParameterInfo(){ ParameterName = "Month", ParameterValue = month },
                new ParameterInfo(){ ParameterName = "Year", ParameterValue = year }
            };

            report.MonthlyWages = SqlHelper.GetRecords<MonthlyWage>("MonthlyWageReport_Get", parameters, confType);
            report.MonthlyIncentives = SqlHelper.GetRecords<MonthlyIncentive>("MonthlyWageIncentive_Get", parameters, confType);

            return report;
        }

        public static MonthlyWageRecord MonthlySocsoReport(string agencyId, int month, int year)
        {
            var report = new MonthlyWageRecord();
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo(){ ParameterName = "AgencyId", ParameterValue = agencyId },
                new ParameterInfo(){ ParameterName = "Month", ParameterValue = month },
                new ParameterInfo(){ ParameterName = "Year", ParameterValue = year }
            };

            report.MonthlyWages = SqlHelper.GetRecords<MonthlyWage>("MonthlySocsoReport_Get", parameters, confType);

            return report;
        }
    }
}
