﻿using DatabaseConnection.Data;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Teams.DAL
{
    public class TeamControl
    {
        private const string confType = "kumfatt";
        public static string PreNewTeam(string createdBy)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = createdBy }
            };

            return SqlHelper.GetRecord<string>("emp.Team_New_Pre", parameters, confType); ;
        }
        public static bool NewTeam(Team tm)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TeamId", ParameterValue = tm.TeamId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = tm.Name },
                new ParameterInfo() { ParameterName = "Keyword", ParameterValue = tm.Keyword },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = tm.Description },
                new ParameterInfo() { ParameterName = "TeamLeadId", ParameterValue = tm.TeamLeadId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
            };

            var teamId = SqlHelper.GetRecord<string>("emp.Team_New", parameters, confType);

            foreach (var m in tm.Members)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TeamId", ParameterValue = tm.TeamId },
                    new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = m.EmployeeId },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.TeamMember_New", parameters, confType);
            }

            foreach (var v in tm.Vehicles)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TeamId", ParameterValue = teamId },
                    new ParameterInfo() { ParameterName = "VehicleTypeId", ParameterValue = v.VehicleTypeId },
                    new ParameterInfo() { ParameterName = "RegistrationNo", ParameterValue = v.RegistrationNo },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.TeamVehicle_New", parameters, confType);
            }

            LoggerControl.LogAction(tm.AccessId, teamId, "Create", "Added new team.", tm.CreatedBy);

            return string.IsNullOrEmpty(teamId) ? false : true;
        }

        public static bool UpdateTeam(Team tm)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();

            foreach (var m in tm.Members)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TeamId", ParameterValue = tm.TeamId },
                    new ParameterInfo() { ParameterName = "EmployeeId", ParameterValue = m.EmployeeId },
                    new ParameterInfo() { ParameterName = "MemberId", ParameterValue = m.MemberId },
                    new ParameterInfo() { ParameterName = "DeletedFlag", ParameterValue = m.DeletedFlag },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.TeamMember_Update", parameters, confType);
            }

            foreach (var v in tm.Vehicles)
            {
                parameters = new List<ParameterInfo>
                {
                    new ParameterInfo() { ParameterName = "TeamId", ParameterValue = tm.TeamId },
                    new ParameterInfo() { ParameterName = "VehicleId", ParameterValue = v.VehicleId },
                    new ParameterInfo() { ParameterName = "VehicleTypeId", ParameterValue = v.VehicleTypeId },
                    new ParameterInfo() { ParameterName = "RegistrationNo", ParameterValue = v.RegistrationNo },
                    new ParameterInfo() { ParameterName = "DeletedFlag", ParameterValue = v.DeletedFlag },
                    new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
                };
                SqlHelper.ExecuteQuery("emp.TeamVehicle_Update", parameters, confType);
            }

            parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TeamId", ParameterValue = tm.TeamId },
                new ParameterInfo() { ParameterName = "Name", ParameterValue = tm.Name },
                new ParameterInfo() { ParameterName = "Keyword", ParameterValue = tm.Keyword },
                new ParameterInfo() { ParameterName = "Description", ParameterValue = tm.Description },
                new ParameterInfo() { ParameterName = "TeamLeadId", ParameterValue = tm.TeamLeadId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = tm.CreatedBy }
            };

            LoggerControl.LogAction(tm.AccessId, tm.TeamId, "Update", "Edited existing team.", tm.CreatedBy);


            var result = SqlHelper.ExecuteQuery("emp.Team_Update", parameters, confType);
            return result > 0 ? true : false;
        }
        public static bool DeleteTeam(Team team)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TeamId", ParameterValue = team.TeamId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = team.CreatedBy },
            };

            LoggerControl.LogAction(team.AccessId, team.TeamId, "Delete", "Removed existing team.", team.CreatedBy);

            return SqlHelper.ExecuteQuery("emp.Team_Delete", parameters, confType) > 0 ? true : false;
        }
        public static bool DeleteMember(string memberId, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "MemberId", ParameterValue = memberId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = createdBy },
            };
            return SqlHelper.ExecuteQuery("emp.TeamMember_Delete", parameters, confType) > 0 ? true : false;
        }
        public static string NewTeamVehicle(Vehicle vehicle, string teamId)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "TeamId", ParameterValue = teamId },
                new ParameterInfo() { ParameterName = "VehicleTypeId", ParameterValue = vehicle.VehicleTypeId },
                new ParameterInfo() { ParameterName = "RegistrationNo", ParameterValue = vehicle.RegistrationNo },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = vehicle.CreatedBy },
            };
            return SqlHelper.GetRecord<string>("emp.TeamVehicle_New", parameters, confType);
        }
        public static Vehicle NewVehicleDocument(Vehicle vehicle, string teamId)
        {
            var parameters = new List<ParameterInfo>();

            if (!string.IsNullOrEmpty(vehicle.VehicleId))
            {
                foreach (var vd in vehicle.VehicleDocuments)
                {
                    parameters = new List<ParameterInfo>
                    {
                        new ParameterInfo() { ParameterName = "VehicleId", ParameterValue = vehicle.VehicleId },
                        new ParameterInfo() { ParameterName = "VehicleDocumentTypeId", ParameterValue = vd.VehicleDocumentTypeId },
                        new ParameterInfo() { ParameterName = "FileName", ParameterValue = vd.FileName },
                        new ParameterInfo() { ParameterName = "Path", ParameterValue = vd.Path },
                        new ParameterInfo() { ParameterName = "ExpiryDate", ParameterValue = vd.ExpiryDate },
                        new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = vehicle.CreatedBy },
                    };
                    vd.VehicleDocumentId = SqlHelper.GetRecord<string>("emp.VehicleDocument_New", parameters, confType);
                }
            }

            return vehicle;
        }

        public static bool DeleteVehicle(string vehicleId, string createdBy)
        {
            var parameters = new List<ParameterInfo>
            {
                new ParameterInfo() { ParameterName = "VehicleId", ParameterValue = vehicleId },
                new ParameterInfo() { ParameterName = "CreatedBy", ParameterValue = createdBy },
            };
            return SqlHelper.ExecuteQuery("emp.TeamVehicle_Delete", parameters, confType) > 0 ? true : false;
        }
        public static Team GetTeamById(string teamId)
        {
            var parameters = new List<ParameterInfo> { new ParameterInfo() { ParameterName = "TeamId", ParameterValue = teamId } };
            var team = new Team();

            team = SqlHelper.GetRecord<Team>("emp.Team_Get_By_Id", parameters, confType);
            team.Members = SqlHelper.GetRecords<TeamMember>("emp.TeamMember_Get", parameters, confType);
            team.Vehicles = SqlHelper.GetRecords<Vehicle>("emp.TeamVehicle_Get", parameters, confType);
            foreach (var v in team.Vehicles)
            {
                v.VehicleDocuments = SqlHelper.GetRecords<VehicleDocument>("emp.VehicleDocument_Get", new List<ParameterInfo> {
                    new ParameterInfo(){ ParameterName = "VehicleId", ParameterValue = v.VehicleId}}, confType);
            }
            return team;
        }
        public static IList<Team> TeamList()
        {
            return SqlHelper.GetRecords<Team>("emp.Team_Get_All", new List<ParameterInfo>(), confType);
        }
    }
}
