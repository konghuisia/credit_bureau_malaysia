﻿using DataAccessHelper.KumFattWebHelper.Teams.DAL;
using ObjectClasses.KumFattWeb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessHelper.KumFattWebHelper.Teams.Store
{
    public class TeamStore
    {
        public Task<string> PreNewTeam(string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.PreNewTeam(createdBy);
            });
        }
        public Task<bool> NewTeam(Team tm)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.NewTeam(tm);
            });
        }
        public Task<bool> UpdateTeam(Team tm)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.UpdateTeam(tm);
            });
        }
        public Task<Team> GetTeamById(string teamId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.GetTeamById(teamId);
            });
        }
        public Task<List<Team>> TeamList()
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.TeamList()?.ToList() ?? new List<Team>();
            });
        }
        public Task<bool> DeleteTeam(Team team)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.DeleteTeam(team);
            });
        }
        public Task<string> NewTeamVehicle(Vehicle vehicle, string teamId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.NewTeamVehicle(vehicle, teamId);
            });
        }
        public Task<Vehicle> NewVehicleDocument(Vehicle vehicle, string teamId)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.NewVehicleDocument(vehicle, teamId);
            });
        }
        public Task<bool> DeleteVehicle(string vehicleId, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.DeleteVehicle(vehicleId, createdBy);
            });
        }

        public Task<bool> DeleteMember(string memberId, string createdBy)
        {
            return Task.Factory.StartNew(() =>
            {
                return TeamControl.DeleteMember(memberId, createdBy);
            });
        }
    }
}
